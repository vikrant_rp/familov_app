import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { AlertController, ToastController } from 'ionic-angular';
import { RestProvider } from '../providers/rest/rest';



//import { MyOrdersPage } from '../pages/my-orders/my-orders';
//import { MyProfilePage } from '../pages/my-profile/my-profile';

//import { PaymentPage } from '../pages/payment/payment';
//import { ThankYouPage } from '../pages/thankyou/thankyou';
//import { CheckoutPage } from '../pages/checkout/checkout';
//import { ConfirmOrderPage } from '../pages/confirm-order/confirm-order';
//import { OrderDetailsPage } from '../pages/order-details/order-details';

import { AboutUsPage } from '../pages/about-us/about-us';
import { ChangePasswordPage } from '../pages/change-password/change-password';
import { FaqPage } from '../pages/faq/faq';
import { ForgotPasswordPage } from '../pages/forgot-password/forgot-password';
import { HomePage } from '../pages/home/home';
import { InviteFriendPage } from '../pages/invite-friend/invite-friend';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { TermsPage } from '../pages/terms/terms';
import { ProductsPage } from '../pages/products/products';
import { ProductDetailsPage } from '../pages/product-details/product-details';
import { MyCartPage } from '../pages/my-cart/my-cart';
//import * as Constants from '../util/constants';

@Component({
  templateUrl: 'app.html'
})
export class FamiLov {
@ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;
  // rootPage: any = PaymentPage;

  pages: Array<{title: string, component: any, icon: any}>;
  apiToken: string;
  LoginUser: any;
  IsUserLogin: any;
  alert: any;
  cartItems: any;
  cartItemsLen: any = 0;
  currencyData: any;

  constructor(
      public platform: Platform
      , public statusBar: StatusBar
      , public splashScreen: SplashScreen
      ,public restProvider: RestProvider
      , private toastCtrl:   ToastController
      , private alertCtrl: AlertController) {
    this.initializeApp();

    /*this.restProvider.LoadingSpinner();
     setTimeout( () => {
     alert('1234');
     this.restProvider.hideLoadingSpinner();
     },300);*/

    /*this.platform.registerBackButtonAction(
     () => {
     if(this.nav.canGoBack()) {
     this.nav.pop();
     }
     else {
     if(this.alert){
     this.alert.dismiss();
     this.alert =null;
     }
     else{
     this.showAlert();
     }
     }
     });*/

    // used for an example of ngFor and navigation
    /*this.apiToken = localStorage.getItem('apiToken');
     if (this.apiToken == undefined || this.apiToken == null) {
     this.getToken();
     }*/
    this.getCurrency();

    this.cartItems = localStorage.getItem('cartItems');
    if (this.cartItems != undefined || this.cartItems != null) {
      this.cartItemsLen = JSON.parse(this.cartItems).length;
    }

    this.LoginUser = localStorage.getItem('LoginUser');
    if (this.LoginUser == undefined || this.LoginUser == null) {

      this.IsUserLogin = false;
      this.pages = [
        { title: 'Home',    component: HomePage,    icon: 'home' },
        { title: 'Shopping Cart',   component: MyCartPage,    icon: 'cart' },
        { title: 'Need help?', component: FaqPage, icon: 'help-circle' },
        { title: 'Terms of services',   component: TermsPage,   icon: 'warning' },
        { title: 'About',   component: AboutUsPage,   icon: 'information-circle' }
      ];
    }
    else {
      this.IsUserLogin = true;
      this.LoginUser = JSON.parse(this.LoginUser);
      this.pages = [
        { title: 'Home',    component: HomePage,    icon: 'home' },
        { title: 'Shopping Cart',   component: MyCartPage,    icon: 'cart' },
          /* { title: 'My Profile',    component: MyProfilePage, icon: 'person' },
         { title: 'Orders History',  component: MyOrdersPage,  icon: 'time' },*/
        { title: 'Earn money',    component: InviteFriendPage,   icon: 'trophy' },
        { title: 'Need help?', component: FaqPage, icon: 'help-circle' },
        { title: 'Terms of services',   component: TermsPage,   icon: 'warning' },
        { title: 'About',   component: AboutUsPage,   icon: 'information-circle' }
      ];
    }

    setTimeout( () => {
      this.currencyData = localStorage.getItem('currencyData');
    },1000);

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    //TODO-VIX for menu
    console.log("333");
    this.nav.setRoot(page.component);
    /*if (page.component == MyProfilePage) {
      this.nav.push(page.component);
    }
    else {
      this.nav.setRoot(page.component);
    }*/
  }

  getToken() {
    this.restProvider.getToken().then(
            data => {
          // console.log(data);
          // alert();
          var token = '123456789';//data.key;
          // console.log(token);
          localStorage.setItem('apiToken',token);
        }
    );
  }

  getCurrency() {
    this.restProvider.getCurrency().then(
            data => {
          //console.table(data);
          //console.log("============");
          //console.log(data['response_data']['currency_data'].iCurrencyId);
          //console.log("******============");
          //console.log(data['response_data'].currency_data);
          //console.log("******============");
          //console.log(data.response_data.currency_data);
          localStorage.setItem('currencyData',JSON.stringify(data['response_data']['currency_data']));
          //}
        }
    );
  }

  openLogin() {
    this.nav.push(LoginPage);
  }

  openSignup() {
    this.nav.push(SignupPage);
  }

  logoutUser() {
    localStorage.removeItem('LoginUser');
    // localStorage.removeItem('cartItems');
    location.reload();
  }

  showAlert() {
    this.alert = this.alertCtrl.create({
      title: 'Exit?',
      message: 'Do you want to exit the app?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            this.alert =null;
          }
        },
        {
          text: 'Exit',
          handler: () => {
            this.platform.exitApp();
          }
        }
      ]
    });
    //alert.present();
    //toast.present();
  }

  showToast() {
    let toast = this.toastCtrl.create({
      message: 'Press Again to exit',
      duration: 2000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }
}

import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { FamiLov } from './app.component';
// import { HomePage } from '../pages/home/home';
// import { ListPage } from '../pages/list/list';

/*
import { HomePage } from '../pages/home/home';

import { MyOrdersPage } from '../pages/my-orders/my-orders';
import { MyProfilePage } from '../pages/my-profile/my-profile';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { ChangePasswordPage } from '../pages/change-password/change-password';

import { PaymentPage } from '../pages/payment/payment';
import { ThankYouPage } from '../pages/thankyou/thankyou';

import { CheckoutPage } from '../pages/checkout/checkout';
import { ConfirmOrderPage } from '../pages/confirm-order/confirm-order';
import { OrderDetailsPage } from '../pages/order-details/order-details';

import { FaqPage } from '../pages/faq/faq';
import { InviteFriendPage } from '../pages/invite-friend/invite-friend';
import { TermsPage } from '../pages/terms/terms';
import { AboutUsPage } from '../pages/about-us/about-us';
*/
import { AboutUsPage } from '../pages/about-us/about-us';
import { ChangePasswordPage } from '../pages/change-password/change-password';
import { FaqPage } from '../pages/faq/faq';
import { ForgotPasswordPage } from '../pages/forgot-password/forgot-password';
import { HomePage } from '../pages/home/home';
import { InviteFriendPage } from '../pages/invite-friend/invite-friend';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { TermsPage } from '../pages/terms/terms';
import { ProductsPage } from '../pages/products/products';
import { ProductDetailsPage } from '../pages/product-details/product-details';
import { MyCartPage } from '../pages/my-cart/my-cart';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HttpClientModule } from '@angular/common/http';
import { RestProvider } from '../providers/rest/rest';

import { PayPal } from '@ionic-native/paypal';
import { Stripe } from '@ionic-native/stripe';

@NgModule({
  declarations: [
    FamiLov,
    HomePage,
    ProductsPage,
    MyCartPage,
    /*CheckoutPage,
    ConfirmOrderPage,
    MyOrdersPage,
    MyProfilePage,*/
    LoginPage,
    SignupPage,
    ForgotPasswordPage,
    ChangePasswordPage,
    /*OrderDetailsPage,
    PaymentPage,
    ThankYouPage,*/
    ProductDetailsPage,
    FaqPage,
    InviteFriendPage,
    TermsPage,
    AboutUsPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(FamiLov),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    FamiLov,
    HomePage,
    ProductsPage,
    MyCartPage,
    /*CheckoutPage,
    ConfirmOrderPage,
    MyOrdersPage,
    MyProfilePage,*/
    LoginPage,
    SignupPage,
    ForgotPasswordPage,
    ChangePasswordPage,
    /*OrderDetailsPage,
    PaymentPage,
    ThankYouPage,*/
    ProductDetailsPage,
    FaqPage,
    InviteFriendPage,
    TermsPage,
    AboutUsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    RestProvider,
    PayPal,
    Stripe
  ]
})
export class AppModule {}

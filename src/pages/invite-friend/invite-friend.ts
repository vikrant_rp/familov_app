import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FaqPage } from '../faq/faq';
@Component({
  selector: 'page-invite-friend',
  templateUrl: 'invite-friend.html'
})
export class InviteFriendPage {
  selectedItem: any;
  icons: string[];
  items: Array<{title: string, note: string, icon: string}>;

  frequentQuestions = [
    {
        title: "What is Familov.com?",
        description: "Everything is in the name. With familov.com, order from abroad and we deliver your family to the country in less than 36 hours." },
    {
        title: "How can I create an account?",
        description: "Sur la page de démarrage de Familov, cliquez sur Inscrivez-vous (ou allez directement à la page Créer un compte). Remplissez les informations et votre mot de passe et confirmez-les une deuxième fois. Votre adresse de courriel sera directement liée au compte." },
    {
        title: "I forgot my password?",
        description: "Vous avez déjà créé un compte mais vous ne vous souvenez plus de votre mot de passe? Cliquez sur «Login» en haut de la page. Puis cliquez sur 'Mot de passe oublié?'. Remplissez votre adresse e-mail et un nouveau mot de passe vous sera envoyé par e-mail." },
    {
        title: "How long does a delivery take?",
        description: "En moyenne, nous avons besoin de moins de 36 heures à partir du moment où nous avons recu le payement jusqu'au moment de la livraison à votre proche. Le délai de livraison peut varier d'une boutique à l'autre et dépend du nombre de commandes que la boutique doit préparer et la disponibilité du bénéficiaire." },
    {
        title: "How much is shipping?",
        description: "Nous facturons un forfait de 2,95 € (2,50€ pendant le phase béta) pour le service et 0,00€ pour les livraison en magasin.En fonction de lieu de residence le coût des livraison à domicile varie." },
    {
        title: "What happens after placing my order?",
        description: "L'ordre arrive dans notre système et après que nous recevons le paiement, il est envoyé directement à la boutique (Shopper) que vous avez commandé. Après seulement quelques secondes, les magasins reçoivent l'ordre par le biais de notre propre système Shop-Connect.<br> <br>1- Le magasin recoit tous les détails de votre commande <br> 2- Votre proche recoit le code de retrait par SMS<br> 3-Le livreur prépare le paquet et recontacte le bénéficiaire dans les 36 heures pour la livraison <br>4-Vous recevez une confirmation par SMS ou e-mail que votre commande est bien livrée. <br>NB: Nous travaillons avec partenaires n et des livreurs proffessionnels pour gerer vos commanades. Ne vous inquiétez pas, ils ont été formés individuellement pes sont formés pour donner la meilleure qualité , soigner vos articles et les livrer." },
    {
        title: "Where can I see my receipts?",
        description: "Vous pouvez afficher vos reçus de deux façons: Email: Lorsque votre commande est passée, vous recevrez un courriel avec un reçu. <br> <br> Sur le site Web cliquez sur votre nom dans le coin supérieur droit et un menu apparaîtra. Sélectionnez Mes commandes. Cliquez sur un numéro de commande pour afficher un reçu détaillé de cet ordre." },
    {
        title: "How is my order confirmed?",
        description: "Une fois votre commande passée, vous recevrez un e-mail de confirmation envoyé à l'adresse e-mail que vous avez saisie lors de votre commande. Conservez ce courriel et votre numéro de commande pour toute question ou remarque concernant votre commande. Lorsque vous recevez l'e-mail de confirmation, Familov fait le reste. Il suffit de s'asseoir, de se détendre et de profiter!" },
    {
        title: "I have not received a confirmation email",
        description: "Si un numéro de commande s'affiche à l'écran, cela signifie que votre commande a été envoyée. L'e-mail de confirmation peut être dans votre «spam» plutôt que dans votre boîte de réception. Si vous ne recevez pas de numéro de commande, veuillez contacter notre service à la clientèle pour connaître l'état de votre commande. Vous devez fournir l'adresse e-mail que vous avez saisie lors de la commande et le nom de la boutique sélectionnée afin que nous puissions trouver votre commande." }
  ];

  paymentQuestions = [
    {
        title: "What types of payment can I use?",
        description: "Carte de crédit (VISA / American Express / Master Card /)<br>Pay Pal <br> Virement" },
    {
        title: "How secure are online payments?",
        description: "Lors du traitement des paiements en ligne sur Familov.com, nous utilisons des pages SSL sécurisées. Cela signifie que ces pages assurent la protection de vos données personnelles et de paiement. De cette façon, vous pouvez être sûr que cette information est seulement visible pour vous et pour nous, pour le traitement de votre commande." },
    {
        title: "Why do I have to pay transaction fees?",
        description: "Familov.com est seulement un intermédiaire, entre vous en tant que consommateur et la Boutique. Vu le faible revenu que nous gagnons sur chaque commande, nous devons facturer pour l'utilisation d'une méthode de paiement en ligne. Les coûts couvrent l'entretien technique des paiements en ligne, les coûts de transaction que les banques facturent et nos frais administratifs pour le traitement des paiements. Tous les sites facturent ces coûts, mais ils les ajoutent souvent directement aux frais d'envoi et / ou d'administration.. Nous vous remercions pour votre compréhension." },
    {
        title: "My online payment failed",
        description: "Si vous pensez que le paiement en ligne a échoué, vérifiez toujours si vous avez reçu un e-mail de confirmation. Cet e-mail vous permettra de voir si votre commande a été soumise et si la commande est déjà payée ou non. <br><br> L'e-mail de confirmation est envoyé directement une fois le paiement terminé. Cela peut prendre un peu plus longtemps si le paiement est traité par la banque, la société de carte de crédit ou toute autre société de paiement. Cela peut prendre jusqu'à 15 minutes. Si après 15 minutes vous n'avez toujours pas de courriel de confirmation dans votre boîte de réception, contactez notre service à la clientèle. Ils vous diront si l'ordre et le paiement ont réussi." },
    {
        title: "I still have a lot of questions to ask you?",
        description: "Contactez nous! Nous serons là à :<br><br> Email:hello@familov.com <br><br> Whatsapp : (+49 1525 9948834)<br><br> Chat : www.familov.com" }
];

  shownGroup = null;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    // If we navigated to this page, we will have an item available as a nav param
    this.selectedItem = navParams.get('item');

    // Let's populate this page with some filler content for funzies
    this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
    'american-football', 'boat', 'bluetooth', 'build'];

    this.items = [];
    for (let i = 1; i < 11; i++) {
      this.items.push({
        title: 'Item ' + i,
        note: 'This is item #' + i,
        icon: this.icons[Math.floor(Math.random() * this.icons.length)]
      });
    }
  }

  toggleGroup(group) {
      if (this.isGroupShown(group)) {
          this.shownGroup = null;
      } else {
          this.shownGroup = group;
      }
  };
  isGroupShown(group) {
      return this.shownGroup === group;
  };

  itemTapped(event, item) {
    // That's right, we're pushing to ourselves!
    this.navCtrl.push(FaqPage, {
      item: item
    });
  }
}

import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';

import { RestProvider } from '../../providers/rest/rest';

import { HomePage } from '../home/home';
// import { PaymentPage } from '../payment/payment';

//import { CheckoutPage } from '../checkout/checkout';
//import { ProductsPage } from '../products/products';
//import { MyCartPage } from '../my-cart/my-cart';

@Component({
	selector: 'page-my-cart',
	templateUrl: 'my-cart.html'
})
export class MyCartPage {

	// public qty1: number;
	// public qty2: number;
	// public qty3: number;
	cartItems: any;
	cartTotal: any;
	serviceFee: any;
	promoDiscount: any;
	promoID: any;
	promocodeData: any;
	totalPrice: any;
	cartItemsLen: any = 0;
	currencyData: any;
	currencySymbol: any;
	currencyName: any;
	selected_shop_id: any = 0;
	config: any ;
	promo_code_data: any ;
	tempTot: any = 0;
	constructor(public navCtrl: NavController, public alertCtrl: AlertController, public restProvider: RestProvider) {
		// this.qty1 = 1;
		// this.qty2 = 1;
		// this.qty3 = 1;
		this.cartItems = false;
		this.cartTotal = 0;
		this.serviceFee = 0;
		this.promoDiscount = 0;
		this.promoID = 0;

		var selected_shop = localStorage.getItem('selectedShopLoc');
		if (selected_shop != null && selected_shop != undefined) {
			this.selected_shop_id = JSON.parse(selected_shop).shop_id;
		}

		this.currencySymbol = '&euro;';
		this.currencyName = 'EUR';

		this.cartItems = localStorage.getItem('cartItems');
		if (this.cartItems == undefined || this.cartItems == null) {
			this.cartItems = [];
		}
		else {
			this.cartItems = JSON.parse(localStorage.getItem('cartItems'));
			this.cartItemsLen = this.cartItems.length;
			for (var i = 0; i < this.cartItems.length; ++i) {
				var total = (this.cartItems[i].product_prices * this.cartItems[i].qty).toFixed(2);
				this.cartTotal = (parseFloat(this.cartTotal) + parseFloat(total)).toFixed(2);
			}
		}

		this.currencyData = localStorage.getItem('currencyData');
		if (this.currencyData == undefined || this.currencyData == null) {
			this.currencyData = [];
			this.currencySymbol = '&euro;';
			this.currencyName = 'EUR';
		}
		else {
			this.currencyData = JSON.parse(this.currencyData);
			this.currencySymbol = this.currencyData.vSymbol;
			this.currencyName = this.currencyData.vName;

		}
		this.getConfigData();
		this.config = JSON.parse(localStorage.getItem('config'));
		console.log(this.config );
		if (this.config == undefined || this.config == null) {
			this.getConfigData();
			//this.serviceFee = this.config.service_fees;
		}
		//this.restProvider.showToast('Item added in warning.','toast-warning');
		//this.getPromoCode();
		this.promo_code_data = JSON.parse(localStorage.getItem('promo_code_data'));
		if (this.promo_code_data = undefined || this.promo_code_data == null) {
			this.promo_code_data = [];
			this.promoDiscount = 0;
		}else{
			this.promo_code_data = JSON.parse(localStorage.getItem('promo_code_data'));
			this.promoDiscount = this.promo_code_data.promo_code_amount;
		}
		/*this.cartItems = localStorage.getItem('cartItems');
		 if (this.cartItems != undefined || this.cartItems != null) {
		 this.cartItemsLen = JSON.parse(this.cartItems).length;
		 }*/
		this.totalPrice = 0;
		setTimeout( () => {
			this.serviceFee = this.config.service_fees;
			this.totalPrice = ((parseFloat(this.cartTotal) + parseFloat(this.serviceFee)) - parseFloat(this.promoDiscount)).toFixed(2);
		},1000);

		//this.totalPrice = (parseFloat(this.cartTotal) + parseFloat(this.serviceFee) + parseFloat(this.promoDiscount)).toFixed(2);
	}
	/*getPromoCode() {
	 //this.restProvider.getPromoCode(this.promo_code,parseFloat(this.cartTotal))
	 this.restProvider.getPromoCode()
	 .then(
	 (result) => {
	 console.log(result);
	 if (result.status == true) {
	 this.config = result.data;
	 this.serviceFee = result.data.service_fees;
	 localStorage.setItem('config',JSON.stringify(this.config));

	 }
	 else {
	 if (typeof result.message == "string") {
	 this.restProvider.showErrorMsg(this.statusError,result.message);
	 }
	 else {
	 this.restProvider.showErrorMsg(this.statusError,'Oops..!! <br>No internet connection!<br>Please check you internet connectivity.');
	 }
	 }
	 },
	 (err) => {
	 console.log(err);
	 this.restProvider.showErrorMsg(this.statusError,'Opps..!! <br> Something went wrong.');
	 }
	 );
	 }*/
	getConfigData() {
		this.restProvider.getConfigData()
			.then(
			(result) => {
				console.log(result);
				if (result['status'] == true) {
					this.config = result['response_data'];
					this.serviceFee = result['response_data']['service_fees'];
					localStorage.setItem('config',JSON.stringify(this.config));
					this.totalPrice = ((parseFloat(this.cartTotal) + parseFloat(this.serviceFee)) - parseFloat(this.promoDiscount)).toFixed(2);
				}
				else {
					this.restProvider.hideLoadingSpinner();
					this.restProvider.showErrorMsg(this.restProvider.statusString,this.restProvider.statusErrorMessageAPI);
					this.goToHome({});
				}
			},
			(err) => {
				this.restProvider.hideLoadingSpinner();
				this.goToHome({});
				this.restProvider.showErrorMsg(this.restProvider.statusString,this.restProvider.statusErrorMessageAPI);
			}
		);
	}

	goToHome(params) {
		if (!params) params = {};
		this.navCtrl.setRoot(HomePage);
	}


	addProductToCart(product,type) {
		var exist = this.cartItems.filter(function (cartItem) { return cartItem.product_id == product.product_id });
		var cart_total:any = 0;
		if(exist.length > 0) {
			for (var i = 0; i < this.cartItems.length; ++i) {
				if(this.cartItems[i].product_id == product.product_id) {
					if(type == 'add') {
						this.cartItems[i].qty++;
					}
					else {
						if(this.cartItems[i].qty > 1){
							this.cartItems[i].qty--;
						}
					}
				}
				var total = (this.cartItems[i].product_prices * this.cartItems[i].qty).toFixed(2);
				cart_total = (parseFloat(cart_total) + parseFloat(total)).toFixed(2);
			}
			localStorage.setItem('cartItems',JSON.stringify(this.cartItems));
		}
		else {
			product.qty = 1;
			cart_total = product.product_prices;
			this.cartItems.push(product);
			localStorage.setItem('cartItems',JSON.stringify(this.cartItems));
		}
		this.cartTotal = cart_total;
		this.totalPrice = (parseFloat(this.cartTotal) + parseFloat(this.serviceFee) + parseFloat(this.promoDiscount)).toFixed(2);
	}

}

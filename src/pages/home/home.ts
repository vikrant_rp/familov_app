import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';

import { RestProvider } from '../../providers/rest/rest';

/*import { ProductDetailsPage } from '../product-details/product-details';

 import { MyCartPage } from '../my-cart/my-cart';
 import { FaqPage } from '../faq/faq';*/
import { FaqPage } from '../faq/faq';
import { ProductsPage } from '../products/products';

@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})

export class HomePage {

	CountryList: any = {};
	selectedShopLoc: any = {};
	cartItems: any = {};
	CityList: any;
	ShopList: any;
	cartItemsLen: any = 0;
	constructor(
		public navCtrl: NavController
		, public restProvider: RestProvider
		, public alertCtrl: AlertController) {
		//this.restProvider.LoadingSpinner();

		//	this.statusErrorMessage = 'Error..!! <br>Something went wrong!';
	}

	ionViewCanEnter() {
		this.restProvider.LoadingSpinner();
		this.getHomeData();

		this.CountryList = localStorage.getItem('CountryList');
		/*if (this.CountryList == null || this.CountryList == undefined) {
		 }*/

		if (this.CountryList == null || this.CountryList == undefined) {
			this.CountryList = [{'country_id':'-1','country_name':'Select country'}];
		}
		if (this.CityList == null || this.CityList == undefined) {
			this.CityList = [{'city_id':'-1','city_name':'Select city'}];
		}
		if (this.ShopList == null || this.ShopList == undefined) {
			this.ShopList = [{'shop_id':'-1','shop_name':'Select shop'}];
		}
		// console.log(this.CityList);
		this.selectedShopLoc = JSON.parse(localStorage.getItem('selectedShopLoc'));

		if(typeof this.CountryList == 'string') {
			this.CountryList = JSON.parse(this.CountryList);
		}


		this.cartItems = localStorage.getItem('cartItems');
		if (this.cartItems != undefined || this.cartItems != null) {
			this.cartItemsLen = JSON.parse(this.cartItems).length;
		}
		//this.restProvider.hideLoadingSpinner();
		//this.restProvider.showToast('Item added in cart successfully.','toast-error');

	}

	goToHelp(params) {
		if (!params) params = {};
		this.navCtrl.setRoot(FaqPage);
	}

	searchProducts() {
		var searchData = {};
		searchData['country_id'] = this['receiver_country'];//this.receiver_country;
		searchData['city_id'] = this['receiver_city'];
		searchData['shop_id'] = this['receiver_shop'];
		var tempMsg = '';
		if (searchData['country_id'] == null || searchData['country_id'] == undefined  || searchData['country_id'] == '-1') {
			tempMsg += this.restProvider.errorString("Please select country",tempMsg);
		}
		if (searchData['city_id'] == null || searchData['city_id'] == undefined || searchData['city_id'] == '-1') {
			tempMsg += this.restProvider.errorString("Please select city",tempMsg);
		}
		if (searchData['shop_id'] == null || searchData['shop_id'] == undefined  || searchData['shop_id'] == '-1') {
			tempMsg += this.restProvider.errorString("Please select shop",tempMsg);
		}

		if (tempMsg != '') {
			this.restProvider.showErrorMsg(this.restProvider.statusError,tempMsg);
			return false;
		}

		var isConfirm = false;
		var params = {};
		var selectedShop = localStorage.getItem('selectedShopLoc');
		var cartItems = localStorage.getItem('cartItems');
		if (cartItems != undefined && cartItems != null) {
			cartItems = JSON.parse(cartItems);
		}
		else {
			cartItems = null;
		}
		if (cartItems != null && cartItems != undefined && cartItems.length > 0) {
			if(selectedShop != null && selectedShop != undefined && selectedShop != '') {
				selectedShop = JSON.parse(selectedShop);
				if (searchData['country_id'] == selectedShop['country_id'] && searchData['city_id'] == selectedShop['city_id'] && searchData['shop_id'] == selectedShop['shop_id']
				) {
					isConfirm = true;
					localStorage.setItem('selectedShopLoc',JSON.stringify(searchData));
					//var params = {};
					params['searchData'] = searchData;

					this.navCtrl.setRoot(ProductsPage, params); //TODO-VIX
				}

				else {
					var title = 'Are You Sure Want To Change Shop?';
					var message = 'If you change stores, you will lose your cart.';

					let alert = this.alertCtrl.create({
						title: title,
						message: message,
						buttons: [
							{
								text: 'No',
								role: 'cancel',
								handler: () => {
									console.log('Cancel clicked');
									this.openCartPage();
								}
							},
							{
								text: 'Yes',
								handler: () => {
									localStorage.removeItem('cartItems');
									isConfirm = true;
									localStorage.setItem('selectedShopLoc',JSON.stringify(searchData));
								//	var params = {};
									params['searchData'] = searchData;
									this.navCtrl.setRoot(ProductsPage, params);//TODO-VIX
								}
							}
						]
					});
					alert.present();
				}
			}
			else {
				isConfirm = true;
				localStorage.removeItem('cartItems');
				localStorage.setItem('selectedShopLoc',JSON.stringify(searchData));
				//var params = {};
				params['searchData'] = searchData;

				this.navCtrl.setRoot(ProductsPage, params);
			}
		}
		else {
			isConfirm = true;
			localStorage.setItem('selectedShopLoc',JSON.stringify(searchData));
		//	var params = {};
			params['searchData'] = searchData;
			this.navCtrl.setRoot(ProductsPage, params);
		}
		return false;
	}
	/*searchProducts() {
		var searchData = {};
		searchData.country_id = this.receiver_country;
		searchData.city_id = this.receiver_city;
		searchData.shop_id = this.receiver_shop;
		console.log(searchData);
		var tempMsg = '';
		if (searchData.country_id == null || searchData.country_id == undefined) {
			tempMsg += this.restProvider.errorString("Please select country",tempMsg);
		}
		if (searchData.city_id == null || searchData.city_id == undefined) {
			tempMsg += this.restProvider.errorString("Please select city",tempMsg);
		}
		if (searchData.shop_id == null || searchData.shop_id == undefined) {
			tempMsg += this.restProvider.errorString("Please select shop",tempMsg);
		}

		if (tempMsg != '') {
			this.restProvider.showErrorMsg(this.restProvider.statusError,tempMsg);
			return false;
		}

		var isConfirm = false;

		var selectedShop = localStorage.getItem('selectedShopLoc');
		var cartItems = localStorage.getItem('cartItems');
		if (cartItems != undefined && cartItems != null) {
			cartItems = JSON.parse(cartItems);
		}
		else {
			cartItems = [];
		}
		if (cartItems != null && cartItems != undefined && cartItems.length > 0) {
			if(selectedShop != null && selectedShop != undefined && selectedShop != '') {
				selectedShop = JSON.parse(selectedShop);
				console.log(selectedShop);
				console.log(searchData);
				if (searchData.country_id == selectedShop.country_id && searchData.city_id == selectedShop.city_id && searchData.shop_id == selectedShop.shop_id
				) {
					isConfirm = true;
					localStorage.setItem('selectedShopLoc',JSON.stringify(searchData));
					var params = {};
					params.searchData = searchData;

					//this.navCtrl.setRoot(ProductsPage, params); //TODO-VIX
				}

				else {
					var title = 'Are You Sure Want To Change Shop?';
					var message = 'If you change stores, you will lose your cart.';

					let alert = this.alertCtrl.create({
						title: title,
						message: message,
						buttons: [
							{
								text: 'No',
								role: 'cancel',
								handler: () => {
									console.log('Cancel clicked');
									this.openCartPage();
								}
							},
							{
								text: 'Yes',
								handler: () => {
									localStorage.removeItem('cartItems');
									isConfirm = true;
									localStorage.setItem('selectedShopLoc',JSON.stringify(searchData));
									var params = {};
									params.searchData = searchData;
									//this.navCtrl.setRoot(ProductsPage, params);//TODO-VIX
								}
							}
						]
					});
					alert.present();
				}
			}
			else {
				isConfirm = true;
				localStorage.removeItem('cartItems');
				localStorage.setItem('selectedShopLoc',JSON.stringify(searchData));
				var params = {};
				params.searchData = searchData;

				this.navCtrl.setRoot(ProductsPage, params);
			}
		}
		else {
			isConfirm = true;
			localStorage.setItem('selectedShopLoc',JSON.stringify(searchData));
			var params = {};
			params.searchData = searchData;
			this.navCtrl.setRoot(ProductsPage, params);
		}
		return false;
	}*/
	openCartPage() {
		//TODO-VIX this.navCtrl.push(MyCartPage);
	}
	getHomeData() {
		//this.restProvider.showToast(Constants.MSG_INTERNET_CONNECTION,'toast-error');
		this.restProvider.getHomeData()
			.then(
			(result) => {
				//console.log(result);
				this.restProvider.hideLoadingSpinner();
				var tempLoginMsg = localStorage.getItem('LoginMsg');
				if (tempLoginMsg != null && tempLoginMsg != undefined) {
					this.restProvider.showToast(tempLoginMsg ,'toast-success');
					localStorage.removeItem('LoginMsg');
				}
				if (result['status'] === true) {
					this.CountryList = result['response_data']['country_list'];
					if(result['response_data']['country_count'] == 0){
						this.restProvider.showErrorMsg(this.restProvider.statusString,result['message']);
					}
					localStorage.setItem('CountryList',JSON.stringify(this.CountryList));
				}
			},
			(err) => {
				this.restProvider.hideLoadingSpinner();
				this.restProvider.showErrorMsg(this.restProvider.statusError,this.restProvider.statusErrorMessage);
			}
		);
	}

	getCityList($event) {
		this.restProvider.LoadingSpinner();
		this.restProvider.getCities($event)
			.then(
			(result) => {
				this.restProvider.hideLoadingSpinner();
				if (result['status'] === true) {
					this.CityList = result['response_data']['city_list'];
				}
				else {
					this.CityList = [{'city_id':'-1','city_name':'Select city'}];
					this.ShopList = [{'shop_id':'-1','shop_name':'Select shop'}];
					this.restProvider.showErrorMsg(this.restProvider.statusError,result['message']);
				}
			},
			(err) => {
				this.restProvider.hideLoadingSpinner();
				this.restProvider.showErrorMsg(this.restProvider.statusError,this.restProvider.statusErrorMessage);
			}
		);
	}

	getShopList($event) {
		this.restProvider.LoadingSpinner();
		this.restProvider.getShops($event)
			.then(
			(result) => {
				this.restProvider.hideLoadingSpinner();
				if (result['status'] === true) {
					//if(result.result['response_data']['shop_list'].length > 0) {
					this.ShopList = result['response_data']['shop_list'];
					//}
				}
				else {
					this.ShopList = [{'shop_id':'-1','shop_name':'Select shop'}];
					this.restProvider.showErrorMsg(this.restProvider.statusError,result['message']);
				}
			},
			(err) => {
				this.restProvider.hideLoadingSpinner();
				this.restProvider.showErrorMsg(this.restProvider.statusError,this.restProvider.statusErrorMessage);
			}
		);
	}
}

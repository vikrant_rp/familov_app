import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { RestProvider } from '../../providers/rest/rest';
import { ProductDetailsPage } from '../product-details/product-details';
import { MyCartPage } from '../my-cart/my-cart';

@Component({
	selector: 'page-products',
	templateUrl: 'products.html'
})
export class ProductsPage {

	// product_type: Array<{languagename: string}>; // , component: any, icon: any
	category_list: any;
	product_list: any;
	shop_info: any;
	noMoreItemsAvailable: boolean;
	cartItems: any;
	cartItemsLen: any;
	showSearchBar: any;
	currencyData: any;
	currencySymbol: any;
	currencyName: any;
	searchData: any;
	IsFilter: any;

	curPage: any;
	totalPage: any;
	all_total_products: any;
	isPaginationScroll: any;
	isValidateShow: any;

	constructor(
		public navCtrl: NavController
		, public navParams: NavParams
		, public restProvider: RestProvider
	) {
		this.restProvider.LoadingSpinner();

		/*this.curPage = 1;
		this.totalPage = 1;
		this.isPaginationScroll = false;
		this.isValidateShow = true;*/

		// this.currencySymbol = '$';
		// this.currencyName = 'USD';
		/*this.currencySymbol = '$';
		this.currencyName = 'EUR';

		this.IsFilter = false;

		this.all_total_products = 0;
		this.shop_info = [];
		this.shop_info.shop_name = '';

		this.cartItemsLen = 0;
		this.showSearchBar = false;
		this.searchData = this.navParams.get('searchData');
		console.log(this.navParams.get('searchData'));

		this.noMoreItemsAvailable = false;

		this.currencyData = localStorage.getItem('currencyData');
		if (this.currencyData == undefined || this.currencyData == null) {
			this.currencyData = [];
			this.currencySymbol = '$';
			this.currencyName = 'EUR';
		}
		else {
			this.currencyData = JSON.parse(this.currencyData);
			this.currencySymbol = this.currencyData.vSymbol;
			this.currencyName = this.currencyData.vName;
		}

		this.searchData.currency = this.currencyName;

		console.log(this.currencySymbol);
		// console.log(this.currencyName)

		this.cartItems = localStorage.getItem('cartItems');
		if (this.cartItems == undefined || this.cartItems == null) {
			this.cartItems = [];
		}
		else {
			this.cartItems = JSON.parse(this.cartItems);
			this.cartItemsLen = this.cartItems.length;
		}

		this.searchData.page = this.curPage;
		//this.searchProducts(this.searchData);
		this.restProvider.hideLoadingSpinner();*/
		this.restProvider.hideLoadingSpinner();
	}

	ionViewCanEnter() {
		this.restProvider.LoadingSpinner();

		this.curPage = 1;
		this.totalPage = 1;
		this.isPaginationScroll = false;
		this.isValidateShow = true;

		// this.currencySymbol = '$';
		// this.currencyName = 'USD';
		this.currencySymbol = '&euro;';
		this.currencyName = 'EUR';

		this.IsFilter = false;

		this.all_total_products = 0;
		this.shop_info = [];
		this.shop_info.shop_name = '';

		this.cartItemsLen = 0;
		this.showSearchBar = false;
		this.searchData = this.navParams.get('searchData');
		this.noMoreItemsAvailable = false;

		this.currencyData = localStorage.getItem('currencyData');
		if (this.currencyData == undefined || this.currencyData == null) {
			this.currencyData = [];
			this.currencySymbol = '&euro;';
			this.currencyName = 'EUR';
		}
		else {
			this.currencyData = JSON.parse(this.currencyData);
			this.currencySymbol = this.currencyData.vSymbol;
			this.currencyName = this.currencyData.vName;
		}

		this.searchData.currency = this.currencyName;

		// console.log(this.currencySymbol);
		// console.log(this.currencyName)

		this.cartItems = localStorage.getItem('cartItems');
		if (this.cartItems == undefined || this.cartItems == null) {
			this.cartItems = [];
		}
		else {
			this.cartItems = JSON.parse(this.cartItems);
			this.cartItemsLen = this.cartItems.length;
		}

		this.searchData.page = this.curPage;
		this.searchProducts(this.searchData);
		//this.restProvider.hideLoadingSpinner();
		//this.restProvider.hideLoadingSpinner();
	}

	showSearch() {
		this.showSearchBar = true;
		document.getElementById('searchGrid').style.marginTop = '10vh';
	}
	hideSearch() {
		this.showSearchBar = false;
		document.getElementById('searchGrid').style.marginTop = '1vh';
	}

	/*loadMore() {
    	this.product_list.push({ id: $scope.items.length});
    	if ($scope.items.length == 20) {
      		$scope.noMoreItemsAvailable = true;
    	}
    	console.log('scroll.infiniteScrollComplete');
  	}*/

	viewProductDetails(product) {
		var params = {};
		params['productDetails'] = product;
		this.navCtrl.push(ProductDetailsPage, params);
	}

	openCartPage() {
		this.navCtrl.push(MyCartPage);
	}

  	searchProducts(params) {
		//this.restProvider.LoadingSpinner();
		this.restProvider.getProducts(this.searchData)
			.then(
			(result) => {
				 //console.log(result);
				if (result['status'] == true) {
					var all_total_products_response = result['response_data']['all_total_products'];
					var product_list_response = result['response_data']['product_list'];
					this.all_total_products = all_total_products_response;
					this.totalPage = Math.ceil(all_total_products_response / 12);
					//console.log(this.totalPage);
					if(all_total_products_response > 0) {
						if (result['response_data']['total_products'] > 0) {
							if (this.isPaginationScroll == true) {
								for (var i = 0; i < product_list_response.length; i++) {
									this.isPaginationScroll = false;
									// this.all_product_list.push(product_list_response[i]);
									this.product_list.push(product_list_response[i]);
								}
								//this.restProvider.showErrorMsg(this.restProvider.statusString,"in IF");
							}
							else {
								//this.restProvider.showErrorMsg(this.restProvider.statusString,"in else");
								this.isPaginationScroll = true;
								this.product_list = product_list_response;
							}
							this.category_list = result['response_data']['category_list'];
							this.shop_info = result['response_data']['shop_info'];
						}
						// console.log(this.shop_info.shop_name);
						// var selector = document.getElementById("cat_id_"+(this.searchData.category_id == '-1' ? '0' : this.searchData.category_id));
						// selector.classList.add('selectedCategory');
						this.restProvider.hideLoadingSpinner();

					}
					else {
						this.category_list = result['response_data']['category_list'];
						this.shop_info = result['response_data']['shop_info'];
						this.isValidateShow  = false;
						// this.all_product_list = [];
						this.product_list = [];
						this.restProvider.hideLoadingSpinner();
						this.restProvider.showErrorMsg(this.restProvider.statusString,result['message']);
					}
				}
				else {
					this.isValidateShow  = false;
					this.restProvider.hideLoadingSpinner();
					this.restProvider.showErrorMsg(this.restProvider.statusString,this.restProvider.statusErrorMessageAPI);
				}
			},
			(err) => {
				// console.log(err);
				this.restProvider.hideLoadingSpinner();
				this.restProvider.showErrorMsg(this.restProvider.statusString,this.restProvider.statusErrorMessageAPI);
			}
		);
		return false;
	}

	doInfinite(infiniteScroll) {
		this.isValidateShow = false;
		if (this.curPage < this.totalPage) {
			this.curPage = parseInt(this.curPage) + 1;
			this.searchData.page = this.curPage;
			this.searchProducts(this.searchData);
		}/*else if (this.curPage == this.totalPage) {
			this.curPage = parseInt(this.curPage) + 1;
			this.searchData.page = this.curPage;
			this.searchProducts(this.searchData);
		}*/
		else {
			if (this.curPage > 1 && this.all_total_products <= this.product_list.length) {
				infiniteScroll.enable(false);
			}
		}

		setTimeout(() => {
			this.isPaginationScroll = true;
			// console.log('curPage = ' + this.curPage);
			// console.log('totalPage = ' + this.totalPage);

			/*if (this.curPage <= this.totalPage) {
			 this.curPage = parseInt(this.curPage) + 1;
			 this.searchData.page = this.curPage;
			 this.searchProducts(this.searchData);
			 }
			 else {
			 infiniteScroll.enable(false);
			 }*/

			// console.log('Async operation has ended');
			infiniteScroll.complete();
			this.isValidateShow = true;
		}, 2000);
	}

	filterProducts(text) {
		if (text.length > 0) {
			this.searchData.searchText = text;
			// this.searchProducts();
		}
		else {
			this.searchData.searchText = null;
		}
		this.curPage = 1;
		this.searchData.page = this.curPage;
		document.getElementById("cat_id_0").click();
		this.searchProducts(this.searchData);
	}

	filterProductByCategory(category_id) {
		// if (category_id > 0) {
		this.restProvider.LoadingSpinner();
		this.searchData.category_id = category_id;
		this.curPage = 1;
		this.isPaginationScroll = false;
		this.searchData.page = this.curPage;
		var elems = document.querySelectorAll(".ion-scroll-hor .selectedCategory");
		[].forEach.call(elems, function(el) {
			el.classList.remove("selectedCategory");
		});
		var selector = document.getElementById("cat_id_"+(category_id == '-1' ? '0' : category_id));
		selector.classList.add('selectedCategory');
		this.searchProducts(this.searchData);
		setTimeout( () => {
			var selector = document.getElementById("cat_id_"+(category_id == '-1' ? '0' : category_id));
			selector.classList.add('selectedCategory');
			this.restProvider.hideLoadingSpinner();
		},1000);
	}

	addProductToCart(product) {
		if (this.cartItems != null && this.cartItems != undefined && this.cartItems.length > 0) {
			var exist = this.cartItems.filter(function (cartItem) { return cartItem.product_id == product.product_id });
			if(exist.length > 0) {
				for (var i = 0; i < this.cartItems.length; ++i) {
					if(this.cartItems[i].product_id == product.product_id) {
						this.cartItems[i].qty += 1;
					}
				}
				localStorage.setItem('cartItems',JSON.stringify(this.cartItems));
			}
			else {
				product.qty = 1;
				this.cartItems.push(product);
				localStorage.setItem('cartItems',JSON.stringify(this.cartItems));
			}
		}
		else {
			product.qty = 1;
			this.cartItems = [];
			this.cartItems.push(product);
			localStorage.setItem('cartItems',JSON.stringify(this.cartItems));
		}
		this.cartItemsLen = this.cartItems.length;
		//this.restProvider.showSuccessMsg('Success','Item added in cart successfully.');
		this.restProvider.showToast('Item added in cart successfully.',this.restProvider.statusSuccess);
		//this.restProvider.showErrorMsg(this.restProvider.statusError,this.restProvider.statusErrorMessage);
	}
}

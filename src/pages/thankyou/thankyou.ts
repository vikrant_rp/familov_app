
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { HomePage } from '../home/home';
//import { MyCartPage } from '../my-cart/my-cart';

@Component({
	selector: 'page-thankyou',
	templateUrl: 'thankyou.html'
})

export class ThankYouPage {

	cartItemsLen: any = 0;
	cartItems:any = {};
	constructor(public navCtrl: NavController) {
		this.cartItems = localStorage.getItem('cartItems');
	    if (this.cartItems != undefined || this.cartItems != null) {
	      this.cartItemsLen = JSON.parse(this.cartItems).length;
	    }
	}

	gotoHome(params) {
		if (!params) params = {};
		this.navCtrl.setRoot(HomePage);
	}

	openCartPage() {
		//TODO-VIX this.navCtrl.push(MyCartPage);
	}
}
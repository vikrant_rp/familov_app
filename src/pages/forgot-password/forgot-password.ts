
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { LoginPage } from '../login/login';
import { MyCartPage } from '../my-cart/my-cart';

@Component({
	selector: 'page-forgot-password',
	templateUrl: 'forgot-password.html'
})

export class ForgotPasswordPage {

	cartItemsLen: any = 0;
	cartItems: any = {};
	constructor(public navCtrl: NavController) {
		this.cartItems = localStorage.getItem('cartItems');
	    if (this.cartItems != undefined || this.cartItems != null) {
			this.cartItemsLen = JSON.parse(this.cartItems).length;
	    }
	}

	goToLogin(params) {
		if (!params) params = {};
		this.navCtrl.push(LoginPage);
	}

	openCartPage() {
		this.navCtrl.push(MyCartPage);
	}
}
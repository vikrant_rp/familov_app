import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { RestProvider } from '../../providers/rest/rest';

import { LoginPage } from '../login/login';
//import { MyCartPage } from '../my-cart/my-cart';

import { TermsPage } from '../terms/terms';

import 'rxjs/add/operator/map';

@Component({
	selector: 'page-signup',
	templateUrl: 'signup.html'
})

export class SignupPage {

	signupData:any = {};
	cartItems:any = {};
	cartItemsLen: any = 0;

	constructor(public navCtrl: NavController, public restProvider: RestProvider) {
		this.restProvider.LoadingSpinner();
		this.cartItems = localStorage.getItem('cartItems');
	    if (this.cartItems != undefined || this.cartItems != null) {
	      this.cartItemsLen = JSON.parse(this.cartItems).length;
	    }
	    this.restProvider.hideLoadingSpinner();
	}

	goBack() {
		this.navCtrl.pop();
	}
	
	goToLogin(params) {
		if (!params) params = {};
		this.navCtrl.push(LoginPage);
	}

	openCartPage() {
		//TODO-VIX this.navCtrl.push(MyCartPage);
	}

	doSignup() {
		var tempMsg = '';
		if(this.signupData.first_name == "" || this.signupData.first_name == undefined) {
			/*this.restProvider.showErrorMsg(this.restProvider.statusError,"Name is required");
			return false;*/
			tempMsg += this.restProvider.errorString("First name is required",tempMsg);
		}
		if(this.signupData.last_name == "" || this.signupData.last_name == undefined) {
			/*this.restProvider.showErrorMsg(this.restProvider.statusError,"Full name is required");
			return false;*/
			tempMsg += this.restProvider.errorString("Last name is required",tempMsg);
		}
		if(this.signupData.email == "" || this.signupData.email == undefined) {
			/*this.restProvider.showErrorMsg(this.restProvider.statusError,"Email is required");
			return false;*/
			tempMsg += this.restProvider.errorString("Email is required",tempMsg);
		}
		if(this.signupData.phone_number == "" || this.signupData.phone_number == undefined) {
			/*this.restProvider.showErrorMsg(this.restProvider.statusError,"Phone number is required");
			return false;*/
			tempMsg += this.restProvider.errorString("Phone Number is required",tempMsg);
		}
		if(this.signupData.password == "" || this.signupData.password == undefined) {
			/*this.restProvider.showErrorMsg(this.restProvider.statusError,"Password is required");
			return false;*/
			tempMsg += this.restProvider.errorString("Password is required",tempMsg);
		}
		if(this.signupData.repeat_password == "" || this.signupData.repeat_password == undefined) {
			/*this.restProvider.showErrorMsg(this.restProvider.statusError,"Confirm password is required");
			return false;*/
			tempMsg += this.restProvider.errorString("Confirm password is required",tempMsg);
		}else{
			if(this.signupData.password != this.signupData.repeat_password ) {
				/*this.restProvider.showErrorMsg(this.restProvider.statusError,"Confirm password is required");
				return false;*/
				tempMsg += this.restProvider.errorString("Confirm Password is do not match!",tempMsg);
			}
		}

		if (tempMsg != '') {
			this.restProvider.showErrorMsg(this.restProvider.statusError,tempMsg);
			return false;
		}

		this.restProvider.LoadingSpinner();
		this.signupData.phone_code = '+91';
		this.restProvider.signupUser(this.signupData)
		.then(
			(result) => {
				this.restProvider.hideLoadingSpinner();
				// result = JSON.parse(result);
				if (result['status'] === true) {
					this.restProvider.showSuccessMsg(this.restProvider.statusString,result['message']);
					setTimeout( () => {
						this.goToLogin(null);
					},200);
				}
				else {
					this.restProvider.showErrorMsg(this.restProvider.statusError,result['message']);
				}
			},
			(err) => {
				this.restProvider.hideLoadingSpinner();
				this.restProvider.showErrorMsg(this.restProvider.statusError,this.restProvider.statusErrorMessage);
			}
		);
	}
	goToTerms() {
		this.navCtrl.setRoot(TermsPage);		
	}
}
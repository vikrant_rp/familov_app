
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { RestProvider } from '../../providers/rest/rest';

/*import { MyProfilePage } from '../my-profile/my-profile';
import { MyCartPage } from '../my-cart/my-cart';*/

@Component({
	selector: 'page-change-password',
	templateUrl: 'change-password.html'
})

export class ChangePasswordPage {

	changePass: any = {};
	cartItems: any = {};
	LoginUser: any;
	IsUserLogin: any;
	apiSuccessMsg: string;
	apiErrorMsg: string;
	cartItemsLen: any = 0;
	
	constructor(
		public navCtrl: NavController
		, public restProvider: RestProvider) {
		this.LoginUser = JSON.parse(localStorage.getItem('LoginUser'));
		// console.log(this.LoginUser.customer_id);
		this.apiSuccessMsg = 'Success';
		this.apiErrorMsg = 'Error';

		this.cartItems = localStorage.getItem('cartItems');
		if (this.cartItems != undefined || this.cartItems != null) {
			this.cartItemsLen = JSON.parse(this.cartItems).length;
		}

	}

	/*goToProfile(params) {
		if (!params) params = {};
		this.navCtrl.setRoot(MyProfilePage);
	}

	openCartPage() {
		this.navCtrl.push(MyCartPage);
	}

	submitRequest() {
		this.changePass.customer_id = this.LoginUser.customer_id;
		this.restProvider.changePassword(this.changePass)
		.then(
			(result) => {
				// console.log(result);
				if (result.status == true) {
					this.restProvider.showSuccessMsg(this.apiSuccessMsg, result.message);
					this.changePass.current_password = '';
					this.changePass.new_password = '';
					this.changePass.repeat_new_password = '';
				}
				else {
					this.restProvider.showErrorMsg(this.apiErrorMsg, 'Something went wrong, Please try again.');
				}
			},
			(err) => {
				// console.log(err);
				this.restProvider.showErrorMsg(this.statusError,'Opps..!! <br> Something went wrong.');
			}
		);
	}*/
	
}
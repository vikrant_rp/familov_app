
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { RestProvider } from '../../providers/rest/rest';

import { HomePage } from '../home/home';
import { SignupPage } from '../signup/signup';
import { ForgotPasswordPage } from '../forgot-password/forgot-password';
//import { MyCartPage } from '../my-cart/my-cart';

@Component({
	selector: 'page-login',
	templateUrl: 'login.html'
})

export class LoginPage {

	loginData:any = {};

	constructor(public navCtrl: NavController, public restProvider: RestProvider) {
	}

	goBack() {
		this.navCtrl.pop();
	}

	goToHome(params) {
		if (!params) params = {};
		this.navCtrl.setRoot(HomePage);
	}

	goToSignup(params) {
		if (!params) params = {};
		this.navCtrl.push(SignupPage);
	}

	goToForgotPass(params) {
		if (!params) params = {};
		this.navCtrl.push(ForgotPasswordPage);
	}
	//TODO-VIX
	/*openCartPage() {
	 this.navCtrl.push(MyCartPage);
	 }*/

	doLogin() {
		var tempMsg = '';
		if(this.loginData.email == "" || this.loginData.email == undefined) {
			//this.restProvider.showErrorMsg(this.restProvider.statusError,"Email is required");
			//return false;
			tempMsg += this.restProvider.errorString("Email is required",tempMsg);
		}
		if(this.loginData.password == "" || this.loginData.password == undefined) {
			/*this.restProvider.showErrorMsg(this.restProvider.statusError,"Password is required");
			 return false;*/
			tempMsg += this.restProvider.errorString("Password is required",tempMsg);
		}
		if (tempMsg != '') {
			this.restProvider.showErrorMsg(this.restProvider.statusError,tempMsg);
			return false;
		}
		this.restProvider.LoadingSpinner();
		this.restProvider.loginUser(this.loginData)
			.then(
			(result) => {
				//this.restProvider.hideLoadingSpinner();
				// alert('result');
				if (result['status'] === true) {
					localStorage.setItem('LoginUser',JSON.stringify(result['response_data']));
					localStorage.setItem('LoginMsg',result['message']);
					//this.restProvider.showSuccessMsg(this.restProvider.statusSuccess,result['message']);
					//localStorage.setItem('LoginUser',JSON.stringify(result['response_data']));
					location.reload();
					//this.navCtrl.setRoot(HomePage);
					setTimeout( () => {
						//this.restProvider.showToast(result['message'],'toast-success');
						// this.goToHome();
						//location.reload();
					},500);
				}
				else {
					this.restProvider.hideLoadingSpinner();
					this.restProvider.showErrorMsg(this.restProvider.statusError,result['message']);
				}
			},
			(err) => {
				this.restProvider.hideLoadingSpinner();
				this.restProvider.showErrorMsg(this.restProvider.statusError,this.restProvider.statusErrorMessage);
			}
		);
	}
}
import { Component } from '@angular/core';
import { NavController, AlertController, NavParams } from 'ionic-angular';


import { RestProvider } from '../../providers/rest/rest';

import { HomePage } from '../home/home';

@Component({
	selector: 'page-product-details',
	templateUrl: 'product-details.html'
})

export class ProductDetailsPage {
	public qty1: number;
	productDetails: any;
	cartItems: any;
	cartItemsLen: any = 0;
	currencyData: any;
	currencySymbol: any;
	currencyName: any;

	constructor(
		public navCtrl: NavController
		, public alertCtrl: AlertController
		, public navParams: NavParams
		, public restProvider: RestProvider) {
		this.productDetails = this.navParams.get('productDetails');
		//console.log(this.productDetails);
		this.productDetails.qty = 1;
		this.productDetails.spec_price = 0;

		this.currencySymbol = '&euro;';
		this.currencyName = 'EUR';

		this.cartItems = localStorage.getItem('cartItems');
		if (this.cartItems == undefined || this.cartItems == null) {
			this.cartItems = [];
		}
		else {
			this.cartItems = JSON.parse(localStorage.getItem('cartItems'));
			this.cartItemsLen = this.cartItems.length;
		}

		this.currencyData = localStorage.getItem('currencyData');
		if (this.currencyData == undefined || this.currencyData == null) {
			this.currencyData = [];
			this.currencySymbol = '&euro;';
			this.currencyName = 'EUR';
		}
		else {
			this.currencyData = JSON.parse(this.currencyData);
			this.currencySymbol = this.currencyData.vSymbol;
			this.currencyName = this.currencyData.vName;
		}
		// console.log(this.currencySymbol);
		// console.log(this.currencyData);
	}

	ionViewCanEnter() {
		this.productDetails = this.navParams.get('productDetails');
		this.productDetails.qty = 1;
		this.productDetails.spec_price = 0;
		if(this.productDetails.convert_special_product_prices > 0){
			this.productDetails.spec_price = 1;
		}
		this.currencySymbol = '&euro;';
		this.currencyName = 'EUR';

		this.cartItems = localStorage.getItem('cartItems');
		if (this.cartItems == undefined || this.cartItems == null) {
			this.cartItems = [];
		}
		else {
			this.cartItems = JSON.parse(localStorage.getItem('cartItems'));
			this.cartItemsLen = this.cartItems.length;

			/*for (var i = 0; i < this.cartItems.length; ++i) {
				if(this.cartItems[i].product_id == this.productDetails.product_id) {
					this.productDetails.qty = this.cartItems[i].qty;
				}
			}*/

			// var exist = this.cartItems.filter(function (cartItem) { return cartItem.product_id == this.productDetails.product_id });
			// console.log(exist);

			// if(exist.length > 0) {
			// 	for (var i = 0; i < this.cartItems.length; ++i) {
			// 		if(this.cartItems[i].product_id == this.productDetails.product_id) {
			// 			this.productDetails.qty = this.cartItems[i].qty;
			// 		}
			// 	}
			// }
		}

		this.currencyData = localStorage.getItem('currencyData');
		if (this.currencyData == undefined || this.currencyData == null) {
			this.currencyData = [];
			this.currencySymbol = '&euro;';
			this.currencyName = 'EUR';
		}
		else {
			this.currencyData = JSON.parse(this.currencyData);
			this.currencySymbol = this.currencyData.vSymbol;
			this.currencyName = this.currencyData.vName;
		}
	}

	gotoHome(params) {
		if (!params) params = {};
		this.navCtrl.setRoot(HomePage);
	}

	presentAlert() {
		const alert = this.alertCtrl.create({
			title: 'Done',
			subTitle: 'Item has been added in cart successfully.',
			buttons: ['OK']
		});
		alert.present();
	}

	openCartPage() {
		//this.navCtrl.push(MyCartPage);
	}

	addQty() {
		this.productDetails.qty++;
	}

	subQty() {
		if(this.productDetails.qty > 1) {
			this.productDetails.qty--;
		}
		/*if (no == 1) {
	    	if(this.qty1 > 1){
		    	this.qty1--;
		    }
		}*/
	}	
	addProductToCart(product,type) {
		this.cartItems = localStorage.getItem('cartItems');
		if (this.cartItems == undefined || this.cartItems == null) {
			this.cartItems = [];
		}
		else {
			this.cartItems = JSON.parse(localStorage.getItem('cartItems'));
			this.cartItemsLen = this.cartItems.length;
		}
		if (this.productDetails.qty > 0) {
			if (this.cartItemsLen > 0) {
				var exist = this.cartItems.filter(function (cartItem) { return cartItem.product_id == product.product_id });
				if(exist.length > 0) {
					for (var i = 0; i < this.cartItems.length; ++i) {
						if(this.cartItems[i].product_id == product.product_id) {
							/*if(type == 'add') {
								// this.cartItems[i].qty++;
								// this.productDetails.qty = this.cartItems[i].qty;
								this.cartItems[i].qty = this.productDetails.qty;
								this.restProvider.showSuccessMsg('Success','Item added in cart successfully.');
							}
							else {
								if(this.cartItems[i].qty > 1){
									// this.cartItems[i].qty--;
									// this.productDetails.qty = this.cartItems[i].qty;
									this.cartItems[i].qty = this.productDetails.qty;
									this.restProvider.showSuccessMsg('Success','Item quantity updated successfully.');
								}
								else {
									this.removeItem(this.productDetails.product_id);
								}
							}*/
							this.cartItems[i].qty = parseInt(this.cartItems[i].qty) + parseInt(this.productDetails.qty);
							this.restProvider.showSuccessMsg('Success','Item added in cart successfully.');
						}
					}
					localStorage.setItem('cartItems',JSON.stringify(this.cartItems));
				}
				else {
					product.qty = this.productDetails.qty;
					if (this.productDetails.qty > 0) {
						// this.productDetails.qty = 1;
						this.cartItems.push(product);
						this.cartItemsLen = this.cartItems.length;
						localStorage.setItem('cartItems',JSON.stringify(this.cartItems));
						this.restProvider.showSuccessMsg('Success','Item added in cart successfully.');
					}
				}
			}
			else {
				product.qty = this.productDetails.qty;
				// this.productDetails.qty = 1;
				this.cartItems.push(product);
				this.cartItemsLen = this.cartItems.length;;
				localStorage.setItem('cartItems',JSON.stringify(this.cartItems));
				this.restProvider.showSuccessMsg('Success','Item added in cart successfully.');
			}
		}
		this.cartItemsLen = this.cartItems.length;
	}
	removeItem(product_id) {
		var index = this.cartItems.map(function(e) { return e.product_id; }).indexOf(''+product_id);
		this.cartItems.splice(index,1);
		localStorage.setItem('cartItems',JSON.stringify(this.cartItems));
		this.cartItemsLen = this.cartItems.length;
	}
}
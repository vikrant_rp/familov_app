import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { AlertController, LoadingController, Loading,ToastController } from 'ionic-angular';
import { Headers } from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class RestProvider {

	apiUrl: string;
	apiToken: string;
	loading: any;
	statusString: string;
	statusSuccess: string;
	statusError: string;
	statusErrorMessage: string;
	statusErrorMessageAPI: string;
	constructor(
		public http: HttpClient
		, public alertCtrl: AlertController
		, public loadingCtrl: LoadingController
		,public toastCtrl: ToastController
	) {
		this.statusString = '';
		this.statusSuccess = 'Success';
		this.statusError = 'Error!';
		this.statusErrorMessage = '<br>Something went wrong!';
		this.statusErrorMessageAPI = '<b>Something went wrong! Kindly contact to administrator.</b>';
		// console.log('Hello RestProvider Provider');

		this.apiUrl = 'http://103.21.58.248/ci_familov/api/';
		// this.apiUrl = 'http://localhost/ci_familov/api/';
		// this.apiUrl = 'https://familov.com/ci_familov/api/';

		this.apiToken = '123456789';//localStorage.getItem('apiToken');
		this.loading = Loading;
		// alert(localStorage.getItem('apiToken'));
	}

	showToast(msg,type) {

		let toast = this.toastCtrl.create({
			message: msg,
			duration: 3000,
			position: 'top',
			showCloseButton : true,
			dismissOnPageChange : true,
			cssClass: type
		});

		toast.onDidDismiss(() => {
			console.log('Dismissed toast');
		});

		toast.present();
	}

	showSuccessMsg(title,subTitle) {
		let alert = this.alertCtrl.create({
			title: title,
			subTitle: subTitle,
			buttons: ['Done']
		});
		alert.present();
	}

	showErrorMsg(title,subTitle) {
		let alert = this.alertCtrl.create({
			title: title,
			subTitle: subTitle,
			cssClass:'text-red',
			buttons: ['Ok']
		});
		alert.present();
	}

	showWarningMsg(title,subTitle) {
		let alert = this.alertCtrl.create({
			title: title,
			subTitle: subTitle,
			cssClass:'text-red',
			buttons: ['Close']
		});
		alert.present();
	}

	showConfirmationAlert(title, message, cancelText, confirmText) {
		let alert = this.alertCtrl.create({
			title: title,
			message: message,
			buttons: [
				{
					text: cancelText,
					role: 'cancel',
					handler: () => {
						return false;// console.log('Cancel clicked');
					}
				},
				{
					text: confirmText,
					handler: () => {
						return true;// console.log('Buy clicked');
					}
				}
			]
		});
		alert.present();
	}
	errorString(msg,errString){
		if(errString != ''){
			return "<br>"+msg;
		}
		return msg;
	}
	LoadingSpinner() {
		this.loading = this.loadingCtrl.create({
			content: 'Please wait...'
		});
		this.loading.present();
		/*setTimeout(() => {
		 this.loading.dismiss();
		 }, 1000);*/
	}

	hideLoadingSpinner() {
		if(this.loading != null){
			this.loading.dismiss();
			this.loading = null
		}
	}

	getToken() {
		return new Promise(
				resolve => {
				this.http.get(this.apiUrl+'token/token')
					.subscribe(
						data => {
						this.apiToken = '112121212';//data.key;
						// alert(this.apiToken);
						resolve(data);
					},
						err => {
						console.log(err);
					}
				);
			}
		);
	}

	getCurrency() {
		return new Promise(
				resolve => {
				this.http.get(this.apiUrl+'home/get_currency')
					.subscribe(
						data => {
						resolve(data);
					},
						err => {
						//console.log(err);
						this.showErrorMsg(this.statusError,this.statusErrorMessageAPI); //TODO-FORCE_FULL CLOSE
					}
				);
			}
		);
	}

	signupUser(data) {
		let body = new FormData();
		body.append('first_name',data.first_name);
		body.append('last_name',data.last_name);
		body.append('email',data.email);
		body.append('phone_code',data.phone_code);
		body.append('phone_number',data.phone_number);
		body.append('password',data.password);
		body.append('repeat_password',data.repeat_password);
		return new Promise(
			(resolve, reject) => {
				this.http.post(
					this.apiUrl + 'auth/sign_up',
					body,
					{
						//headers: headers
					}
				).subscribe(res => {
						resolve(res);
					}, (err) => {
						reject(err);
						this.showErrorMsg(this.statusError,this.statusErrorMessageAPI);
					}
				);
			}
		);
	}

	loginUser(data) {
		let body = new FormData();
		body.append('email',data.email);
		body.append('password',data.password);
		return new Promise(
			(resolve, reject) => {
				/*var headers = new Headers();
				 headers.append('Content-Type', 'application/json');*/
				this.http.post(
					this.apiUrl + 'auth/login',
					body,
					{
						//	headers: headers
					}
				).subscribe(res => {
						resolve(res);
					}, (err) => {
						reject(err);
						this.showErrorMsg(this.statusError,this.statusErrorMessageAPI);
					}
				);
			}
		);
	}

	getHomeData() {
		return new Promise(
			(resolve, reject) => {
				//headers.append('authorization', 'Bearer ' + this.apiToken);
				//this.getHeaders = 'authorization=Bearer '+localStorage.getItem('apiToken');
				setTimeout( () => {
					this.http.get(
						this.apiUrl + 'home/home',
						{

						}

					).subscribe(
							res => {
							resolve(res);
							//console.log(res);
						},
						(err) => {
							reject(err);
							//console.log(err);
							this.showErrorMsg(this.statusError,this.statusErrorMessageAPI); //TODO-FORCE_FULL CLOSE
						});
				},200);
			}
		);
	}

	getCities(country_id) {
		return new Promise(
			(resolve, reject) => {

				/*var headers = new Headers();
				 headers.append('Content-Type', 'application/json');*/
				//headers.append('authorization', 'Bearer ' + this.apiToken);
				// var headers = new HttpHeaders()
				// 				.set('authorization','Bearer ' + this.apiToken)
				// 				.set('Access-Control-Allow-Methods','POST')

				this.http.get(
					this.apiUrl + 'home/city?country_id='+country_id,
					{
						//headers: headers
					}
				)
					// .map(res => res.json())
					.subscribe(res => {
						resolve(res);
					}, (err) => {
						reject(err);
						this.showErrorMsg(this.statusError,this.statusErrorMessageAPI);
					});
			}
		);
	}

	getShops(city_id) {
		return new Promise(
			(resolve, reject) => {

				var headers = new Headers();
				headers.append('Content-Type', 'application/json');
				//headers.append('authorization', 'Bearer ' + this.apiToken);
				// var headers = new HttpHeaders()
				// 				.set('authorization','Bearer ' + this.apiToken)
				// 				.set('Access-Control-Allow-Methods','POST')
				this.http.get(
					this.apiUrl + 'home/shop?city_id='+city_id,
					{
						//headers: headers
					}
				)
					// .map(res => res.json())
					.subscribe(res => {
						resolve(res);
					}, (err) => {
						reject(err);
						this.showErrorMsg(this.statusError,this.statusErrorMessageAPI);
					});
			}
		);
	}

	getProducts(searchData) {
		return new Promise(
			(resolve, reject) => {
				var link = '&country_id='+searchData.country_id+'&city_id='+searchData.city_id+'&shop_id='+searchData.shop_id+'&currency='+searchData.currency+'&page='+searchData.page;
				if (searchData.category_id != null && searchData.category_id != undefined && searchData.category_id > 0) {
					link += '&category_id='+searchData.category_id;
				}
				if (searchData.searchText != null && searchData.searchText != undefined && searchData.searchText != '') {
					link += '&search_keyword='+searchData.searchText;
				}

				this.http.get(
					this.apiUrl + 'product/product_list?'+link,
					{
						//headers: headers
					}
				).subscribe(res => {
						resolve(res);
					}, (err) => {
						reject(err);
						this.showErrorMsg(this.statusError,this.statusErrorMessageAPI);
					});
			}
		);
	}

	changePassword(userData) {
		return new Promise(
			(resolve, reject) => {

				var body = new FormData();
				for (var i in userData) {
					body.append(i,userData[i]);
				}
				//body.append('authorization', 'Bearer ' + this.apiToken);

				var headers = new Headers();
				headers.append('Content-Type', 'application/json');
				//headers.append('authorization', 'Bearer ' + this.apiToken);
				// var headers = new HttpHeaders()
				// 				.set('authorization','Bearer ' + this.apiToken)
				// 				.set('Access-Control-Allow-Methods','POST')

				this.http.post(
					this.apiUrl + 'user/change_password',
					body,
					{
						//headers: headers
					}
				)
					// .map(res => res.json())
					.subscribe(res => {
						resolve(res);
					}, (err) => {
						reject(err);
						this.showErrorMsg(this.statusError,this.statusErrorMessageAPI);
					});
			}
		);
	}

	/*getOrdersList(customer_id) {
	 // customer_id = 129;
	 return new Promise(
	 resolve => {
	 this.http.get(this.apiUrl+'product/order_list?'+this.getHeaders+'&customer_id='+customer_id)
	 .subscribe(
	 data => {
	 resolve(data);
	 },
	 err => {
	 console.log(err);
	 }
	 );
	 }
	 );
	 }

	 getOrderDetails(customer_id,order_id) {
	 // customer_id = 129;
	 // order_id = 693;
	 return new Promise(
	 resolve => {
	 this.http.get(this.apiUrl+'product/order_detail_by_id?'+this.getHeaders+'&customer_id='+customer_id+'&order_id='+order_id)
	 .subscribe(
	 data => {
	 resolve(data);
	 },
	 err => {
	 console.log(err);
	 }
	 );
	 }
	 );
	 }

	 updateProfile(profileData) {
	 return new Promise(
	 (resolve, reject) => {

	 var body = new FormData();
	 for (var i in profileData) {
	 body.append(i,profileData[i]);
	 }
	 body.append('authorization', 'Bearer ' + this.apiToken);

	 var headers = new Headers();
	 headers.append('Content-Type', 'application/json');
	 headers.append('authorization', 'Bearer ' + this.apiToken);
	 // var headers = new HttpHeaders()
	 // 				.set('authorization','Bearer ' + this.apiToken)
	 // 				.set('Access-Control-Allow-Methods','POST')

	 this.http.post(
	 this.apiUrl + 'user/profile_update',
	 body,
	 {
	 //headers: headers
	 }
	 )
	 // .map(res => res.json())
	 .subscribe(res => {
	 resolve(res);
	 }, (err) => {
	 reject(err);
	 });
	 }
	 );
	 }
	 */
	getUserData(customer_id) {
		return new Promise(
			(resolve, reject) => {
				this.http.get(this.apiUrl+'user/profile?customer_id='+customer_id)
					.subscribe(
						data => {
						resolve(data);
					},
						err => {
						reject(err);
						this.showErrorMsg(this.statusError,this.statusErrorMessageAPI);
					}
				);
			}
		);
	}

	applyPromocode(data) {

		let body = new FormData();
		body.append('promo_code',data.promo_code);
		body.append('cart_amount_total',data.cart_amount_total);
		body.append('currency',data.currency);
		body.append('authorization',"Bearer "+this.apiToken);

		return new Promise(
			(resolve, reject) => {

				var headers = new Headers();
				headers.append('Content-Type', 'application/json');
				this.http.post(
					this.apiUrl + 'checkout/order_promo_code',
					body,
					{
						//	headers: headers
					}
				)
					// .map(res => res.json())
					.subscribe(res => {
						resolve(res);
					}, (err) => {
						reject(err);
					});

			}
		);
	}

	/*getShopDeliveryOptions(shop_id, currency) {
	 return new Promise(
	 resolve => {
	 this.http.get(this.apiUrl+'checkout/shop_delivery_option?'+this.getHeaders+'&shop_id='+shop_id+'&currency='+currency)
	 .subscribe(
	 data => {
	 resolve(data);
	 },
	 err => {
	 console.log(err);
	 }
	 );
	 }
	 );
	 }*/

	getConfigData() {
		return new Promise(
			(resolve, reject) => {
				setTimeout( () => {
					this.http.get(
						this.apiUrl + 'home/config',//+this.getHeaders,
						{
						}
					)
						// .map(res => res.json())
						.subscribe(res => {
							resolve(res);
						}, (err) => {
							reject(err);
							this.showErrorMsg(this.statusError,this.statusErrorMessageAPI);
						});
				},200);
			}
		);
	}

	/*checkoutWithBankTransfer(data) {
	 let body = new FormData();
	 body.append('email',data.email);
	 body.append('password',data.password);

	 body.append('authorization',"Bearer "+this.apiToken);

	 return new Promise(
	 (resolve, reject) => {
	 var headers = new Headers();
	 headers.append('Content-Type', 'application/json');
	 this.http.post(
	 this.apiUrl + 'auth/login',
	 body,
	 {
	 //	headers: headers
	 }
	 )
	 // .map(res => res.json())
	 .subscribe(res => {
	 resolve(res);
	 }, (err) => {
	 reject(err);
	 });

	 }
	 );
	 }*/
}

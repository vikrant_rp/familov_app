webpackJsonp([0],{

/***/ 106:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login_login__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__terms_terms__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//import { MyCartPage } from '../my-cart/my-cart';


var SignupPage = (function () {
    function SignupPage(navCtrl, restProvider) {
        this.navCtrl = navCtrl;
        this.restProvider = restProvider;
        this.signupData = {};
        this.cartItems = {};
        this.cartItemsLen = 0;
        this.restProvider.LoadingSpinner();
        this.cartItems = localStorage.getItem('cartItems');
        if (this.cartItems != undefined || this.cartItems != null) {
            this.cartItemsLen = JSON.parse(this.cartItems).length;
        }
        this.restProvider.hideLoadingSpinner();
    }
    SignupPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    SignupPage.prototype.goToLogin = function (params) {
        if (!params)
            params = {};
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__login_login__["a" /* LoginPage */]);
    };
    SignupPage.prototype.openCartPage = function () {
        //TODO-VIX this.navCtrl.push(MyCartPage);
    };
    SignupPage.prototype.doSignup = function () {
        var _this = this;
        var tempMsg = '';
        if (this.signupData.first_name == "" || this.signupData.first_name == undefined) {
            /*this.restProvider.showErrorMsg(this.restProvider.statusError,"Name is required");
            return false;*/
            tempMsg += this.restProvider.errorString("First name is required", tempMsg);
        }
        if (this.signupData.last_name == "" || this.signupData.last_name == undefined) {
            /*this.restProvider.showErrorMsg(this.restProvider.statusError,"Full name is required");
            return false;*/
            tempMsg += this.restProvider.errorString("Last name is required", tempMsg);
        }
        if (this.signupData.email == "" || this.signupData.email == undefined) {
            /*this.restProvider.showErrorMsg(this.restProvider.statusError,"Email is required");
            return false;*/
            tempMsg += this.restProvider.errorString("Email is required", tempMsg);
        }
        if (this.signupData.phone_number == "" || this.signupData.phone_number == undefined) {
            /*this.restProvider.showErrorMsg(this.restProvider.statusError,"Phone number is required");
            return false;*/
            tempMsg += this.restProvider.errorString("Phone Number is required", tempMsg);
        }
        if (this.signupData.password == "" || this.signupData.password == undefined) {
            /*this.restProvider.showErrorMsg(this.restProvider.statusError,"Password is required");
            return false;*/
            tempMsg += this.restProvider.errorString("Password is required", tempMsg);
        }
        if (this.signupData.repeat_password == "" || this.signupData.repeat_password == undefined) {
            /*this.restProvider.showErrorMsg(this.restProvider.statusError,"Confirm password is required");
            return false;*/
            tempMsg += this.restProvider.errorString("Confirm password is required", tempMsg);
        }
        else {
            if (this.signupData.password != this.signupData.repeat_password) {
                /*this.restProvider.showErrorMsg(this.restProvider.statusError,"Confirm password is required");
                return false;*/
                tempMsg += this.restProvider.errorString("Confirm Password is do not match!", tempMsg);
            }
        }
        if (tempMsg != '') {
            this.restProvider.showErrorMsg(this.restProvider.statusError, tempMsg);
            return false;
        }
        this.restProvider.LoadingSpinner();
        this.signupData.phone_code = '+91';
        this.restProvider.signupUser(this.signupData)
            .then(function (result) {
            _this.restProvider.hideLoadingSpinner();
            // result = JSON.parse(result);
            if (result['status'] === true) {
                _this.restProvider.showSuccessMsg(_this.restProvider.statusString, result['message']);
                setTimeout(function () {
                    _this.goToLogin(null);
                }, 200);
            }
            else {
                _this.restProvider.showErrorMsg(_this.restProvider.statusError, result['message']);
            }
        }, function (err) {
            _this.restProvider.hideLoadingSpinner();
            _this.restProvider.showErrorMsg(_this.restProvider.statusError, _this.restProvider.statusErrorMessage);
        });
    };
    SignupPage.prototype.goToTerms = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__terms_terms__["a" /* TermsPage */]);
    };
    return SignupPage;
}());
SignupPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-signup',template:/*ion-inline-start:"D:\app\familov_app\src\pages\signup\signup.html"*/'<!-- <ion-header>\n\n	<ion-navbar>\n\n		<button ion-button menuToggle class="buttonWhiteFont">\n\n			<ion-icon name="menu"></ion-icon>\n\n		</button>\n\n		<ion-title>\n\n			<ion-grid>\n\n    		<ion-row>\n\n    			<ion-col col-8 class="headerTitle">Signup</ion-col>\n\n    			<ion-col col-4><ion-icon class="right" name="cart" item-end (click)="openCartPage()"></ion-icon></ion-col>\n\n    		</ion-row>\n\n    	</ion-grid>\n\n		</ion-title>\n\n	</ion-navbar>\n\n</ion-header> -->\n\n\n\n<ion-content padding id="page7">\n\n	\n\n	<ion-grid>\n\n		<ion-row>\n\n			<ion-col col-2>\n\n				<ion-icon color="secondary" name="arrow-back" (click)="goBack()" style="margin-top: 2vh;"></ion-icon>\n\n			</ion-col>\n\n			<ion-col col-8>\n\n				<img src="assets/imgs/familov-logo.png" style="max-height: 10vh;">\n\n			</ion-col>\n\n		</ion-row>\n\n	</ion-grid>\n\n\n\n	<div class="spacer" style="height:15px;" id="login-spacer1"></div>\n\n	<div align="center"><img src="assets/imgs/laugh.png" style="max-height: 15vh;" /></div>\n\n	<h2 align="center" style="color: lightgray;">Register  <b class="textBlue">It\'s free !</b></h2>\n\n	<!--<div align="center">\n\n		<button id="login-button1" icon-right ion-button color="default" style="background: #3b5998 !important;width: 40vh;">\n\n			With Facebook\n\n			<ion-icon name="logo-facebook"></ion-icon>\n\n		</button>\n\n	</div>-->\n\n	<h4 align="center" style="color: lightgray;">Create your account.</h4>\n\n	<div class="spacer" style="height:15px;" id="login-spacer2"></div>\n\n\n\n	<form id="signup-form3" (ngSubmit)="doSignup()" method="post">\n\n		<ion-list id="signup-list3">\n\n\n\n			<ion-item id="signup-input5" class="item80" style="border: 1px solid #eee;margin: 1vh auto;">\n\n				<ion-label>\n\n					<!-- Nick Name -->\n\n					<ion-icon name="person" color="light"></ion-icon>\n\n				</ion-label>\n\n				<ion-input type="text" placeholder="Nickname" [(ngModel)]="signupData.first_name" name="first_name"></ion-input>\n\n			</ion-item>\n\n\n\n			<ion-item id="signup-input5" class="item80" style="border: 1px solid #eee;margin: 1vh auto;">\n\n				<ion-label>\n\n					<!-- Name -->\n\n					<ion-icon name="contact" color="light"></ion-icon>\n\n				</ion-label>\n\n				<ion-input type="text" placeholder="Name" [(ngModel)]="signupData.last_name" name="last_name"></ion-input>\n\n			</ion-item>\n\n			\n\n			<ion-item id="signup-input6" class="item80" style="border: 1px solid #eee;margin: 1vh auto;">\n\n				<ion-label>\n\n					<!-- E-mail -->\n\n					<ion-icon name="mail" color="light"></ion-icon>\n\n				</ion-label>\n\n				<ion-input type="email" placeholder="E-mail" [(ngModel)]="signupData.email" name="email"></ion-input>\n\n			</ion-item>\n\n			\n\n			<ion-item id="signup-input6" class="item80" style="border: 1px solid #eee;margin: 1vh auto;">\n\n				<ion-label>\n\n					<!-- Phone -->\n\n					<ion-icon name="call" color="light"></ion-icon>\n\n				</ion-label>\n\n				<ion-input type="number" placeholder="Phone" [(ngModel)]="signupData.phone_number" name="phone_number"></ion-input>\n\n			</ion-item>\n\n\n\n			<ion-item id="signup-input7" class="item80" style="border: 1px solid #eee;margin: 1vh auto;">\n\n				<ion-label>\n\n					<!-- Password -->\n\n					<ion-icon name="lock" color="light"></ion-icon>\n\n				</ion-label>\n\n				<ion-input type="password" placeholder="Password" [(ngModel)]="signupData.password" name="password"></ion-input>\n\n			</ion-item>\n\n\n\n			<ion-item id="signup-input7" class="item80" style="border: 1px solid #eee;margin: 1vh auto;">\n\n				<ion-label>\n\n					<!-- Confirm Password -->\n\n					<ion-icon name="lock" color="light"></ion-icon>\n\n				</ion-label>\n\n				<ion-input type="password" placeholder="Confirm Password" [(ngModel)]="signupData.repeat_password" name="repeat_password"></ion-input>\n\n			</ion-item>\n\n\n\n		</ion-list>\n\n\n\n		<p align="center" style="font-size: 12px;">\n\n			<!-- By clicking Signup you agree to the <a>Terms</a> and <a>Services</a>. -->\n\n			By creation an account you accept our <a class="text-green-fam" (click)="goToTerms()">Terms of service</a>\n\n			<br>\n\n			<!-- Do you already have a Familov account? <br>\n\n			<button id="login-button2" ion-button clear color="positive" (click)="goToLogin()">\n\n				Connect\n\n			</button> -->\n\n		</p>\n\n\n\n		<div align="center">\n\n			<button id="signup-button5" ion-button color="secondary" style="width: 40vh;font-weight: bold;" type="submit">\n\n				Create account\n\n			</button>\n\n		</div>\n\n\n\n		<!-- <br> -->\n\n\n\n		<p align="center">\n\n			<b class="textBlue">Already have an account?</b>\n\n			<a color="secondary" class="text-green-fam" ion-button (click)="goToLogin()" clear style="margin-top: -1px;text-transform: none;">\n\n				Sign In\n\n			</a>\n\n		</p>\n\n\n\n	</form>\n\n</ion-content>'/*ion-inline-end:"D:\app\familov_app\src\pages\signup\signup.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */]])
], SignupPage);

//# sourceMappingURL=signup.js.map

/***/ }),

/***/ 107:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TermsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TermsPage = (function () {
    function TermsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    return TermsPage;
}());
TermsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-terms',template:/*ion-inline-start:"D:\app\familov_app\src\pages\terms\terms.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n        <ion-title>Terms of services</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n\n    <h3 align="center" style="margin: 5vh auto;">Terms of sales.</h3>\n\n    \n\n    <div style="padding: auto 1vh 1vh 1vh;margin: auto 2vh 10vh 2vh;">\n\n        <p class="pinkLbl">1. Preamble</p>\n\n        <div class="termsDesc">\n\n            These general conditions of use and sale of the service under the Familov.com trade name exclusively govern the relationship between Familov.com and the user of the Service.\n\n            \n\n            <br><br>\n\n            <div style="padding-left: 4vh;">\n\n                1. Familov.com allows Users to order products or vouchers, on the internet, for individuals residing in Cameroon (the "Beneficiaries"). As soon as the payment of the order is validated, the Beneficiary is informed by Familov.com by means of a unique confidential code communicated by SMS.\n\n\n\n                <br><br>\n\n                2. The unique code provided by Familov.com allows the Beneficiary to take possession of the products from partner businesses, a list of which is available on the Familov.com website.\n\n                \n\n                <br><br>\n\n                3. These general conditions of sale define the terms and conditions of an order on the site www.familov.com. The User recognizes when placing an order via the site to have read the general conditions of sale and expressly declares to accept them without reservation and does not require a handwritten signature of this document.\n\n            </div>\n\n        </div>\n\n        \n\n        <p class="pinkLbl">2. Opening of the account and conditions of use</p>\n\n        <div class="termsDesc">\n\n            The User certifies to be at least 18 years of age and have the legal capacity or to hold a parental authorization to open a user account with Familov.com and to make orders.\n\n            \n\n            <br><br>\n\n            When creating the Account, the User communicates to Familov.com his name (s), first name (s). The User also communicates a valid e-mail address, chooses his username and a password. The username and password are proof of the identity of the User and commit to any order placed through them.\n\n\n\n            <br><br>The user is the full and exclusive manager of his username and password. He alone will bear the consequences that may result from any use by third parties who knew of them, unless he demonstrates that the knowledge of his username and password by another person results from a fault of Familov. In case of forgetfulness of his password or his username or in case of fear that a third party could have knowledge of it, the user disposes on the site of a function allowing him to find his identifier and to choose a New Password.\n\n        </div>\n\n        \n\n        <p class="pinkLbl">3. Using the Account and Ordering Products</p>\n\n        <div style="padding-left: 4vh">\n\n            * The Account is strictly personal, and can not be shared or exchanged with a third party.\n\n            \n\n            <br><br>\n\n            * Orders are collected in Euros, legal currency in the European Monetary Union. In the event that the User does not reside in a country where the Euro is legal tender, the User will do his business with the bank holding the bank account of the exchange rate that will be applied for this purpose.\n\n\n\n            <br><br>\n\n            * The purchases made by the Users are made by internet, by means of a valid credit card, a paypal account or by bank transfer.\n\n            \n\n            <br><br>\n\n            * For each order, the User indicates the identity (surname and first name) and the mobile phone number of the Beneficiary. The User receives a confirmation of the purchase made by e-mail.\n\n            \n\n            <br><br>\n\n            * Familov.com undertakes to communicate by SMS to the Beneficiary designated by the User when ordering, a unique code to enter into possession of the products. In case of delays, losses, errors or omissions resulting from a breakdown of telecommunications, Familov.com can not be held responsible for the non-delivery of the SMS.\n\n            \n\n            <br><br>\n\n            * The unique code provided by Familov.com is indispensable to the Beneficiary and must be presented to Partners to get possession of the products.\n\n        </div>\n\n        \n\n        <p class="pinkLbl">4. Exclusion of service</p>\n\n        <div class="termsDesc">Familov.com may suspend automatically and immediately the use of the Service by the User or the Beneficiary in case of suspicion of fraud, attempted fraud or fraud of these persons. Familov.com may suspend by right and immediately the use of the Service by the User for breaching any of his obligations such as failure to pay sums due at the time of an order.</div>\n\n\n\n        <p class="pinkLbl">5. Personal data and control</p>\n\n        <div class="termsDesc">\n\n            The User must at all times ensure that the information provided by him is accurate and up-to-date. All boxes marked as mandatory in the registration procedure on the Site must contain valid information. Any attempt of fraud via the presentation of inaccurate information on the User or the Beneficiary will be communicated to the competent authorities and may lead to the immediate refusal of access to the Site and the Service.\n\n            \n\n            <br><br>\n\n            Each User has a right of access to the data concerning him collected and processed by Familov.com as well as a right to modify and delete data concerning him. He authorizes Familov.com and its partners to transmit information. to the authorities in charge of regulation as required by law. The details of the User will not be disclosed to third parties, except with the express consent of the User\n\n            \n\n            <br><br>\n\n            All information relating to any order on Familov.com is subject to data processing. This treatment aims to fight against fraud of all kinds. Payment service providers by credit card or bank and Familov are the beneficiaries of the data related to all orders. Any unpaid order or suspicion of fraud in the payment for any reason automatically cancels the order.\n\n        </div>\n\n\n\n        <p class="pinkLbl"> 6. Methods of payment</p>\n\n        <div class="termsDesc">Payment is made by credit card, via Pay Pal, by bank transfer. In the absence of authorization to pay by credit card, the order is not made available to the User. Familov.com reserves the right not to respond to the order of a User for whom problems have already occurred. In the case of a payment by transfer, the user has a period of 7 days to send the settlement to the account of the Familov.</div>\n\n\n\n        <p class="pinkLbl">7. Prices</p>\n\n        <div class="termsDesc">The prices listed on the product lists and product sheets do not include the cost of home delivery. The total price indicated in the confirmation of the reception of the order includes the price of the products and if necessary the expenses of delivery.</div>\n\n\n\n        <p class="pinkLbl">8. Delivery and withdrawal</p>\n\n        <div class="termsDesc">\n\n            Delivery times are 36 hours depending on the availability of the beneficiary. The beneficiary is required to check the condition of the products and the products opened or marked can not be returned.\n\n            \n\n            <br><br>\n\n            The User communicates at the time of his order the name and mobile phone number of the recipient of the order in Cameroon. This phone number will be used to verify the identity of the Beneficiary during the delivery or withdrawal in the store.\n\n            \n\n            <br><br>\n\n            <div style="padding-left: 5vh;">\n\n                a- Delivery in a partner point:\n\n                The User can choose to place the order at the disposal of his Beneficiary in a partner point of sale. The order will be available 36 hours after the payment. When the Beneficiary arrives at the partner point of sale, to withdraw his order he must:\n\n                \n\n                <br><br>\n\n                * Present your identity document (national identity card)\n\n                \n\n                <br><br>\n\n                * Present the SMS code of the telephone number registered for the Beneficiary when taking the order by the user\n\n\n\n                <br><br>\n\n                * Sign a delivery note\n\n                \n\n                <br><br>\n\n                b- Home delivery:\n\n                The user can opt for home delivery and products will be delivered within 36h working days after payment of the order. Familov then contacts the Beneficiary to schedule the delivery. If the mobile phone number indicated to reach the Beneficiary is not valid, Familov immediately informs the user by e-mail who then chooses either to cancel the order, or to communicate another mobile phone number; he has a delay of 24 hours from the mail received by Familov and the driver will then try again to join the Beneficiary to organize the delivery. Upon delivery,\n\n            \n\n                <br><br>\n\n                * Present your identity document (national identity card)\n\n                \n\n                <br><br>\n\n                * Present the SMS code of the telephone number registered for the Beneficiary when taking the order by the user\n\n                \n\n                <br><br>\n\n                * Sign a delivery note\n\n            \n\n                <br><br>\n\n                * Possible delays in delivery do not entitle the User to cancel the order, refuse the products or claim damages and interest.\n\n            </div>\n\n        </div>\n\n\n\n        <p class="pinkLbl">9. Complaint and Retraction</p>\n\n        <div class="termsDesc">In case of claim on the products, the User and the beneficiary have a maximum of 24 hours following delivery to make specific reservations and Familov.com reserves the right to analyze the merits of these and communicate his response to the User in the shortest possible time. The User has a right of withdrawal of seven working days. The retratation must be written form and does not apply to fresh and perishable products.</div>\n\n\n\n        <p class="pinkLbl">10. Responsibility of Familov.com</p>\n\n        <div class="termsDesc">\n\n        Familov.com can only be held liable for proven facts that are exclusively and directly attributable to it. Only are likely to be compensated the direct damages and some suffered by the User.\n\n        \n\n        <br><br>\n\n        Familov.com acting as intermediary can not be held liable for damages of any kind, material or immaterial or bodily that could result from theft or misuse of the Site. The responsibility of Familov.com can not be held liable for any risk inherent in the use of the internet, such as suspension of service, intrusion, the presence of computer viruses or shopping by a third party.\n\n        \n\n        <br><br>\n\n        Familov.com can not be held responsible for errors made by the User or the Beneficiary in connection with the use of the Service.\n\n        \n\n        <br><br>\n\n        Familov.com puts in place the means necessary for the smooth operation of the Service. As such, the responsibility of Familov.com will not be engaged in cases of force majeure within the meaning of the German case law.</div>\n\n\n\n        <p class="pinkLbl">11. Liability of the User and the Beneficiary</p>\n\n        <div class="termsDesc">\n\n            * The Beneficiary must ensure the preservation of his mobile phone and the unique code communicated to him by Familov.com by SMS. The unique code transmitted by Familov.com is untransmissible and strictly for personal use. In the event that the Beneficiary makes a shared use of the mobile phone on which Familov.com sends the unique code to the Beneficiary, Familov.com can not be held responsible for withdrawals made by any other person using the mobile phone.\n\n            \n\n            <br><br>\n\n            * As long as no objection has been made to Familov.com as a result of the loss or theft of the Recipient\'s mobile phone, the User and the Beneficiary assume the consequences of any withdrawals made.\n\n            \n\n            <br><br>\n\n            * The Service must be used in compliance with these general conditions of use, the legislation in force and in accordance with the use for which it is intended.\n\n            \n\n            <br><br>\n\n            * Familov.com disclaims any liability for the use of the Service that does not comply with these conditions.\n\n        </div>\n\n\n\n        <p class="pinkLbl">12. Formation of the contract and proof of the transaction</p>\n\n        <div class="termsDesc">\n\n            * The commands are summarized before the User is prompted to validate them. The User is then invited to "confirm the payment"; The validation of order by a click constitutes firm and final validation under the reservation referred to in the following paragraph.\n\n            \n\n            <br><br>\n\n            * Any order with choice of payment by credit card is considered effective only when the payment centers concerned have given their agreement. In case of refusal of said centers, the order is automatically canceled and the user warned by a message on the screen.\n\n            \n\n            <br><br>\n\n            * The computerized data, securely stored in the Familov.com computer systems will be considered as proof of communications, orders and payments between the parties. They are kept within the legal periods of commercial sale and can be produced as evidence.\n\n        </div>\n\n\n\n        <p class="pinkLbl">13. Confirmation, preparation and formation of the contract</p>\n\n        <div class="termsDesc">\n\n            Familov.com confirms to the User the receipt of his order by an email summarizing the products ordered, the means of payment chosen, the means of delivery chosen, the total invoice for the service and a unique order code.\n\n            \n\n            <br><br>\n\n            Familov.com reserves the right not to accept an order, for a legitimate reason, such as a difficulty of supply of a product, a problem concerning the understanding of the order received (illegible document ...), a foreseeable problem concerning the delivery to be made (example: delivery requested in a geographical area where there is a proven risk of aggression) or due to the abnormality of the order made on the Familov.com site.\n\n            \n\n            <br><br>\n\n            In the event that an order can not be accepted for one of the reasons mentioned above, Familov.com informs the User. The transfer of risks to the User occurs at the time of provision or delivery. The information given by the user, when placing an order, commits him: in case of error of the User in the wording of his details or those of the place of delivery, including his name, first name, address , telephone number, e-mail address or credit or debit card number, resulting in the loss of the products, the User remains responsible for the payment of the lost products.\n\n        </div>\n\n\n\n        <p class="pinkLbl">14. Financial conditions and service charges</p>\n\n        <div class="termsDesc">\n\n            The rates applicable by Familov.com are available on the Site these rates are subject to change. Familov.com will charge the User a commission for the use of the Service. This commission corresponds to € 2.50.\n\n            \n\n            <br><br>\n\n            Subject to the right of withdrawal provided for in ARTICLE 9, the User gives irrevocable mandate to Familov.com to credit, for its order and for its account, the account of the Partner of the amount of the order made for the Beneficiary, minus all amounts due by the Partner to Familov.com, within the limit of the amount of the electronic sums credited to the Account.\n\n        </div>\n\n\n\n        <p class="pinkLbl">15. Products, availability and warranty</p>\n\n        <div class="termsDesc">\n\n            Familov.com makes available to the user before all the order all the essential characteristics of the products present in the catalogs published by familov.com and this according to the availability of the stocks. The legal warranty of the products available is ensured by the supplier and the manufacturer. The photographs that may accompany this description have no contractual value. The products offered by Familov are intended to be made available or delivered in Cameroon.\n\n            \n\n            <br><br>\n\n            The order is executed no later than 7 working days from the working day following the day the buyer placed his order. However, in case of unavailability of the ordered product, especially because of our partner supermarkets, the User will be informed at the earliest. In such a case, familov undertakes to offer the buyer a similar product at a similar price.\n\n            \n\n            <br><br>\n\n            The beneficiary benefits from the legal guarantee of conformity and latent defects on the products sold\n\n        </div>\n\n\n\n        <p class="pinkLbl">16. Provisions in case of dispute</p>\n\n        <div class="termsDesc">\n\n            The present conditions of sale on line are subject to the Allamande law. In case of dispute or claim, the User will first contact Familov.com for a direct conciliation of the parties. When a solution is not found one month after the claim, jurisdiction is assigned to the competent courts.\n\n        </div>\n\n\n\n        <p class="pinkLbl">17. Intellectual property</p>\n\n        <div class="termsDesc">\n\n            The Site, the applications and all the elements contained therein (information, data, sounds, images, drawings, graphics, distinctive signs, logos and brands) are the exclusive property of Familov.com. All of these elements are protected by intellectual property rights and as such, is protected against any unauthorized use by law.\n\n            \n\n            <br><br>\n\n            It is strictly forbidden to use or reproduce the name Familov.com and / or its logo, alone or associated, for any purpose whatsoever, and in particular for advertising purposes, without the prior written consent of Familov. com.\n\n            \n\n            <br><br>\n\n            It is also forbidden for the User to decompile or disassemble the Site, the applications, to use them for commercial purposes or to infringe in any way whatsoever the intellectual property rights of Familov.com\n\n        </div>\n\n\n\n        <p class="pinkLbl">18. General conditions</p>\n\n        <div class="termsDesc">\n\n            These terms and conditions come into force from its posting online. They govern any order placed until it is withdrawn.\n\n            \n\n            <br><br>\n\n            It is strictly forbidden to use or reproduce the name Familov.com and / or its logo, alone or associated, for any purpose whatsoever, and in particular for advertising purposes, without the prior written consent of Familov. com.\n\n            \n\n            <br><br>\n\n            Familov.com reserves the right to modify the Website, the service provided and the general conditions of use and invites the User to each new order to read carefully the general conditions of sale in force.\n\n        </div>\n\n\n\n        <p class="pinkLbl">19. Customer Service</p>\n\n        <div class="termsDesc">\n\n            For any difficulty, the user or the Beneficiary can contact Familov.com\n\n            \n\n            <br><br>\n\n            * By email: hello@familov.com.\n\n            \n\n            <br><br>\n\n            * By phone or Whatsapp: +49 1525 9948834\n\n            \n\n            <br><br>\n\n            * Chat: www.familov.com\n\n        </div>\n\n    </div>\n\n    \n\n</ion-content>'/*ion-inline-end:"D:\app\familov_app\src\pages\terms\terms.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
], TermsPage);

//# sourceMappingURL=terms.js.map

/***/ }),

/***/ 117:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 117;

/***/ }),

/***/ 158:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 158;

/***/ }),

/***/ 203:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutUsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AboutUsPage = (function () {
    function AboutUsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    return AboutUsPage;
}());
AboutUsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-about-us',template:/*ion-inline-start:"D:\app\familov_app\src\pages\about-us\about-us.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n        <ion-title>About US</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n\n\n\n    <h3 align="center" style="margin: 5vh auto;">Familov is a web platform that allows people living abroad to purchase products and services from local stores and have them delivered to their families back home.</h3>\n\n\n\n    <div align="center">\n\n        <img src="assets/imgs/teamwork.png" style="max-height: 150px;">\n\n        <p>"We are an entrepreneurial social and collaborative venture, with a real mission ...</p>\n\n    </div>\n\n    \n\n    <div align="center" style="margin-top: 5vh;padding: 1vh;">\n\n        <h3>Why us ?</h3>\n\n        <p>\n\n            Far from the major international institutions, the African diaspora remains the continent\'s largest donor. In 2015, the equivalent of $ 35 billion was transferred by expatriate African communities to their relatives who remained in the countries of origin. An invaluable gain, but part of the value remains confiscated upstream because of high commissions, which can easily exceed the 10% threshold. Sometimes 15%. A shortfall for the families who receive the funds.\n\n        </p>\n\n    </div>\n\n\n\n    <ion-card style="background: #36cd75;padding: 1vh;color: #FFF;">\n\n        <div align="center" style="margin-top: 1vh;">\n\n            <h1>Our Mission</h1>\n\n            <p>Maximize the financial, social and human impact of money transfer</p>\n\n        </div>\n\n    </ion-card>\n\n\n\n    <ion-card style="background: #36cd75;padding: 1vh;color: #FFF;">\n\n        <div align="center" style="margin-top: 1vh;">\n\n            <h1>Price</h1>\n\n            <p>Social Innovation Award at the start of Google Week-End Saar 2015</p>\n\n        </div>\n\n    </ion-card>\n\n    \n\n\n\n    <h1 align="center" style="margin-top: 2vh;">Our value</h1>\n\n    \n\n    <ion-card style="padding: 2vh;margin-bottom: 2vh;">\n\n        <div align="center" style="margin-top: 1vh;">\n\n            <ion-icon name="thumbs-up"></ion-icon>\n\n            <h3>Embrace transparency</h3>\n\n            <p>We value and respect our customers and partners. If it was not for them, we would not be us.</p>\n\n        </div>\n\n    </ion-card>\n\n\n\n    <ion-card style="padding: 2vh;margin-bottom: 2vh;">\n\n        <div align="center" style="margin-top: 1vh;">\n\n            <ion-icon name="football"></ion-icon>\n\n            <h3>Positive impact</h3>\n\n            <p>We care about each other and believe in the relationships in which people value and support each other.</p>\n\n        </div>\n\n    </ion-card>\n\n\n\n    <ion-card style="padding: 2vh;margin-bottom: 2vh;">\n\n        <div align="center" style="margin-top: 1vh;">\n\n            <ion-icon name="heart"></ion-icon>\n\n            <h3>Passion</h3>\n\n            <p>We like to help our clients achieve their goals, take care of their loved ones or simply increase their savings.</p>\n\n        </div>\n\n    </ion-card>\n\n\n\n</ion-content>'/*ion-inline-end:"D:\app\familov_app\src\pages\about-us\about-us.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
], AboutUsPage);

//# sourceMappingURL=about-us.js.map

/***/ }),

/***/ 204:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__product_details_product_details__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__my_cart_my_cart__ = __webpack_require__(56);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ProductsPage = (function () {
    function ProductsPage(navCtrl, navParams, restProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restProvider = restProvider;
        this.restProvider.LoadingSpinner();
        /*this.curPage = 1;
        this.totalPage = 1;
        this.isPaginationScroll = false;
        this.isValidateShow = true;*/
        // this.currencySymbol = '$';
        // this.currencyName = 'USD';
        /*this.currencySymbol = '$';
        this.currencyName = 'EUR';

        this.IsFilter = false;

        this.all_total_products = 0;
        this.shop_info = [];
        this.shop_info.shop_name = '';

        this.cartItemsLen = 0;
        this.showSearchBar = false;
        this.searchData = this.navParams.get('searchData');
        console.log(this.navParams.get('searchData'));

        this.noMoreItemsAvailable = false;

        this.currencyData = localStorage.getItem('currencyData');
        if (this.currencyData == undefined || this.currencyData == null) {
            this.currencyData = [];
            this.currencySymbol = '$';
            this.currencyName = 'EUR';
        }
        else {
            this.currencyData = JSON.parse(this.currencyData);
            this.currencySymbol = this.currencyData.vSymbol;
            this.currencyName = this.currencyData.vName;
        }

        this.searchData.currency = this.currencyName;

        console.log(this.currencySymbol);
        // console.log(this.currencyName)

        this.cartItems = localStorage.getItem('cartItems');
        if (this.cartItems == undefined || this.cartItems == null) {
            this.cartItems = [];
        }
        else {
            this.cartItems = JSON.parse(this.cartItems);
            this.cartItemsLen = this.cartItems.length;
        }

        this.searchData.page = this.curPage;
        //this.searchProducts(this.searchData);
        this.restProvider.hideLoadingSpinner();*/
        this.restProvider.hideLoadingSpinner();
    }
    ProductsPage.prototype.ionViewCanEnter = function () {
        this.restProvider.LoadingSpinner();
        this.curPage = 1;
        this.totalPage = 1;
        this.isPaginationScroll = false;
        this.isValidateShow = true;
        // this.currencySymbol = '$';
        // this.currencyName = 'USD';
        this.currencySymbol = '&euro;';
        this.currencyName = 'EUR';
        this.IsFilter = false;
        this.all_total_products = 0;
        this.shop_info = [];
        this.shop_info.shop_name = '';
        this.cartItemsLen = 0;
        this.showSearchBar = false;
        this.searchData = this.navParams.get('searchData');
        this.noMoreItemsAvailable = false;
        this.currencyData = localStorage.getItem('currencyData');
        if (this.currencyData == undefined || this.currencyData == null) {
            this.currencyData = [];
            this.currencySymbol = '&euro;';
            this.currencyName = 'EUR';
        }
        else {
            this.currencyData = JSON.parse(this.currencyData);
            this.currencySymbol = this.currencyData.vSymbol;
            this.currencyName = this.currencyData.vName;
        }
        this.searchData.currency = this.currencyName;
        // console.log(this.currencySymbol);
        // console.log(this.currencyName)
        this.cartItems = localStorage.getItem('cartItems');
        if (this.cartItems == undefined || this.cartItems == null) {
            this.cartItems = [];
        }
        else {
            this.cartItems = JSON.parse(this.cartItems);
            this.cartItemsLen = this.cartItems.length;
        }
        this.searchData.page = this.curPage;
        this.searchProducts(this.searchData);
        //this.restProvider.hideLoadingSpinner();
        //this.restProvider.hideLoadingSpinner();
    };
    ProductsPage.prototype.showSearch = function () {
        this.showSearchBar = true;
        document.getElementById('searchGrid').style.marginTop = '10vh';
    };
    ProductsPage.prototype.hideSearch = function () {
        this.showSearchBar = false;
        document.getElementById('searchGrid').style.marginTop = '1vh';
    };
    /*loadMore() {
        this.product_list.push({ id: $scope.items.length});
        if ($scope.items.length == 20) {
            $scope.noMoreItemsAvailable = true;
        }
        console.log('scroll.infiniteScrollComplete');
    }*/
    ProductsPage.prototype.viewProductDetails = function (product) {
        var params = {};
        params['productDetails'] = product;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__product_details_product_details__["a" /* ProductDetailsPage */], params);
    };
    ProductsPage.prototype.openCartPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__my_cart_my_cart__["a" /* MyCartPage */]);
    };
    ProductsPage.prototype.searchProducts = function (params) {
        var _this = this;
        //this.restProvider.LoadingSpinner();
        this.restProvider.getProducts(this.searchData)
            .then(function (result) {
            //console.log(result);
            if (result['status'] == true) {
                var all_total_products_response = result['response_data']['all_total_products'];
                var product_list_response = result['response_data']['product_list'];
                _this.all_total_products = all_total_products_response;
                _this.totalPage = Math.ceil(all_total_products_response / 12);
                //console.log(this.totalPage);
                if (all_total_products_response > 0) {
                    if (result['response_data']['total_products'] > 0) {
                        if (_this.isPaginationScroll == true) {
                            for (var i = 0; i < product_list_response.length; i++) {
                                _this.isPaginationScroll = false;
                                // this.all_product_list.push(product_list_response[i]);
                                _this.product_list.push(product_list_response[i]);
                            }
                            //this.restProvider.showErrorMsg(this.restProvider.statusString,"in IF");
                        }
                        else {
                            //this.restProvider.showErrorMsg(this.restProvider.statusString,"in else");
                            _this.isPaginationScroll = true;
                            _this.product_list = product_list_response;
                        }
                        _this.category_list = result['response_data']['category_list'];
                        _this.shop_info = result['response_data']['shop_info'];
                    }
                    // console.log(this.shop_info.shop_name);
                    // var selector = document.getElementById("cat_id_"+(this.searchData.category_id == '-1' ? '0' : this.searchData.category_id));
                    // selector.classList.add('selectedCategory');
                    _this.restProvider.hideLoadingSpinner();
                }
                else {
                    _this.category_list = result['response_data']['category_list'];
                    _this.shop_info = result['response_data']['shop_info'];
                    _this.isValidateShow = false;
                    // this.all_product_list = [];
                    _this.product_list = [];
                    _this.restProvider.hideLoadingSpinner();
                    _this.restProvider.showErrorMsg(_this.restProvider.statusString, result['message']);
                }
            }
            else {
                _this.isValidateShow = false;
                _this.restProvider.hideLoadingSpinner();
                _this.restProvider.showErrorMsg(_this.restProvider.statusString, _this.restProvider.statusErrorMessageAPI);
            }
        }, function (err) {
            // console.log(err);
            _this.restProvider.hideLoadingSpinner();
            _this.restProvider.showErrorMsg(_this.restProvider.statusString, _this.restProvider.statusErrorMessageAPI);
        });
        return false;
    };
    ProductsPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        this.isValidateShow = false;
        if (this.curPage < this.totalPage) {
            this.curPage = parseInt(this.curPage) + 1;
            this.searchData.page = this.curPage;
            this.searchProducts(this.searchData);
        } /*else if (this.curPage == this.totalPage) {
            this.curPage = parseInt(this.curPage) + 1;
            this.searchData.page = this.curPage;
            this.searchProducts(this.searchData);
        }*/
        else {
            if (this.curPage > 1 && this.all_total_products <= this.product_list.length) {
                infiniteScroll.enable(false);
            }
        }
        setTimeout(function () {
            _this.isPaginationScroll = true;
            // console.log('curPage = ' + this.curPage);
            // console.log('totalPage = ' + this.totalPage);
            /*if (this.curPage <= this.totalPage) {
             this.curPage = parseInt(this.curPage) + 1;
             this.searchData.page = this.curPage;
             this.searchProducts(this.searchData);
             }
             else {
             infiniteScroll.enable(false);
             }*/
            // console.log('Async operation has ended');
            infiniteScroll.complete();
            _this.isValidateShow = true;
        }, 2000);
    };
    ProductsPage.prototype.filterProducts = function (text) {
        if (text.length > 0) {
            this.searchData.searchText = text;
            // this.searchProducts();
        }
        else {
            this.searchData.searchText = null;
        }
        this.curPage = 1;
        this.searchData.page = this.curPage;
        document.getElementById("cat_id_0").click();
        this.searchProducts(this.searchData);
    };
    ProductsPage.prototype.filterProductByCategory = function (category_id) {
        var _this = this;
        // if (category_id > 0) {
        this.restProvider.LoadingSpinner();
        this.searchData.category_id = category_id;
        this.curPage = 1;
        this.isPaginationScroll = false;
        this.searchData.page = this.curPage;
        var elems = document.querySelectorAll(".ion-scroll-hor .selectedCategory");
        [].forEach.call(elems, function (el) {
            el.classList.remove("selectedCategory");
        });
        var selector = document.getElementById("cat_id_" + (category_id == '-1' ? '0' : category_id));
        selector.classList.add('selectedCategory');
        this.searchProducts(this.searchData);
        setTimeout(function () {
            var selector = document.getElementById("cat_id_" + (category_id == '-1' ? '0' : category_id));
            selector.classList.add('selectedCategory');
            _this.restProvider.hideLoadingSpinner();
        }, 1000);
    };
    ProductsPage.prototype.addProductToCart = function (product) {
        if (this.cartItems != null && this.cartItems != undefined && this.cartItems.length > 0) {
            var exist = this.cartItems.filter(function (cartItem) { return cartItem.product_id == product.product_id; });
            if (exist.length > 0) {
                for (var i = 0; i < this.cartItems.length; ++i) {
                    if (this.cartItems[i].product_id == product.product_id) {
                        this.cartItems[i].qty += 1;
                    }
                }
                localStorage.setItem('cartItems', JSON.stringify(this.cartItems));
            }
            else {
                product.qty = 1;
                this.cartItems.push(product);
                localStorage.setItem('cartItems', JSON.stringify(this.cartItems));
            }
        }
        else {
            product.qty = 1;
            this.cartItems = [];
            this.cartItems.push(product);
            localStorage.setItem('cartItems', JSON.stringify(this.cartItems));
        }
        this.cartItemsLen = this.cartItems.length;
        //this.restProvider.showSuccessMsg('Success','Item added in cart successfully.');
        this.restProvider.showToast('Item added in cart successfully.', this.restProvider.statusSuccess);
        //this.restProvider.showErrorMsg(this.restProvider.statusError,this.restProvider.statusErrorMessage);
    };
    return ProductsPage;
}());
ProductsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-products',template:/*ion-inline-start:"D:\app\familov_app\src\pages\products\products.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <button ion-button menuToggle class="buttonWhiteFont">\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n        <ion-title>\n\n            <ion-grid>\n\n                <ion-row>\n\n                    <ion-col col-8 class="headerTitle"><div align="center">Shop</div></ion-col>\n\n                    <ion-col col-2>\n\n                        <ion-icon name="search" *ngIf="!showSearchBar" item-end (click)="showSearch()"></ion-icon>\n\n                        <ion-icon name="close" *ngIf="showSearchBar" item-end (click)="hideSearch()"></ion-icon>\n\n                    </ion-col>\n\n                    <ion-col col-2 class="badgeCol">\n\n                        <!-- <ion-icon class="right" name="cart" item-end (click)="openCartPage()"></ion-icon> -->\n\n                        <span class="badgeIcon"> {{ cartItemsLen }}</span>\n\n                        <ion-icon name="cart" item-end (click)="openCartPage()"></ion-icon>\n\n                    </ion-col>\n\n                </ion-row>\n\n            </ion-grid>\n\n        </ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content padding id="page3" class="grayBackground">\n\n    <div style="background: #FFF" style="position: fixed; width: 100%; background: #FFF; z-index: 99999999;">\n\n        <ion-input *ngIf="showSearchBar" (change)="filterProducts($event.target.value)" type="text" placeholder="Search" style="margin-top: 1vh;border-bottom: 1px solid #eee;"></ion-input>\n\n    </div>\n\n\n\n    <ion-grid id="searchGrid" *ngIf="all_total_products > 0">\n\n        <ion-row>\n\n            <ion-col col-3>\n\n                <div align="center"><img src="assets/imgs/shopbanner-icon.png" style="max-height: 8vh;" /></div>\n\n            </ion-col>\n\n            <ion-col col-9>\n\n                <h4 class="textBlue" style="font-weight: bold;margin-bottom: 2px;margin-top: 1vh;">{{ shop_info.shop_name }}</h4>\n\n                <!-- <span>TEST Shop name display</span> -->\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-grid>\n\n\n\n    <hr>\n\n    <ion-scroll scrollX="true" direction="x" class="ion-scroll-hor" style="margin-top: 2vh;margin-bottom: 2vh;" *ngIf="all_total_products > 0">\n\n        <button clear class="ion-card-scroll selectedCategory category_list_btn" color="secondary" (click)="filterProductByCategory(-1)" id="cat_id_0" style="margin-top: 1px;color: lightgray;line-height: 1vh;">\n\n            All Products\n\n        </button>\n\n        <button clear class="ion-card-scroll category_list_btn" *ngFor="let category of category_list" color="secondary" (click)="filterProductByCategory(category.category_id)" id="cat_id_{{category.category_id}}" style="margin-top: 1px;color: lightgray;line-height: 1vh;">\n\n            {{ category.category_name }}\n\n        </button>\n\n    </ion-scroll>\n\n    <!-- <hr> -->\n\n    <div align="center">\n\n        <ion-grid style="margin-bottom: 5vh;" *ngIf="all_total_products > 0">\n\n            <ion-row>\n\n                <ion-col col-6 align="center" class="products" *ngFor="let product of product_list" style="background: #FFF;color: #000;">\n\n                    <div class="price-box" *ngIf="product.discount != 0">{{ product.discount }}</div>\n\n                    <img src="{{ product.product_image }}" class="proImg" (click)="viewProductDetails(product)">\n\n                    <br>\n\n                    <p class="productTitle" (click)="viewProductDetails(product)">{{ product.product_name | slice:0:20 }} {{ product.product_name.length > 20 ? \'....\' : \'\' }} </p>\n\n                    <br>\n\n                    <span class="priceLbl" *ngIf="product.convert_special_product_prices > 0">\n\n                        <small style="margin-right: 5vh; text-decoration: line-through;color: #9c9191;" >\n\n                            <span [innerHtml]="currencySymbol">{{ currencyName }}</span>\n\n                            {{ product.convert_product_prices }}\n\n                        </small>\n\n                        <span [innerHtml]="currencySymbol">{{ currencyName }}</span> {{ product.convert_special_product_prices }}\n\n                    </span>\n\n                    <span class="priceLbl" *ngIf="product.convert_special_product_prices == 0">\n\n                        <span [innerHtml]="currencySymbol">{{ currencyName }}</span> {{ product.convert_product_prices }}\n\n                    </span>\n\n                    <button color="secondary" class="moreDetails" ion-button (click)="addProductToCart(product)">Add to cart</button>\n\n                </ion-col>\n\n            </ion-row>\n\n        </ion-grid>\n\n    </div>\n\n\n\n    <ion-infinite-scroll (ionInfinite)="doInfinite($event)" style="opacity: 1;z-index: 9999999;">\n\n        <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Loading more data..."></ion-infinite-scroll-content>\n\n    </ion-infinite-scroll>\n\n\n\n    <ion-grid style="margin-bottom: 5vh;" *ngIf="all_total_products == 0">\n\n        <ion-row>\n\n            <ion-col col-12 align="center" style="background: #FFF;color: #000;">\n\n                <img src="assets/imgs/tired.png" style="margin-top: 5vh;max-height: 15vh;">\n\n                <br>\n\n                <h1 class="view-cart-empty">Products are not available in this shop!<br>Please try another shop.</h1>\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-grid>\n\n\n\n    <!-- <ion-infinite-scroll ng-if="!noMoreItemsAvailable" on-infinite="loadMore()" distance="10%"></ion-infinite-scroll> -->\n\n\n\n    <div class="fixed-outside" style="position: fixed;bottom: 0;left: 0;width: 100%;" align="center" *ngIf="isValidateShow">\n\n        <button class="button-md-height" color="secondary" ion-button icon-right block style="margin-bottom: 0px;text-transform: none;height: 2.5em;">Validate <ion-icon name="arrow-forward"></ion-icon></button>\n\n    </div>\n\n\n\n\n\n</ion-content>'/*ion-inline-end:"D:\app\familov_app\src\pages\products\products.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */]])
], ProductsPage);

//# sourceMappingURL=products.js.map

/***/ }),

/***/ 205:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(42);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ProductDetailsPage = (function () {
    function ProductDetailsPage(navCtrl, alertCtrl, navParams, restProvider) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.navParams = navParams;
        this.restProvider = restProvider;
        this.cartItemsLen = 0;
        this.productDetails = this.navParams.get('productDetails');
        //console.log(this.productDetails);
        this.productDetails.qty = 1;
        this.productDetails.spec_price = 0;
        this.currencySymbol = '&euro;';
        this.currencyName = 'EUR';
        this.cartItems = localStorage.getItem('cartItems');
        if (this.cartItems == undefined || this.cartItems == null) {
            this.cartItems = [];
        }
        else {
            this.cartItems = JSON.parse(localStorage.getItem('cartItems'));
            this.cartItemsLen = this.cartItems.length;
        }
        this.currencyData = localStorage.getItem('currencyData');
        if (this.currencyData == undefined || this.currencyData == null) {
            this.currencyData = [];
            this.currencySymbol = '&euro;';
            this.currencyName = 'EUR';
        }
        else {
            this.currencyData = JSON.parse(this.currencyData);
            this.currencySymbol = this.currencyData.vSymbol;
            this.currencyName = this.currencyData.vName;
        }
        // console.log(this.currencySymbol);
        // console.log(this.currencyData);
    }
    ProductDetailsPage.prototype.ionViewCanEnter = function () {
        this.productDetails = this.navParams.get('productDetails');
        this.productDetails.qty = 1;
        this.productDetails.spec_price = 0;
        if (this.productDetails.convert_special_product_prices > 0) {
            this.productDetails.spec_price = 1;
        }
        this.currencySymbol = '&euro;';
        this.currencyName = 'EUR';
        this.cartItems = localStorage.getItem('cartItems');
        if (this.cartItems == undefined || this.cartItems == null) {
            this.cartItems = [];
        }
        else {
            this.cartItems = JSON.parse(localStorage.getItem('cartItems'));
            this.cartItemsLen = this.cartItems.length;
            /*for (var i = 0; i < this.cartItems.length; ++i) {
                if(this.cartItems[i].product_id == this.productDetails.product_id) {
                    this.productDetails.qty = this.cartItems[i].qty;
                }
            }*/
            // var exist = this.cartItems.filter(function (cartItem) { return cartItem.product_id == this.productDetails.product_id });
            // console.log(exist);
            // if(exist.length > 0) {
            // 	for (var i = 0; i < this.cartItems.length; ++i) {
            // 		if(this.cartItems[i].product_id == this.productDetails.product_id) {
            // 			this.productDetails.qty = this.cartItems[i].qty;
            // 		}
            // 	}
            // }
        }
        this.currencyData = localStorage.getItem('currencyData');
        if (this.currencyData == undefined || this.currencyData == null) {
            this.currencyData = [];
            this.currencySymbol = '&euro;';
            this.currencyName = 'EUR';
        }
        else {
            this.currencyData = JSON.parse(this.currencyData);
            this.currencySymbol = this.currencyData.vSymbol;
            this.currencyName = this.currencyData.vName;
        }
    };
    ProductDetailsPage.prototype.gotoHome = function (params) {
        if (!params)
            params = {};
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */]);
    };
    ProductDetailsPage.prototype.presentAlert = function () {
        var alert = this.alertCtrl.create({
            title: 'Done',
            subTitle: 'Item has been added in cart successfully.',
            buttons: ['OK']
        });
        alert.present();
    };
    ProductDetailsPage.prototype.openCartPage = function () {
        //this.navCtrl.push(MyCartPage);
    };
    ProductDetailsPage.prototype.addQty = function () {
        this.productDetails.qty++;
    };
    ProductDetailsPage.prototype.subQty = function () {
        if (this.productDetails.qty > 1) {
            this.productDetails.qty--;
        }
        /*if (no == 1) {
            if(this.qty1 > 1){
                this.qty1--;
            }
        }*/
    };
    ProductDetailsPage.prototype.addProductToCart = function (product, type) {
        this.cartItems = localStorage.getItem('cartItems');
        if (this.cartItems == undefined || this.cartItems == null) {
            this.cartItems = [];
        }
        else {
            this.cartItems = JSON.parse(localStorage.getItem('cartItems'));
            this.cartItemsLen = this.cartItems.length;
        }
        if (this.productDetails.qty > 0) {
            if (this.cartItemsLen > 0) {
                var exist = this.cartItems.filter(function (cartItem) { return cartItem.product_id == product.product_id; });
                if (exist.length > 0) {
                    for (var i = 0; i < this.cartItems.length; ++i) {
                        if (this.cartItems[i].product_id == product.product_id) {
                            /*if(type == 'add') {
                                // this.cartItems[i].qty++;
                                // this.productDetails.qty = this.cartItems[i].qty;
                                this.cartItems[i].qty = this.productDetails.qty;
                                this.restProvider.showSuccessMsg('Success','Item added in cart successfully.');
                            }
                            else {
                                if(this.cartItems[i].qty > 1){
                                    // this.cartItems[i].qty--;
                                    // this.productDetails.qty = this.cartItems[i].qty;
                                    this.cartItems[i].qty = this.productDetails.qty;
                                    this.restProvider.showSuccessMsg('Success','Item quantity updated successfully.');
                                }
                                else {
                                    this.removeItem(this.productDetails.product_id);
                                }
                            }*/
                            this.cartItems[i].qty = parseInt(this.cartItems[i].qty) + parseInt(this.productDetails.qty);
                            this.restProvider.showSuccessMsg('Success', 'Item added in cart successfully.');
                        }
                    }
                    localStorage.setItem('cartItems', JSON.stringify(this.cartItems));
                }
                else {
                    product.qty = this.productDetails.qty;
                    if (this.productDetails.qty > 0) {
                        // this.productDetails.qty = 1;
                        this.cartItems.push(product);
                        this.cartItemsLen = this.cartItems.length;
                        localStorage.setItem('cartItems', JSON.stringify(this.cartItems));
                        this.restProvider.showSuccessMsg('Success', 'Item added in cart successfully.');
                    }
                }
            }
            else {
                product.qty = this.productDetails.qty;
                // this.productDetails.qty = 1;
                this.cartItems.push(product);
                this.cartItemsLen = this.cartItems.length;
                ;
                localStorage.setItem('cartItems', JSON.stringify(this.cartItems));
                this.restProvider.showSuccessMsg('Success', 'Item added in cart successfully.');
            }
        }
        this.cartItemsLen = this.cartItems.length;
    };
    ProductDetailsPage.prototype.removeItem = function (product_id) {
        var index = this.cartItems.map(function (e) { return e.product_id; }).indexOf('' + product_id);
        this.cartItems.splice(index, 1);
        localStorage.setItem('cartItems', JSON.stringify(this.cartItems));
        this.cartItemsLen = this.cartItems.length;
    };
    return ProductDetailsPage;
}());
ProductDetailsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-product-details',template:/*ion-inline-start:"D:\app\familov_app\src\pages\product-details\product-details.html"*/'<ion-header>\n\n	<ion-navbar>\n\n		<button ion-button menuToggle class="buttonWhiteFont">\n\n			<ion-icon name="menu"></ion-icon>\n\n		</button>\n\n		<ion-title>\n\n			<ion-grid>\n\n	    		<ion-row>\n\n	    			<ion-col col-10 class="headerTitle"><div align="center">Product Details</div></ion-col>\n\n	    			<ion-col col-2 class="badgeCol">\n\n	    				<!-- <ion-icon class="right" name="cart" item-end (click)="openCartPage()"></ion-icon> -->\n\n	    				<span class="badgeIcon"> {{ cartItemsLen }}</span>\n\n						<ion-icon name="cart" item-end (click)="openCartPage()"></ion-icon>\n\n	    			</ion-col>\n\n	    		</ion-row>\n\n	    	</ion-grid>\n\n		</ion-title>\n\n	</ion-navbar>\n\n</ion-header>\n\n<ion-content padding id="page5" class="grayBackground">\n\n	\n\n	<div class="spacer" style="height:30px;" id="login-spacer2"></div>\n\n\n\n   <!-- <ion-slides autoplay=\'2500\' loop=\'true\'>\n\n    <ion-slide>\n\n      <img src="assets/imgs/slide3.png">\n\n    </ion-slide>\n\n    <ion-slide>\n\n      <img src="assets/imgs/slide2.png">\n\n    </ion-slide>\n\n    <ion-slide>\n\n      <img src="assets/imgs/slide1.png">\n\n    </ion-slide>\n\n  </ion-slides> -->\n\n\n\n  <ion-card>\n\n  	<div align="center">\n\n  		<img src="{{ productDetails.product_image }}" style="max-height: 150px;width: auto;"  class="proImg" >\n\n  		<br>\n\n  		<p style="margin-top: 5vh;font-size: 20px;"><b>{{ productDetails.product_name }}</b></p>\n\n  		<p style="margin-top: 5vh;font-size: 18px;" *ngIf="productDetails.spec_price > 0">\n\n  			<small style="margin-right: 5vh; text-decoration: line-through; " >\n\n  				<span [innerHtml]="currencySymbol">{{ currencyName }}</span>\n\n  				{{ productDetails.convert_product_prices }}\n\n			</small>\n\n  			<b style="color: #32db64;" >\n\n  				<span [innerHtml]="currencySymbol">{{ currencyName }}</span>\n\n  				{{ productDetails.convert_special_product_prices }}\n\n  			</b>\n\n		</p>\n\n		<p style="margin-top: 5vh;font-size: 18px;" *ngIf="productDetails.spec_price == 0">\n\n			<b style="color: #32db64;" >\n\n				<span [innerHtml]="currencySymbol">{{ currencyName }}</span>\n\n				{{ productDetails.convert_product_prices }}\n\n			</b>\n\n		</p>\n\n\n\n  	</div>\n\n  </ion-card>\n\n\n\n	<div class="spacer" style="height:25px;" id="login-spacer4"></div>\n\n\n\n	<!-- <ion-card>\n\n		<ion-grid>\n\n			<ion-row class="ionRow">\n\n				<ion-col col-4>\n\n					<b>Name:</b>\n\n				</ion-col>\n\n				<ion-col col-8>\n\n					Product name\n\n				</ion-col>\n\n			</ion-row>\n\n\n\n			<ion-row class="ionRow">\n\n				<ion-col col-4>\n\n					<b>Details:</b>\n\n				</ion-col>\n\n				<ion-col col-8>\n\n					Product description\n\n				</ion-col>\n\n			</ion-row>\n\n\n\n			<ion-row class="ionRow">\n\n				<ion-col col-4>\n\n					<b>Price:</b>\n\n				</ion-col>\n\n				<ion-col col-8>\n\n					$100.00\n\n				</ion-col>\n\n			</ion-row>\n\n\n\n		</ion-grid>\n\n\n\n		<div align="center" style="margin: 1.5vh">\n\n			<button color="dark" ion-button (click)="presentAlert()" style="width: 25vh;">Add to cart</button>\n\n		</div>\n\n\n\n	</ion-card> -->\n\n\n\n	<!-- <ion-card style="padding: 3vh;"> -->\n\n		<div style="padding: 3vh;margin-bottom: 5vh;">\n\n			<h2 style="font-weight: bold;">\n\n				Description\n\n			</h2>\n\n			<p style="color: lightgrey;text-transform: none;" [innerHtml]="productDetails.product_desc"></p>\n\n		</div>\n\n	<!-- </ion-card> -->\n\n\n\n\n\n<div class="fixed-outside" style="position: fixed;bottom: 0;width: 100%;background: #FFF;" align="center">\n\n\n\n	<ion-grid style="padding: 0px;margin: 0px 5px;">\n\n		<ion-row>\n\n			<ion-col col-6>\n\n				<!-- <button color="secondary" ion-button>TEST</button> -->\n\n				<ion-grid style="margin-top: 1vh;">\n\n					<ion-row style="max-width: 20vh;">\n\n						<ion-col col-4 style="padding: 0px 5px;color: lightgray !important;">\n\n							<ion-icon style="color: lightgray !important;" (click)="subQty()" name="remove-circle" style="font-size: 2.1em;"></ion-icon>\n\n						</ion-col>\n\n						<ion-col col-4><!-- style="border: 1px solid;" style="padding: 0px 5px;" -->\n\n							<ion-label style="margin: 2px 0 0px 0;font-weight: bold;">{{ productDetails.qty }}</ion-label>\n\n							<!-- <ion-input type="number" name="qty1" [(ngModel)]="productDetails.qty" style="background: gray;max-width: 70%;color: #FFF;"></ion-input> -->\n\n						</ion-col>\n\n						<ion-col col-4 style="padding: 0px 5px;">\n\n							<ion-icon color="secondary" (click)="addQty()" name="add-circle" style="font-size: 2.1em;"></ion-icon>\n\n						</ion-col>\n\n					</ion-row>\n\n				</ion-grid>\n\n			</ion-col>\n\n			<ion-col col-6>\n\n				<button color="secondary" style="width: 90%;margin-top: 1vh;" ion-button (click)="addProductToCart(productDetails,\'add\')">Add to cart</button>\n\n			</ion-col>\n\n		</ion-row>\n\n	</ion-grid>\n\n</div>\n\n\n\n</ion-content>'/*ion-inline-end:"D:\app\familov_app\src\pages\product-details\product-details.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */]])
], ProductDetailsPage);

//# sourceMappingURL=product-details.js.map

/***/ }),

/***/ 206:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InviteFriendPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__faq_faq__ = __webpack_require__(55);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var InviteFriendPage = (function () {
    function InviteFriendPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.frequentQuestions = [
            {
                title: "What is Familov.com?",
                description: "Everything is in the name. With familov.com, order from abroad and we deliver your family to the country in less than 36 hours."
            },
            {
                title: "How can I create an account?",
                description: "Sur la page de démarrage de Familov, cliquez sur Inscrivez-vous (ou allez directement à la page Créer un compte). Remplissez les informations et votre mot de passe et confirmez-les une deuxième fois. Votre adresse de courriel sera directement liée au compte."
            },
            {
                title: "I forgot my password?",
                description: "Vous avez déjà créé un compte mais vous ne vous souvenez plus de votre mot de passe? Cliquez sur «Login» en haut de la page. Puis cliquez sur 'Mot de passe oublié?'. Remplissez votre adresse e-mail et un nouveau mot de passe vous sera envoyé par e-mail."
            },
            {
                title: "How long does a delivery take?",
                description: "En moyenne, nous avons besoin de moins de 36 heures à partir du moment où nous avons recu le payement jusqu'au moment de la livraison à votre proche. Le délai de livraison peut varier d'une boutique à l'autre et dépend du nombre de commandes que la boutique doit préparer et la disponibilité du bénéficiaire."
            },
            {
                title: "How much is shipping?",
                description: "Nous facturons un forfait de 2,95 € (2,50€ pendant le phase béta) pour le service et 0,00€ pour les livraison en magasin.En fonction de lieu de residence le coût des livraison à domicile varie."
            },
            {
                title: "What happens after placing my order?",
                description: "L'ordre arrive dans notre système et après que nous recevons le paiement, il est envoyé directement à la boutique (Shopper) que vous avez commandé. Après seulement quelques secondes, les magasins reçoivent l'ordre par le biais de notre propre système Shop-Connect.<br> <br>1- Le magasin recoit tous les détails de votre commande <br> 2- Votre proche recoit le code de retrait par SMS<br> 3-Le livreur prépare le paquet et recontacte le bénéficiaire dans les 36 heures pour la livraison <br>4-Vous recevez une confirmation par SMS ou e-mail que votre commande est bien livrée. <br>NB: Nous travaillons avec partenaires n et des livreurs proffessionnels pour gerer vos commanades. Ne vous inquiétez pas, ils ont été formés individuellement pes sont formés pour donner la meilleure qualité , soigner vos articles et les livrer."
            },
            {
                title: "Where can I see my receipts?",
                description: "Vous pouvez afficher vos reçus de deux façons: Email: Lorsque votre commande est passée, vous recevrez un courriel avec un reçu. <br> <br> Sur le site Web cliquez sur votre nom dans le coin supérieur droit et un menu apparaîtra. Sélectionnez Mes commandes. Cliquez sur un numéro de commande pour afficher un reçu détaillé de cet ordre."
            },
            {
                title: "How is my order confirmed?",
                description: "Une fois votre commande passée, vous recevrez un e-mail de confirmation envoyé à l'adresse e-mail que vous avez saisie lors de votre commande. Conservez ce courriel et votre numéro de commande pour toute question ou remarque concernant votre commande. Lorsque vous recevez l'e-mail de confirmation, Familov fait le reste. Il suffit de s'asseoir, de se détendre et de profiter!"
            },
            {
                title: "I have not received a confirmation email",
                description: "Si un numéro de commande s'affiche à l'écran, cela signifie que votre commande a été envoyée. L'e-mail de confirmation peut être dans votre «spam» plutôt que dans votre boîte de réception. Si vous ne recevez pas de numéro de commande, veuillez contacter notre service à la clientèle pour connaître l'état de votre commande. Vous devez fournir l'adresse e-mail que vous avez saisie lors de la commande et le nom de la boutique sélectionnée afin que nous puissions trouver votre commande."
            }
        ];
        this.paymentQuestions = [
            {
                title: "What types of payment can I use?",
                description: "Carte de crédit (VISA / American Express / Master Card /)<br>Pay Pal <br> Virement"
            },
            {
                title: "How secure are online payments?",
                description: "Lors du traitement des paiements en ligne sur Familov.com, nous utilisons des pages SSL sécurisées. Cela signifie que ces pages assurent la protection de vos données personnelles et de paiement. De cette façon, vous pouvez être sûr que cette information est seulement visible pour vous et pour nous, pour le traitement de votre commande."
            },
            {
                title: "Why do I have to pay transaction fees?",
                description: "Familov.com est seulement un intermédiaire, entre vous en tant que consommateur et la Boutique. Vu le faible revenu que nous gagnons sur chaque commande, nous devons facturer pour l'utilisation d'une méthode de paiement en ligne. Les coûts couvrent l'entretien technique des paiements en ligne, les coûts de transaction que les banques facturent et nos frais administratifs pour le traitement des paiements. Tous les sites facturent ces coûts, mais ils les ajoutent souvent directement aux frais d'envoi et / ou d'administration.. Nous vous remercions pour votre compréhension."
            },
            {
                title: "My online payment failed",
                description: "Si vous pensez que le paiement en ligne a échoué, vérifiez toujours si vous avez reçu un e-mail de confirmation. Cet e-mail vous permettra de voir si votre commande a été soumise et si la commande est déjà payée ou non. <br><br> L'e-mail de confirmation est envoyé directement une fois le paiement terminé. Cela peut prendre un peu plus longtemps si le paiement est traité par la banque, la société de carte de crédit ou toute autre société de paiement. Cela peut prendre jusqu'à 15 minutes. Si après 15 minutes vous n'avez toujours pas de courriel de confirmation dans votre boîte de réception, contactez notre service à la clientèle. Ils vous diront si l'ordre et le paiement ont réussi."
            },
            {
                title: "I still have a lot of questions to ask you?",
                description: "Contactez nous! Nous serons là à :<br><br> Email:hello@familov.com <br><br> Whatsapp : (+49 1525 9948834)<br><br> Chat : www.familov.com"
            }
        ];
        this.shownGroup = null;
        // If we navigated to this page, we will have an item available as a nav param
        this.selectedItem = navParams.get('item');
        // Let's populate this page with some filler content for funzies
        this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
            'american-football', 'boat', 'bluetooth', 'build'];
        this.items = [];
        for (var i = 1; i < 11; i++) {
            this.items.push({
                title: 'Item ' + i,
                note: 'This is item #' + i,
                icon: this.icons[Math.floor(Math.random() * this.icons.length)]
            });
        }
    }
    InviteFriendPage.prototype.toggleGroup = function (group) {
        if (this.isGroupShown(group)) {
            this.shownGroup = null;
        }
        else {
            this.shownGroup = group;
        }
    };
    ;
    InviteFriendPage.prototype.isGroupShown = function (group) {
        return this.shownGroup === group;
    };
    ;
    InviteFriendPage.prototype.itemTapped = function (event, item) {
        // That's right, we're pushing to ourselves!
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__faq_faq__["a" /* FaqPage */], {
            item: item
        });
    };
    return InviteFriendPage;
}());
InviteFriendPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-invite-friend',template:/*ion-inline-start:"D:\app\familov_app\src\pages\invite-friend\invite-friend.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Invite Your Friends</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n<h3 align="center" style="margin: 5vh auto;">Invite your friend to earn money.</h3>\n\n\n\n  <!-- <button ion-button color="dark">Dark</button> -->\n\n\n\n  <!-- <ion-item> -->\n\n      <ion-input style="display: none;" value="http://103.21.58.248/familov/sign-up/referral58ef1d0f3f0d56.33885431" disabled></ion-input>\n\n      <p align="center" style="background: lightgray;margin: 1vh;padding: 1vh;word-break: break-all;">http://103.21.58.248/familov/sign-up/referral58ef1d0f3f0d56.33885431</p>\n\n  <!-- </ion-item> -->\n\n\n\n  <div align="center">\n\n      <button ion-button color="secondary">Copy the link</button>\n\n  </div>\n\n\n\n  <h3 align="center" style="margin: 5vh auto;">Or click to invite friends on Facebook</h3>\n\n\n\n  <div align="center">\n\n      <button ion-button color="secondary">Facebook</button>\n\n  </div>\n\n\n\n  <ion-grid style="margin: 2vh auto;">\n\n      <div style="padding: 2vh;background: #EEE;" align="center">\n\n        <ion-icon style="font-size: 3em;" name="cash"></ion-icon>\n\n        <p>Your Win</p>\n\n        <button ion-button round color="secondary">0.00</button>\n\n      </div>\n\n  </ion-grid>\n\n\n\n  <ion-grid style="margin: 2vh auto;">\n\n        <div style="padding: 2vh;background: #EEE;" align="center">\n\n          <ion-icon style="font-size: 3em;" name="checkmark-circle-outline"></ion-icon>\n\n          <p>You have used always</p>\n\n          <button ion-button round color="secondary">0.00</button>\n\n        </div>\n\n  </ion-grid>\n\n\n\n  <ion-grid style="margin: 2vh auto;">\n\n        <div style="padding: 2vh;background: #EEE;" align="center">\n\n          <ion-icon style="font-size: 3em;" name="logo-usd"></ion-icon>\n\n          <p>Available for checkout</p>\n\n          <button ion-button round color="secondary">0.00</button>\n\n        </div>\n\n  </ion-grid>\n\n\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"D:\app\familov_app\src\pages\invite-friend\invite-friend.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
], InviteFriendPage);

//# sourceMappingURL=invite-friend.js.map

/***/ }),

/***/ 207:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ForgotPasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__my_cart_my_cart__ = __webpack_require__(56);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ForgotPasswordPage = (function () {
    function ForgotPasswordPage(navCtrl) {
        this.navCtrl = navCtrl;
        this.cartItemsLen = 0;
        this.cartItems = {};
        this.cartItems = localStorage.getItem('cartItems');
        if (this.cartItems != undefined || this.cartItems != null) {
            this.cartItemsLen = JSON.parse(this.cartItems).length;
        }
    }
    ForgotPasswordPage.prototype.goToLogin = function (params) {
        if (!params)
            params = {};
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__login_login__["a" /* LoginPage */]);
    };
    ForgotPasswordPage.prototype.openCartPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__my_cart_my_cart__["a" /* MyCartPage */]);
    };
    return ForgotPasswordPage;
}());
ForgotPasswordPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-forgot-password',template:/*ion-inline-start:"D:\app\familov_app\src\pages\forgot-password\forgot-password.html"*/'<ion-header>\n\n	<ion-navbar>\n\n		<button ion-button menuToggle class="buttonWhiteFont">\n\n			<ion-icon name="menu"></ion-icon>\n\n		</button>\n\n		<ion-title>\n\n			<ion-grid>\n\n    		<ion-row>\n\n    			<ion-col col-10 class="headerTitle">Forgot Password</ion-col>\n\n    			<ion-col col-2 class="badgeCol">\n\n					<!-- <ion-icon class="right" name="cart" item-end (click)="openCartPage()"></ion-icon> -->\n\n					<span class="badgeIcon"> {{ cartItemsLen }}</span>\n\n					<ion-icon name="cart" item-end (click)="openCartPage()"></ion-icon>\n\n    			</ion-col>\n\n    		</ion-row>\n\n    	</ion-grid>\n\n		</ion-title>\n\n	</ion-navbar>\n\n</ion-header>\n\n<ion-content padding id="page5">\n\n	\n\n	<div class="spacer" style="height:20px;" id="login-spacer1"></div>\n\n	<h2 align="center">Enter your registered Email and we will email you a link to reset your password.</h2>\n\n	<div class="spacer" style="height:20px;" id="login-spacer2"></div>\n\n\n\n	<form id="login-form1">\n\n		<ion-list id="login-list1">\n\n			<ion-item id="login-input1">\n\n				<ion-label floating>\n\n					Email\n\n				</ion-label>\n\n				<ion-input type="email" placeholder=""></ion-input>\n\n			</ion-item>\n\n		</ion-list>\n\n\n\n		<button id="login-button1" ion-button color="secondary" block>\n\n			Confirm\n\n		</button>\n\n		\n\n		<button id="login-button2" ion-button clear color="positive" block (click)="goToLogin()">\n\n			Click here to Login\n\n		</button>\n\n\n\n	</form>\n\n</ion-content>'/*ion-inline-end:"D:\app\familov_app\src\pages\forgot-password\forgot-password.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]])
], ForgotPasswordPage);

//# sourceMappingURL=forgot-password.js.map

/***/ }),

/***/ 208:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(226);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 21:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RestProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(279);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_toPromise__ = __webpack_require__(280);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_toPromise__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var RestProvider = (function () {
    function RestProvider(http, alertCtrl, loadingCtrl, toastCtrl) {
        this.http = http;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.statusString = '';
        this.statusSuccess = 'Success';
        this.statusError = 'Error!';
        this.statusErrorMessage = '<br>Something went wrong!';
        this.statusErrorMessageAPI = '<b>Something went wrong! Kindly contact to administrator.</b>';
        // console.log('Hello RestProvider Provider');
        //this.apiUrl = 'http://103.21.58.248/ci_familov/api/';
        this.apiUrl = 'http://localhost/ci_familov/api/';
        this.apiToken = '123456789'; //localStorage.getItem('apiToken');
        this.loading = __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* Loading */];
        // alert(localStorage.getItem('apiToken'));
    }
    RestProvider.prototype.showToast = function (msg, type) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'top',
            showCloseButton: true,
            dismissOnPageChange: true,
            cssClass: type
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    RestProvider.prototype.showSuccessMsg = function (title, subTitle) {
        var alert = this.alertCtrl.create({
            title: title,
            subTitle: subTitle,
            buttons: ['Done']
        });
        alert.present();
    };
    RestProvider.prototype.showErrorMsg = function (title, subTitle) {
        var alert = this.alertCtrl.create({
            title: title,
            subTitle: subTitle,
            cssClass: 'text-red',
            buttons: ['Ok']
        });
        alert.present();
    };
    RestProvider.prototype.showWarningMsg = function (title, subTitle) {
        var alert = this.alertCtrl.create({
            title: title,
            subTitle: subTitle,
            cssClass: 'text-red',
            buttons: ['Close']
        });
        alert.present();
    };
    RestProvider.prototype.showConfirmationAlert = function (title, message, cancelText, confirmText) {
        var alert = this.alertCtrl.create({
            title: title,
            message: message,
            buttons: [
                {
                    text: cancelText,
                    role: 'cancel',
                    handler: function () {
                        return false; // console.log('Cancel clicked');
                    }
                },
                {
                    text: confirmText,
                    handler: function () {
                        return true; // console.log('Buy clicked');
                    }
                }
            ]
        });
        alert.present();
    };
    RestProvider.prototype.errorString = function (msg, errString) {
        if (errString != '') {
            return "<br>" + msg;
        }
        return msg;
    };
    RestProvider.prototype.LoadingSpinner = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        this.loading.present();
        /*setTimeout(() => {
         this.loading.dismiss();
         }, 1000);*/
    };
    RestProvider.prototype.hideLoadingSpinner = function () {
        if (this.loading != null) {
            this.loading.dismiss();
            this.loading = null;
        }
    };
    RestProvider.prototype.getToken = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.http.get(_this.apiUrl + 'token/token')
                .subscribe(function (data) {
                _this.apiToken = '112121212'; //data.key;
                // alert(this.apiToken);
                resolve(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    RestProvider.prototype.getCurrency = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.http.get(_this.apiUrl + 'home/get_currency')
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                //console.log(err);
                _this.showErrorMsg(_this.statusError, _this.statusErrorMessageAPI); //TODO-FORCE_FULL CLOSE
            });
        });
    };
    RestProvider.prototype.signupUser = function (data) {
        var _this = this;
        var body = new FormData();
        body.append('first_name', data.first_name);
        body.append('last_name', data.last_name);
        body.append('email', data.email);
        body.append('phone_code', data.phone_code);
        body.append('phone_number', data.phone_number);
        body.append('password', data.password);
        body.append('repeat_password', data.repeat_password);
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + 'auth/sign_up', body, {}).subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
                _this.showErrorMsg(_this.statusError, _this.statusErrorMessageAPI);
            });
        });
    };
    RestProvider.prototype.loginUser = function (data) {
        var _this = this;
        var body = new FormData();
        body.append('email', data.email);
        body.append('password', data.password);
        return new Promise(function (resolve, reject) {
            /*var headers = new Headers();
             headers.append('Content-Type', 'application/json');*/
            _this.http.post(_this.apiUrl + 'auth/login', body, {}).subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
                _this.showErrorMsg(_this.statusError, _this.statusErrorMessageAPI);
            });
        });
    };
    RestProvider.prototype.getHomeData = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            //headers.append('authorization', 'Bearer ' + this.apiToken);
            //this.getHeaders = 'authorization=Bearer '+localStorage.getItem('apiToken');
            setTimeout(function () {
                _this.http.get(_this.apiUrl + 'home/home', {}).subscribe(function (res) {
                    resolve(res);
                    //console.log(res);
                }, function (err) {
                    reject(err);
                    //console.log(err);
                    _this.showErrorMsg(_this.statusError, _this.statusErrorMessageAPI); //TODO-FORCE_FULL CLOSE
                });
            }, 200);
        });
    };
    RestProvider.prototype.getCities = function (country_id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            /*var headers = new Headers();
             headers.append('Content-Type', 'application/json');*/
            //headers.append('authorization', 'Bearer ' + this.apiToken);
            // var headers = new HttpHeaders()
            // 				.set('authorization','Bearer ' + this.apiToken)
            // 				.set('Access-Control-Allow-Methods','POST')
            _this.http.get(_this.apiUrl + 'home/city?country_id=' + country_id, {})
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
                _this.showErrorMsg(_this.statusError, _this.statusErrorMessageAPI);
            });
        });
    };
    RestProvider.prototype.getShops = function (city_id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Headers */]();
            headers.append('Content-Type', 'application/json');
            //headers.append('authorization', 'Bearer ' + this.apiToken);
            // var headers = new HttpHeaders()
            // 				.set('authorization','Bearer ' + this.apiToken)
            // 				.set('Access-Control-Allow-Methods','POST')
            _this.http.get(_this.apiUrl + 'home/shop?city_id=' + city_id, {})
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
                _this.showErrorMsg(_this.statusError, _this.statusErrorMessageAPI);
            });
        });
    };
    RestProvider.prototype.getProducts = function (searchData) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var link = '&country_id=' + searchData.country_id + '&city_id=' + searchData.city_id + '&shop_id=' + searchData.shop_id + '&currency=' + searchData.currency + '&page=' + searchData.page;
            if (searchData.category_id != null && searchData.category_id != undefined && searchData.category_id > 0) {
                link += '&category_id=' + searchData.category_id;
            }
            if (searchData.searchText != null && searchData.searchText != undefined && searchData.searchText != '') {
                link += '&search_keyword=' + searchData.searchText;
            }
            _this.http.get(_this.apiUrl + 'product/product_list?' + link, {}).subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
                _this.showErrorMsg(_this.statusError, _this.statusErrorMessageAPI);
            });
        });
    };
    RestProvider.prototype.changePassword = function (userData) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var body = new FormData();
            for (var i in userData) {
                body.append(i, userData[i]);
            }
            //body.append('authorization', 'Bearer ' + this.apiToken);
            var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Headers */]();
            headers.append('Content-Type', 'application/json');
            //headers.append('authorization', 'Bearer ' + this.apiToken);
            // var headers = new HttpHeaders()
            // 				.set('authorization','Bearer ' + this.apiToken)
            // 				.set('Access-Control-Allow-Methods','POST')
            _this.http.post(_this.apiUrl + 'user/change_password', body, {})
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
                _this.showErrorMsg(_this.statusError, _this.statusErrorMessageAPI);
            });
        });
    };
    /*getOrdersList(customer_id) {
     // customer_id = 129;
     return new Promise(
     resolve => {
     this.http.get(this.apiUrl+'product/order_list?'+this.getHeaders+'&customer_id='+customer_id)
     .subscribe(
     data => {
     resolve(data);
     },
     err => {
     console.log(err);
     }
     );
     }
     );
     }

     getOrderDetails(customer_id,order_id) {
     // customer_id = 129;
     // order_id = 693;
     return new Promise(
     resolve => {
     this.http.get(this.apiUrl+'product/order_detail_by_id?'+this.getHeaders+'&customer_id='+customer_id+'&order_id='+order_id)
     .subscribe(
     data => {
     resolve(data);
     },
     err => {
     console.log(err);
     }
     );
     }
     );
     }

     updateProfile(profileData) {
     return new Promise(
     (resolve, reject) => {

     var body = new FormData();
     for (var i in profileData) {
     body.append(i,profileData[i]);
     }
     body.append('authorization', 'Bearer ' + this.apiToken);

     var headers = new Headers();
     headers.append('Content-Type', 'application/json');
     headers.append('authorization', 'Bearer ' + this.apiToken);
     // var headers = new HttpHeaders()
     // 				.set('authorization','Bearer ' + this.apiToken)
     // 				.set('Access-Control-Allow-Methods','POST')

     this.http.post(
     this.apiUrl + 'user/profile_update',
     body,
     {
     //headers: headers
     }
     )
     // .map(res => res.json())
     .subscribe(res => {
     resolve(res);
     }, (err) => {
     reject(err);
     });
     }
     );
     }
     */
    RestProvider.prototype.getUserData = function (customer_id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get(_this.apiUrl + 'user/profile?customer_id=' + customer_id)
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                reject(err);
                _this.showErrorMsg(_this.statusError, _this.statusErrorMessageAPI);
            });
        });
    };
    RestProvider.prototype.applyPromocode = function (data) {
        var _this = this;
        var body = new FormData();
        body.append('promo_code', data.promo_code);
        body.append('cart_amount_total', data.cart_amount_total);
        body.append('currency', data.currency);
        body.append('authorization', "Bearer " + this.apiToken);
        return new Promise(function (resolve, reject) {
            var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Headers */]();
            headers.append('Content-Type', 'application/json');
            _this.http.post(_this.apiUrl + 'checkout/order_promo_code', body, {})
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    /*getShopDeliveryOptions(shop_id, currency) {
     return new Promise(
     resolve => {
     this.http.get(this.apiUrl+'checkout/shop_delivery_option?'+this.getHeaders+'&shop_id='+shop_id+'&currency='+currency)
     .subscribe(
     data => {
     resolve(data);
     },
     err => {
     console.log(err);
     }
     );
     }
     );
     }*/
    RestProvider.prototype.getConfigData = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            setTimeout(function () {
                _this.http.get(_this.apiUrl + 'home/config', //+this.getHeaders,
                {})
                    .subscribe(function (res) {
                    resolve(res);
                }, function (err) {
                    reject(err);
                    _this.showErrorMsg(_this.statusError, _this.statusErrorMessageAPI);
                });
            }, 200);
        });
    };
    return RestProvider;
}());
RestProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* ToastController */]])
], RestProvider);

//# sourceMappingURL=rest.js.map

/***/ }),

/***/ 226:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(266);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_about_us_about_us__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_change_password_change_password__ = __webpack_require__(282);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_faq_faq__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_forgot_password_forgot_password__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_home_home__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_invite_friend_invite_friend__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_login_login__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_signup_signup__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_terms_terms__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_products_products__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_product_details_product_details__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_my_cart_my_cart__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_status_bar__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_native_splash_screen__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__angular_common_http__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__providers_rest_rest__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_native_paypal__ = __webpack_require__(283);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__ionic_native_stripe__ = __webpack_require__(284);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




// import { HomePage } from '../pages/home/home';
// import { ListPage } from '../pages/list/list';
/*
import { HomePage } from '../pages/home/home';

import { MyOrdersPage } from '../pages/my-orders/my-orders';
import { MyProfilePage } from '../pages/my-profile/my-profile';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { ChangePasswordPage } from '../pages/change-password/change-password';

import { PaymentPage } from '../pages/payment/payment';
import { ThankYouPage } from '../pages/thankyou/thankyou';

import { CheckoutPage } from '../pages/checkout/checkout';
import { ConfirmOrderPage } from '../pages/confirm-order/confirm-order';
import { OrderDetailsPage } from '../pages/order-details/order-details';

import { FaqPage } from '../pages/faq/faq';
import { InviteFriendPage } from '../pages/invite-friend/invite-friend';
import { TermsPage } from '../pages/terms/terms';
import { AboutUsPage } from '../pages/about-us/about-us';
*/


















var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* FamiLov */],
            __WEBPACK_IMPORTED_MODULE_8__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_13__pages_products_products__["a" /* ProductsPage */],
            __WEBPACK_IMPORTED_MODULE_15__pages_my_cart_my_cart__["a" /* MyCartPage */],
            /*CheckoutPage,
            ConfirmOrderPage,
            MyOrdersPage,
            MyProfilePage,*/
            __WEBPACK_IMPORTED_MODULE_10__pages_login_login__["a" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_11__pages_signup_signup__["a" /* SignupPage */],
            __WEBPACK_IMPORTED_MODULE_7__pages_forgot_password_forgot_password__["a" /* ForgotPasswordPage */],
            __WEBPACK_IMPORTED_MODULE_5__pages_change_password_change_password__["a" /* ChangePasswordPage */],
            /*OrderDetailsPage,
            PaymentPage,
            ThankYouPage,*/
            __WEBPACK_IMPORTED_MODULE_14__pages_product_details_product_details__["a" /* ProductDetailsPage */],
            __WEBPACK_IMPORTED_MODULE_6__pages_faq_faq__["a" /* FaqPage */],
            __WEBPACK_IMPORTED_MODULE_9__pages_invite_friend_invite_friend__["a" /* InviteFriendPage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_terms_terms__["a" /* TermsPage */],
            __WEBPACK_IMPORTED_MODULE_4__pages_about_us_about_us__["a" /* AboutUsPage */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_18__angular_common_http__["b" /* HttpClientModule */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* FamiLov */], {}, {
                links: []
            }),
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* FamiLov */],
            __WEBPACK_IMPORTED_MODULE_8__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_13__pages_products_products__["a" /* ProductsPage */],
            __WEBPACK_IMPORTED_MODULE_15__pages_my_cart_my_cart__["a" /* MyCartPage */],
            /*CheckoutPage,
            ConfirmOrderPage,
            MyOrdersPage,
            MyProfilePage,*/
            __WEBPACK_IMPORTED_MODULE_10__pages_login_login__["a" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_11__pages_signup_signup__["a" /* SignupPage */],
            __WEBPACK_IMPORTED_MODULE_7__pages_forgot_password_forgot_password__["a" /* ForgotPasswordPage */],
            __WEBPACK_IMPORTED_MODULE_5__pages_change_password_change_password__["a" /* ChangePasswordPage */],
            /*OrderDetailsPage,
            PaymentPage,
            ThankYouPage,*/
            __WEBPACK_IMPORTED_MODULE_14__pages_product_details_product_details__["a" /* ProductDetailsPage */],
            __WEBPACK_IMPORTED_MODULE_6__pages_faq_faq__["a" /* FaqPage */],
            __WEBPACK_IMPORTED_MODULE_9__pages_invite_friend_invite_friend__["a" /* InviteFriendPage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_terms_terms__["a" /* TermsPage */],
            __WEBPACK_IMPORTED_MODULE_4__pages_about_us_about_us__["a" /* AboutUsPage */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_16__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_17__ionic_native_splash_screen__["a" /* SplashScreen */],
            { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["v" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] },
            __WEBPACK_IMPORTED_MODULE_19__providers_rest_rest__["a" /* RestProvider */],
            __WEBPACK_IMPORTED_MODULE_20__ionic_native_paypal__["a" /* PayPal */],
            __WEBPACK_IMPORTED_MODULE_21__ionic_native_stripe__["a" /* Stripe */]
        ]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 266:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FamiLov; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_rest_rest__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_about_us_about_us__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_faq_faq__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_home_home__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_invite_friend_invite_friend__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_login_login__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_signup_signup__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_terms_terms__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_my_cart_my_cart__ = __webpack_require__(56);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






//import { MyOrdersPage } from '../pages/my-orders/my-orders';
//import { MyProfilePage } from '../pages/my-profile/my-profile';
//import { PaymentPage } from '../pages/payment/payment';
//import { ThankYouPage } from '../pages/thankyou/thankyou';
//import { CheckoutPage } from '../pages/checkout/checkout';
//import { ConfirmOrderPage } from '../pages/confirm-order/confirm-order';
//import { OrderDetailsPage } from '../pages/order-details/order-details';








//import * as Constants from '../util/constants';
var FamiLov = (function () {
    function FamiLov(platform, statusBar, splashScreen, restProvider, toastCtrl, alertCtrl) {
        var _this = this;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.restProvider = restProvider;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */];
        this.cartItemsLen = 0;
        this.initializeApp();
        /*this.restProvider.LoadingSpinner();
         setTimeout( () => {
         alert('1234');
         this.restProvider.hideLoadingSpinner();
         },300);*/
        /*this.platform.registerBackButtonAction(
         () => {
         if(this.nav.canGoBack()) {
         this.nav.pop();
         }
         else {
         if(this.alert){
         this.alert.dismiss();
         this.alert =null;
         }
         else{
         this.showAlert();
         }
         }
         });*/
        // used for an example of ngFor and navigation
        /*this.apiToken = localStorage.getItem('apiToken');
         if (this.apiToken == undefined || this.apiToken == null) {
         this.getToken();
         }*/
        this.getCurrency();
        this.cartItems = localStorage.getItem('cartItems');
        if (this.cartItems != undefined || this.cartItems != null) {
            this.cartItemsLen = JSON.parse(this.cartItems).length;
        }
        this.LoginUser = localStorage.getItem('LoginUser');
        if (this.LoginUser == undefined || this.LoginUser == null) {
            this.IsUserLogin = false;
            this.pages = [
                { title: 'Home', component: __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */], icon: 'home' },
                { title: 'Shopping Cart', component: __WEBPACK_IMPORTED_MODULE_12__pages_my_cart_my_cart__["a" /* MyCartPage */], icon: 'cart' },
                { title: 'Need help?', component: __WEBPACK_IMPORTED_MODULE_6__pages_faq_faq__["a" /* FaqPage */], icon: 'help-circle' },
                { title: 'Terms of services', component: __WEBPACK_IMPORTED_MODULE_11__pages_terms_terms__["a" /* TermsPage */], icon: 'warning' },
                { title: 'About', component: __WEBPACK_IMPORTED_MODULE_5__pages_about_us_about_us__["a" /* AboutUsPage */], icon: 'information-circle' }
            ];
        }
        else {
            this.IsUserLogin = true;
            this.LoginUser = JSON.parse(this.LoginUser);
            this.pages = [
                { title: 'Home', component: __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */], icon: 'home' },
                { title: 'Shopping Cart', component: __WEBPACK_IMPORTED_MODULE_12__pages_my_cart_my_cart__["a" /* MyCartPage */], icon: 'cart' },
                /* { title: 'My Profile',    component: MyProfilePage, icon: 'person' },
               { title: 'Orders History',  component: MyOrdersPage,  icon: 'time' },*/
                { title: 'Earn money', component: __WEBPACK_IMPORTED_MODULE_8__pages_invite_friend_invite_friend__["a" /* InviteFriendPage */], icon: 'trophy' },
                { title: 'Need help?', component: __WEBPACK_IMPORTED_MODULE_6__pages_faq_faq__["a" /* FaqPage */], icon: 'help-circle' },
                { title: 'Terms of services', component: __WEBPACK_IMPORTED_MODULE_11__pages_terms_terms__["a" /* TermsPage */], icon: 'warning' },
                { title: 'About', component: __WEBPACK_IMPORTED_MODULE_5__pages_about_us_about_us__["a" /* AboutUsPage */], icon: 'information-circle' }
            ];
        }
        setTimeout(function () {
            _this.currencyData = localStorage.getItem('currencyData');
        }, 1000);
    }
    FamiLov.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    FamiLov.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        //TODO-VIX for menu
        console.log("333");
        this.nav.setRoot(page.component);
        /*if (page.component == MyProfilePage) {
          this.nav.push(page.component);
        }
        else {
          this.nav.setRoot(page.component);
        }*/
    };
    FamiLov.prototype.getToken = function () {
        this.restProvider.getToken().then(function (data) {
            // console.log(data);
            // alert();
            var token = '123456789'; //data.key;
            // console.log(token);
            localStorage.setItem('apiToken', token);
        });
    };
    FamiLov.prototype.getCurrency = function () {
        this.restProvider.getCurrency().then(function (data) {
            //console.table(data);
            //console.log("============");
            //console.log(data['response_data']['currency_data'].iCurrencyId);
            //console.log("******============");
            //console.log(data['response_data'].currency_data);
            //console.log("******============");
            //console.log(data.response_data.currency_data);
            localStorage.setItem('currencyData', JSON.stringify(data['response_data']['currency_data']));
            //}
        });
    };
    FamiLov.prototype.openLogin = function () {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_9__pages_login_login__["a" /* LoginPage */]);
    };
    FamiLov.prototype.openSignup = function () {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_10__pages_signup_signup__["a" /* SignupPage */]);
    };
    FamiLov.prototype.logoutUser = function () {
        localStorage.removeItem('LoginUser');
        // localStorage.removeItem('cartItems');
        location.reload();
    };
    FamiLov.prototype.showAlert = function () {
        var _this = this;
        this.alert = this.alertCtrl.create({
            title: 'Exit?',
            message: 'Do you want to exit the app?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                        _this.alert = null;
                    }
                },
                {
                    text: 'Exit',
                    handler: function () {
                        _this.platform.exitApp();
                    }
                }
            ]
        });
        //alert.present();
        //toast.present();
    };
    FamiLov.prototype.showToast = function () {
        var toast = this.toastCtrl.create({
            message: 'Press Again to exit',
            duration: 2000,
            position: 'bottom'
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    return FamiLov;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Nav */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Nav */])
], FamiLov.prototype, "nav", void 0);
FamiLov = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"D:\app\familov_app\src\app\app.html"*/'\n\n<ion-menu [content]="content">\n\n	<!-- <ion-header>\n\n		<ion-toolbar>\n\n			<ion-title>Menu</ion-title>\n\n		</ion-toolbar>\n\n	</ion-header> -->\n\n\n\n	<ion-content>\n\n\n\n		<div class="grayBackground" *ngIf="IsUserLogin">\n\n			<div class="spacer" style="width:268px;height:20px;" id="famiLov-spacer4"></div>\n\n			<div>\n\n				<img src="assets/imgs/avatar.ico" style="display:block;width:130px;height:130px;margin-left:auto;margin-right:auto;border-radius: 50%;border: 1px solid #FFF;" />\n\n			</div>\n\n			<h3 align="center">{{ LoginUser.username }} {{ LoginUser.last_name }}</h3>\n\n			<div class="spacer" style="width:268px;height:10px;" id="famiLov-spacer5"></div>\n\n		</div>\n\n\n\n		<div class="grayBackground" align="center" *ngIf="!IsUserLogin">\n\n			<div class="spacer" style="width:268px;height:20px;" id="famiLov-spacer4"></div>\n\n			<img src="assets/imgs/attention (1).png" style="display:block;width:55px;height:55px;margin-left:auto;margin-right:auto;" />\n\n			<h3 align="center">Please login or signup</h3>\n\n			<ion-grid>\n\n				<ion-row>\n\n					<ion-col col-6 style="padding: 3px;">\n\n						<button class="signupBtn" ion-button menuToggle (click)="openSignup()">\n\n						Sign up\n\n						</button>\n\n					</ion-col>\n\n					<ion-col col-6 style="padding: 3px;">\n\n						<button style="width: 100%;height: 2.3em;text-transform: none;font-size: 20px;font-weight: bold; " ion-button color="secondary" menuToggle (click)="openLogin()">\n\n						Login\n\n						</button>\n\n					</ion-col>\n\n				</ion-row>\n\n			</ion-grid>\n\n		</div>\n\n\n\n		<ion-list>\n\n			<ion-item color="none" menuClose *ngFor="let p of pages" (click)="openPage(p)" id="famiLov-menu-{{p.title}}">\n\n				{{p.title}}\n\n				<ion-icon name="{{p.icon}}" color="secondary" item-start></ion-icon>\n\n			</ion-item>\n\n		</ion-list>\n\n		<br>\n\n		<div class="fixed-outside" style="position: fixed;bottom: 0;left: 0;width: 100%;background: #FFF;" align="center">\n\n			<button *ngIf="IsUserLogin" class="button-md-height" color="secondary" ion-button block style="margin-bottom: 0px;" (click)="logoutUser()">Logout</button>\n\n			<ion-label *ngIf="!IsUserLogin" class="button-md-height" style="margin-bottom: 1vh;">2017@FamiLov</ion-label>\n\n		</div>\n\n\n\n	</ion-content>\n\n\n\n</ion-menu>\n\n\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"D:\app\familov_app\src\app\app.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
        __WEBPACK_IMPORTED_MODULE_4__providers_rest_rest__["a" /* RestProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
], FamiLov);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 282:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChangePasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*import { MyProfilePage } from '../my-profile/my-profile';
import { MyCartPage } from '../my-cart/my-cart';*/
var ChangePasswordPage = (function () {
    function ChangePasswordPage(navCtrl, restProvider) {
        this.navCtrl = navCtrl;
        this.restProvider = restProvider;
        this.changePass = {};
        this.cartItems = {};
        this.cartItemsLen = 0;
        this.LoginUser = JSON.parse(localStorage.getItem('LoginUser'));
        // console.log(this.LoginUser.customer_id);
        this.apiSuccessMsg = 'Success';
        this.apiErrorMsg = 'Error';
        this.cartItems = localStorage.getItem('cartItems');
        if (this.cartItems != undefined || this.cartItems != null) {
            this.cartItemsLen = JSON.parse(this.cartItems).length;
        }
    }
    return ChangePasswordPage;
}());
ChangePasswordPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-change-password',template:/*ion-inline-start:"D:\app\familov_app\src\pages\change-password\change-password.html"*/'<ion-header>\n\n	<ion-navbar>\n\n		<button ion-button menuToggle class="buttonWhiteFont">\n\n			<ion-icon name="menu"></ion-icon>\n\n		</button>\n\n		<ion-title>\n\n			<!-- Change Password -->\n\n			<ion-grid>\n\n	    		<ion-row>\n\n	    			<ion-col col-10 class="headerTitle">Change Password</ion-col>\n\n	    			<ion-col col-2 class="badgeCol">\n\n						<!-- <ion-icon class="right" name="cart" item-end (click)="openCartPage()"></ion-icon> -->\n\n						<span class="badgeIcon"> {{ cartItemsLen }}</span>\n\n						<ion-icon name="cart" item-end (click)="openCartPage()"></ion-icon>\n\n	    			</ion-col>\n\n	    		</ion-row>\n\n	    	</ion-grid>\n\n		</ion-title>\n\n	</ion-navbar>\n\n</ion-header>\n\n<ion-content padding id="page5">\n\n	\n\n	<div class="spacer" style="height:20px;" id="login-spacer1"></div>\n\n	<!-- <h2 align="center">Enter password and confirm password to change your account password.</h2> -->\n\n	<h2 align="center">Change my password.</h2>\n\n	<br>\n\n	<div align="center"><ion-icon name="lock" color="secondary" style="font-size: 4em"></ion-icon></div>\n\n	<div class="spacer" style="height:20px;" id="login-spacer2"></div>\n\n\n\n	<form id="login-form1" (ngSubmit)="submitRequest()" style="margin: 2vh 5vh;">\n\n		<ion-list id="login-list1">\n\n			<ion-item id="login-input1" style="border: 1px solid #EEE;margin: 1vh auto;">\n\n				<ion-label><!-- floating -->\n\n					<ion-icon name="lock"></ion-icon>\n\n				</ion-label>\n\n				<ion-input type="password" placeholder="Old Password" [(ngModel)]="changePass.current_password" name="current_password"></ion-input>\n\n			</ion-item>\n\n			<ion-item id="login-input1" style="border: 1px solid #EEE;margin: 1vh auto;">\n\n				<ion-label><!-- floating -->\n\n					<ion-icon name="lock"></ion-icon>\n\n				</ion-label>\n\n				<ion-input type="password" placeholder="New Password" [(ngModel)]="changePass.new_password" name="new_password"></ion-input>\n\n			</ion-item>\n\n			<ion-item id="login-input1" style="border: 1px solid #EEE;margin: 1vh auto;">\n\n				<ion-label><!-- floating -->\n\n					<ion-icon name="lock"></ion-icon>\n\n				</ion-label>\n\n				<ion-input type="password" placeholder="Confirm Password" [(ngModel)]="changePass.repeat_new_password" name="repeat_new_password"></ion-input>\n\n			</ion-item>\n\n		</ion-list>\n\n\n\n		<div class="fixed-outside" style="position: fixed;bottom: 0;left: 0;width: 100%;" align="center">\n\n			<button type="submit" color="secondary" ion-button block style="text-transform: none;margin-bottom: 0;padding: 1em;font-size: 20px;" icon-right>Change <ion-icon name="checkmark"></ion-icon></button>\n\n		</div>\n\n\n\n	</form>\n\n\n\n\n\n</ion-content>'/*ion-inline-end:"D:\app\familov_app\src\pages\change-password\change-password.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */]])
], ChangePasswordPage);

//# sourceMappingURL=change-password.js.map

/***/ }),

/***/ 42:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__faq_faq__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__products_products__ = __webpack_require__(204);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*import { ProductDetailsPage } from '../product-details/product-details';

 import { MyCartPage } from '../my-cart/my-cart';
 import { FaqPage } from '../faq/faq';*/


var HomePage = (function () {
    function HomePage(navCtrl, restProvider, alertCtrl) {
        //this.restProvider.LoadingSpinner();
        this.navCtrl = navCtrl;
        this.restProvider = restProvider;
        this.alertCtrl = alertCtrl;
        this.CountryList = {};
        this.selectedShopLoc = {};
        this.cartItems = {};
        this.cartItemsLen = 0;
        //	this.statusErrorMessage = 'Error..!! <br>Something went wrong!';
    }
    HomePage.prototype.ionViewCanEnter = function () {
        this.restProvider.LoadingSpinner();
        this.getHomeData();
        this.CountryList = localStorage.getItem('CountryList');
        /*if (this.CountryList == null || this.CountryList == undefined) {
         }*/
        if (this.CountryList == null || this.CountryList == undefined) {
            this.CountryList = [{ 'country_id': '-1', 'country_name': 'Select country' }];
        }
        if (this.CityList == null || this.CityList == undefined) {
            this.CityList = [{ 'city_id': '-1', 'city_name': 'Select city' }];
        }
        if (this.ShopList == null || this.ShopList == undefined) {
            this.ShopList = [{ 'shop_id': '-1', 'shop_name': 'Select shop' }];
        }
        // console.log(this.CityList);
        this.selectedShopLoc = JSON.parse(localStorage.getItem('selectedShopLoc'));
        if (typeof this.CountryList == 'string') {
            this.CountryList = JSON.parse(this.CountryList);
        }
        this.cartItems = localStorage.getItem('cartItems');
        if (this.cartItems != undefined || this.cartItems != null) {
            this.cartItemsLen = JSON.parse(this.cartItems).length;
        }
        //this.restProvider.hideLoadingSpinner();
        //this.restProvider.showToast('Item added in cart successfully.','toast-error');
    };
    HomePage.prototype.goToHelp = function (params) {
        if (!params)
            params = {};
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__faq_faq__["a" /* FaqPage */]);
    };
    HomePage.prototype.searchProducts = function () {
        var _this = this;
        var searchData = {};
        searchData['country_id'] = this['receiver_country']; //this.receiver_country;
        searchData['city_id'] = this['receiver_city'];
        searchData['shop_id'] = this['receiver_shop'];
        var tempMsg = '';
        if (searchData['country_id'] == null || searchData['country_id'] == undefined || searchData['country_id'] == '-1') {
            tempMsg += this.restProvider.errorString("Please select country", tempMsg);
        }
        if (searchData['city_id'] == null || searchData['city_id'] == undefined || searchData['city_id'] == '-1') {
            tempMsg += this.restProvider.errorString("Please select city", tempMsg);
        }
        if (searchData['shop_id'] == null || searchData['shop_id'] == undefined || searchData['shop_id'] == '-1') {
            tempMsg += this.restProvider.errorString("Please select shop", tempMsg);
        }
        if (tempMsg != '') {
            this.restProvider.showErrorMsg(this.restProvider.statusError, tempMsg);
            return false;
        }
        var isConfirm = false;
        var params = {};
        var selectedShop = localStorage.getItem('selectedShopLoc');
        var cartItems = localStorage.getItem('cartItems');
        if (cartItems != undefined && cartItems != null) {
            cartItems = JSON.parse(cartItems);
        }
        else {
            cartItems = null;
        }
        if (cartItems != null && cartItems != undefined && cartItems.length > 0) {
            if (selectedShop != null && selectedShop != undefined && selectedShop != '') {
                selectedShop = JSON.parse(selectedShop);
                if (searchData['country_id'] == selectedShop['country_id'] && searchData['city_id'] == selectedShop['city_id'] && searchData['shop_id'] == selectedShop['shop_id']) {
                    isConfirm = true;
                    localStorage.setItem('selectedShopLoc', JSON.stringify(searchData));
                    //var params = {};
                    params['searchData'] = searchData;
                    this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__products_products__["a" /* ProductsPage */], params); //TODO-VIX
                }
                else {
                    var title = 'Are You Sure Want To Change Shop?';
                    var message = 'If you change stores, you will lose your cart.';
                    var alert_1 = this.alertCtrl.create({
                        title: title,
                        message: message,
                        buttons: [
                            {
                                text: 'No',
                                role: 'cancel',
                                handler: function () {
                                    console.log('Cancel clicked');
                                    _this.openCartPage();
                                }
                            },
                            {
                                text: 'Yes',
                                handler: function () {
                                    localStorage.removeItem('cartItems');
                                    isConfirm = true;
                                    localStorage.setItem('selectedShopLoc', JSON.stringify(searchData));
                                    //	var params = {};
                                    params['searchData'] = searchData;
                                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__products_products__["a" /* ProductsPage */], params); //TODO-VIX
                                }
                            }
                        ]
                    });
                    alert_1.present();
                }
            }
            else {
                isConfirm = true;
                localStorage.removeItem('cartItems');
                localStorage.setItem('selectedShopLoc', JSON.stringify(searchData));
                //var params = {};
                params['searchData'] = searchData;
                this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__products_products__["a" /* ProductsPage */], params);
            }
        }
        else {
            isConfirm = true;
            localStorage.setItem('selectedShopLoc', JSON.stringify(searchData));
            //	var params = {};
            params['searchData'] = searchData;
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__products_products__["a" /* ProductsPage */], params);
        }
        return false;
    };
    /*searchProducts() {
        var searchData = {};
        searchData.country_id = this.receiver_country;
        searchData.city_id = this.receiver_city;
        searchData.shop_id = this.receiver_shop;
        console.log(searchData);
        var tempMsg = '';
        if (searchData.country_id == null || searchData.country_id == undefined) {
            tempMsg += this.restProvider.errorString("Please select country",tempMsg);
        }
        if (searchData.city_id == null || searchData.city_id == undefined) {
            tempMsg += this.restProvider.errorString("Please select city",tempMsg);
        }
        if (searchData.shop_id == null || searchData.shop_id == undefined) {
            tempMsg += this.restProvider.errorString("Please select shop",tempMsg);
        }

        if (tempMsg != '') {
            this.restProvider.showErrorMsg(this.restProvider.statusError,tempMsg);
            return false;
        }

        var isConfirm = false;

        var selectedShop = localStorage.getItem('selectedShopLoc');
        var cartItems = localStorage.getItem('cartItems');
        if (cartItems != undefined && cartItems != null) {
            cartItems = JSON.parse(cartItems);
        }
        else {
            cartItems = [];
        }
        if (cartItems != null && cartItems != undefined && cartItems.length > 0) {
            if(selectedShop != null && selectedShop != undefined && selectedShop != '') {
                selectedShop = JSON.parse(selectedShop);
                console.log(selectedShop);
                console.log(searchData);
                if (searchData.country_id == selectedShop.country_id && searchData.city_id == selectedShop.city_id && searchData.shop_id == selectedShop.shop_id
                ) {
                    isConfirm = true;
                    localStorage.setItem('selectedShopLoc',JSON.stringify(searchData));
                    var params = {};
                    params.searchData = searchData;

                    //this.navCtrl.setRoot(ProductsPage, params); //TODO-VIX
                }

                else {
                    var title = 'Are You Sure Want To Change Shop?';
                    var message = 'If you change stores, you will lose your cart.';

                    let alert = this.alertCtrl.create({
                        title: title,
                        message: message,
                        buttons: [
                            {
                                text: 'No',
                                role: 'cancel',
                                handler: () => {
                                    console.log('Cancel clicked');
                                    this.openCartPage();
                                }
                            },
                            {
                                text: 'Yes',
                                handler: () => {
                                    localStorage.removeItem('cartItems');
                                    isConfirm = true;
                                    localStorage.setItem('selectedShopLoc',JSON.stringify(searchData));
                                    var params = {};
                                    params.searchData = searchData;
                                    //this.navCtrl.setRoot(ProductsPage, params);//TODO-VIX
                                }
                            }
                        ]
                    });
                    alert.present();
                }
            }
            else {
                isConfirm = true;
                localStorage.removeItem('cartItems');
                localStorage.setItem('selectedShopLoc',JSON.stringify(searchData));
                var params = {};
                params.searchData = searchData;

                this.navCtrl.setRoot(ProductsPage, params);
            }
        }
        else {
            isConfirm = true;
            localStorage.setItem('selectedShopLoc',JSON.stringify(searchData));
            var params = {};
            params.searchData = searchData;
            this.navCtrl.setRoot(ProductsPage, params);
        }
        return false;
    }*/
    HomePage.prototype.openCartPage = function () {
        //TODO-VIX this.navCtrl.push(MyCartPage);
    };
    HomePage.prototype.getHomeData = function () {
        var _this = this;
        //this.restProvider.showToast(Constants.MSG_INTERNET_CONNECTION,'toast-error');
        this.restProvider.getHomeData()
            .then(function (result) {
            //console.log(result);
            _this.restProvider.hideLoadingSpinner();
            var tempLoginMsg = localStorage.getItem('LoginMsg');
            if (tempLoginMsg != null && tempLoginMsg != undefined) {
                _this.restProvider.showToast(tempLoginMsg, 'toast-success');
                localStorage.removeItem('LoginMsg');
            }
            if (result['status'] === true) {
                _this.CountryList = result['response_data']['country_list'];
                if (result['response_data']['country_count'] == 0) {
                    _this.restProvider.showErrorMsg(_this.restProvider.statusString, result['message']);
                }
                localStorage.setItem('CountryList', JSON.stringify(_this.CountryList));
            }
        }, function (err) {
            _this.restProvider.hideLoadingSpinner();
            _this.restProvider.showErrorMsg(_this.restProvider.statusError, _this.restProvider.statusErrorMessage);
        });
    };
    HomePage.prototype.getCityList = function ($event) {
        var _this = this;
        this.restProvider.LoadingSpinner();
        this.restProvider.getCities($event)
            .then(function (result) {
            _this.restProvider.hideLoadingSpinner();
            if (result['status'] === true) {
                _this.CityList = result['response_data']['city_list'];
            }
            else {
                _this.CityList = [{ 'city_id': '-1', 'city_name': 'Select city' }];
                _this.ShopList = [{ 'shop_id': '-1', 'shop_name': 'Select shop' }];
                _this.restProvider.showErrorMsg(_this.restProvider.statusError, result['message']);
            }
        }, function (err) {
            _this.restProvider.hideLoadingSpinner();
            _this.restProvider.showErrorMsg(_this.restProvider.statusError, _this.restProvider.statusErrorMessage);
        });
    };
    HomePage.prototype.getShopList = function ($event) {
        var _this = this;
        this.restProvider.LoadingSpinner();
        this.restProvider.getShops($event)
            .then(function (result) {
            _this.restProvider.hideLoadingSpinner();
            if (result['status'] === true) {
                //if(result.result['response_data']['shop_list'].length > 0) {
                _this.ShopList = result['response_data']['shop_list'];
                //}
            }
            else {
                _this.ShopList = [{ 'shop_id': '-1', 'shop_name': 'Select shop' }];
                _this.restProvider.showErrorMsg(_this.restProvider.statusError, result['message']);
            }
        }, function (err) {
            _this.restProvider.hideLoadingSpinner();
            _this.restProvider.showErrorMsg(_this.restProvider.statusError, _this.restProvider.statusErrorMessage);
        });
    };
    return HomePage;
}());
HomePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-home',template:/*ion-inline-start:"D:\app\familov_app\src\pages\home\home.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle class="buttonWhiteFont">\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>\n\n    	<ion-grid>\n\n    		<ion-row>\n\n    			<ion-col col-10 class="headerTitle">\n\n    				<div align="center">Familov</div>\n\n    			</ion-col>\n\n    			<ion-col col-2 class="badgeCol">\n\n					<!-- <ion-icon class="right" name="cart" item-end (click)="openCartPage()"></ion-icon> -->\n\n					<span class="badgeIcon"> {{ cartItemsLen }}</span>\n\n					<ion-icon name="cart" class="badgeIconCart" item-end (click)="openCartPage()"></ion-icon>\n\n    			</ion-col>\n\n    		</ion-row>\n\n    	</ion-grid>\n\n	</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n\n\n  <!-- <h3 align="center">Welcome</h3> -->\n\n\n\n  <div style="min-height: 10vh;padding: 3vh;">\n\n    <!-- <ion-slides autoplay=\'2000\' loop=\'true\'>\n\n    <ion-slide>\n\n      <img src="assets/imgs/slide3.png">\n\n    </ion-slide>\n\n    <ion-slide>\n\n      <img src="assets/imgs/slide2.png">\n\n    </ion-slide>\n\n    <ion-slide>\n\n      <img src="assets/imgs/slide1.png">\n\n    </ion-slide>\n\n  </ion-slides> -->\n\n  <div align="center">\n\n	  <img src="assets/imgs/angel.png" style="height: 10vh; width: auto;">\n\n	  <h1 class="main-header-title textBlue" align="center">How do you want to support your family today?</h1>\n\n	</div>\n\n</div>\n\n\n\n<!-- <div class="spacer" style="height:20px;" id="login-spacer2"></div> -->\n\n<!-- <hr> -->\n\n  <!-- <h3 align="center">Our Products</h3> -->\n\n  <!-- <h3 align="center">Search Products</h3> -->\n\n  <h4 align="center" class="sub-header-title" >\n\n  	Buy from abroad foods and basics needs <br>\n\n  	we deliver your loved ones within 36 h.\n\n  </h4>\n\n<!-- <hr> -->\n\n\n\n<!-- <ion-grid>\n\n  <ion-row>\n\n    <ion-col col-6 align="center" class="products">\n\n        <img src="assets/imgs/slide2.png">\n\n        <br>\n\n        <span class="productTitle">Product 1</span>\n\n        <button color="danger" class="moreDetails" ion-button (click)="viewProductDetails()">More Info</button>\n\n    </ion-col>\n\n    <ion-col col-6 align="center" class="products">\n\n      <img src="assets/imgs/slide2.png">\n\n      <br>\n\n      <span class="productTitle">Product 2</span>\n\n      <button color="danger" class="moreDetails" ion-button (click)="viewProductDetails()">More Info</button>\n\n    </ion-col>\n\n    <ion-col col-6 align="center" class="products">\n\n      <img src="assets/imgs/slide3.png">\n\n      <br>\n\n      <span class="productTitle">Product 3</span>\n\n      <button color="danger" class="moreDetails" ion-button (click)="viewProductDetails()">More Info</button>\n\n    </ion-col>\n\n    <ion-col col-6 align="center" class="products">\n\n      <img src="assets/imgs/slide3.png">\n\n      <br>\n\n      <span class="productTitle">Product 3</span>\n\n      <button color="danger" class="moreDetails" ion-button (click)="viewProductDetails()">More Info</button>\n\n    </ion-col>\n\n  </ion-row>\n\n  </ion-grid> -->\n\n\n\n<!-- <ion-card> -->\n\n<div align="center">\n\n<ion-item class="home-dropdown">\n\n  <ion-label> <ion-icon color="light" name="pin"></ion-icon> &nbsp;Receiver Country</ion-label><!-- planet -->\n\n  <ion-select [(ngModel)]="receiver_country" (ionChange)="getCityList($event)"><!-- placeholder="Receiver Country" -->\n\n  		<ion-option *ngFor="let country of CountryList" value="{{ country.country_id }}"> {{ country.country_name }} </ion-option>\n\n  </ion-select>\n\n</ion-item>\n\n\n\n<ion-item class="home-dropdown">\n\n  <ion-label> <ion-icon color="light" name="pin"></ion-icon> &nbsp;Receiver City</ion-label><!-- navigate -->\n\n  <ion-select [(ngModel)]="receiver_city" (ionChange)="getShopList($event)"><!-- placeholder="Receiver City" -->\n\n    <ion-option *ngFor="let city of CityList" value="{{ city.city_id }}"> {{ city.city_name }} </ion-option>\n\n  </ion-select>\n\n</ion-item>\n\n\n\n\n\n<ion-item class="home-dropdown">\n\n  <ion-label> <ion-icon color="light" name="briefcase"></ion-icon> &nbsp;Receiver Shop</ion-label><!-- locate -->\n\n  <ion-select [(ngModel)]="receiver_shop"><!-- placeholder="Receiver Shop" -->\n\n    <ion-option *ngFor="let shop of ShopList" value="{{ shop.shop_id }}"> {{ shop.shop_name }} </ion-option>\n\n  </ion-select>\n\n</ion-item>\n\n</div>\n\n<div align="center">\n\n	<br>\n\n	<button color="secondary" ion-button (click)="searchProducts()" style="width: 47vh;" class="home-dropdown login-button">Enter</button>\n\n	<br>\n\n	<button color="secondary" ion-button (click)="goToHelp()" style="width: 20vh;text-transform: none;" clear>Need help?</button>\n\n</div>\n\n\n\n<!-- </ion-card> -->\n\n\n\n<!-- <hr>\n\n	<h3 align="center" style="color: #302E64;font-weight: bold;text-transform: uppercase;">How it works</h3>\n\n	\n\n	<hr>\n\n	\n\n	<ion-card class="grayBackground">\n\n		<div align="center" style="padding: 10px;">\n\n			<h2 class="howWorksNumber" color="secondary" ion-button>1</h2>\n\n			<br>\n\n			<ion-icon name=\'cart\' class="bigIcon" color="secondary"></ion-icon>\n\n			<br><br>\n\n			<h2><b>Make the choice online</b></h2>\n\n			<h3 style="margin: 2vh;padding: 3vh;">Choose the products you want to offer and tell us the name and phone number of the recipient.</h3>\n\n		</div>		\n\n	</ion-card>\n\n\n\n	<ion-card class="grayBackground">\n\n		<div align="center" style="padding: 10px;">\n\n			<h2 class="howWorksNumber" color="secondary" ion-button>2</h2>\n\n			<br>\n\n			<ion-icon name=\'cloud-done\' class="bigIcon" color="secondary"></ion-icon>\n\n			<br><br>\n\n			<h2><b>Pay securely</b></h2>\n\n			<h3 style="margin: 2vh;padding: 3vh;">Use the secure method of payment that suits you - credit card (VISA, MASTER) or your PayPal account.</h3>\n\n		</div>		\n\n	</ion-card>\n\n\n\n	<ion-card class="grayBackground">\n\n		<div align="center" style="padding: 10px;">\n\n			<h2 class="howWorksNumber" color="secondary" ion-button>3</h2>\n\n			<br>\n\n			<ion-icon name=\'chatbubbles\' class="bigIcon" color="secondary"></ion-icon>\n\n			<br><br>\n\n			<h2><b>SMS withdrawal code</b></h2>\n\n			<h3 style="margin: 2vh;padding: 3vh;">We send the details of your package to the store and the unique SMS withdrawal code to the recipient.</h3>\n\n		</div>		\n\n	</ion-card>\n\n\n\n	<ion-card class="grayBackground">\n\n		<div align="center" style="padding: 10px;">\n\n			<ion-icon name="checkmark-circle-outline" class="bigIcon" color="secondary"></ion-icon>\n\n			<br>\n\n			<ion-icon name=\'cube\' class="bigIcon" color="secondary"></ion-icon>\n\n			<br><br>\n\n			<h2><b>Delivery and confirmation</b></h2>\n\n			<h3 style="margin: 2vh;padding: 3vh;">The package is prepared and the beneficiary is contacted within 36 H * for delivery and you receive confirmation that everything went well.</h3>\n\n		</div>		\n\n	</ion-card>\n\n<hr>\n\n\n\n<div class="grayBackground" align="center" style="padding: 1vh;">\n\n	<h3 align="center" style="margin-top: 1vh;">Unbeatable hardships</h3>\n\n	<ion-grid>\n\n		<ion-row class="rowPadding">\n\n			<ion-col col-6>\n\n				<ion-icon name="card" class="bigIcon" color="primary"></ion-icon><br><br>\n\n				<b>Send more, buy less</b><br>\n\n				Save up to 95% vs. traditional money transfer provider and local price Guarantee!\n\n			</ion-col>\n\n			<ion-col col-6>\n\n				<ion-icon name="lock" class="bigIcon" color="primary"></ion-icon><br><br>\n\n				<b>Send more, buy less</b><br>\n\n				Save up to 95% vs. traditional money transfer provider and local price Guarantee!\n\n			</ion-col>\n\n		</ion-row>\n\n\n\n		<ion-row class="rowPadding">\n\n			<ion-col col-6>\n\n				<ion-icon name="lock" class="bigIcon" color="primary"></ion-icon><br><br>\n\n				<b>Send more, buy less</b><br>\n\n				Save up to 95% vs. traditional money transfer provider and local price Guarantee!\n\n			</ion-col>\n\n			<ion-col col-6>\n\n				<ion-icon name="laptop" class="bigIcon" color="primary"></ion-icon><br><br>\n\n				<b>Send more, buy less</b><br>\n\n				Save up to 95% vs. traditional money transfer provider and local price Guarantee!\n\n			</ion-col>\n\n		</ion-row>\n\n\n\n		<ion-row class="rowPadding">\n\n			<ion-col col-6>\n\n				<ion-icon name="people" class="bigIcon" color="primary"></ion-icon><br><br>\n\n				<b>Send more, buy less</b><br>\n\n				Save up to 95% vs. traditional money transfer provider and local price Guarantee!\n\n			</ion-col>\n\n			<ion-col col-6>\n\n				<ion-icon name="help-circle" class="bigIcon" color="primary"></ion-icon><br><br>\n\n				<b>Send more, buy less</b><br>\n\n				Save up to 95% vs. traditional money transfer provider and local price Guarantee!\n\n			</ion-col>\n\n		</ion-row>\n\n\n\n	</ion-grid>\n\n</div> -->\n\n<br>\n\n<br>\n\n<br>\n\n  <!-- <p>\n\n    If you get lost, the <a href="http://ionicframework.com/docs/v2">docs</a> will show you the way.\n\n  </p> -->\n\n\n\n  <!-- <button ion-button secondary menuToggle>Toggle Menu</button> -->\n\n</ion-content>\n\n'/*ion-inline-end:"D:\app\familov_app\src\pages\home\home.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
], HomePage);

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 55:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FaqPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FaqPage = FaqPage_1 = (function () {
    function FaqPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.frequentQuestions = [
            {
                title: "What is Familov.com?",
                description: "Everything is in the name. With familov.com, order from abroad and we deliver your family to the country in less than 36 hours."
            },
            {
                title: "How can I create an account?",
                description: "Sur la page de démarrage de Familov, cliquez sur Inscrivez-vous (ou allez directement à la page Créer un compte). Remplissez les informations et votre mot de passe et confirmez-les une deuxième fois. Votre adresse de courriel sera directement liée au compte."
            },
            {
                title: "I forgot my password?",
                description: "Vous avez déjà créé un compte mais vous ne vous souvenez plus de votre mot de passe? Cliquez sur «Login» en haut de la page. Puis cliquez sur 'Mot de passe oublié?'. Remplissez votre adresse e-mail et un nouveau mot de passe vous sera envoyé par e-mail."
            },
            {
                title: "How long does a delivery take?",
                description: "En moyenne, nous avons besoin de moins de 36 heures à partir du moment où nous avons recu le payement jusqu'au moment de la livraison à votre proche. Le délai de livraison peut varier d'une boutique à l'autre et dépend du nombre de commandes que la boutique doit préparer et la disponibilité du bénéficiaire."
            },
            {
                title: "How much is shipping?",
                description: "Nous facturons un forfait de 2,95 € (2,50€ pendant le phase béta) pour le service et 0,00€ pour les livraison en magasin.En fonction de lieu de residence le coût des livraison à domicile varie."
            },
            {
                title: "What happens after placing my order?",
                description: "L'ordre arrive dans notre système et après que nous recevons le paiement, il est envoyé directement à la boutique (Shopper) que vous avez commandé. Après seulement quelques secondes, les magasins reçoivent l'ordre par le biais de notre propre système Shop-Connect.<br> <br>1- Le magasin recoit tous les détails de votre commande <br> 2- Votre proche recoit le code de retrait par SMS<br> 3-Le livreur prépare le paquet et recontacte le bénéficiaire dans les 36 heures pour la livraison <br>4-Vous recevez une confirmation par SMS ou e-mail que votre commande est bien livrée. <br>NB: Nous travaillons avec partenaires n et des livreurs proffessionnels pour gerer vos commanades. Ne vous inquiétez pas, ils ont été formés individuellement pes sont formés pour donner la meilleure qualité , soigner vos articles et les livrer."
            },
            {
                title: "Where can I see my receipts?",
                description: "Vous pouvez afficher vos reçus de deux façons: Email: Lorsque votre commande est passée, vous recevrez un courriel avec un reçu. <br> <br> Sur le site Web cliquez sur votre nom dans le coin supérieur droit et un menu apparaîtra. Sélectionnez Mes commandes. Cliquez sur un numéro de commande pour afficher un reçu détaillé de cet ordre."
            },
            {
                title: "How is my order confirmed?",
                description: "Une fois votre commande passée, vous recevrez un e-mail de confirmation envoyé à l'adresse e-mail que vous avez saisie lors de votre commande. Conservez ce courriel et votre numéro de commande pour toute question ou remarque concernant votre commande. Lorsque vous recevez l'e-mail de confirmation, Familov fait le reste. Il suffit de s'asseoir, de se détendre et de profiter!"
            },
            {
                title: "I have not received a confirmation email",
                description: "Si un numéro de commande s'affiche à l'écran, cela signifie que votre commande a été envoyée. L'e-mail de confirmation peut être dans votre «spam» plutôt que dans votre boîte de réception. Si vous ne recevez pas de numéro de commande, veuillez contacter notre service à la clientèle pour connaître l'état de votre commande. Vous devez fournir l'adresse e-mail que vous avez saisie lors de la commande et le nom de la boutique sélectionnée afin que nous puissions trouver votre commande."
            }
        ];
        this.paymentQuestions = [
            {
                title: "What types of payment can I use?",
                description: "Carte de crédit (VISA / American Express / Master Card /)<br>Pay Pal <br> Virement"
            },
            {
                title: "How secure are online payments?",
                description: "Lors du traitement des paiements en ligne sur Familov.com, nous utilisons des pages SSL sécurisées. Cela signifie que ces pages assurent la protection de vos données personnelles et de paiement. De cette façon, vous pouvez être sûr que cette information est seulement visible pour vous et pour nous, pour le traitement de votre commande."
            },
            {
                title: "Why do I have to pay transaction fees?",
                description: "Familov.com est seulement un intermédiaire, entre vous en tant que consommateur et la Boutique. Vu le faible revenu que nous gagnons sur chaque commande, nous devons facturer pour l'utilisation d'une méthode de paiement en ligne. Les coûts couvrent l'entretien technique des paiements en ligne, les coûts de transaction que les banques facturent et nos frais administratifs pour le traitement des paiements. Tous les sites facturent ces coûts, mais ils les ajoutent souvent directement aux frais d'envoi et / ou d'administration.. Nous vous remercions pour votre compréhension."
            },
            {
                title: "My online payment failed",
                description: "Si vous pensez que le paiement en ligne a échoué, vérifiez toujours si vous avez reçu un e-mail de confirmation. Cet e-mail vous permettra de voir si votre commande a été soumise et si la commande est déjà payée ou non. <br><br> L'e-mail de confirmation est envoyé directement une fois le paiement terminé. Cela peut prendre un peu plus longtemps si le paiement est traité par la banque, la société de carte de crédit ou toute autre société de paiement. Cela peut prendre jusqu'à 15 minutes. Si après 15 minutes vous n'avez toujours pas de courriel de confirmation dans votre boîte de réception, contactez notre service à la clientèle. Ils vous diront si l'ordre et le paiement ont réussi."
            },
            {
                title: "I still have a lot of questions to ask you?",
                description: "Contactez nous! Nous serons là à :<br><br> Email:hello@familov.com <br><br> Whatsapp : (+49 1525 9948834)<br><br> Chat : www.familov.com"
            }
        ];
        this.shownGroup = null;
        // If we navigated to this page, we will have an item available as a nav param
        this.selectedItem = navParams.get('item');
        // Let's populate this page with some filler content for funzies
        this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
            'american-football', 'boat', 'bluetooth', 'build'];
        this.items = [];
        for (var i = 1; i < 11; i++) {
            this.items.push({
                title: 'Item ' + i,
                note: 'This is item #' + i,
                icon: this.icons[Math.floor(Math.random() * this.icons.length)]
            });
        }
    }
    FaqPage.prototype.toggleGroup = function (group) {
        if (this.isGroupShown(group)) {
            this.shownGroup = null;
        }
        else {
            this.shownGroup = group;
        }
    };
    ;
    FaqPage.prototype.isGroupShown = function (group) {
        return this.shownGroup === group;
    };
    ;
    FaqPage.prototype.itemTapped = function (event, item) {
        // That's right, we're pushing to ourselves!
        this.navCtrl.push(FaqPage_1, {
            item: item
        });
    };
    return FaqPage;
}());
FaqPage = FaqPage_1 = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-faq',template:/*ion-inline-start:"D:\app\familov_app\src\pages\faq\faq.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Frequently Questions</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n<h3 align="center" style="margin: 5vh auto;">Frequently Asked Questions</h3>\n\n\n\n  <ion-card *ngFor="let d of frequentQuestions; let i=index" text-wrap (click)="toggleGroup(i)" [ngClass]="{active: isGroupShown(i)}" style="background: #EEE;">\n\n  <ion-card-content>\n\n    <ion-card-title>\n\n      <ion-grid>\n\n        <ion-row>\n\n          <ion-col col-10>\n\n            <ion-label style="font-size: 15px;margin: 4px;" color="primary">{{ d.title }}</ion-label>\n\n          </ion-col>\n\n          <ion-col col-2 style="text-align: right;">\n\n            <ion-icon style="font-size: 1.3em;" color="success" name="arrow-dropright" [name]="isGroupShown(i) ? \'arrow-dropdown\' : \'arrow-dropright\'"></ion-icon>\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-grid>\n\n      </ion-card-title>\n\n    <div *ngIf="isGroupShown(i)" [innerHtml]="d.description"></div><!-- {{d.description}} -->\n\n  </ion-card-content>\n\n</ion-card>\n\n\n\n<h3 align="center" style="margin: 5vh auto;">Payment</h3>\n\n\n\n<ion-card *ngFor="let d of paymentQuestions; let i=index" text-wrap (click)="toggleGroup(i)" [ngClass]="{active: isGroupShown(i)}" style="background: #EEE;">\n\n  <ion-card-content>\n\n    <ion-card-title>\n\n      <ion-grid>\n\n        <ion-row>\n\n          <ion-col col-10>\n\n            <ion-label style="font-size: 15px;margin: 4px;" color="primary">{{ d.title }}</ion-label>\n\n          </ion-col>\n\n          <ion-col col-2 style="text-align: right;">\n\n            <ion-icon style="font-size: 1.3em;" color="success" name="arrow-dropright" [name]="isGroupShown(i) ? \'arrow-dropdown\' : \'arrow-dropright\'"></ion-icon>\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-grid>\n\n      </ion-card-title>\n\n    <div *ngIf="isGroupShown(i)" [innerHtml]="d.description"></div><!-- {{d.description}} -->\n\n  </ion-card-content>\n\n</ion-card>\n\n\n\n  <div *ngIf="selectedItem" padding>\n\n    You navigated here from <b>{{selectedItem.title}}</b>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"D:\app\familov_app\src\pages\faq\faq.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
], FaqPage);

var FaqPage_1;
//# sourceMappingURL=faq.js.map

/***/ }),

/***/ 56:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyCartPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(42);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// import { PaymentPage } from '../payment/payment';
//import { CheckoutPage } from '../checkout/checkout';
//import { ProductsPage } from '../products/products';
//import { MyCartPage } from '../my-cart/my-cart';
var MyCartPage = (function () {
    function MyCartPage(navCtrl, alertCtrl, restProvider) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.restProvider = restProvider;
        this.cartItemsLen = 0;
        this.selected_shop_id = 0;
        this.tempTot = 0;
        // this.qty1 = 1;
        // this.qty2 = 1;
        // this.qty3 = 1;
        this.cartItems = false;
        this.cartTotal = 0;
        this.serviceFee = 0;
        this.promoDiscount = 0;
        this.promoID = 0;
        var selected_shop = localStorage.getItem('selectedShopLoc');
        if (selected_shop != null && selected_shop != undefined) {
            this.selected_shop_id = JSON.parse(selected_shop).shop_id;
        }
        this.currencySymbol = '&euro;';
        this.currencyName = 'EUR';
        this.cartItems = localStorage.getItem('cartItems');
        if (this.cartItems == undefined || this.cartItems == null) {
            this.cartItems = [];
        }
        else {
            this.cartItems = JSON.parse(localStorage.getItem('cartItems'));
            this.cartItemsLen = this.cartItems.length;
            for (var i = 0; i < this.cartItems.length; ++i) {
                var total = (this.cartItems[i].product_prices * this.cartItems[i].qty).toFixed(2);
                this.cartTotal = (parseFloat(this.cartTotal) + parseFloat(total)).toFixed(2);
            }
        }
        this.currencyData = localStorage.getItem('currencyData');
        if (this.currencyData == undefined || this.currencyData == null) {
            this.currencyData = [];
            this.currencySymbol = '&euro;';
            this.currencyName = 'EUR';
        }
        else {
            this.currencyData = JSON.parse(this.currencyData);
            this.currencySymbol = this.currencyData.vSymbol;
            this.currencyName = this.currencyData.vName;
        }
        this.getConfigData();
        this.config = JSON.parse(localStorage.getItem('config'));
        console.log(this.config);
        if (this.config == undefined || this.config == null) {
            this.getConfigData();
            //this.serviceFee = this.config.service_fees;
        }
        //this.restProvider.showToast('Item added in warning.','toast-warning');
        //this.getPromoCode();
        this.promo_code_data = JSON.parse(localStorage.getItem('promo_code_data'));
        if (this.promo_code_data = undefined || this.promo_code_data == null) {
            this.promo_code_data = [];
            this.promoDiscount = 0;
        }
        else {
            this.promo_code_data = JSON.parse(localStorage.getItem('promo_code_data'));
            this.promoDiscount = this.promo_code_data.promo_code_amount;
        }
        /*this.cartItems = localStorage.getItem('cartItems');
         if (this.cartItems != undefined || this.cartItems != null) {
         this.cartItemsLen = JSON.parse(this.cartItems).length;
         }*/
        this.totalPrice = 0;
        setTimeout(function () {
            _this.serviceFee = _this.config.service_fees;
            _this.totalPrice = ((parseFloat(_this.cartTotal) + parseFloat(_this.serviceFee)) - parseFloat(_this.promoDiscount)).toFixed(2);
        }, 1000);
        //this.totalPrice = (parseFloat(this.cartTotal) + parseFloat(this.serviceFee) + parseFloat(this.promoDiscount)).toFixed(2);
    }
    /*getPromoCode() {
     //this.restProvider.getPromoCode(this.promo_code,parseFloat(this.cartTotal))
     this.restProvider.getPromoCode()
     .then(
     (result) => {
     console.log(result);
     if (result.status == true) {
     this.config = result.data;
     this.serviceFee = result.data.service_fees;
     localStorage.setItem('config',JSON.stringify(this.config));

     }
     else {
     if (typeof result.message == "string") {
     this.restProvider.showErrorMsg(this.statusError,result.message);
     }
     else {
     this.restProvider.showErrorMsg(this.statusError,'Oops..!! <br>No internet connection!<br>Please check you internet connectivity.');
     }
     }
     },
     (err) => {
     console.log(err);
     this.restProvider.showErrorMsg(this.statusError,'Opps..!! <br> Something went wrong.');
     }
     );
     }*/
    MyCartPage.prototype.getConfigData = function () {
        var _this = this;
        this.restProvider.getConfigData()
            .then(function (result) {
            console.log(result);
            if (result['status'] == true) {
                _this.config = result['response_data'];
                _this.serviceFee = result['response_data']['service_fees'];
                localStorage.setItem('config', JSON.stringify(_this.config));
                _this.totalPrice = ((parseFloat(_this.cartTotal) + parseFloat(_this.serviceFee)) - parseFloat(_this.promoDiscount)).toFixed(2);
            }
            else {
                _this.restProvider.hideLoadingSpinner();
                _this.restProvider.showErrorMsg(_this.restProvider.statusString, _this.restProvider.statusErrorMessageAPI);
                _this.goToHome({});
            }
        }, function (err) {
            _this.restProvider.hideLoadingSpinner();
            _this.goToHome({});
            _this.restProvider.showErrorMsg(_this.restProvider.statusString, _this.restProvider.statusErrorMessageAPI);
        });
    };
    MyCartPage.prototype.goToHome = function (params) {
        if (!params)
            params = {};
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */]);
    };
    MyCartPage.prototype.addProductToCart = function (product, type) {
        var exist = this.cartItems.filter(function (cartItem) { return cartItem.product_id == product.product_id; });
        var cart_total = 0;
        if (exist.length > 0) {
            for (var i = 0; i < this.cartItems.length; ++i) {
                if (this.cartItems[i].product_id == product.product_id) {
                    if (type == 'add') {
                        this.cartItems[i].qty++;
                    }
                    else {
                        if (this.cartItems[i].qty > 1) {
                            this.cartItems[i].qty--;
                        }
                    }
                }
                var total = (this.cartItems[i].product_prices * this.cartItems[i].qty).toFixed(2);
                cart_total = (parseFloat(cart_total) + parseFloat(total)).toFixed(2);
            }
            localStorage.setItem('cartItems', JSON.stringify(this.cartItems));
        }
        else {
            product.qty = 1;
            cart_total = product.product_prices;
            this.cartItems.push(product);
            localStorage.setItem('cartItems', JSON.stringify(this.cartItems));
        }
        this.cartTotal = cart_total;
        this.totalPrice = (parseFloat(this.cartTotal) + parseFloat(this.serviceFee) + parseFloat(this.promoDiscount)).toFixed(2);
    };
    return MyCartPage;
}());
MyCartPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-my-cart',template:/*ion-inline-start:"D:\app\familov_app\src\pages\my-cart\my-cart.html"*/'<ion-header>\n\n	<ion-navbar>\n\n		<button ion-button menuToggle class="buttonWhiteFont">\n\n			<ion-icon name="menu"></ion-icon>\n\n		</button>\n\n		<ion-title>\n\n			<ion-grid>\n\n				<ion-row>\n\n					<ion-col col-10 class="headerTitle"><div align="center">View Cart</div></ion-col>\n\n					<ion-col col-2 class="badgeCol">\n\n						<!-- <ion-icon class="right" name="cart" item-end (click)="openCartPage()"></ion-icon> -->\n\n						<span class="badgeIcon"> {{ cartItemsLen }}</span>\n\n						<ion-icon name="cart" item-end (click)="openCartPage()"></ion-icon>\n\n					</ion-col>\n\n				</ion-row>\n\n			</ion-grid>\n\n		</ion-title>\n\n	</ion-navbar>\n\n</ion-header>\n\n<ion-content padding id="page4">\n\n\n\n	<div style="vertical-align: middle;margin-top: 15vh;margin-bottom: 20vh;" align="center" *ngIf="cartItems.length == 0">\n\n		<img src="assets/imgs/tired.png" style="max-height: 15vh;width: auto;">\n\n		<h2 class="view-cart-oop">Ouuppss !!!</h2>\n\n		<h3 class="view-cart-emplty" >Your cart is empty</h3>\n\n		<h5 class="view-cart-sel-pro">Please select the products</h5>\n\n		<div align="center">\n\n			<button id="login-button1" ion-button color="secondary" (click)="openShopPage()" style="margin: 1vh;width: 80%;height: 3.5em;">\n\n				Back to Shop\n\n			</button>\n\n		</div>\n\n	</div>\n\n\n\n\n\n	<div *ngIf="cartItems.length > 0">\n\n		<div align="center">\n\n			<div class="checkout-wrap">\n\n				<ul class="checkout-bar">\n\n					<li class="visited first"></li>\n\n					<li class="previous"></li>\n\n					<li class=""></li>\n\n				</ul>\n\n			</div>\n\n		</div>\n\n		<br>\n\n		<!--<h3 align="center" style="margin-top: 3vh;">You have total {{ cartItems.length }} product in basket.</h3>-->\n\n		<hr>\n\n		<ion-list>\n\n			<ion-item style="border: 1px solid #eee; padding: 1px;margin: 1vh auto;" *ngFor="let product of cartItems">\n\n				<ion-thumbnail item-start>\n\n					<img src="{{ product.product_image }}">\n\n				</ion-thumbnail>\n\n				<h2><b>{{ product.product_name }}</b></h2>\n\n				<br>\n\n				<p>\n\n					<!-- <b>Price:</b> -->\n\n					<span *ngIf="product.qty == 1" style="color:#32db64;font-weight: bold;" [innerHtml]="currencySymbol">{{ currencyName }}</span>\n\n					<span *ngIf="product.qty == 1" class="text-green-fam" style="font-weight: bold;">{{ product.product_prices }}</span>\n\n\n\n					<span *ngIf="product.qty > 1" [innerHtml]="currencySymbol">{{ currencyName }}</span>\n\n					<span *ngIf="product.qty > 1">{{ product.product_prices }}</span>\n\n\n\n					<span *ngIf="product.qty > 1" style="color:#32db64;font-weight: bold;margin-left: 1vh;" [innerHtml]="currencySymbol">{{ currencyName }}</span>\n\n\n\n					<span *ngIf="product.qty > 1" style="color:#32db64;font-weight: bold;">{{ (product.product_prices * product.qty).toFixed(2) }}</span>\n\n\n\n				</p>\n\n				<!-- <p>\n\n                    <b>Total:</b>\n\n                    <span [innerHtml]="currencySymbol">{{ currencyName }}</span>\n\n                    {{ (product.product_prices * product.qty).toFixed(2) }}\n\n                </p> -->\n\n				<div item-end>\n\n					<button style="float: right;" color="danger" ion-button clear data-id="{{ product.product_id }}" (click)="removeItem(product.product_id)"><ion-icon name="close"></ion-icon></button><br>\n\n					<div style="float: right;display: -webkit-inline-box;margin-top: 3vh;">\n\n						<!--<ion-icon (click)="addProductToCart(product,\'remove\')" style="padding-left: 4px; padding-top: 1px;width:20px;height:20px;border: 1px solid;border-radius: 50%;font-size: 1em;" name="remove" class="text-bold"></ion-icon>-->\n\n						<ion-icon (click)="addProductToCart(product,\'remove\')" style="" name="remove" class="view-cart-qty-minus" ></ion-icon>\n\n						<p style="margin: 0.1em 1em;" class="text-bold">{{ product.qty }}</p>\n\n						<ion-icon (click)="addProductToCart(product,\'add\')" color="secondary" class="view-cart-qty-plus" style="" name="add"></ion-icon>\n\n					</div>\n\n				</div>\n\n			</ion-item>\n\n\n\n			<!-- <ion-item style="border: 1px solid #eee; padding: 1px;margin: 1vh auto;">\n\n                <ion-thumbnail item-start>\n\n                  <img src="assets/imgs/slide3.png">\n\n                </ion-thumbnail>\n\n                <h2>Product 2</h2>\n\n                <br>\n\n                <p>Hayao Miyazaki • 1988</p>\n\n                <div item-end>\n\n                    <button style="float: right;" color="danger" ion-button clear><ion-icon name="close"></ion-icon></button><br>\n\n\n\n                    <div style="float: right;display: -webkit-inline-box;margin-top: 3vh;">\n\n                        <ion-icon (click)="subQty(2)" style="padding-left: 4px; padding-top: 1px;width:20px;height:20px;border: 1px solid;border-radius: 50%;font-size: 1em;" name="remove"></ion-icon>\n\n                        <p style="margin: 0.1em 1em;">{{ qty2 }}</p>\n\n                        <ion-icon (click)="addQty(2)" color="secondary" style="padding-left: 4px; padding-top: 1px;width:20px;height:20px;border: 1px solid;border-radius: 50%;font-size: 1em;" name="add"></ion-icon>\n\n                    </div>\n\n\n\n                </div>\n\n              </ion-item>\n\n\n\n              <ion-item style="border: 1px solid #eee; padding: 1px;margin: 1vh auto;">\n\n                <ion-thumbnail item-start>\n\n                  <img src="assets/imgs/slide3.png">\n\n                </ion-thumbnail>\n\n                <h2>Product 3</h2>\n\n                <br>\n\n                <p>Hayao Miyazaki • 1988</p>\n\n                <div item-end>\n\n                    <button style="float: right;" color="danger" ion-button clear><ion-icon name="close"></ion-icon></button><br>\n\n\n\n                    <div style="float: right;display: -webkit-inline-box;margin-top: 3vh;">\n\n                        <ion-icon (click)="subQty(3)" style="padding-left: 4px; padding-top: 1px;width:20px;height:20px;border: 1px solid;border-radius: 50%;font-size: 1em;" name="remove"></ion-icon>\n\n                        <p style="margin: 0.1em 1em;">{{ qty3 }}</p>\n\n                        <ion-icon (click)="addQty(3)" color="secondary" style="padding-left: 4px; padding-top: 1px;width:20px;height:20px;border: 1px solid;border-radius: 50%;font-size: 1em;" name="add"></ion-icon>\n\n                    </div>\n\n\n\n                </div>\n\n              </ion-item> -->\n\n		</ion-list>\n\n\n\n		<ion-grid style="margin-bottom: 10vh;">\n\n            <div class="fixed-outside view-cart-pc-text"  align="center">\n\n                <h5 class="view-cart-pc-text2 view-cart-sel-pro" align="center">If you have any promo code <a class="text-green-fam" (click)="showPromocodePrompt()">click here</a></h5>\n\n            </div>\n\n			<ion-row>\n\n				<ion-col col-6>\n\n					<h5 item-start class="text-navyblue-fam">Subtotal:</h5>\n\n					<h5 item-start class="text-navyblue-fam">Services Fees:</h5>\n\n					<h5 item-start class="text-navyblue-fam">Promo Code:</h5>\n\n				</ion-col>\n\n				<ion-col col-6>\n\n					<h5 item-start align="right" class="text-navyblue-fam">\n\n                        {{ cartTotal }}<span [innerHtml]="currencySymbol" >{{ currencyName }}</span>\n\n\n\n					</h5>\n\n					<h5 item-start align="right" class="text-navyblue-fam">\n\n                        {{ serviceFee }}<span [innerHtml]="currencySymbol" >{{ currencyName }}</span>\n\n\n\n					</h5>\n\n					<h5 item-start align="right" class="text-red" > -\n\n                        {{ promoDiscount }}<span [innerHtml]="currencySymbol" >{{ currencyName }}</span>\n\n					</h5>\n\n				</ion-col>\n\n			</ion-row>\n\n		</ion-grid>\n\n\n\n		<!-- <div>\n\n            <ion-grid>\n\n                <ion-row>\n\n                    <ion-col col-8>\n\n                        <h4>You have <a>3 Items</a> In your cart.</h4>\n\n                    </ion-col>\n\n                    <ion-col col-4>\n\n                        <button ion-button color="secondary" (click)="checkoutOrder()">\n\n                            Checkout\n\n                        </button>\n\n                    </ion-col>\n\n                </ion-row>\n\n            </ion-grid>\n\n\n\n            <ion-list>\n\n                <ion-card>\n\n                    <img src="assets/imgs/slide3.png"/>\n\n                    <ion-card-content>\n\n                        <ion-card-title>\n\n                            <ion-grid>\n\n                                <ion-row>\n\n                                    <ion-col col-10>\n\n                                        Product 1\n\n                                    </ion-col>\n\n                                    <ion-col col-2 style="text-align: -webkit-center;">\n\n                                        <span style="text-align: center;font-size: 14px;">\n\n                                            <ion-icon name="trash"></ion-icon>\n\n                                        </span>\n\n                                    </ion-col>\n\n                                </ion-row>\n\n                            </ion-grid>\n\n                        </ion-card-title>\n\n                        <p>\n\n                            The most popular industrial group ever, and largely\n\n                            responsible for bringing the music to a mass audience.\n\n                        </p>\n\n                        <ion-grid>\n\n                            <ion-row>\n\n                                <ion-col col-6 style="display: flex;">\n\n                                    <ion-icon (click)="subQty(1)" name="remove" style="margin: 1.4vh;"></ion-icon>\n\n                                    <ion-input type="number" name="qty1" [(ngModel)]="qty1" style="padding: 0vh 1vh;background: gray;max-width: 30%;color: #FFF;"></ion-input>\n\n                                    <ion-icon (click)="addQty(1)" name="add" style="margin: 1.4vh;"></ion-icon>\n\n                                </ion-col>\n\n                                <ion-col col-6 item-end style="text-align: -webkit-right;">\n\n                                    <span>Price: $19.88</span><br>\n\n                                    <span>Total: $19.88</span>\n\n                                </ion-col>\n\n                            </ion-row>\n\n                        </ion-grid>\n\n                    </ion-card-content>\n\n                </ion-card>\n\n\n\n                <ion-card>\n\n                    <img src="assets/imgs/slide3.png"/>\n\n                    <ion-card-content>\n\n                        <ion-card-title>\n\n                            <ion-grid>\n\n                                <ion-row>\n\n                                    <ion-col col-10>\n\n                                        Product 2\n\n                                    </ion-col>\n\n                                    <ion-col col-2 style="text-align: -webkit-center;">\n\n                                        <span style="text-align: center;font-size: 14px;">\n\n                                            <ion-icon name="trash"></ion-icon>\n\n                                        </span>\n\n                                    </ion-col>\n\n                                </ion-row>\n\n                            </ion-grid>\n\n                        </ion-card-title>\n\n                        <p>\n\n                            The most popular industrial group ever, and largely\n\n                            responsible for bringing the music to a mass audience.\n\n                        </p>\n\n                        <ion-grid>\n\n                            <ion-row>\n\n                                <ion-col col-6 style="display: flex;">\n\n                                    <ion-icon (click)="subQty(2)" name="remove" style="margin: 1.4vh;"></ion-icon>\n\n                                    <ion-input type="number" name="qty2" [(ngModel)]="qty2" style="padding: 0vh 1vh;background: gray;max-width: 30%;color: #FFF;"></ion-input>\n\n                                    <ion-icon (click)="addQty(2)" name="add" style="margin: 1.4vh;"></ion-icon>\n\n                                </ion-col>\n\n                                <ion-col col-6 item-end style="text-align: -webkit-right;">\n\n                                    <span>Price: $12.98</span><br>\n\n                                    <span>Total: $12.98</span>\n\n                                </ion-col>\n\n                            </ion-row>\n\n                        </ion-grid>\n\n                    </ion-card-content>\n\n                </ion-card>\n\n\n\n                <ion-card>\n\n                    <img src="assets/imgs/slide3.png"/>\n\n                    <ion-card-content>\n\n                        <ion-card-title>\n\n                            <ion-grid>\n\n                                <ion-row>\n\n                                    <ion-col col-10>\n\n                                        Product 3\n\n                                    </ion-col>\n\n                                    <ion-col col-2 style="text-align: -webkit-center;">\n\n                                        <span style="text-align: center;font-size: 14px;">\n\n                                            <ion-icon name="trash"></ion-icon>\n\n                                        </span>\n\n                                    </ion-col>\n\n                                </ion-row>\n\n                            </ion-grid>\n\n                        </ion-card-title>\n\n                        <p>\n\n                            The most popular industrial group ever, and largely\n\n                            responsible for bringing the music to a mass audience.\n\n                        </p>\n\n                        <ion-grid>\n\n                            <ion-row>\n\n                                <ion-col col-6 style="display: flex;">\n\n                                    <ion-icon (click)="subQty(3)" name="remove" style="margin: 1.4vh;"></ion-icon>\n\n                                    <ion-input type="number" name="qty3" [(ngModel)]="qty3" style="padding: 0vh 1vh;background: gray;max-width: 30%;color: #FFF;"></ion-input>\n\n                                    <ion-icon (click)="addQty(3)" name="add" style="margin: 1.4vh;"></ion-icon>\n\n                                </ion-col>\n\n                                <ion-col col-6 item-end style="text-align: -webkit-right;">\n\n                                    <span>Price: $198.8</span><br>\n\n                                    <span>Total: $198.8</span>\n\n                                </ion-col>\n\n                            </ion-row>\n\n                        </ion-grid>\n\n                    </ion-card-content>\n\n                </ion-card>\n\n            </ion-list>\n\n\n\n            <hr>\n\n\n\n            <ion-grid>\n\n                <ion-row>\n\n                    <ion-col col-6>\n\n                        <h5 item-start>Subtotal</h5>\n\n                    </ion-col>\n\n                    <ion-col col-6>\n\n                        <h5 item-start align="right">$231.60</h5>\n\n                    </ion-col>\n\n                </ion-row>\n\n            </ion-grid>\n\n\n\n            <hr>\n\n            <ion-grid>\n\n                <ion-row>\n\n                    <ion-col col-6>\n\n                        <h5 item-start>Shipping</h5>\n\n                    </ion-col>\n\n                    <ion-col col-6>\n\n                        <h5 item-start align="right">$8.40</h5>\n\n                    </ion-col>\n\n                </ion-row>\n\n            </ion-grid>\n\n\n\n            <hr>\n\n\n\n            <ion-grid>\n\n                <ion-row>\n\n                    <ion-col col-6>\n\n                        <h3 item-start>Total</h3>\n\n                    </ion-col>\n\n                    <ion-col col-6>\n\n                        <h3 item-start align="right">$240.00</h3>\n\n                    </ion-col>\n\n                </ion-row>\n\n            </ion-grid>\n\n\n\n        </div> -->\n\n\n\n\n\n		<!--<div class="fixed-outside" style="position: fixed;bottom: 0;left: 0;width: 100%;display: inline-flex;" align="center">-->\n\n\n\n		<div class="fixed-outside" style="position: fixed;bottom: 0;left: 0;width: 100%;display: inline-flex;" align="center">\n\n			<button class="button-md-height" color="secondary" ion-button block style="background: #3b5998;height: 2.1em;width:50%;margin-bottom: 0px;text-transform: none;font-size: 20px;font-weight: bold;">Total: <span [innerHtml]="currencySymbol">{{ currencyName }}</span> {{ totalPrice }}</button>\n\n			<!--<button class="button-md-height" color="secondary" ion-button icon-right block style="height: 2.1em;width:100%;margin-bottom: 0px;text-transform: none;font-size: 20px;font-weight: bold;" (click)="checkoutOrder()">Checkout <ion-icon name="arrow-forward"></ion-icon></button>-->\n\n            <button class="button-md-height" color="secondary" ion-button icon-right block style="height: 2.1em;width:50%;margin-bottom: 0px;text-transform: none;font-size: 20px;font-weight: bold;" (click)="checkoutOrder()">Checkout\n\n                <ion-icon name="arrow-forward" icon-right align="right"></ion-icon>\n\n            </button>\n\n		</div>\n\n\n\n\n\n	</div>\n\n\n\n</ion-content>'/*ion-inline-end:"D:\app\familov_app\src\pages\my-cart\my-cart.html"*/
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */]) === "function" && _c || Object])
], MyCartPage);

var _a, _b, _c;
//# sourceMappingURL=my-cart.js.map

/***/ }),

/***/ 57:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__signup_signup__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__forgot_password_forgot_password__ = __webpack_require__(207);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






//import { MyCartPage } from '../my-cart/my-cart';
var LoginPage = (function () {
    function LoginPage(navCtrl, restProvider) {
        this.navCtrl = navCtrl;
        this.restProvider = restProvider;
        this.loginData = {};
    }
    LoginPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    LoginPage.prototype.goToHome = function (params) {
        if (!params)
            params = {};
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */]);
    };
    LoginPage.prototype.goToSignup = function (params) {
        if (!params)
            params = {};
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__signup_signup__["a" /* SignupPage */]);
    };
    LoginPage.prototype.goToForgotPass = function (params) {
        if (!params)
            params = {};
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__forgot_password_forgot_password__["a" /* ForgotPasswordPage */]);
    };
    //TODO-VIX
    /*openCartPage() {
     this.navCtrl.push(MyCartPage);
     }*/
    LoginPage.prototype.doLogin = function () {
        var _this = this;
        var tempMsg = '';
        if (this.loginData.email == "" || this.loginData.email == undefined) {
            //this.restProvider.showErrorMsg(this.restProvider.statusError,"Email is required");
            //return false;
            tempMsg += this.restProvider.errorString("Email is required", tempMsg);
        }
        if (this.loginData.password == "" || this.loginData.password == undefined) {
            /*this.restProvider.showErrorMsg(this.restProvider.statusError,"Password is required");
             return false;*/
            tempMsg += this.restProvider.errorString("Password is required", tempMsg);
        }
        if (tempMsg != '') {
            this.restProvider.showErrorMsg(this.restProvider.statusError, tempMsg);
            return false;
        }
        this.restProvider.LoadingSpinner();
        this.restProvider.loginUser(this.loginData)
            .then(function (result) {
            //this.restProvider.hideLoadingSpinner();
            // alert('result');
            if (result['status'] === true) {
                localStorage.setItem('LoginUser', JSON.stringify(result['response_data']));
                localStorage.setItem('LoginMsg', result['message']);
                //this.restProvider.showSuccessMsg(this.restProvider.statusSuccess,result['message']);
                //localStorage.setItem('LoginUser',JSON.stringify(result['response_data']));
                location.reload();
                //this.navCtrl.setRoot(HomePage);
                setTimeout(function () {
                    //this.restProvider.showToast(result['message'],'toast-success');
                    // this.goToHome();
                    //location.reload();
                }, 500);
            }
            else {
                _this.restProvider.hideLoadingSpinner();
                _this.restProvider.showErrorMsg(_this.restProvider.statusError, result['message']);
            }
        }, function (err) {
            _this.restProvider.hideLoadingSpinner();
            _this.restProvider.showErrorMsg(_this.restProvider.statusError, _this.restProvider.statusErrorMessage);
        });
    };
    return LoginPage;
}());
LoginPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-login',template:/*ion-inline-start:"D:\app\familov_app\src\pages\login\login.html"*/'<!-- <ion-header style="background: #FFF;">\n\n	<ion-navbar>\n\n		<button ion-button menuToggle class="buttonWhiteFont">\n\n			<ion-icon name="menu"></ion-icon>\n\n		</button>\n\n		<ion-title>\n\n			<ion-grid>\n\n	    		<ion-row>\n\n	    			<ion-col col-8 class="headerTitle">Login</ion-col>\n\n	    			<ion-col col-4><ion-icon class="right" name="cart" item-end (click)="openCartPage()"></ion-icon></ion-col>\n\n	    		</ion-row>\n\n	    	</ion-grid>\n\n		</ion-title>\n\n	</ion-navbar>\n\n</ion-header> -->\n\n\n\n<ion-content padding id="page5">\n\n\n\n	<ion-grid>\n\n		<ion-row>\n\n			<ion-col col-2>\n\n				<ion-icon color="secondary" name="arrow-back" (click)="goBack()" style="margin-top: 2vh;"></ion-icon>\n\n			</ion-col>\n\n			<ion-col col-8>\n\n				<img src="assets/imgs/familov-logo.png" style="max-height: 10vh;">\n\n			</ion-col>\n\n		</ion-row>\n\n	</ion-grid>\n\n\n\n	<div class="spacer" style="height:20px;" id="login-spacer1"></div>\n\n	<!-- <h2 align="center">Hi. Great to meet you.!</h2> -->\n\n	<div align="center"><img src="assets/imgs/laugh.png" style="max-height: 10vh;" /></div>\n\n	<!-- <br> -->\n\n	<h2 align="center" class="textBlue">Good to see you again!</h2>\n\n	<br>\n\n	<!-- <div class="spacer" style="height:20px;" id="login-spacer2"></div> -->\n\n\n\n	<form id="login-form1" (ngSubmit)="doLogin()">\n\n		<ion-list id="login-list1">\n\n			<ion-item class="item80" id="login-input1" style="border: 1px solid #eee;margin: 1vh auto;">\n\n				<ion-label>\n\n					<ion-icon name="person" color="light"></ion-icon>\n\n				</ion-label>\n\n				<ion-input type="email" placeholder="E-mail" [(ngModel)]="loginData.email" name="email"></ion-input>\n\n			</ion-item>\n\n			<ion-item class="item80" id="login-input2" style="border: 1px solid #eee;margin: 1vh auto;">\n\n				<ion-label>\n\n					<ion-icon name="lock" color="light"></ion-icon>\n\n				</ion-label>\n\n				<ion-input type="password" placeholder="Your Password" [(ngModel)]="loginData.password" name="password"></ion-input>\n\n			</ion-item>\n\n		</ion-list>\n\n\n\n		<div align="center" style="margin-top: 1vh;height: 5vh;">\n\n			<div class="item80">\n\n				<div style="float: left;">\n\n					<ion-checkbox color="secondary" checked="false" style="position: absolute;"></ion-checkbox>\n\n					<span class="remember-me">Remember Me</span>\n\n				</div>\n\n				<div class="forgot-password-link" id="forgotPassword" (click)="goToForgotPass()"><span>Forgot password?</span></div>\n\n			</div>\n\n		</div>\n\n		<div align="center">\n\n			<button class="item80 login-button" ion-button icon-end color="secondary" style="height: 4rem;" type="submit">\n\n				Login\n\n				<ion-icon name="arrow-round-forward"></ion-icon>\n\n			</button>\n\n		</div>\n\n\n\n		<br>\n\n		<p align="center">\n\n			<b class="textBlue">Not Registered?</b>\n\n			<a color="secondary" ion-button clear (click)="goToSignup()" style="margin-top: -3px;text-transform: none;">\n\n				Sign up here\n\n			</a>\n\n		</p>		\n\n\n\n	</form>\n\n</ion-content>'/*ion-inline-end:"D:\app\familov_app\src\pages\login\login.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */]])
], LoginPage);

//# sourceMappingURL=login.js.map

/***/ })

},[208]);
//# sourceMappingURL=main.js.map
webpackJsonp([0],{

/***/ 107:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__product_details_product_details__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__my_cart_my_cart__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_rest_rest__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ProductsPage = (function () {
    function ProductsPage(navCtrl, navParams, restProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restProvider = restProvider;
        /*this.restProvider.LoadingSpinner();

        this.curPage = 1;
        this.totalPage = 1;
        this.isPaginationScroll = false;
        this.isValidateShow = true;

        // this.currencySymbol = '$';
        // this.currencyName = 'USD';
        this.currencySymbol = '$';
        this.currencyName = 'EUR';

        this.IsFilter = false;

        this.all_total_products = 0;
        this.shop_info = [];
        this.shop_info.shop_name = '';

        this.cartItemsLen = 0;
        this.showSearchBar = false;
        this.searchData = this.navParams.get('searchData');
        
        this.noMoreItemsAvailable = false;

        this.currencyData = localStorage.getItem('currencyData');
        if (this.currencyData == undefined || this.currencyData == null) {
            this.currencyData = [];
            this.currencySymbol = '$';
            this.currencyName = 'EUR';
        }
        else {
            this.currencyData = JSON.parse(this.currencyData);
            this.currencySymbol = this.currencyData.vSymbol;
            this.currencyName = this.currencyData.vName;
        }

        this.searchData.currency = this.currencyName;
        
        console.log(this.currencySymbol);
        // console.log(this.currencyName)

        this.cartItems = localStorage.getItem('cartItems');
        if (this.cartItems == undefined || this.cartItems == null) {
            this.cartItems = [];
        }
        else {
            this.cartItems = JSON.parse(this.cartItems);
            this.cartItemsLen = this.cartItems.length;
        }

        this.searchData.page = this.curPage;
        this.searchProducts(this.searchData);
        this.restProvider.hideLoadingSpinner();*/
    }
    ProductsPage.prototype.ionViewCanEnter = function () {
        this.restProvider.LoadingSpinner();
        this.curPage = 1;
        this.totalPage = 1;
        this.isPaginationScroll = false;
        this.isValidateShow = true;
        // this.currencySymbol = '$';
        // this.currencyName = 'USD';
        this.currencySymbol = '$';
        this.currencyName = 'EUR';
        this.IsFilter = false;
        this.all_total_products = 0;
        this.shop_info = [];
        this.shop_info.shop_name = '';
        this.cartItemsLen = 0;
        this.showSearchBar = false;
        this.searchData = this.navParams.get('searchData');
        this.noMoreItemsAvailable = false;
        this.currencyData = localStorage.getItem('currencyData');
        if (this.currencyData == undefined || this.currencyData == null) {
            this.currencyData = [];
            this.currencySymbol = '$';
            this.currencyName = 'EUR';
        }
        else {
            this.currencyData = JSON.parse(this.currencyData);
            this.currencySymbol = this.currencyData.vSymbol;
            this.currencyName = this.currencyData.vName;
        }
        this.searchData.currency = this.currencyName;
        // console.log(this.currencySymbol);
        // console.log(this.currencyName)
        this.cartItems = localStorage.getItem('cartItems');
        if (this.cartItems == undefined || this.cartItems == null) {
            this.cartItems = [];
        }
        else {
            this.cartItems = JSON.parse(this.cartItems);
            this.cartItemsLen = this.cartItems.length;
        }
        this.searchData.page = this.curPage;
        this.searchProducts(this.searchData);
        this.restProvider.hideLoadingSpinner();
    };
    ProductsPage.prototype.showSearch = function () {
        this.showSearchBar = true;
        document.getElementById('searchGrid').style.marginTop = '10vh';
    };
    ProductsPage.prototype.hideSearch = function () {
        this.showSearchBar = false;
        document.getElementById('searchGrid').style.marginTop = '1vh';
    };
    /*loadMore() {
        this.product_list.push({ id: $scope.items.length});
   
        if ($scope.items.length == 20) {
            $scope.noMoreItemsAvailable = true;
        }

        console.log('scroll.infiniteScrollComplete');
    }*/
    ProductsPage.prototype.viewProductDetails = function (product) {
        console.log(product);
        var params = {};
        params.productDetails = product;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__product_details_product_details__["a" /* ProductDetailsPage */], params);
    };
    ProductsPage.prototype.openCartPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__my_cart_my_cart__["a" /* MyCartPage */]);
    };
    ProductsPage.prototype.filterProducts = function (text) {
        /*// alert(text);
        if (text.length > 0) {
            var exist = this.all_product_list.filter(function (product) { return product.product_name.toLowerCase().indexOf(text.toLowerCase()) != -1 });
            console.log(exist);
            this.product_list = exist;

            this.IsFilter = true;
            var selector = document.getElementById("cat_id_0").click();
        }
        else {
            this.IsFilter = false;
            var selector = document.getElementById("cat_id_0").click();
        }*/
        if (text.length > 0) {
            this.searchData.searchText = text;
            // this.searchProducts();
        }
        else {
            this.searchData.searchText = null;
        }
        this.curPage = 1;
        this.searchData.page = this.curPage;
        document.getElementById("cat_id_0").click();
        this.searchProducts();
    };
    ProductsPage.prototype.filterProductByCategory = function (category_id) {
        // if (category_id > 0) {
        this.searchData.category_id = category_id;
        this.curPage = 1;
        this.searchData.page = this.curPage;
        var elems = document.querySelectorAll(".ion-scroll-hor .selectedCategory");
        [].forEach.call(elems, function (el) {
            el.classList.remove("selectedCategory");
        });
        this.searchProducts(this.searchData);
        setTimeout(function () {
            var selector = document.getElementById("cat_id_" + (category_id == '-1' ? '0' : category_id));
            selector.classList.add('selectedCategory');
        }, 200);
        // document.getElementsByClassName('ion-card-scroll');
        // selectedCategory
        /*this.product_list = [];
        if(this.IsFilter == true) {
            for (var i = 0; i < this.product_list.length; i++) {
                // console.log(this.product_list[i].category_id);
                if (this.product_list[i].category_id == category_id) {
                    this.product_list.push(this.product_list[i]);
                }
            }
        }
        else {
            for (var i = 0; i < this.all_product_list.length; i++) {
                // console.log(this.all_product_list[i].category_id);
                if (this.all_product_list[i].category_id == category_id) {
                    this.product_list.push(this.all_product_list[i]);
                }
            }
        }*/
        // }
        /*else {

            var elems = document.querySelectorAll(".ion-scroll-hor .selectedCategory");
            [].forEach.call(elems, function(el) {
                el.classList.remove("selectedCategory");
            });
            var selector = document.getElementById("cat_id_0");
            selector.classList.add('selectedCategory');

            if (this.IsFilter == false) {
                this.product_list = this.all_product_list;
            }
        }*/
    };
    ProductsPage.prototype.addProductToCart = function (product) {
        if (this.cartItems != null && this.cartItems != undefined && this.cartItems.length > 0) {
            var exist = this.cartItems.filter(function (cartItem) { return cartItem.product_id == product.product_id; });
            if (exist.length > 0) {
                for (var i = 0; i < this.cartItems.length; ++i) {
                    if (this.cartItems[i].product_id == product.product_id) {
                        this.cartItems[i].qty += 1;
                    }
                }
                localStorage.setItem('cartItems', JSON.stringify(this.cartItems));
            }
            else {
                product.qty = 1;
                this.cartItems.push(product);
                localStorage.setItem('cartItems', JSON.stringify(this.cartItems));
            }
        }
        else {
            product.qty = 1;
            this.cartItems = [];
            this.cartItems.push(product);
            localStorage.setItem('cartItems', JSON.stringify(this.cartItems));
        }
        this.cartItemsLen = this.cartItems.length;
        this.restProvider.showSuccessMsg('Success', 'Item added in cart successfully.');
    };
    ProductsPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        this.isValidateShow = false;
        // console.log('Begin async operation');
        // alert(this.curPage);
        // alert(this.totalPage);
        if (this.curPage < this.totalPage) {
            this.curPage = parseInt(this.curPage) + 1;
            this.searchData.page = this.curPage;
            this.searchProducts(this.searchData);
        }
        else {
            if (this.curPage > 1 && this.all_total_products <= this.product_list.length) {
                infiniteScroll.enable(false);
            }
        }
        setTimeout(function () {
            _this.isPaginationScroll = true;
            // console.log('curPage = ' + this.curPage);
            // console.log('totalPage = ' + this.totalPage);
            /*if (this.curPage <= this.totalPage) {
                this.curPage = parseInt(this.curPage) + 1;
                this.searchData.page = this.curPage;
                this.searchProducts(this.searchData);
            }
            else {
                infiniteScroll.enable(false);
            }*/
            // console.log('Async operation has ended');
            infiniteScroll.complete();
            _this.isValidateShow = true;
        }, 1000);
    };
    ProductsPage.prototype.searchProducts = function (params) {
        // var searchData = {};
        // searchData.country_id = this.receiver_country;
        // searchData.city_id = this.receiver_city;
        // searchData.shop_id = this.receiver_shop;
        // console.log(this.searchData);
        var _this = this;
        this.restProvider.getProducts(this.searchData)
            .then(function (result) {
            // console.log(result);
            if (result.status == true) {
                _this.all_total_products = result.data.all_total_products;
                _this.totalPage = Math.ceil(result.data.all_total_products / 12);
                console.log(_this.totalPage);
                if (result.data.all_total_products > 0) {
                    if (result.data.total_products > 0) {
                        if (_this.isPaginationScroll == true) {
                            for (var i = 0; i < result.data.product_list.length; i++) {
                                _this.isPaginationScroll = false;
                                // this.all_product_list.push(result.data.product_list[i]);
                                _this.product_list.push(result.data.product_list[i]);
                            }
                        }
                        else {
                            // this.all_product_list = result.data.product_list;
                            _this.product_list = result.data.product_list;
                        }
                        _this.category_list = result.data.category_list;
                        _this.shop_info = result.data.shop_info;
                    }
                    // console.log(this.shop_info.shop_name);
                    // var selector = document.getElementById("cat_id_"+(this.searchData.category_id == '-1' ? '0' : this.searchData.category_id));
                    // selector.classList.add('selectedCategory');
                }
                else {
                    // this.all_product_list = [];
                    _this.product_list = [];
                    // this.restProvider.showErrorMsg(this.statusError,'Opps..!! <br> No products found in selected area.!!!');
                }
            }
            else {
                if (typeof result.message == "string") {
                    _this.restProvider.showErrorMsg(_this.statusError, result.message);
                }
                else {
                    _this.restProvider.showErrorMsg(_this.statusError, 'Opps..!! <br> Something went wrong.');
                }
            }
        }, function (err) {
            // console.log(err);
            _this.restProvider.showErrorMsg(_this.statusError, 'Opps..!! <br> Something went wrong.');
        });
        return false;
    };
    ProductsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-products',template:/*ion-inline-start:"E:\Projects\IonicBackendBrains\FamilovApp\src\pages\products\products.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle class="buttonWhiteFont">\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>\n      <ion-grid>\n        <ion-row>\n          <ion-col col-8 class="headerTitle"><div align="center">Shop</div></ion-col>\n          <ion-col col-2>\n            <ion-icon name="search" *ngIf="!showSearchBar" item-end (click)="showSearch()"></ion-icon>\n            <ion-icon name="close" *ngIf="showSearchBar" item-end (click)="hideSearch()"></ion-icon>\n            </ion-col>\n            <ion-col col-2 class="badgeCol">\n            <!-- <ion-icon class="right" name="cart" item-end (click)="openCartPage()"></ion-icon> -->\n              <span class="badgeIcon"> {{ cartItemsLen }}</span>\n            <ion-icon name="cart" item-end (click)="openCartPage()"></ion-icon>\n            </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content padding id="page3" class="grayBackground">\n	 \n    <div style="background: #FFF" style="position: fixed; width: 100%; background: #FFF; z-index: 99999999;">\n      <ion-input *ngIf="showSearchBar" (change)="filterProducts($event.target.value)" type="text" placeholder="Search" style="margin-top: 1vh;border-bottom: 1px solid #eee;"></ion-input>\n    </div>\n\n    <ion-grid id="searchGrid">\n      <ion-row>\n        <ion-col col-3>\n          <div align="center"><img src="assets/imgs/shopbanner-icon.png" style="max-height: 8vh;" /></div>\n        </ion-col>\n        <ion-col col-9>\n          <h4 class="textBlue" style="font-weight: bold;margin-bottom: 2px;margin-top: 1vh;">{{ shop_info.shop_name }}</h4>\n          <!-- <span>TEST Shop name display</span> -->\n        </ion-col>\n      </ion-row>\n    </ion-grid> \n\n\n        <hr>\n        <ion-scroll scrollX="true" direction="x" class="ion-scroll-hor" style="margin-top: 2vh;margin-bottom: 2vh;">\n       \n\n        <button clear class="ion-card-scroll selectedCategory" color="secondary" (click)="filterProductByCategory(-1)" id="cat_id_0" style="margin-top: 1px;color: lightgray;">\n              All Products\n        </button>\n\n        <button clear class="ion-card-scroll" *ngFor="let category of category_list" color="secondary" (click)="filterProductByCategory(category.category_id)" id="cat_id_{{category.category_id}}" style="margin-top: 1px;color: lightgray;">\n              {{ category.category_name }}\n        </button>\n\n\n       \n\n        </ion-scroll>\n\n        <!-- <hr> -->\n        \n  <div align="center">\n    <ion-grid style="margin-bottom: 5vh;" *ngIf="all_total_products > 0">\n    <ion-row>\n      <ion-col col-6 align="center" class="products" *ngFor="let product of product_list" style="background: #FFF;color: #000;">\n        	<img src="{{ product.product_image }}" class="proImg" (click)="viewProductDetails(product)">\n        	<br>\n          <p class="productTitle" (click)="viewProductDetails(product)">{{ product.product_name | slice:0:20 }} {{ product.product_name.length > 20 ? \'....\' : \'\' }} </p>\n        	<br><span class="priceLbl"> <span [innerHtml]="currencySymbol">{{ currencyName }}</span> {{ product.product_prices }}</span>\n        	<button color="secondary" class="moreDetails" ion-button (click)="addProductToCart(product)">Add to cart</button>\n      </ion-col>\n    </ion-row>\n    </ion-grid>\n  </div>\n\n<ion-infinite-scroll (ionInfinite)="doInfinite($event)" style="opacity: 1;z-index: 9999999;">\n   <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Loading more data..."></ion-infinite-scroll-content>\n </ion-infinite-scroll>\n\n<ion-grid style="margin-bottom: 5vh;" *ngIf="all_total_products == 0">\n  <ion-row>\n    <ion-col col-12 align="center" style="background: #FFF;color: #000;">\n        <img src="assets/imgs/tired.png" style="margin-top: 5vh;max-height: 15vh;">\n        <br>\n        <h1 style="color: midnightblue;">No products found.</h1>\n    </ion-col>\n  </ion-row>\n</ion-grid>\n\n<!-- <ion-infinite-scroll ng-if="!noMoreItemsAvailable" on-infinite="loadMore()" distance="10%"></ion-infinite-scroll> -->\n\n<div class="fixed-outside" style="position: fixed;bottom: 0;left: 0;width: 100%;" align="center" *ngIf="isValidateShow">\n  <button class="button-md-height" color="secondary" ion-button icon-right block style="margin-bottom: 0px;text-transform: none;height: 2.5em;">Validate <ion-icon name="arrow-forward"></ion-icon></button>\n</div>\n\n\n</ion-content>'/*ion-inline-end:"E:\Projects\IonicBackendBrains\FamilovApp\src\pages\products\products.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_4__providers_rest_rest__["a" /* RestProvider */]])
    ], ProductsPage);
    return ProductsPage;
}());

//# sourceMappingURL=products.js.map

/***/ }),

/***/ 108:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaymentPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__thankyou_thankyou__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__my_cart_my_cart__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_paypal__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_stripe__ = __webpack_require__(214);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var PaymentPage = (function () {
    function PaymentPage(navCtrl, payPal, navParams, platform, stripe) {
        // this.payment_method_credit = true;
        var _this = this;
        this.navCtrl = navCtrl;
        this.payPal = payPal;
        this.navParams = navParams;
        this.platform = platform;
        this.stripe = stripe;
        this.payment_method_credit = false;
        this.payment_method_paypal = false;
        this.payment_method_bank_transfer = false;
        this.cartItemsLen = 0;
        var paymentMethod = this.navParams.get('paymentMethod');
        if (paymentMethod == 'sofort_oberweisung') {
            paymentMethod = 'credit';
        }
        setTimeout(function () {
            if (paymentMethod != null && paymentMethod != undefined) {
                var showOpt = document.getElementById('type_' + paymentMethod);
                showOpt.classList.add('segment-activated');
                _this.showPaymentOption(paymentMethod);
            }
            else {
                var showOpt = document.getElementById('type_credit');
                showOpt.classList.add('segment-activated');
                _this.showPaymentOption('credit');
            }
        }, 200);
        this.cartItems = localStorage.getItem('cartItems');
        if (this.cartItems != undefined || this.cartItems != null) {
            this.cartItemsLen = JSON.parse(this.cartItems).length;
        }
        // this.payViaStripe();
        // this.payViaPaypal();
    }
    PaymentPage.prototype.orderConfirmed = function (params) {
        if (!params)
            params = {};
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__thankyou_thankyou__["a" /* ThankYouPage */]);
    };
    PaymentPage.prototype.showPaymentOption = function (type) {
        if (type == 'credit') {
            this.payment_method_credit = true;
            this.payment_method_paypal = false;
            this.payment_method_bank_transfer = false;
        }
        else if (type == 'paypal') {
            this.payment_method_credit = false;
            this.payment_method_paypal = true;
            this.payment_method_bank_transfer = false;
        }
        else if (type == 'bank_transfer') {
            this.payment_method_credit = false;
            this.payment_method_paypal = false;
            this.payment_method_bank_transfer = true;
        }
    };
    PaymentPage.prototype.openCartPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__my_cart_my_cart__["a" /* MyCartPage */]);
    };
    PaymentPage.prototype.payViaPaypal = function () {
        var _this = this;
        this.payPal.init({
            PayPalEnvironmentProduction: 'YOUR_PRODUCTION_CLIENT_ID',
            PayPalEnvironmentSandbox: 'AW8gW9FIU6hT1x9nLesEQsbdYKNTR-iPiYbta-XM4m0V9IdaQG2SCvYZIxr9ddhcSorRIJuIarbqNMGX' // $config['client_id'] = 'AW8gW9FIU6hT1x9nLesEQsbdYKNTR-iPiYbta-XM4m0V9IdaQG2SCvYZIxr9ddhcSorRIJuIarbqNMGX';
        })
            .then(function () {
            // Environments: PayPalEnvironmentNoNetwork, PayPalEnvironmentSandbox, PayPalEnvironmentProduction
            _this.payPal.prepareToRender('PayPalEnvironmentSandbox', new __WEBPACK_IMPORTED_MODULE_4__ionic_native_paypal__["b" /* PayPalConfiguration */]({}))
                .then(function () {
                var payment = new __WEBPACK_IMPORTED_MODULE_4__ionic_native_paypal__["c" /* PayPalPayment */]('3.33', 'USD', 'Description', 'sale');
                _this.payPal.renderSinglePaymentUI(payment)
                    .then(function (result) {
                    console.log(result);
                    // Successfully paid
                    // Example sandbox response
                    //
                    // {
                    //   "client": {
                    //     "environment": "sandbox",
                    //     "product_name": "PayPal iOS SDK",
                    //     "paypal_sdk_version": "2.16.0",
                    //     "platform": "iOS"
                    //   },
                    //   "response_type": "payment",
                    //   "response": {
                    //     "id": "PAY-1AB23456CD789012EF34GHIJ",
                    //     "state": "approved",
                    //     "create_time": "2016-10-03T13:33:33Z",
                    //     "intent": "sale"
                    //   }
                    // }
                }, function (error) {
                    console.log(error);
                    // Error or render dialog closed without being successful
                });
            }, function () {
                // Error in configuration
            });
        }, function () {
            // Error in initialization, maybe PayPal isn't supported or something else
        });
    };
    PaymentPage.prototype.payViaStripe = function () {
        var _this = this;
        this.stripe.setPublishableKey('pk_test_zJeMJhXgQCvHy1h6PevnfQZV');
        var card = {
            number: '4242424242424242',
            expMonth: 12,
            expYear: 2020,
            cvc: '220'
        };
        var tokenkey = '';
        this.platform.ready().then(function () {
            _this.stripe.createCardToken(card)
                .then(function (token) {
                console.log(token.id);
                tokenkey = token.id;
            })
                .catch(function (error) { return console.error(error); });
        });
        console.log(tokenkey);
    };
    PaymentPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-payment',template:/*ion-inline-start:"E:\Projects\IonicBackendBrains\FamilovApp\src\pages\payment\payment.html"*/'<ion-header>\n	<ion-navbar>\n		<button ion-button menuToggle class="buttonWhiteFont">\n			<ion-icon name="menu"></ion-icon>\n		</button>\n		<ion-title>\n			<ion-grid>\n    		<ion-row>\n    			<ion-col col-10 class="headerTitle">Confirm Payment</ion-col>\n    			<ion-col col-2 class="badgeCol">\n    				<!-- <ion-icon class="right" name="cart" item-end (click)="openCartPage()"></ion-icon> -->\n    				<span class="badgeIcon"> {{ cartItemsLen }}</span>\n					<ion-icon name="cart" item-end (click)="openCartPage()"></ion-icon>\n    			</ion-col>\n    		</ion-row>\n    	</ion-grid>\n		</ion-title>\n	</ion-navbar>\n</ion-header>\n<ion-content padding id="page5">\n\n	<!-- <div padding>\n		<ion-segment [(ngModel)]="cat">\n			<ion-segment-button value="men" class="segment-button segment-activated">\n				Men\n			</ion-segment-button>\n			<ion-segment-button value="women">\n				Women\n			</ion-segment-button>\n		</ion-segment>\n	</div> -->\n\n	<div class="bar bar-header bar-dark">\n		<ion-segment full="true" assertive>\n			<ion-segment-button id="type_credit" (click)="showPaymentOption(\'credit\')" value="credit">\n				Credit Card\n			</ion-segment-button>\n			<ion-segment-button id="type_paypal" (click)="showPaymentOption(\'paypal\')" value="paypal">\n				Paypal\n			</ion-segment-button>\n			<ion-segment-button id="type_bank_transfer" (click)="showPaymentOption(\'bank_transfer\')" value="bank_transfer">\n				Bank Transfer\n			</ion-segment-button>\n		</ion-segment>\n	</div>\n\n	<!-- <div [ngSwitch]="cat">\n		<ion-list *ngSwitchCase="\'men\'">\n			<ion-item>\n				<ion-thumbnail item-left>\n					<img src="img/men-image1.png">\n				</ion-thumbnail>\n				<h2>Cothing</h2>\n			</ion-item>\n			<ion-item>\n				<ion-thumbnail item-left>\n					<img src="img/men-image2.png">\n				</ion-thumbnail>\n				<h2>Watches</h2>\n			</ion-item>\n			<ion-item>\n				<ion-thumbnail item-left>\n					<img src="img/men-image3.png">\n				</ion-thumbnail>\n				<h2>Footwear</h2>\n			</ion-item>\n		</ion-list>\n\n		<ion-list *ngSwitchCase="\'women\'">\n			<ion-item>\n				<ion-thumbnail item-left>\n					<img src="img/women-image1.png">\n				</ion-thumbnail>\n				<h2>Cothing</h2>\n			</ion-item>\n			<ion-item>\n				<ion-thumbnail item-left>\n					<img src="img/women-image1.png">\n				</ion-thumbnail>\n				<h2>Watches</h2>\n			</ion-item>\n			<ion-item>\n				<ion-thumbnail item-left>\n					<img src="/img/women-image1.png">\n				</ion-thumbnail>\n				<h2>Footwear</h2>\n			</ion-item>\n		</ion-list>\n	</div> -->\n\n	<div style="padding: 1vh; margin: 1vh;">\n		<div *ngIf="payment_method_credit">\n			<div class="spacer" style="height:20px;" id="login-spacer1"></div>\n			<h2>Card Details</h2>\n			<span>Enter Your debit or credit card details.</span>\n			<div class="spacer" style="height:20px;" id="login-spacer2"></div>\n\n			<form id="login-form1">\n				<ion-list id="login-list1">\n\n					<ion-item id="login-input1" class="itemInput">\n						<ion-input type="number" placeholder="Card number" autofocus></ion-input>\n					</ion-item>\n					\n					<ion-item id="login-input1" class="itemInput">\n						<ion-input type="number" placeholder="Exp. Year"></ion-input>\n					</ion-item>\n					\n					<ion-item id="login-input1" class="itemInput">\n						<ion-input type="number" placeholder="Exp. Year"></ion-input>\n					</ion-item>\n					\n					<ion-item id="login-input2" class="itemInput">\n						<ion-input type="number" placeholder="Cvv"></ion-input>\n					</ion-item>\n\n					<ion-item id="login-input2">\n					</ion-item>\n\n				</ion-list>\n\n				<div class="spacer" style="height:15px;" id="login-spacer4"></div>\n\n				<button id="login-button1" ion-button color="secondary" block (click)="payViaStripe	()">\n					Place Order\n				</button>\n\n			</form>\n		</div>\n\n		<div *ngIf="payment_method_paypal">\n			<div class="spacer" style="height:20px;" id="login-spacer1"></div>\n			<h2>Paypal</h2>\n\n			<button id="login-button1" ion-button color="secondary" block (click)="payViaPaypal()">\n				Pay via paypal\n			</button>\n\n			<!-- <span>Enter Your paypal details and done payment using paypal.</span>\n			<div class="spacer" style="height:20px;" id="login-spacer2"></div>\n\n			<form id="login-form1">\n				<ion-list id="login-list1">\n\n					<ion-item id="login-input1" class="itemInput">\n						<ion-input type="Email" placeholder="Enter your paypal EmailId" autofocus></ion-input>\n					</ion-item>\n				</ion-list>\n\n				<div class="spacer" style="height:15px;" id="login-spacer4"></div>\n\n				<button id="login-button1" ion-button color="secondary" block (click)="orderConfirmed()">\n					Place Order\n				</button>\n\n			</form> -->\n		</div>\n\n		<div *ngIf="payment_method_bank_transfer">\n			<div class="spacer" style="height:20px;" id="login-spacer1"></div>\n			<h2>Bank Transfer</h2>\n			<span>Enter Your bank details and confirm your order.</span>\n			<div class="spacer" style="height:20px;" id="login-spacer2"></div>\n\n			<form id="login-form1">\n				<ion-list id="login-list1">\n\n					<ion-item id="login-input1" class="itemInput">\n						<ion-input type="text" placeholder="Bank account holder name" autofocus></ion-input>\n					</ion-item>\n\n					<ion-item id="login-input1" class="itemInput">\n						<ion-input type="number" placeholder="Bank account number" autofocus></ion-input>\n					</ion-item>\n					\n					<!-- <ion-item id="login-input1" class="itemInput">\n						<ion-input type="number" placeholder="Exp. Year"></ion-input>\n					</ion-item>\n					\n					<ion-item id="login-input1" class="itemInput">\n						<ion-input type="number" placeholder="Exp. Year"></ion-input>\n					</ion-item>\n					\n					<ion-item id="login-input2" class="itemInput">\n						<ion-input type="number" placeholder="Cvv"></ion-input>\n					</ion-item> -->\n				</ion-list>\n\n				<div class="spacer" style="height:15px;" id="login-spacer4"></div>\n\n				<button id="login-button1" ion-button color="secondary" block (click)="orderConfirmed()">\n					Place Order\n				</button>\n\n			</form>\n		</div>\n	</div>\n	\n	\n</ion-content>'/*ion-inline-end:"E:\Projects\IonicBackendBrains\FamilovApp\src\pages\payment\payment.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_paypal__["a" /* PayPal */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Platform */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_stripe__["a" /* Stripe */]])
    ], PaymentPage);
    return PaymentPage;
}());

//# sourceMappingURL=payment.js.map

/***/ }),

/***/ 109:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ThankYouPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__my_cart_my_cart__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ThankYouPage = (function () {
    function ThankYouPage(navCtrl) {
        this.navCtrl = navCtrl;
        this.cartItemsLen = 0;
        this.cartItems = localStorage.getItem('cartItems');
        if (this.cartItems != undefined || this.cartItems != null) {
            this.cartItemsLen = JSON.parse(this.cartItems).length;
        }
    }
    ThankYouPage.prototype.gotoHome = function (params) {
        if (!params)
            params = {};
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
    };
    ThankYouPage.prototype.openCartPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__my_cart_my_cart__["a" /* MyCartPage */]);
    };
    ThankYouPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-thankyou',template:/*ion-inline-start:"E:\Projects\IonicBackendBrains\FamilovApp\src\pages\thankyou\thankyou.html"*/'<ion-header>\n	<ion-navbar>\n		<button ion-button menuToggle class="buttonWhiteFont">\n			<ion-icon name="menu"></ion-icon>\n		</button>\n		<ion-title>\n			<ion-grid>\n    		<ion-row>\n    			<ion-col col-10 class="headerTitle"><div align="center">Thank You</div></ion-col>\n    			<ion-col col-2 class="badgeCol">\n    				<!-- <ion-icon class="right" name="cart" item-end (click)="openCartPage()"></ion-icon> -->\n    				<span class="badgeIcon"> {{ cartItemsLen }}</span>\n					<ion-icon name="cart" item-end (click)="openCartPage()"></ion-icon>\n    			</ion-col>\n    		</ion-row>\n    	</ion-grid>\n		</ion-title>\n	</ion-navbar>\n</ion-header>\n<ion-content padding id="page5">\n	\n	<div class="spacer" style="height:30px;" id="login-spacer2"></div>\n\n	<ion-card>\n		<div align="center" style="padding: 10px;">\n			<!-- <ion-icon name="checkmark-circle-outline" class="bigIcon"></ion-icon> -->\n			<!-- <h1>Thank You</h1> -->\n			<div align="center">\n				<img src="assets/imgs/party (1).png" style="max-height: 15vh;max-width: 15vh;">\n			</div>\n			<h1 style="font-weight: bold;">Youpppiii!</h1>\n			<br>\n			<!-- <h3>Your placed has been placed successfully.You can track your shipment with the order id below.</h3><br> -->\n			<h2 style="color: #051c46;font-weight: bold;">Your package for #ReceiverName is successfully sended !</h2><br>\n			<!-- <button id="login-button1" ion-button color="dark">\n				#ORD2017110201\n			</button> -->\n			<h3 style="color: #051c46">\n				CODE: XZU5H7L\n			</h3>\n			<br>\n\n			<h3>You have nothing more to do.After we receive the payment #Receiver will be contact within 36 hours for the delivery</h3><br>\n			<br>\n			<br>\n			<br>\n			<ion-icon name="cloud-download" style="font-size: 5em;"></ion-icon>\n			<br>\n			<!-- <h4 align="center" color="secondary">Send an another package</h4> -->\n			<button id="login-button1" ion-button color="secondary" clear block>\n				Send an another package\n			</button>\n		</div>		\n	</ion-card>\n\n	<div class="spacer" style="height:15px;" id="login-spacer4"></div>\n\n	<!-- <div align="center">\n		<button id="login-button1" ion-button color="default" round (click)="gotoHome()">\n			BACK TO SHOP\n		</button>\n	</div> -->\n\n</ion-content>'/*ion-inline-end:"E:\Projects\IonicBackendBrains\FamilovApp\src\pages\thankyou\thankyou.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]])
    ], ThankYouPage);
    return ThankYouPage;
}());

//# sourceMappingURL=thankyou.js.map

/***/ }),

/***/ 11:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyCartPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__checkout_checkout__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__products_products__ = __webpack_require__(107);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// import { PaymentPage } from '../payment/payment';


// import { MyCartPage } from '../my-cart/my-cart';
var MyCartPage = (function () {
    function MyCartPage(navCtrl, alertCtrl, restProvider) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.restProvider = restProvider;
        this.cartItemsLen = 0;
        this.selected_shop_id = 0;
        // this.qty1 = 1;
        // this.qty2 = 1;
        // this.qty3 = 1;
        this.cartItems = false;
        this.cartTotal = 0;
        this.serviceFee = 0;
        this.promoDiscount = 0;
        this.promoID = 0;
        var selected_shop = localStorage.getItem('selectedShopLoc');
        if (selected_shop != null && selected_shop != undefined) {
            this.selected_shop_id = JSON.parse(selected_shop).shop_id;
        }
        this.currencySymbol = '&euro;';
        this.currencyName = 'EUR';
        this.cartItems = localStorage.getItem('cartItems');
        if (this.cartItems == undefined || this.cartItems == null) {
            this.cartItems = [];
        }
        else {
            this.cartItems = JSON.parse(localStorage.getItem('cartItems'));
            this.cartItemsLen = this.cartItems.length;
            for (var i = 0; i < this.cartItems.length; ++i) {
                var total = (this.cartItems[i].product_prices * this.cartItems[i].qty).toFixed(2);
                this.cartTotal = (parseFloat(this.cartTotal) + parseFloat(total)).toFixed(2);
            }
        }
        this.currencyData = localStorage.getItem('currencyData');
        if (this.currencyData == undefined || this.currencyData == null) {
            this.currencyData = [];
            this.currencySymbol = '&euro;';
            this.currencyName = 'EUR';
        }
        else {
            this.currencyData = JSON.parse(this.currencyData);
            this.currencySymbol = this.currencyData.vSymbol;
            this.currencyName = this.currencyData.vName;
        }
        /*this.cartItems = localStorage.getItem('cartItems');
        if (this.cartItems != undefined || this.cartItems != null) {
          this.cartItemsLen = JSON.parse(this.cartItems).length;
        }*/
        this.totalPrice = (parseFloat(this.cartTotal) + parseFloat(this.serviceFee) + parseFloat(this.promoDiscount)).toFixed(2);
    }
    MyCartPage_1 = MyCartPage;
    MyCartPage.prototype.goToHome = function (params) {
        if (!params)
            params = {};
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */]);
    };
    MyCartPage.prototype.checkoutOrder = function (params) {
        var param = {};
        param.selected_shop_id = this.selected_shop_id;
        param.currency = this.currencyName;
        param.currencySymbol = this.currencySymbol;
        param.promoID = this.promoID;
        param.promoDiscount = this.promoDiscount;
        param.cartItems = this.cartItems;
        param.totalPrice = this.totalPrice;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__checkout_checkout__["a" /* CheckoutPage */], param);
        // if (!params) params = {};
        // this.navCtrl.push(CheckoutPage);
    };
    MyCartPage.prototype.removeItem = function (product_id) {
        // console.log(this.cartItems.length);
        // console.log(this.cartItems);
        var index = this.cartItems.map(function (e) { return e.product_id; }).indexOf('' + product_id);
        this.cartItems.splice(index, 1);
        // console.log(this.cartItems.length);
        // console.log(this.cartItems);
        var cart_total = 0;
        for (var i = 0; i < this.cartItems.length; ++i) {
            var total = (this.cartItems[i].product_prices * this.cartItems[i].qty).toFixed(2);
            cart_total = (parseFloat(cart_total) + parseFloat(total)).toFixed(2);
        }
        this.cartTotal = cart_total;
        this.totalPrice = (parseFloat(this.cartTotal) + parseFloat(this.serviceFee) + parseFloat(this.promoDiscount)).toFixed(2);
        this.cartItemsLen = this.cartItems.length;
        // alert(this.cartItemsLen);
        localStorage.setItem('cartItems', JSON.stringify(this.cartItems));
    };
    MyCartPage.prototype.addQty = function (no) {
        /*if (no == 1) {
            this.qty1++;
        }
        else if (no == 2) {
            this.qty2++;
        }
        else if (no == 3) {
            this.qty3++;
        }*/
    };
    MyCartPage.prototype.subQty = function (no) {
        /*if (no == 1) {
            if(this.qty1 > 1){
                this.qty1--;
            }
        }
        else if (no == 2) {
            if(this.qty2 > 1){
                this.qty2--;
            }
        }
        else if (no == 3) {
            if(this.qty3 > 1){
                this.qty3--;
            }
        }*/
    };
    MyCartPage.prototype.addProductToCart = function (product, type) {
        var exist = this.cartItems.filter(function (cartItem) { return cartItem.product_id == product.product_id; });
        var cart_total = 0;
        if (exist.length > 0) {
            for (var i = 0; i < this.cartItems.length; ++i) {
                if (this.cartItems[i].product_id == product.product_id) {
                    if (type == 'add') {
                        this.cartItems[i].qty++;
                    }
                    else {
                        if (this.cartItems[i].qty > 1) {
                            this.cartItems[i].qty--;
                        }
                    }
                }
                var total = (this.cartItems[i].product_prices * this.cartItems[i].qty).toFixed(2);
                cart_total = (parseFloat(cart_total) + parseFloat(total)).toFixed(2);
            }
            localStorage.setItem('cartItems', JSON.stringify(this.cartItems));
        }
        else {
            product.qty = 1;
            cart_total = product.product_prices;
            this.cartItems.push(product);
            localStorage.setItem('cartItems', JSON.stringify(this.cartItems));
        }
        this.cartTotal = cart_total;
        this.totalPrice = (parseFloat(this.cartTotal) + parseFloat(this.serviceFee) + parseFloat(this.promoDiscount)).toFixed(2);
    };
    MyCartPage.prototype.openCartPage = function () {
        this.navCtrl.setRoot(MyCartPage_1);
    };
    MyCartPage.prototype.showPromocodePrompt = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Promocode',
            message: "Enter promocode here and apply",
            inputs: [
                {
                    name: 'promo_code',
                    placeholder: 'promocode'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Save',
                    handler: function (data) {
                        console.log('Saved clicked');
                        _this.promocodeData = { 'promo_code': data.promo_code, 'cart_amount_total': _this.totalPrice, 'currency': _this.currencyName };
                        // console.log(this.promocodeData);
                        // console.log(this.totalPrice);
                        // return false;
                        _this.restProvider.LoadingSpinner();
                        _this.restProvider.applyPromocode(_this.promocodeData)
                            .then(function (result) {
                            console.log(result);
                            _this.restProvider.hideLoadingSpinner();
                            // alert('result');
                            if (result.status == true) {
                                _this.restProvider.showSuccessMsg(_this.statusSuccess, result.message);
                                _this.promoDiscount = parseFloat(result.data.promo_info.promo_code_amount);
                                _this.promoID = result.data.promo_info.promo_id;
                                // alert(this.promoDiscount);
                                // alert(this.promoID);
                                /*localStorage.setItem('LoginUser',JSON.stringify(result.data));
                                setTimeout( () => {
                                    // this.goToHome();
                                    location.reload();
                                },500);*/
                            }
                            else {
                                // this.restProvider.showErrorMsg(this.statusError,result.message);
                                // this.restProvider.showErrorMsg(this.statusError,'Something went wrong, Please try again.');
                                if (typeof result.message == "object") {
                                    var message = '';
                                    for (var i in result.message) {
                                        message += result.message[i] + '<br>';
                                    }
                                    _this.restProvider.showErrorMsg(_this.statusError, message);
                                }
                                else if (typeof result.message == "string") {
                                    _this.restProvider.showErrorMsg(_this.statusError, result.message);
                                }
                                else {
                                    // this.restProvider.showErrorMsg(this.statusError,result.message);
                                    _this.restProvider.showErrorMsg(_this.statusError, 'Invalid username or password..!!');
                                }
                            }
                        }, function (err) {
                            _this.restProvider.hideLoadingSpinner();
                            console.log(err);
                        });
                    }
                }
            ]
        });
        prompt.present();
    };
    MyCartPage.prototype.openShopPage = function () {
        var selectedShop = JSON.parse(localStorage.getItem('selectedShopLoc'));
        if (selectedShop != null && selectedShop != undefined) {
            var params = {};
            params.searchData = selectedShop;
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__products_products__["a" /* ProductsPage */], params);
        }
        else {
            this.goToHome();
        }
    };
    MyCartPage = MyCartPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-my-cart',template:/*ion-inline-start:"E:\Projects\IonicBackendBrains\FamilovApp\src\pages\my-cart\my-cart.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle class="buttonWhiteFont">\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>\n      <ion-grid>\n    		<ion-row>\n    			<ion-col col-10 class="headerTitle"><div align="center">View Cart</div></ion-col>\n    			<ion-col col-2 class="badgeCol">\n    				<!-- <ion-icon class="right" name="cart" item-end (click)="openCartPage()"></ion-icon> -->\n    				<span class="badgeIcon"> {{ cartItemsLen }}</span>\n					<ion-icon name="cart" item-end (click)="openCartPage()"></ion-icon>\n    			</ion-col>\n    		</ion-row>\n    	</ion-grid>\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content padding id="page4">\n\n	<div style="vertical-align: middle;margin-top: 15vh;margin-bottom: 20vh;" align="center" *ngIf="cartItems.length == 0">\n		<img src="assets/imgs/tired.png" style="max-height: 15vh;width: auto;">\n		      <h2>Ouuppss !!!</h2>\n		      <h3 style="color: darkblue;">Your cart is empty</h3>\n		      <br>\n		      <br>\n		      <p>Please select the products</p>\n		  <div align="center">\n			<button id="login-button1" ion-button color="secondary" (click)="openShopPage()" style="margin: 1vh;width: 80%;height: 3.5em;">\n				Back to Shop\n			</button>\n		</div>\n	</div>\n\n\n	<div *ngIf="cartItems.length > 0">\n\n		<div align="center">\n			<div class="checkout-wrap">\n				<ul class="checkout-bar">\n					<li class="visited first"></li>\n					<li class="previous"></li><!-- visited -->\n					<li class=""></li><!-- active -->\n				</ul>\n			</div>\n		</div>\n\n		<br>\n\n		<h3 align="center" style="margin-top: 3vh;">You have total {{ cartItems.length }} in basket.</h3>\n		<hr>\n	<ion-list>\n\n		<ion-item style="border: 1px solid #eee; padding: 1px;margin: 1vh auto;" *ngFor="let product of cartItems">\n			<ion-thumbnail item-start>\n				<img src="{{ product.product_image }}">\n			</ion-thumbnail>\n			<h2><b>{{ product.product_name }}</b></h2>\n			<br>\n			<br>\n			<p>\n				<!-- <b>Price:</b> -->\n				<span *ngIf="product.qty == 1" style="color:#32db64;font-weight: bold;" [innerHtml]="currencySymbol">{{ currencyName }}</span>\n				<span *ngIf="product.qty == 1" style="color:#32db64;font-weight: bold;">{{ product.product_prices }}</span>\n\n				<span *ngIf="product.qty > 1" [innerHtml]="currencySymbol">{{ currencyName }}</span>\n				<span *ngIf="product.qty > 1">{{ product.product_prices }}</span>\n\n				<span *ngIf="product.qty > 1" style="color:#32db64;font-weight: bold;margin-left: 1vh;" [innerHtml]="currencySymbol">{{ currencyName }}</span>\n\n				<span *ngIf="product.qty > 1" style="color:#32db64;font-weight: bold;">{{ (product.product_prices * product.qty).toFixed(2) }}</span>\n\n			</p>\n			<!-- <p>\n				<b>Total:</b>\n				<span [innerHtml]="currencySymbol">{{ currencyName }}</span>\n				{{ (product.product_prices * product.qty).toFixed(2) }}\n			</p> -->\n			<div item-end>\n				<button style="float: right;" color="danger" ion-button clear data-id="{{ product.product_id }}" (click)="removeItem(product.product_id)"><ion-icon name="close"></ion-icon></button><br>\n				<div style="float: right;display: -webkit-inline-box;margin-top: 3vh;">\n					<ion-icon (click)="addProductToCart(product,\'remove\')" style="padding-left: 4px; padding-top: 1px;width:20px;height:20px;border: 1px solid;border-radius: 50%;font-size: 1em;" name="remove"></ion-icon>\n					<p style="margin: 0.1em 1em;">{{ product.qty }}</p>\n					<ion-icon (click)="addProductToCart(product,\'add\')" color="secondary" style="padding-left: 4px; padding-top: 1px;width:20px;height:20px;border: 1px solid;border-radius: 50%;font-size: 1em;" name="add"></ion-icon>\n				</div>\n			</div>\n		</ion-item>\n\n  <!-- <ion-item style="border: 1px solid #eee; padding: 1px;margin: 1vh auto;">\n    <ion-thumbnail item-start>\n      <img src="assets/imgs/slide3.png">\n    </ion-thumbnail>\n    <h2>Product 2</h2>\n    <br>\n    <p>Hayao Miyazaki • 1988</p>\n    <div item-end>\n    	<button style="float: right;" color="danger" ion-button clear><ion-icon name="close"></ion-icon></button><br>\n    	\n    	<div style="float: right;display: -webkit-inline-box;margin-top: 3vh;">\n			<ion-icon (click)="subQty(2)" style="padding-left: 4px; padding-top: 1px;width:20px;height:20px;border: 1px solid;border-radius: 50%;font-size: 1em;" name="remove"></ion-icon>\n			<p style="margin: 0.1em 1em;">{{ qty2 }}</p>\n			<ion-icon (click)="addQty(2)" color="secondary" style="padding-left: 4px; padding-top: 1px;width:20px;height:20px;border: 1px solid;border-radius: 50%;font-size: 1em;" name="add"></ion-icon>\n    	</div>\n\n    </div>\n  </ion-item>\n\n  <ion-item style="border: 1px solid #eee; padding: 1px;margin: 1vh auto;">\n    <ion-thumbnail item-start>\n      <img src="assets/imgs/slide3.png">\n    </ion-thumbnail>\n    <h2>Product 3</h2>\n    <br>\n    <p>Hayao Miyazaki • 1988</p>\n    <div item-end>\n    	<button style="float: right;" color="danger" ion-button clear><ion-icon name="close"></ion-icon></button><br>\n    	\n    	<div style="float: right;display: -webkit-inline-box;margin-top: 3vh;">\n			<ion-icon (click)="subQty(3)" style="padding-left: 4px; padding-top: 1px;width:20px;height:20px;border: 1px solid;border-radius: 50%;font-size: 1em;" name="remove"></ion-icon>\n			<p style="margin: 0.1em 1em;">{{ qty3 }}</p>\n			<ion-icon (click)="addQty(3)" color="secondary" style="padding-left: 4px; padding-top: 1px;width:20px;height:20px;border: 1px solid;border-radius: 50%;font-size: 1em;" name="add"></ion-icon>\n    	</div>\n\n    </div>\n  </ion-item> -->\n</ion-list>\n\n<ion-grid style="margin-bottom: 10vh;">\n	<ion-row>\n	    <ion-col col-6>\n		    <h5 item-start>Subtotal</h5>\n		    <h5 item-start>Services Fees</h5>\n		    <h5 item-start>Promocode Discount</h5>\n		</ion-col>\n		<ion-col col-6>\n	    	<h5 item-start align="right">\n		    	<span [innerHtml]="currencySymbol">{{ currencyName }}</span>\n		    	{{ cartTotal }}\n		   	</h5>\n	    	<h5 item-start align="right">\n		    	<span [innerHtml]="currencySymbol">{{ currencyName }}</span>\n		    	{{ serviceFee }}\n		   	</h5>\n	    	<h5 item-start align="right">\n		    	<span [innerHtml]="currencySymbol">{{ currencyName }}</span>\n		    	{{ promoDiscount }}\n		   	</h5>\n	    </ion-col>\n	</ion-row>\n</ion-grid>\n\n	<!-- <div>\n		<ion-grid>\n			<ion-row>\n			    <ion-col col-8>\n				    <h4>You have <a>3 Items</a> In your cart.</h4>\n				</ion-col>\n				<ion-col col-4>\n			    	<button ion-button color="secondary" (click)="checkoutOrder()">\n						Checkout\n					</button>\n			    </ion-col>\n			</ion-row>\n		</ion-grid>\n\n		<ion-list>\n			<ion-card>\n				<img src="assets/imgs/slide3.png"/>\n				<ion-card-content>\n					<ion-card-title>\n						<ion-grid>\n							<ion-row>\n								<ion-col col-10>\n									Product 1\n								</ion-col>\n								<ion-col col-2 style="text-align: -webkit-center;">\n									<span style="text-align: center;font-size: 14px;">\n										<ion-icon name="trash"></ion-icon>\n									</span>\n								</ion-col>\n							</ion-row>\n						</ion-grid>\n					</ion-card-title>\n					<p>\n						The most popular industrial group ever, and largely\n						responsible for bringing the music to a mass audience.\n					</p>\n					<ion-grid>\n						<ion-row>\n							<ion-col col-6 style="display: flex;">\n								<ion-icon (click)="subQty(1)" name="remove" style="margin: 1.4vh;"></ion-icon>\n								<ion-input type="number" name="qty1" [(ngModel)]="qty1" style="padding: 0vh 1vh;background: gray;max-width: 30%;color: #FFF;"></ion-input>\n								<ion-icon (click)="addQty(1)" name="add" style="margin: 1.4vh;"></ion-icon>\n							</ion-col>\n							<ion-col col-6 item-end style="text-align: -webkit-right;">\n								<span>Price: $19.88</span><br>\n								<span>Total: $19.88</span>\n							</ion-col>\n						</ion-row>\n					</ion-grid>\n				</ion-card-content>\n			</ion-card>\n\n			<ion-card>\n				<img src="assets/imgs/slide3.png"/>\n				<ion-card-content>\n					<ion-card-title>\n						<ion-grid>\n							<ion-row>\n								<ion-col col-10>\n									Product 2\n								</ion-col>\n								<ion-col col-2 style="text-align: -webkit-center;">\n									<span style="text-align: center;font-size: 14px;">\n										<ion-icon name="trash"></ion-icon>\n									</span>\n								</ion-col>\n							</ion-row>\n						</ion-grid>\n					</ion-card-title>\n					<p>\n						The most popular industrial group ever, and largely\n						responsible for bringing the music to a mass audience.\n					</p>\n					<ion-grid>\n						<ion-row>\n							<ion-col col-6 style="display: flex;">\n								<ion-icon (click)="subQty(2)" name="remove" style="margin: 1.4vh;"></ion-icon>\n								<ion-input type="number" name="qty2" [(ngModel)]="qty2" style="padding: 0vh 1vh;background: gray;max-width: 30%;color: #FFF;"></ion-input>\n								<ion-icon (click)="addQty(2)" name="add" style="margin: 1.4vh;"></ion-icon>\n							</ion-col>\n							<ion-col col-6 item-end style="text-align: -webkit-right;">\n								<span>Price: $12.98</span><br>\n								<span>Total: $12.98</span>\n							</ion-col>\n						</ion-row>\n					</ion-grid>\n				</ion-card-content>\n			</ion-card>\n\n			<ion-card>\n				<img src="assets/imgs/slide3.png"/>\n				<ion-card-content>\n					<ion-card-title>\n						<ion-grid>\n							<ion-row>\n								<ion-col col-10>\n									Product 3\n								</ion-col>\n								<ion-col col-2 style="text-align: -webkit-center;">\n									<span style="text-align: center;font-size: 14px;">\n										<ion-icon name="trash"></ion-icon>\n									</span>\n								</ion-col>\n							</ion-row>\n						</ion-grid>\n					</ion-card-title>\n					<p>\n						The most popular industrial group ever, and largely\n						responsible for bringing the music to a mass audience.\n					</p>\n					<ion-grid>\n						<ion-row>\n							<ion-col col-6 style="display: flex;">\n								<ion-icon (click)="subQty(3)" name="remove" style="margin: 1.4vh;"></ion-icon>\n								<ion-input type="number" name="qty3" [(ngModel)]="qty3" style="padding: 0vh 1vh;background: gray;max-width: 30%;color: #FFF;"></ion-input>\n								<ion-icon (click)="addQty(3)" name="add" style="margin: 1.4vh;"></ion-icon>\n							</ion-col>\n							<ion-col col-6 item-end style="text-align: -webkit-right;">\n								<span>Price: $198.8</span><br>\n								<span>Total: $198.8</span>\n							</ion-col>\n						</ion-row>\n					</ion-grid>\n				</ion-card-content>\n			</ion-card>\n		</ion-list>\n\n		<hr>\n\n		<ion-grid>\n			<ion-row>\n			    <ion-col col-6>\n				    <h5 item-start>Subtotal</h5>\n				</ion-col>\n				<ion-col col-6>\n			    	<h5 item-start align="right">$231.60</h5>\n			    </ion-col>\n			</ion-row>\n		</ion-grid>\n\n		<hr>\n		<ion-grid>\n			<ion-row>\n			    <ion-col col-6>\n				    <h5 item-start>Shipping</h5>\n				</ion-col>\n				<ion-col col-6>\n				    <h5 item-start align="right">$8.40</h5>\n			    </ion-col>\n			</ion-row>\n		</ion-grid>\n\n		<hr>\n\n		<ion-grid>\n			<ion-row>\n			    <ion-col col-6>\n				    <h3 item-start>Total</h3>\n				</ion-col>\n				<ion-col col-6>\n			    	<h3 item-start align="right">$240.00</h3>\n			    </ion-col>\n			</ion-row>\n		</ion-grid>\n\n	</div> -->\n\n<div class="fixed-outside" style="background: #FFF;color:#000;left: 0;position: fixed;bottom: 6.5vh;width: 100%;display: inline-flex;" align="center">\n	<p style="margin: 0px;padding: 0px;margin-left: 2vh;color: lightgray;" align="center">Do you have a &nbsp;&nbsp;<a style="color:#32db64;" (click)="showPromocodePrompt()">&nbsp; Promo Code </a> &nbsp;? If not Please checkout</p><br>\n</div>\n<div class="fixed-outside" style="position: fixed;bottom: 0;left: 0;width: 100%;display: inline-flex;" align="center">\n	<button class="button-md-height" color="secondary" ion-button block style="background: #3b5998;height: 2.1em;width:50%;margin-bottom: 0px;text-transform: none;font-size: 20px;font-weight: bold;">Total: <span [innerHtml]="currencySymbol">{{ currencyName }}</span> {{ totalPrice }}</button>\n	<button class="button-md-height" color="secondary" ion-button icon-right block style="height: 2.1em;width:50%;margin-bottom: 0px;text-transform: none;font-size: 20px;font-weight: bold;" (click)="checkoutOrder()">Checkout <ion-icon name="arrow-forward"></ion-icon></button>\n</div>\n\n</div>\n\n</ion-content>'/*ion-inline-end:"E:\Projects\IonicBackendBrains\FamilovApp\src\pages\my-cart\my-cart.html"*/
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */]) === "function" && _c || Object])
    ], MyCartPage);
    return MyCartPage;
    var MyCartPage_1, _a, _b, _c;
}());

//# sourceMappingURL=my-cart.js.map

/***/ }),

/***/ 110:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TermsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TermsPage = (function () {
    function TermsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    TermsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-terms',template:/*ion-inline-start:"E:\Projects\IonicBackendBrains\FamilovApp\src\pages\terms\terms.html"*/'<ion-header>\n    <ion-navbar>\n        <button ion-button menuToggle>\n            <ion-icon name="menu"></ion-icon>\n        </button>\n        <ion-title>Terms of services</ion-title>\n    </ion-navbar>\n</ion-header>\n<ion-content padding>\n    <h3 align="center" style="margin: 5vh auto;">Terms of sales.</h3>\n    \n    <div style="padding: auto 1vh 1vh 1vh;margin: auto 2vh 10vh 2vh;">\n        <p class="pinkLbl">1. Preamble</p>\n        <div class="termsDesc">\n            These general conditions of use and sale of the service under the Familov.com trade name exclusively govern the relationship between Familov.com and the user of the Service.\n            \n            <br><br>\n            <div style="padding-left: 4vh;">\n                1. Familov.com allows Users to order products or vouchers, on the internet, for individuals residing in Cameroon (the "Beneficiaries"). As soon as the payment of the order is validated, the Beneficiary is informed by Familov.com by means of a unique confidential code communicated by SMS.\n\n                <br><br>\n                2. The unique code provided by Familov.com allows the Beneficiary to take possession of the products from partner businesses, a list of which is available on the Familov.com website.\n                \n                <br><br>\n                3. These general conditions of sale define the terms and conditions of an order on the site www.familov.com. The User recognizes when placing an order via the site to have read the general conditions of sale and expressly declares to accept them without reservation and does not require a handwritten signature of this document.\n            </div>\n        </div>\n        \n        <p class="pinkLbl">2. Opening of the account and conditions of use</p>\n        <div class="termsDesc">\n            The User certifies to be at least 18 years of age and have the legal capacity or to hold a parental authorization to open a user account with Familov.com and to make orders.\n            \n            <br><br>\n            When creating the Account, the User communicates to Familov.com his name (s), first name (s). The User also communicates a valid e-mail address, chooses his username and a password. The username and password are proof of the identity of the User and commit to any order placed through them.\n\n            <br><br>The user is the full and exclusive manager of his username and password. He alone will bear the consequences that may result from any use by third parties who knew of them, unless he demonstrates that the knowledge of his username and password by another person results from a fault of Familov. In case of forgetfulness of his password or his username or in case of fear that a third party could have knowledge of it, the user disposes on the site of a function allowing him to find his identifier and to choose a New Password.\n        </div>\n        \n        <p class="pinkLbl">3. Using the Account and Ordering Products</p>\n        <div style="padding-left: 4vh">\n            * The Account is strictly personal, and can not be shared or exchanged with a third party.\n            \n            <br><br>\n            * Orders are collected in Euros, legal currency in the European Monetary Union. In the event that the User does not reside in a country where the Euro is legal tender, the User will do his business with the bank holding the bank account of the exchange rate that will be applied for this purpose.\n\n            <br><br>\n            * The purchases made by the Users are made by internet, by means of a valid credit card, a paypal account or by bank transfer.\n            \n            <br><br>\n            * For each order, the User indicates the identity (surname and first name) and the mobile phone number of the Beneficiary. The User receives a confirmation of the purchase made by e-mail.\n            \n            <br><br>\n            * Familov.com undertakes to communicate by SMS to the Beneficiary designated by the User when ordering, a unique code to enter into possession of the products. In case of delays, losses, errors or omissions resulting from a breakdown of telecommunications, Familov.com can not be held responsible for the non-delivery of the SMS.\n            \n            <br><br>\n            * The unique code provided by Familov.com is indispensable to the Beneficiary and must be presented to Partners to get possession of the products.\n        </div>\n        \n        <p class="pinkLbl">4. Exclusion of service</p>\n        <div class="termsDesc">Familov.com may suspend automatically and immediately the use of the Service by the User or the Beneficiary in case of suspicion of fraud, attempted fraud or fraud of these persons. Familov.com may suspend by right and immediately the use of the Service by the User for breaching any of his obligations such as failure to pay sums due at the time of an order.</div>\n\n        <p class="pinkLbl">5. Personal data and control</p>\n        <div class="termsDesc">\n            The User must at all times ensure that the information provided by him is accurate and up-to-date. All boxes marked as mandatory in the registration procedure on the Site must contain valid information. Any attempt of fraud via the presentation of inaccurate information on the User or the Beneficiary will be communicated to the competent authorities and may lead to the immediate refusal of access to the Site and the Service.\n            \n            <br><br>\n            Each User has a right of access to the data concerning him collected and processed by Familov.com as well as a right to modify and delete data concerning him. He authorizes Familov.com and its partners to transmit information. to the authorities in charge of regulation as required by law. The details of the User will not be disclosed to third parties, except with the express consent of the User\n            \n            <br><br>\n            All information relating to any order on Familov.com is subject to data processing. This treatment aims to fight against fraud of all kinds. Payment service providers by credit card or bank and Familov are the beneficiaries of the data related to all orders. Any unpaid order or suspicion of fraud in the payment for any reason automatically cancels the order.\n        </div>\n\n        <p class="pinkLbl"> 6. Methods of payment</p>\n        <div class="termsDesc">Payment is made by credit card, via Pay Pal, by bank transfer. In the absence of authorization to pay by credit card, the order is not made available to the User. Familov.com reserves the right not to respond to the order of a User for whom problems have already occurred. In the case of a payment by transfer, the user has a period of 7 days to send the settlement to the account of the Familov.</div>\n\n        <p class="pinkLbl">7. Prices</p>\n        <div class="termsDesc">The prices listed on the product lists and product sheets do not include the cost of home delivery. The total price indicated in the confirmation of the reception of the order includes the price of the products and if necessary the expenses of delivery.</div>\n\n        <p class="pinkLbl">8. Delivery and withdrawal</p>\n        <div class="termsDesc">\n            Delivery times are 36 hours depending on the availability of the beneficiary. The beneficiary is required to check the condition of the products and the products opened or marked can not be returned.\n            \n            <br><br>\n            The User communicates at the time of his order the name and mobile phone number of the recipient of the order in Cameroon. This phone number will be used to verify the identity of the Beneficiary during the delivery or withdrawal in the store.\n            \n            <br><br>\n            <div style="padding-left: 5vh;">\n                a- Delivery in a partner point:\n                The User can choose to place the order at the disposal of his Beneficiary in a partner point of sale. The order will be available 36 hours after the payment. When the Beneficiary arrives at the partner point of sale, to withdraw his order he must:\n                \n                <br><br>\n                * Present your identity document (national identity card)\n                \n                <br><br>\n                * Present the SMS code of the telephone number registered for the Beneficiary when taking the order by the user\n\n                <br><br>\n                * Sign a delivery note\n                \n                <br><br>\n                b- Home delivery:\n                The user can opt for home delivery and products will be delivered within 36h working days after payment of the order. Familov then contacts the Beneficiary to schedule the delivery. If the mobile phone number indicated to reach the Beneficiary is not valid, Familov immediately informs the user by e-mail who then chooses either to cancel the order, or to communicate another mobile phone number; he has a delay of 24 hours from the mail received by Familov and the driver will then try again to join the Beneficiary to organize the delivery. Upon delivery,\n            \n                <br><br>\n                * Present your identity document (national identity card)\n                \n                <br><br>\n                * Present the SMS code of the telephone number registered for the Beneficiary when taking the order by the user\n                \n                <br><br>\n                * Sign a delivery note\n            \n                <br><br>\n                * Possible delays in delivery do not entitle the User to cancel the order, refuse the products or claim damages and interest.\n            </div>\n        </div>\n\n        <p class="pinkLbl">9. Complaint and Retraction</p>\n        <div class="termsDesc">In case of claim on the products, the User and the beneficiary have a maximum of 24 hours following delivery to make specific reservations and Familov.com reserves the right to analyze the merits of these and communicate his response to the User in the shortest possible time. The User has a right of withdrawal of seven working days. The retratation must be written form and does not apply to fresh and perishable products.</div>\n\n        <p class="pinkLbl">10. Responsibility of Familov.com</p>\n        <div class="termsDesc">\n        Familov.com can only be held liable for proven facts that are exclusively and directly attributable to it. Only are likely to be compensated the direct damages and some suffered by the User.\n        \n        <br><br>\n        Familov.com acting as intermediary can not be held liable for damages of any kind, material or immaterial or bodily that could result from theft or misuse of the Site. The responsibility of Familov.com can not be held liable for any risk inherent in the use of the internet, such as suspension of service, intrusion, the presence of computer viruses or shopping by a third party.\n        \n        <br><br>\n        Familov.com can not be held responsible for errors made by the User or the Beneficiary in connection with the use of the Service.\n        \n        <br><br>\n        Familov.com puts in place the means necessary for the smooth operation of the Service. As such, the responsibility of Familov.com will not be engaged in cases of force majeure within the meaning of the German case law.</div>\n\n        <p class="pinkLbl">11. Liability of the User and the Beneficiary</p>\n        <div class="termsDesc">\n            * The Beneficiary must ensure the preservation of his mobile phone and the unique code communicated to him by Familov.com by SMS. The unique code transmitted by Familov.com is untransmissible and strictly for personal use. In the event that the Beneficiary makes a shared use of the mobile phone on which Familov.com sends the unique code to the Beneficiary, Familov.com can not be held responsible for withdrawals made by any other person using the mobile phone.\n            \n            <br><br>\n            * As long as no objection has been made to Familov.com as a result of the loss or theft of the Recipient\'s mobile phone, the User and the Beneficiary assume the consequences of any withdrawals made.\n            \n            <br><br>\n            * The Service must be used in compliance with these general conditions of use, the legislation in force and in accordance with the use for which it is intended.\n            \n            <br><br>\n            * Familov.com disclaims any liability for the use of the Service that does not comply with these conditions.\n        </div>\n\n        <p class="pinkLbl">12. Formation of the contract and proof of the transaction</p>\n        <div class="termsDesc">\n            * The commands are summarized before the User is prompted to validate them. The User is then invited to "confirm the payment"; The validation of order by a click constitutes firm and final validation under the reservation referred to in the following paragraph.\n            \n            <br><br>\n            * Any order with choice of payment by credit card is considered effective only when the payment centers concerned have given their agreement. In case of refusal of said centers, the order is automatically canceled and the user warned by a message on the screen.\n            \n            <br><br>\n            * The computerized data, securely stored in the Familov.com computer systems will be considered as proof of communications, orders and payments between the parties. They are kept within the legal periods of commercial sale and can be produced as evidence.\n        </div>\n\n        <p class="pinkLbl">13. Confirmation, preparation and formation of the contract</p>\n        <div class="termsDesc">\n            Familov.com confirms to the User the receipt of his order by an email summarizing the products ordered, the means of payment chosen, the means of delivery chosen, the total invoice for the service and a unique order code.\n            \n            <br><br>\n            Familov.com reserves the right not to accept an order, for a legitimate reason, such as a difficulty of supply of a product, a problem concerning the understanding of the order received (illegible document ...), a foreseeable problem concerning the delivery to be made (example: delivery requested in a geographical area where there is a proven risk of aggression) or due to the abnormality of the order made on the Familov.com site.\n            \n            <br><br>\n            In the event that an order can not be accepted for one of the reasons mentioned above, Familov.com informs the User. The transfer of risks to the User occurs at the time of provision or delivery. The information given by the user, when placing an order, commits him: in case of error of the User in the wording of his details or those of the place of delivery, including his name, first name, address , telephone number, e-mail address or credit or debit card number, resulting in the loss of the products, the User remains responsible for the payment of the lost products.\n        </div>\n\n        <p class="pinkLbl">14. Financial conditions and service charges</p>\n        <div class="termsDesc">\n            The rates applicable by Familov.com are available on the Site these rates are subject to change. Familov.com will charge the User a commission for the use of the Service. This commission corresponds to € 2.50.\n            \n            <br><br>\n            Subject to the right of withdrawal provided for in ARTICLE 9, the User gives irrevocable mandate to Familov.com to credit, for its order and for its account, the account of the Partner of the amount of the order made for the Beneficiary, minus all amounts due by the Partner to Familov.com, within the limit of the amount of the electronic sums credited to the Account.\n        </div>\n\n        <p class="pinkLbl">15. Products, availability and warranty</p>\n        <div class="termsDesc">\n            Familov.com makes available to the user before all the order all the essential characteristics of the products present in the catalogs published by familov.com and this according to the availability of the stocks. The legal warranty of the products available is ensured by the supplier and the manufacturer. The photographs that may accompany this description have no contractual value. The products offered by Familov are intended to be made available or delivered in Cameroon.\n            \n            <br><br>\n            The order is executed no later than 7 working days from the working day following the day the buyer placed his order. However, in case of unavailability of the ordered product, especially because of our partner supermarkets, the User will be informed at the earliest. In such a case, familov undertakes to offer the buyer a similar product at a similar price.\n            \n            <br><br>\n            The beneficiary benefits from the legal guarantee of conformity and latent defects on the products sold\n        </div>\n\n        <p class="pinkLbl">16. Provisions in case of dispute</p>\n        <div class="termsDesc">\n            The present conditions of sale on line are subject to the Allamande law. In case of dispute or claim, the User will first contact Familov.com for a direct conciliation of the parties. When a solution is not found one month after the claim, jurisdiction is assigned to the competent courts.\n        </div>\n\n        <p class="pinkLbl">17. Intellectual property</p>\n        <div class="termsDesc">\n            The Site, the applications and all the elements contained therein (information, data, sounds, images, drawings, graphics, distinctive signs, logos and brands) are the exclusive property of Familov.com. All of these elements are protected by intellectual property rights and as such, is protected against any unauthorized use by law.\n            \n            <br><br>\n            It is strictly forbidden to use or reproduce the name Familov.com and / or its logo, alone or associated, for any purpose whatsoever, and in particular for advertising purposes, without the prior written consent of Familov. com.\n            \n            <br><br>\n            It is also forbidden for the User to decompile or disassemble the Site, the applications, to use them for commercial purposes or to infringe in any way whatsoever the intellectual property rights of Familov.com\n        </div>\n\n        <p class="pinkLbl">18. General conditions</p>\n        <div class="termsDesc">\n            These terms and conditions come into force from its posting online. They govern any order placed until it is withdrawn.\n            \n            <br><br>\n            It is strictly forbidden to use or reproduce the name Familov.com and / or its logo, alone or associated, for any purpose whatsoever, and in particular for advertising purposes, without the prior written consent of Familov. com.\n            \n            <br><br>\n            Familov.com reserves the right to modify the Website, the service provided and the general conditions of use and invites the User to each new order to read carefully the general conditions of sale in force.\n        </div>\n\n        <p class="pinkLbl">19. Customer Service</p>\n        <div class="termsDesc">\n            For any difficulty, the user or the Beneficiary can contact Familov.com\n            \n            <br><br>\n            * By email: hello@familov.com.\n            \n            <br><br>\n            * By phone or Whatsapp: +49 1525 9948834\n            \n            <br><br>\n            * Chat: www.familov.com\n        </div>\n    </div>\n    \n</ion-content>'/*ion-inline-end:"E:\Projects\IonicBackendBrains\FamilovApp\src\pages\terms\terms.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], TermsPage);
    return TermsPage;
}());

//# sourceMappingURL=terms.js.map

/***/ }),

/***/ 111:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FaqPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FaqPage = (function () {
    function FaqPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.frequentQuestions = [
            {
                title: "What is Familov.com?",
                description: "Everything is in the name. With familov.com, order from abroad and we deliver your family to the country in less than 36 hours."
            },
            {
                title: "How can I create an account?",
                description: "Sur la page de démarrage de Familov, cliquez sur Inscrivez-vous (ou allez directement à la page Créer un compte). Remplissez les informations et votre mot de passe et confirmez-les une deuxième fois. Votre adresse de courriel sera directement liée au compte."
            },
            {
                title: "I forgot my password?",
                description: "Vous avez déjà créé un compte mais vous ne vous souvenez plus de votre mot de passe? Cliquez sur «Login» en haut de la page. Puis cliquez sur 'Mot de passe oublié?'. Remplissez votre adresse e-mail et un nouveau mot de passe vous sera envoyé par e-mail."
            },
            {
                title: "How long does a delivery take?",
                description: "En moyenne, nous avons besoin de moins de 36 heures à partir du moment où nous avons recu le payement jusqu'au moment de la livraison à votre proche. Le délai de livraison peut varier d'une boutique à l'autre et dépend du nombre de commandes que la boutique doit préparer et la disponibilité du bénéficiaire."
            },
            {
                title: "How much is shipping?",
                description: "Nous facturons un forfait de 2,95 € (2,50€ pendant le phase béta) pour le service et 0,00€ pour les livraison en magasin.En fonction de lieu de residence le coût des livraison à domicile varie."
            },
            {
                title: "What happens after placing my order?",
                description: "L'ordre arrive dans notre système et après que nous recevons le paiement, il est envoyé directement à la boutique (Shopper) que vous avez commandé. Après seulement quelques secondes, les magasins reçoivent l'ordre par le biais de notre propre système Shop-Connect.<br> <br>1- Le magasin recoit tous les détails de votre commande <br> 2- Votre proche recoit le code de retrait par SMS<br> 3-Le livreur prépare le paquet et recontacte le bénéficiaire dans les 36 heures pour la livraison <br>4-Vous recevez une confirmation par SMS ou e-mail que votre commande est bien livrée. <br>NB: Nous travaillons avec partenaires n et des livreurs proffessionnels pour gerer vos commanades. Ne vous inquiétez pas, ils ont été formés individuellement pes sont formés pour donner la meilleure qualité , soigner vos articles et les livrer."
            },
            {
                title: "Where can I see my receipts?",
                description: "Vous pouvez afficher vos reçus de deux façons: Email: Lorsque votre commande est passée, vous recevrez un courriel avec un reçu. <br> <br> Sur le site Web cliquez sur votre nom dans le coin supérieur droit et un menu apparaîtra. Sélectionnez Mes commandes. Cliquez sur un numéro de commande pour afficher un reçu détaillé de cet ordre."
            },
            {
                title: "How is my order confirmed?",
                description: "Une fois votre commande passée, vous recevrez un e-mail de confirmation envoyé à l'adresse e-mail que vous avez saisie lors de votre commande. Conservez ce courriel et votre numéro de commande pour toute question ou remarque concernant votre commande. Lorsque vous recevez l'e-mail de confirmation, Familov fait le reste. Il suffit de s'asseoir, de se détendre et de profiter!"
            },
            {
                title: "I have not received a confirmation email",
                description: "Si un numéro de commande s'affiche à l'écran, cela signifie que votre commande a été envoyée. L'e-mail de confirmation peut être dans votre «spam» plutôt que dans votre boîte de réception. Si vous ne recevez pas de numéro de commande, veuillez contacter notre service à la clientèle pour connaître l'état de votre commande. Vous devez fournir l'adresse e-mail que vous avez saisie lors de la commande et le nom de la boutique sélectionnée afin que nous puissions trouver votre commande."
            }
        ];
        this.paymentQuestions = [
            {
                title: "What types of payment can I use?",
                description: "Carte de crédit (VISA / American Express / Master Card /)<br>Pay Pal <br> Virement"
            },
            {
                title: "How secure are online payments?",
                description: "Lors du traitement des paiements en ligne sur Familov.com, nous utilisons des pages SSL sécurisées. Cela signifie que ces pages assurent la protection de vos données personnelles et de paiement. De cette façon, vous pouvez être sûr que cette information est seulement visible pour vous et pour nous, pour le traitement de votre commande."
            },
            {
                title: "Why do I have to pay transaction fees?",
                description: "Familov.com est seulement un intermédiaire, entre vous en tant que consommateur et la Boutique. Vu le faible revenu que nous gagnons sur chaque commande, nous devons facturer pour l'utilisation d'une méthode de paiement en ligne. Les coûts couvrent l'entretien technique des paiements en ligne, les coûts de transaction que les banques facturent et nos frais administratifs pour le traitement des paiements. Tous les sites facturent ces coûts, mais ils les ajoutent souvent directement aux frais d'envoi et / ou d'administration.. Nous vous remercions pour votre compréhension."
            },
            {
                title: "My online payment failed",
                description: "Si vous pensez que le paiement en ligne a échoué, vérifiez toujours si vous avez reçu un e-mail de confirmation. Cet e-mail vous permettra de voir si votre commande a été soumise et si la commande est déjà payée ou non. <br><br> L'e-mail de confirmation est envoyé directement une fois le paiement terminé. Cela peut prendre un peu plus longtemps si le paiement est traité par la banque, la société de carte de crédit ou toute autre société de paiement. Cela peut prendre jusqu'à 15 minutes. Si après 15 minutes vous n'avez toujours pas de courriel de confirmation dans votre boîte de réception, contactez notre service à la clientèle. Ils vous diront si l'ordre et le paiement ont réussi."
            },
            {
                title: "I still have a lot of questions to ask you?",
                description: "Contactez nous! Nous serons là à :<br><br> Email:hello@familov.com <br><br> Whatsapp : (+49 1525 9948834)<br><br> Chat : www.familov.com"
            }
        ];
        this.shownGroup = null;
        // If we navigated to this page, we will have an item available as a nav param
        this.selectedItem = navParams.get('item');
        // Let's populate this page with some filler content for funzies
        this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
            'american-football', 'boat', 'bluetooth', 'build'];
        this.items = [];
        for (var i = 1; i < 11; i++) {
            this.items.push({
                title: 'Item ' + i,
                note: 'This is item #' + i,
                icon: this.icons[Math.floor(Math.random() * this.icons.length)]
            });
        }
    }
    FaqPage_1 = FaqPage;
    FaqPage.prototype.toggleGroup = function (group) {
        if (this.isGroupShown(group)) {
            this.shownGroup = null;
        }
        else {
            this.shownGroup = group;
        }
    };
    ;
    FaqPage.prototype.isGroupShown = function (group) {
        return this.shownGroup === group;
    };
    ;
    FaqPage.prototype.itemTapped = function (event, item) {
        // That's right, we're pushing to ourselves!
        this.navCtrl.push(FaqPage_1, {
            item: item
        });
    };
    FaqPage = FaqPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-faq',template:/*ion-inline-start:"E:\Projects\IonicBackendBrains\FamilovApp\src\pages\faq\faq.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Frequently Questions</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n\n<h3 align="center" style="margin: 5vh auto;">Frequently Asked Questions</h3>\n\n  <ion-card *ngFor="let d of frequentQuestions; let i=index" text-wrap (click)="toggleGroup(i)" [ngClass]="{active: isGroupShown(i)}" style="background: #EEE;">\n  <ion-card-content>\n    <ion-card-title>\n      <ion-grid>\n        <ion-row>\n          <ion-col col-10>\n            <ion-label style="font-size: 15px;margin: 4px;" color="primary">{{ d.title }}</ion-label>\n          </ion-col>\n          <ion-col col-2 style="text-align: right;">\n            <ion-icon style="font-size: 1.3em;" color="success" name="arrow-dropright" [name]="isGroupShown(i) ? \'arrow-dropdown\' : \'arrow-dropright\'"></ion-icon>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n      </ion-card-title>\n    <div *ngIf="isGroupShown(i)" [innerHtml]="d.description"></div><!-- {{d.description}} -->\n  </ion-card-content>\n</ion-card>\n\n<h3 align="center" style="margin: 5vh auto;">Payment</h3>\n\n<ion-card *ngFor="let d of paymentQuestions; let i=index" text-wrap (click)="toggleGroup(i)" [ngClass]="{active: isGroupShown(i)}" style="background: #EEE;">\n  <ion-card-content>\n    <ion-card-title>\n      <ion-grid>\n        <ion-row>\n          <ion-col col-10>\n            <ion-label style="font-size: 15px;margin: 4px;" color="primary">{{ d.title }}</ion-label>\n          </ion-col>\n          <ion-col col-2 style="text-align: right;">\n            <ion-icon style="font-size: 1.3em;" color="success" name="arrow-dropright" [name]="isGroupShown(i) ? \'arrow-dropdown\' : \'arrow-dropright\'"></ion-icon>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n      </ion-card-title>\n    <div *ngIf="isGroupShown(i)" [innerHtml]="d.description"></div><!-- {{d.description}} -->\n  </ion-card-content>\n</ion-card>\n\n  <div *ngIf="selectedItem" padding>\n    You navigated here from <b>{{selectedItem.title}}</b>\n  </div>\n</ion-content>\n'/*ion-inline-end:"E:\Projects\IonicBackendBrains\FamilovApp\src\pages\faq\faq.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], FaqPage);
    return FaqPage;
    var FaqPage_1;
}());

//# sourceMappingURL=faq.js.map

/***/ }),

/***/ 124:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 124;

/***/ }),

/***/ 14:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RestProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(298);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_toPromise__ = __webpack_require__(299);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_toPromise__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var RestProvider = (function () {
    function RestProvider(http, alertCtrl, loadingCtrl) {
        // console.log('Hello RestProvider Provider');
        this.http = http;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        // this.apiUrl = "https://jsonplaceholder.typicode.com";
        this.apiUrl = 'http://103.21.58.248/familov/api/';
        // this.apiUrl = 'http://localhost/ci_familov/api/';
        this.apiToken = localStorage.getItem('apiToken');
        this.loading = __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* Loading */];
        // alert(localStorage.getItem('apiToken'));
        this.getHeaders = 'authorization=Bearer ' + this.apiToken;
    }
    RestProvider.prototype.showSuccessMsg = function (title, subTitle) {
        var alert = this.alertCtrl.create({
            title: title,
            subTitle: subTitle,
            buttons: ['Done']
        });
        alert.present();
    };
    RestProvider.prototype.showErrorMsg = function (title, subTitle) {
        var alert = this.alertCtrl.create({
            title: title,
            subTitle: subTitle,
            buttons: ['Ok']
        });
        alert.present();
    };
    RestProvider.prototype.showWarningMsg = function (title, subTitle) {
        var alert = this.alertCtrl.create({
            title: title,
            subTitle: subTitle,
            buttons: ['Close']
        });
        alert.present();
    };
    RestProvider.prototype.showConfirmationAlert = function (title, message, cancelText, confirmText) {
        var alert = this.alertCtrl.create({
            title: title,
            message: message,
            buttons: [
                {
                    text: cancelText,
                    role: 'cancel',
                    handler: function () {
                        return false; // console.log('Cancel clicked');
                    }
                },
                {
                    text: confirmText,
                    handler: function () {
                        return true; // console.log('Buy clicked');
                    }
                }
            ]
        });
        alert.present();
    };
    RestProvider.prototype.LoadingSpinner = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        this.loading.present();
        /*setTimeout(() => {
            this.loading.dismiss();
        }, 1000);*/
    };
    RestProvider.prototype.hideLoadingSpinner = function () {
        this.loading.dismiss();
        this.loading = null;
    };
    RestProvider.prototype.getToken = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.http.get(_this.apiUrl + 'token/token')
                .subscribe(function (data) {
                _this.apiToken = data.key;
                // alert(this.apiToken);
                resolve(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    RestProvider.prototype.getCurrency = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.http.get(_this.apiUrl + 'home/get_currency?' + _this.getHeaders)
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    RestProvider.prototype.signupUser = function (data) {
        var _this = this;
        var body = new FormData();
        body.append('first_name', data.first_name);
        body.append('last_name', data.last_name);
        body.append('email', data.email);
        body.append('phone_code', data.phone_code);
        body.append('phone_number', data.phone_number);
        body.append('password', data.password);
        body.append('repeat_password', data.repeat_password);
        body.append('authorization', "Bearer " + this.apiToken);
        return new Promise(function (resolve, reject) {
            var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Headers */]();
            headers.append('Content-Type', 'application/json');
            headers.append('authorization', 'Bearer ' + _this.apiToken);
            // var headers = new HttpHeaders()
            // 				.set('authorization','Bearer ' + this.apiToken)
            // 				.set('Access-Control-Allow-Methods','POST')
            _this.http.post(_this.apiUrl + 'auth/sign_up', body, {
                headers: headers
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.loginUser = function (data) {
        var _this = this;
        var body = new FormData();
        body.append('email', data.email);
        body.append('password', data.password);
        body.append('authorization', "Bearer " + this.apiToken);
        return new Promise(function (resolve, reject) {
            var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Headers */]();
            headers.append('Content-Type', 'application/json');
            headers.append('authorization', 'Bearer ' + _this.apiToken);
            // var headers = new HttpHeaders()
            // 				.set('authorization','Bearer ' + this.apiToken)
            // 				.set('Access-Control-Allow-Methods','POST')
            _this.http.post(_this.apiUrl + 'auth/login', body, {
                headers: headers
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.getHomeData = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Headers */]();
            // headers.append('Content-Type', 'application/json');
            headers.append('authorization', 'Bearer ' + _this.apiToken);
            // var headers = new HttpHeaders()
            // 				.set('authorization','Bearer ' + this.apiToken)
            //
            _this.getHeaders = 'authorization=Bearer ' + localStorage.getItem('apiToken');
            setTimeout(function () {
                _this.getHeaders = 'authorization=Bearer ' + localStorage.getItem('apiToken');
                _this.http.get(_this.apiUrl + 'home/home?' + _this.getHeaders, {
                    headers: headers
                })
                    .subscribe(function (res) {
                    resolve(res);
                }, function (err) {
                    reject(err);
                });
            }, 200);
        });
    };
    RestProvider.prototype.getCities = function (country_id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Headers */]();
            headers.append('Content-Type', 'application/json');
            headers.append('authorization', 'Bearer ' + _this.apiToken);
            // var headers = new HttpHeaders()
            // 				.set('authorization','Bearer ' + this.apiToken)
            // 				.set('Access-Control-Allow-Methods','POST')
            _this.http.get(_this.apiUrl + 'home/city?' + _this.getHeaders + '&country_id=' + country_id, {
                headers: headers
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.getShops = function (city_id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Headers */]();
            headers.append('Content-Type', 'application/json');
            headers.append('authorization', 'Bearer ' + _this.apiToken);
            // var headers = new HttpHeaders()
            // 				.set('authorization','Bearer ' + this.apiToken)
            // 				.set('Access-Control-Allow-Methods','POST')
            _this.http.get(_this.apiUrl + 'home/shop?' + _this.getHeaders + '&city_id=' + city_id, {
                headers: headers
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.getProducts = function (searchData) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Headers */]();
            headers.append('Content-Type', 'application/json');
            headers.append('authorization', 'Bearer ' + _this.apiToken);
            // var headers = new HttpHeaders()
            // 				.set('authorization','Bearer ' + this.apiToken)
            // 				.set('Access-Control-Allow-Methods','POST')
            var link = '&country_id=' + searchData.country_id + '&city_id=' + searchData.city_id + '&shop_id=' + searchData.shop_id + '&currency=' + searchData.currency + '&page=' + searchData.page;
            if (searchData.category_id != null && searchData.category_id != undefined && searchData.category_id > 0) {
                link += '&category_id=' + searchData.category_id;
            }
            if (searchData.searchText != null && searchData.searchText != undefined && searchData.searchText != '') {
                link += '&search_keyword=' + searchData.searchText;
            }
            _this.http.get(_this.apiUrl + 'product/product_list?' + _this.getHeaders + link, {
                headers: headers
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.changePassword = function (userData) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var body = new FormData();
            for (var i in userData) {
                body.append(i, userData[i]);
            }
            body.append('authorization', 'Bearer ' + _this.apiToken);
            var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Headers */]();
            headers.append('Content-Type', 'application/json');
            headers.append('authorization', 'Bearer ' + _this.apiToken);
            // var headers = new HttpHeaders()
            // 				.set('authorization','Bearer ' + this.apiToken)
            // 				.set('Access-Control-Allow-Methods','POST')
            _this.http.post(_this.apiUrl + 'user/change_password', body, {
                headers: headers
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.getOrdersList = function (customer_id) {
        var _this = this;
        // customer_id = 129;
        return new Promise(function (resolve) {
            _this.http.get(_this.apiUrl + 'product/order_list?' + _this.getHeaders + '&customer_id=' + customer_id)
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    RestProvider.prototype.getOrderDetails = function (customer_id, order_id) {
        var _this = this;
        // customer_id = 129;
        // order_id = 693;
        return new Promise(function (resolve) {
            _this.http.get(_this.apiUrl + 'product/order_detail_by_id?' + _this.getHeaders + '&customer_id=' + customer_id + '&order_id=' + order_id)
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    RestProvider.prototype.updateProfile = function (profileData) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var body = new FormData();
            for (var i in profileData) {
                body.append(i, profileData[i]);
            }
            body.append('authorization', 'Bearer ' + _this.apiToken);
            var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Headers */]();
            headers.append('Content-Type', 'application/json');
            headers.append('authorization', 'Bearer ' + _this.apiToken);
            // var headers = new HttpHeaders()
            // 				.set('authorization','Bearer ' + this.apiToken)
            // 				.set('Access-Control-Allow-Methods','POST')
            _this.http.post(_this.apiUrl + 'user/profile_update', body, {
                headers: headers
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.getUserData = function (customer_id) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.http.get(_this.apiUrl + 'user/profile?' + _this.getHeaders + '&customer_id=' + customer_id)
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.applyPromocode = function (data) {
        var _this = this;
        var body = new FormData();
        body.append('promo_code', data.promo_code);
        body.append('cart_amount_total', data.cart_amount_total);
        body.append('currency', data.currency);
        body.append('authorization', "Bearer " + this.apiToken);
        return new Promise(function (resolve, reject) {
            var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Headers */]();
            headers.append('authorization', 'Bearer ' + _this.apiToken);
            // headers.append('Content-Type', 'application/json');
            // var headers = new HttpHeaders()
            // 				.set('authorization','Bearer ' + this.apiToken)
            // 				.set('Access-Control-Allow-Methods','POST')
            _this.http.post(_this.apiUrl + 'checkout/order_promo_code', body, {
                headers: headers
            })
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RestProvider.prototype.getShopDeliveryOptions = function (shop_id, currency) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.http.get(_this.apiUrl + 'checkout/shop_delivery_option?' + _this.getHeaders + '&shop_id=' + shop_id + '&currency=' + currency)
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    RestProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* LoadingController */]])
    ], RestProvider);
    return RestProvider;
}());

//# sourceMappingURL=rest.js.map

/***/ }),

/***/ 165:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 165;

/***/ }),

/***/ 210:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__my_cart_my_cart__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import { HomePage } from '../home/home';


var ProductDetailsPage = (function () {
    function ProductDetailsPage(navCtrl, alertCtrl, navParams, restProvider) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.navParams = navParams;
        this.restProvider = restProvider;
        this.cartItemsLen = 0;
        /*this.productDetails = this.navParams.get('productDetails');
        this.productDetails.qty = 1;

        this.currencySymbol = '&euro;';
        this.currencyName = 'EUR';

        this.cartItems = localStorage.getItem('cartItems');
        if (this.cartItems == undefined || this.cartItems == null) {
            this.cartItems = [];
        }
        else {
            this.cartItems = JSON.parse(localStorage.getItem('cartItems'));
            this.cartItemsLen = this.cartItems.length;
        }

        this.currencyData = localStorage.getItem('currencyData');
        if (this.currencyData == undefined || this.currencyData == null) {
            this.currencyData = [];
            this.currencySymbol = '&euro;';
            this.currencyName = 'EUR';
        }
        else {
            this.currencyData = JSON.parse(this.currencyData);
            this.currencySymbol = this.currencyData.vSymbol;
            this.currencyName = this.currencyData.vName;
        }*/
        // console.log(this.currencySymbol);
        // console.log(this.currencyData);
    }
    ProductDetailsPage.prototype.ionViewCanEnter = function () {
        this.productDetails = this.navParams.get('productDetails');
        this.productDetails.qty = 1;
        this.currencySymbol = '&euro;';
        this.currencyName = 'EUR';
        this.cartItems = localStorage.getItem('cartItems');
        if (this.cartItems == undefined || this.cartItems == null) {
            this.cartItems = [];
        }
        else {
            this.cartItems = JSON.parse(localStorage.getItem('cartItems'));
            this.cartItemsLen = this.cartItems.length;
            /*for (var i = 0; i < this.cartItems.length; ++i) {
                if(this.cartItems[i].product_id == this.productDetails.product_id) {
                    this.productDetails.qty = this.cartItems[i].qty;
                }
            }*/
            // var exist = this.cartItems.filter(function (cartItem) { return cartItem.product_id == this.productDetails.product_id });
            // console.log(exist);
            // if(exist.length > 0) {
            // 	for (var i = 0; i < this.cartItems.length; ++i) {
            // 		if(this.cartItems[i].product_id == this.productDetails.product_id) {
            // 			this.productDetails.qty = this.cartItems[i].qty;
            // 		}
            // 	}
            // }
        }
        this.currencyData = localStorage.getItem('currencyData');
        if (this.currencyData == undefined || this.currencyData == null) {
            this.currencyData = [];
            this.currencySymbol = '&euro;';
            this.currencyName = 'EUR';
        }
        else {
            this.currencyData = JSON.parse(this.currencyData);
            this.currencySymbol = this.currencyData.vSymbol;
            this.currencyName = this.currencyData.vName;
        }
    };
    /*gotoHome(params) {
        if (!params) params = {};
        this.navCtrl.setRoot(HomePage);
    }*/
    ProductDetailsPage.prototype.presentAlert = function () {
        var alert = this.alertCtrl.create({
            title: 'Done',
            subTitle: 'Item has been added in cart successfully.',
            buttons: ['OK']
        });
        alert.present();
    };
    ProductDetailsPage.prototype.openCartPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__my_cart_my_cart__["a" /* MyCartPage */]);
    };
    ProductDetailsPage.prototype.addQty = function () {
        this.productDetails.qty++;
    };
    ProductDetailsPage.prototype.subQty = function () {
        if (this.productDetails.qty > 1) {
            this.productDetails.qty--;
        }
        /*if (no == 1) {
            if(this.qty1 > 1){
                this.qty1--;
            }
        }*/
    };
    ProductDetailsPage.prototype.addProductToCart = function (product, type) {
        this.cartItems = localStorage.getItem('cartItems');
        if (this.cartItems == undefined || this.cartItems == null) {
            this.cartItems = [];
        }
        else {
            this.cartItems = JSON.parse(localStorage.getItem('cartItems'));
            this.cartItemsLen = this.cartItems.length;
        }
        if (this.productDetails.qty > 0) {
            if (this.cartItemsLen > 0) {
                var exist = this.cartItems.filter(function (cartItem) { return cartItem.product_id == product.product_id; });
                if (exist.length > 0) {
                    for (var i = 0; i < this.cartItems.length; ++i) {
                        if (this.cartItems[i].product_id == product.product_id) {
                            /*if(type == 'add') {
                                // this.cartItems[i].qty++;
                                // this.productDetails.qty = this.cartItems[i].qty;
                                this.cartItems[i].qty = this.productDetails.qty;
                                this.restProvider.showSuccessMsg('Success','Item added in cart successfully.');
                            }
                            else {
                                if(this.cartItems[i].qty > 1){
                                    // this.cartItems[i].qty--;
                                    // this.productDetails.qty = this.cartItems[i].qty;
                                    this.cartItems[i].qty = this.productDetails.qty;
                                    this.restProvider.showSuccessMsg('Success','Item quantity updated successfully.');
                                }
                                else {
                                    this.removeItem(this.productDetails.product_id);
                                }
                            }*/
                            this.cartItems[i].qty = parseInt(this.cartItems[i].qty) + parseInt(this.productDetails.qty);
                            this.restProvider.showSuccessMsg('Success', 'Item added in cart successfully.');
                        }
                    }
                    localStorage.setItem('cartItems', JSON.stringify(this.cartItems));
                }
                else {
                    product.qty = this.productDetails.qty;
                    if (this.productDetails.qty > 0) {
                        // this.productDetails.qty = 1;
                        this.cartItems.push(product);
                        this.cartItemsLen = this.cartItems.length;
                        localStorage.setItem('cartItems', JSON.stringify(this.cartItems));
                        this.restProvider.showSuccessMsg('Success', 'Item added in cart successfully.');
                    }
                }
            }
            else {
                product.qty = this.productDetails.qty;
                // this.productDetails.qty = 1;
                this.cartItems.push(product);
                this.cartItemsLen = this.cartItems.length;
                ;
                localStorage.setItem('cartItems', JSON.stringify(this.cartItems));
                this.restProvider.showSuccessMsg('Success', 'Item added in cart successfully.');
            }
        }
        this.cartItemsLen = this.cartItems.length;
    };
    ProductDetailsPage.prototype.removeItem = function (product_id) {
        var index = this.cartItems.map(function (e) { return e.product_id; }).indexOf('' + product_id);
        this.cartItems.splice(index, 1);
        localStorage.setItem('cartItems', JSON.stringify(this.cartItems));
        this.cartItemsLen = this.cartItems.length;
    };
    ProductDetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-product-details',template:/*ion-inline-start:"E:\Projects\IonicBackendBrains\FamilovApp\src\pages\product-details\product-details.html"*/'<ion-header>\n	<ion-navbar>\n		<button ion-button menuToggle class="buttonWhiteFont">\n			<ion-icon name="menu"></ion-icon>\n		</button>\n		<ion-title>\n			<ion-grid>\n	    		<ion-row>\n	    			<ion-col col-10 class="headerTitle"><div align="center">Product Details</div></ion-col>\n	    			<ion-col col-2 class="badgeCol">\n	    				<!-- <ion-icon class="right" name="cart" item-end (click)="openCartPage()"></ion-icon> -->\n	    				<span class="badgeIcon"> {{ cartItemsLen }}</span>\n						<ion-icon name="cart" item-end (click)="openCartPage()"></ion-icon>\n	    			</ion-col>\n	    		</ion-row>\n	    	</ion-grid>\n		</ion-title>\n	</ion-navbar>\n</ion-header>\n<ion-content padding id="page5" class="grayBackground">\n	\n	<div class="spacer" style="height:30px;" id="login-spacer2"></div>\n\n   <!-- <ion-slides autoplay=\'2500\' loop=\'true\'>\n    <ion-slide>\n      <img src="assets/imgs/slide3.png">\n    </ion-slide>\n    <ion-slide>\n      <img src="assets/imgs/slide2.png">\n    </ion-slide>\n    <ion-slide>\n      <img src="assets/imgs/slide1.png">\n    </ion-slide>\n  </ion-slides> -->\n\n  <ion-card>\n  	<div align="center">\n  		<img src="{{ productDetails.product_image }}" style="max-height: 150px;width: auto;">\n  		<br>\n  		<p style="margin-top: 5vh;font-size: 20px;"><b>{{ productDetails.product_name }}</b></p>\n  		<p style="margin-top: 5vh;font-size: 18px;">\n  			<small style="margin-right: 5vh;">\n  				<span [innerHtml]="currencySymbol">{{ currencyName }}</span>\n  				{{ productDetails.product_prices }}\n  			</small>\n  			<b style="color: #32db64;">\n  				<span [innerHtml]="currencySymbol">{{ currencyName }}</span>\n  				{{ productDetails.product_prices }}\n  			</b>\n		</p> 		\n  	</div>\n  </ion-card>\n\n	<div class="spacer" style="height:25px;" id="login-spacer4"></div>\n\n	<!-- <ion-card>\n		<ion-grid>\n			<ion-row class="ionRow">\n				<ion-col col-4>\n					<b>Name:</b>\n				</ion-col>\n				<ion-col col-8>\n					Product name\n				</ion-col>\n			</ion-row>\n\n			<ion-row class="ionRow">\n				<ion-col col-4>\n					<b>Details:</b>\n				</ion-col>\n				<ion-col col-8>\n					Product description\n				</ion-col>\n			</ion-row>\n\n			<ion-row class="ionRow">\n				<ion-col col-4>\n					<b>Price:</b>\n				</ion-col>\n				<ion-col col-8>\n					$100.00\n				</ion-col>\n			</ion-row>\n\n		</ion-grid>\n\n		<div align="center" style="margin: 1.5vh">\n			<button color="dark" ion-button (click)="presentAlert()" style="width: 25vh;">Add to cart</button>\n		</div>\n\n	</ion-card> -->\n\n	<!-- <ion-card style="padding: 3vh;"> -->\n		<div style="padding: 3vh;margin-bottom: 5vh;">\n			<h2 style="font-weight: bold;">\n				Description\n			</h2>\n			<p style="color: lightgrey;text-transform: none;" [innerHtml]="productDetails.product_desc"></p>\n		</div>\n	<!-- </ion-card> -->\n\n\n<div class="fixed-outside" style="position: fixed;bottom: 0;width: 100%;background: #FFF;" align="center">\n\n	<ion-grid style="padding: 0px;margin: 0px 5px;">\n		<ion-row>\n			<ion-col col-6>\n				<!-- <button color="secondary" ion-button>TEST</button> -->\n				<ion-grid style="margin-top: 1vh;">\n					<ion-row style="max-width: 20vh;">\n						<ion-col col-4 style="padding: 0px 5px;color: lightgray !important;">\n							<ion-icon style="color: lightgray !important;" (click)="subQty()" name="remove-circle" style="font-size: 2.1em;"></ion-icon>\n						</ion-col>\n						<ion-col col-4><!-- style="border: 1px solid;" style="padding: 0px 5px;" -->\n							<ion-label style="margin: 2px 0 0px 0;font-weight: bold;">{{ productDetails.qty }}</ion-label>\n							<!-- <ion-input type="number" name="qty1" [(ngModel)]="productDetails.qty" style="background: gray;max-width: 70%;color: #FFF;"></ion-input> -->\n						</ion-col>\n						<ion-col col-4 style="padding: 0px 5px;">\n							<ion-icon color="secondary" (click)="addQty()" name="add-circle" style="font-size: 2.1em;"></ion-icon>\n						</ion-col>\n					</ion-row>\n				</ion-grid>\n			</ion-col>\n			<ion-col col-6>\n				<button color="secondary" style="width: 90%;margin-top: 1vh;" ion-button (click)="addProductToCart(productDetails,\'add\')">Add to cart</button>\n			</ion-col>\n		</ion-row>\n	</ion-grid>\n</div>\n\n</ion-content>'/*ion-inline-end:"E:\Projects\IonicBackendBrains\FamilovApp\src\pages\product-details\product-details.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__["a" /* RestProvider */]])
    ], ProductDetailsPage);
    return ProductDetailsPage;
}());

//# sourceMappingURL=product-details.js.map

/***/ }),

/***/ 211:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CheckoutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__confirm_order_confirm_order__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__my_cart_my_cart__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__thankyou_thankyou__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__signup_signup__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__login_login__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__payment_payment__ = __webpack_require__(108);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var CheckoutPage = (function () {
    function CheckoutPage(navCtrl, navParams, restProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restProvider = restProvider;
        this.cartItemsLen = 0;
        this.pickup_from_shop_price = '0.00';
        this.home_delivery_price = '0.00';
        // this.cartItems this.navParams.get('cartItems');
        this.currency = this.navParams.get('currency');
        this.currencySymbol = this.navParams.get('currencySymbol');
        this.promoID = this.navParams.get('promoID');
        this.selected_shop_id = this.navParams.get('selected_shop_id');
        this.totalPrice = this.navParams.get('totalPrice');
        this.allTotalPrice = this.totalPrice;
        this.getShopDeliveryOptions();
        this.LoginUser = localStorage.getItem('LoginUser');
        if (this.LoginUser == undefined || this.LoginUser == null) {
            this.IsLogin = false;
        }
        else {
            this.IsLogin = true;
        }
        this.cartItems = localStorage.getItem('cartItems');
        if (this.cartItems != undefined || this.cartItems != null) {
            this.cartItems = JSON.parse(this.cartItems);
            this.cartItemsLen = this.cartItems.length;
        }
        this.currencyData = localStorage.getItem('currencyData');
        if (this.currencyData != undefined || this.currencyData != null) {
            this.currencyData = JSON.parse(this.currencyData);
            this.currencySymbol = this.currencyData.vSymbol;
        }
    }
    CheckoutPage.prototype.getShopDeliveryOptions = function () {
        var _this = this;
        this.restProvider.LoadingSpinner();
        this.restProvider.getShopDeliveryOptions(this.selected_shop_id, this.currency)
            .then(function (result) {
            console.log(result);
            _this.restProvider.hideLoadingSpinner();
            // alert('result');
            if (result.status == true) {
                // this.restProvider.showSuccessMsg(this.statusSuccess,result.message);
                _this.pickup_from_shop_price = parseFloat(result.data.shop_info.pickup_from_shop_price);
                _this.home_delivery_price = parseFloat(result.data.shop_info.home_delivery_price);
            }
            else {
                if (typeof result.message == "object") {
                    var message = '';
                    for (var i in result.message) {
                        message += result.message[i] + '<br>';
                    }
                    _this.restProvider.showErrorMsg(_this.statusError, message);
                }
                else if (typeof result.message == "string") {
                    _this.restProvider.showErrorMsg(_this.statusError, result.message);
                }
                else {
                    // this.restProvider.showErrorMsg(this.statusError,result.message);
                    _this.restProvider.showErrorMsg(_this.statusError, 'Invalid username or password..!!');
                }
            }
        }, function (err) {
            _this.restProvider.hideLoadingSpinner();
            console.log(err);
        });
    };
    CheckoutPage.prototype.goToConfirmOrder = function (params) {
        if (!params)
            params = {};
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__confirm_order_confirm_order__["a" /* ConfirmOrderPage */]);
    };
    CheckoutPage.prototype.orderConfirmed = function (params) {
        if (!params)
            params = {};
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__thankyou_thankyou__["a" /* ThankYouPage */]);
    };
    CheckoutPage.prototype.openPaymentPage = function () {
        var params = {};
        params.paymentMethod = this.paymentMethod;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__payment_payment__["a" /* PaymentPage */], params);
    };
    CheckoutPage.prototype.selectedOption = function (option) {
        if (option == 'pickup') {
            this.allTotalPrice = (parseFloat(this.totalPrice) + parseFloat(this.pickup_from_shop_price)).toFixed(2);
        }
        else {
            this.allTotalPrice = (parseFloat(this.totalPrice) + parseFloat(this.home_delivery_price)).toFixed(2);
        }
    };
    CheckoutPage.prototype.openCartPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__my_cart_my_cart__["a" /* MyCartPage */]);
    };
    CheckoutPage.prototype.goToSignup = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__signup_signup__["a" /* SignupPage */]);
    };
    CheckoutPage.prototype.goToLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__login_login__["a" /* LoginPage */]);
    };
    CheckoutPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-checkout',template:/*ion-inline-start:"E:\Projects\IonicBackendBrains\FamilovApp\src\pages\checkout\checkout.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle class="buttonWhiteFont">\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>      \n      <ion-grid>\n    		<ion-row>\n    			<ion-col col-10 class="headerTitle"><div align="center">Checkout</div></ion-col>\n    			<ion-col col-2 class="badgeCol">\n					<!-- <ion-icon class="right" name="cart" item-end (click)="openCartPage()"></ion-icon> -->\n					<span class="badgeIcon"> {{ cartItemsLen }}</span>\n					<ion-icon name="cart" item-end (click)="openCartPage()"></ion-icon>\n    			</ion-col>\n    		</ion-row>\n    	</ion-grid>\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content padding id="page4">\n\n	<div style="margin: 1vh;padding: 1vh">\n		<h4 style="margin-top: 4vh;">1. Receipient Informations</h4>\n		\n		<form id="editProfile-form">\n			<ion-list id="editProfile-list">\n\n				<ion-item id="signup-input5" style="border: 1px solid #eee;margin: 1vh auto;">\n					<ion-label><ion-icon name="contact"></ion-icon></ion-label>\n					<ion-input type="text" placeholder="Full name"></ion-input>\n				</ion-item>\n				\n				<ion-item id="signup-input6" style="border: 1px solid #eee;margin: 1vh auto;">\n					<ion-label><ion-icon name="call"></ion-icon></ion-label>\n					<ion-input type="number" placeholder="Contact number"></ion-input>\n				</ion-item>\n\n				<ion-label>Do you want to leave a short message?</ion-label>\n				<ion-item id="signup-input5" style="border: 1px solid #eee;margin: 1vh auto;">\n					<ion-textarea name="sometext" placeholder="Ex: God bless you.."></ion-textarea>\n				</ion-item>\n\n			</ion-list>\n\n		</form>\n\n		<br>\n		<ion-list radio-group color="secondary">\n			<!-- <ion-list-header> -->\n				<h4>2. Delivery Type</h4>\n			<!-- </ion-list-header> -->\n\n			<ion-item>\n				<ion-radio checked="true" color="secondary" value="pickup_from_shop" (ionSelect)="selectedOption(\'pickup\')"></ion-radio>\n				<ion-label style="float: right;">Pick up from Shop ( <span [innerHtml]=\'currencySymbol\'></span> {{ pickup_from_shop_price }} )</ion-label>\n			</ion-item>\n\n			<ion-item>\n				<ion-radio color="secondary" value="home_delivery" (ionSelect)="selectedOption(\'home\')"></ion-radio>\n				<ion-label style="float: right;">Home Delivery ( <span [innerHtml]=\'currencySymbol\'></span> {{ home_delivery_price }} )</ion-label>\n			</ion-item>\n		</ion-list>\n\n		<h4>3. Payment Method</h4>\n\n		<ion-list radio-group color="secondary" *ngIf="IsLogin" [(ngModel)]="paymentMethod">\n			<ion-item>\n				<ion-radio checked="true" color="secondary" value="credit"></ion-radio>\n				<ion-label style="float: right;">Credit Card (VISA, Master etc..)</ion-label>\n			</ion-item>\n\n			<ion-item>\n				<ion-radio color="secondary" value="sofort_oberweisung"></ion-radio>\n				<ion-label style="float: right;">Sofort Oberweisung</ion-label>\n			</ion-item>\n\n			<ion-item>\n				<ion-radio color="secondary" value="bank_transfer"></ion-radio>\n				<ion-label style="float: right;">Bank Transfer</ion-label>\n			</ion-item>\n\n			<ion-item>\n				<ion-radio color="secondary" value="paypal"></ion-radio>\n				<ion-label style="float: right;">Paypal</ion-label>\n			</ion-item>\n		</ion-list>\n\n		<div *ngIf="!IsLogin">\n			<ion-grid>\n				<ion-row>\n					<ion-col col-4 style="padding: 3vh;">\n						<div align="center">\n							<img src="assets/imgs/angel.png" style="max-height: 60px;">\n						</div>\n					</ion-col>\n					<ion-col col-8 style="color: gray;line-height: 0.7;font-weight: bold;">\n						<p>It\'s so easy !</p>\n						<p>Please login or signup</p>\n						<p>to finish t place your order</p>\n					</ion-col>\n				</ion-row>\n			</ion-grid>\n			\n			<div align="center">\n				<button style="height: 2em;text-transform: none;font-size: 20px;font-weight: bold; background: darkgrey;" ion-button (click)="goToSignup()">\n					Sign up\n				</button>\n				<button style="height: 2em;text-transform: none;font-size: 20px;font-weight: bold; " ion-button color="secondary" (click)="goToLogin()">\n					Login\n				</button>\n			</div>\n		</div>\n\n		<br><br>\n		<div *ngIf="IsLogin">\n			<div class="fixed-outside" style="position: fixed;bottom: 0;left: 0;width: 100%;display: inline-flex;" align="center">\n				<button id="login-button1" ion-button block style="background: #3b5998;margin-bottom: 0;height: 2.5em;">\n					Total: <span [innerHtml]="currencySymbol"></span> {{ allTotalPrice }}\n				</button>\n				<button id="login-button1" ion-button block color="secondary" (click)="openPaymentPage()" style="margin-bottom: 0;height: 2.5em;">\n					Buy <ion-icon name="arrow-right"></ion-icon>\n				</button>\n			</div>\n		</div>\n	</div>\n\n</ion-content>'/*ion-inline-end:"E:\Projects\IonicBackendBrains\FamilovApp\src\pages\checkout\checkout.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */]])
    ], CheckoutPage);
    return CheckoutPage;
}());

//# sourceMappingURL=checkout.js.map

/***/ }),

/***/ 212:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConfirmOrderPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__payment_payment__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__my_cart_my_cart__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import { ProductDetailsPage } from '../product-details/product-details';


var ConfirmOrderPage = (function () {
    function ConfirmOrderPage(navCtrl) {
        this.navCtrl = navCtrl;
        this.cartItemsLen = 0;
        this.cartItems = localStorage.getItem('cartItems');
        if (this.cartItems != undefined || this.cartItems != null) {
            this.cartItemsLen = JSON.parse(this.cartItems).length;
        }
    }
    /*viewProductDetails(params) {
        if (!params) params = {};
        this.navCtrl.setRoot(ProductDetailsPage);
    }*/
    ConfirmOrderPage.prototype.goToPayment = function (params) {
        if (!params)
            params = {};
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__payment_payment__["a" /* PaymentPage */]);
    };
    ConfirmOrderPage.prototype.openCartPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__my_cart_my_cart__["a" /* MyCartPage */]);
    };
    ConfirmOrderPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-confirm-order',template:/*ion-inline-start:"E:\Projects\IonicBackendBrains\FamilovApp\src\pages\confirm-order\confirm-order.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle class="buttonWhiteFont">\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>\n    <ion-grid>\n    		<ion-row>\n    			<ion-col col-10 class="headerTitle">Confirm Order</ion-col>\n    			<ion-col col-2 class="badgeCol">\n					<!-- <ion-icon class="right" name="cart" item-end (click)="openCartPage()"></ion-icon> -->\n					<span class="badgeIcon"> {{ cartItemsLen }}</span>\n					<ion-icon name="cart" item-end (click)="openCartPage()"></ion-icon>\n    			</ion-col>\n    		</ion-row>\n    	</ion-grid>\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n<div class="spacer" style="height:20px;" id="login-spacer2"></div>\n\n	<ion-card class="grayBackground">\n		<div align="center" style="padding: 10px;">\n			<h2 color="primary">Please, enter the beneficiary information.</h2>\n			<br>\n			<ion-input placeholder="Enter your name" style="border: 1px solid #E0E0E0;"></ion-input>\n			<br>\n			<ion-input placeholder="Enter your contact number" style="border: 1px solid #E0E0E0;"></ion-input>\n		</div>\n	</ion-card>\n\n	<ion-card class="grayBackground">\n		<div align="center" style="padding: 10px;">\n			<h2 color="primary">Custom message.</h2>\n			<br>\n			<span>Do you want to leave a message to the recipient?</span>\n			<br><br>\n			<ion-input placeholder="Enter your contact message" style="border: 1px solid #E0E0E0;"></ion-input>\n			<br>\n		</div>\n	</ion-card>\n\n	<button ion-button danger block (click)="goToPayment()">Payment</button>\n\n<br>\n  <!-- <p>\n    If you get lost, the <a href="http://ionicframework.com/docs/v2">docs</a> will show you the way.\n  </p> -->\n\n  <!-- <button ion-button secondary menuToggle>Toggle Menu</button> -->\n</ion-content>\n'/*ion-inline-end:"E:\Projects\IonicBackendBrains\FamilovApp\src\pages\confirm-order\confirm-order.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]])
    ], ConfirmOrderPage);
    return ConfirmOrderPage;
}());

//# sourceMappingURL=confirm-order.js.map

/***/ }),

/***/ 215:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ForgotPasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__my_cart_my_cart__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ForgotPasswordPage = (function () {
    function ForgotPasswordPage(navCtrl) {
        this.navCtrl = navCtrl;
        this.cartItemsLen = 0;
        this.cartItems = localStorage.getItem('cartItems');
        if (this.cartItems != undefined || this.cartItems != null) {
            this.cartItemsLen = JSON.parse(this.cartItems).length;
        }
    }
    ForgotPasswordPage.prototype.goToLogin = function (params) {
        if (!params)
            params = {};
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__login_login__["a" /* LoginPage */]);
    };
    ForgotPasswordPage.prototype.openCartPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__my_cart_my_cart__["a" /* MyCartPage */]);
    };
    ForgotPasswordPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-forgot-password',template:/*ion-inline-start:"E:\Projects\IonicBackendBrains\FamilovApp\src\pages\forgot-password\forgot-password.html"*/'<ion-header>\n	<ion-navbar>\n		<button ion-button menuToggle class="buttonWhiteFont">\n			<ion-icon name="menu"></ion-icon>\n		</button>\n		<ion-title>\n			<ion-grid>\n    		<ion-row>\n    			<ion-col col-10 class="headerTitle">Forgot Password</ion-col>\n    			<ion-col col-2 class="badgeCol">\n					<!-- <ion-icon class="right" name="cart" item-end (click)="openCartPage()"></ion-icon> -->\n					<span class="badgeIcon"> {{ cartItemsLen }}</span>\n					<ion-icon name="cart" item-end (click)="openCartPage()"></ion-icon>\n    			</ion-col>\n    		</ion-row>\n    	</ion-grid>\n		</ion-title>\n	</ion-navbar>\n</ion-header>\n<ion-content padding id="page5">\n	\n	<div class="spacer" style="height:20px;" id="login-spacer1"></div>\n	<h2 align="center">Enter your registered Email and we will email you a link to reset your password.</h2>\n	<div class="spacer" style="height:20px;" id="login-spacer2"></div>\n\n	<form id="login-form1">\n		<ion-list id="login-list1">\n			<ion-item id="login-input1">\n				<ion-label floating>\n					Email\n				</ion-label>\n				<ion-input type="email" placeholder=""></ion-input>\n			</ion-item>\n		</ion-list>\n\n		<button id="login-button1" ion-button color="secondary" block>\n			Confirm\n		</button>\n		\n		<button id="login-button2" ion-button clear color="positive" block (click)="goToLogin()">\n			Click here to Login\n		</button>\n\n	</form>\n</ion-content>'/*ion-inline-end:"E:\Projects\IonicBackendBrains\FamilovApp\src\pages\forgot-password\forgot-password.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]])
    ], ForgotPasswordPage);
    return ForgotPasswordPage;
}());

//# sourceMappingURL=forgot-password.js.map

/***/ }),

/***/ 216:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyOrdersPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__my_cart_my_cart__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__home_home__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__order_details_order_details__ = __webpack_require__(217);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var MyOrdersPage = (function () {
    function MyOrdersPage(navCtrl, restProvider) {
        this.navCtrl = navCtrl;
        this.restProvider = restProvider;
        this.cartItemsLen = 0;
        this.restProvider.LoadingSpinner();
        this.apiSuccessMsg = 'Success';
        this.apiErrorMsg = 'Error';
        this.LoginUser = JSON.parse(localStorage.getItem('LoginUser'));
        this.ordersList = [];
        this.cartItems = localStorage.getItem('cartItems');
        if (this.cartItems != undefined || this.cartItems != null) {
            this.cartItemsLen = JSON.parse(this.cartItems).length;
        }
        this.currencySymbol = '&euro;';
        this.currencyName = 'EUR';
        this.currencyData = localStorage.getItem('currencyData');
        if (this.currencyData == undefined || this.currencyData == null) {
            this.currencyData = [];
            this.currencySymbol = '&euro;';
            this.currencyName = 'EUR';
        }
        else {
            this.currencyData = JSON.parse(this.currencyData);
            this.currencySymbol = this.currencyData.vSymbol;
            this.currencyName = this.currencyData.vName;
        }
        this.getOrders();
        this.restProvider.hideLoadingSpinner();
    }
    MyOrdersPage.prototype.openCartPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__my_cart_my_cart__["a" /* MyCartPage */]);
    };
    MyOrdersPage.prototype.goToHome = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */]);
    };
    MyOrdersPage.prototype.getOrders = function () {
        var _this = this;
        var customer_id = this.LoginUser.customer_id;
        this.restProvider.getOrdersList(customer_id)
            .then(function (result) {
            console.log(result);
            if (result.status == true) {
                if (result.data.total_orders > 0) {
                    _this.ordersList = result.data.order_list;
                }
            }
            else {
                _this.restProvider.showErrorMsg(_this.apiErrorMsg, 'Something went wrong, Please try again.');
            }
        }, function (err) {
            // console.log(err);
            _this.restProvider.showErrorMsg(_this.statusError, 'Opps..!! <br> Something went wrong.');
        });
    };
    MyOrdersPage.prototype.orderDetails = function (order_id) {
        var params = {};
        params.order_id = order_id;
        params.customer_id = this.LoginUser.customer_id;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__order_details_order_details__["a" /* OrderDetailsPage */], params);
    };
    MyOrdersPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-my-orders',template:/*ion-inline-start:"E:\Projects\IonicBackendBrains\FamilovApp\src\pages\my-orders\my-orders.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle class="buttonWhiteFont">\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>\n      <ion-grid>\n    		<ion-row>\n    			<ion-col col-10 class="headerTitle">My Orders</ion-col>\n    			<ion-col col-2 class="badgeCol">\n    				<!-- <ion-icon class="right" name="cart" item-end (click)="openCartPage()"></ion-icon> -->\n    				<span class="badgeIcon"> {{ cartItemsLen }}</span>\n					<ion-icon name="cart" item-end (click)="openCartPage()"></ion-icon>\n    			</ion-col>\n    		</ion-row>\n    	</ion-grid>\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content padding id="page8">\n\n	<!-- <ion-card>\n		<ion-grid>\n			<ion-row>\n				<ion-col col-9>\n					<!- - <ion-label color="dark" style="background: gray; color: #FFF; width: 4vh; height: 4vh; border-radius: 50%; padding: inherit; text-align: center;">1</ion-label> - ->\n					<h1>1</h1>\n					<button ion-button color="dark" style="margin-left: auto;">F-6184075392</button>\n					<ion-label>November 10, 2017 08:02 AM</ion-label>\n					<ion-label>Bank Transfer</ion-label>\n					<ion-label>Pick up from the shop</ion-label>\n				</ion-col>\n				<ion-col col-3 style="text-align: -webkit-right;">\n					<button style="margin-top: 9vh;width: -webkit-fill-available;" ion-button outline color="secondary">Details</button>\n					<h1>$ 5.14</h1>\n				</ion-col>\n			</ion-row>\n		</ion-grid>\n	</ion-card> -->\n\n	<div *ngIf="ordersList.length == 0">\n		<div style="vertical-align: middle;margin-top: 20vh;margin-bottom: 20vh;" align="center">\n			<img src="assets/imgs/tired.png" style="max-height: 15vh;width: auto;">\n			      <h2>Ouuppss !!!</h2>\n			      <h3 style="color: darkblue;">You have not purchase any items...!</h3>\n			      <br>\n			      <br>\n			      <p>Please purchase items to view list here.</p>\n			  <div align="center">\n				<button id="login-button1" ion-button color="secondary" (click)="goToHome()" style="margin: 1vh;width: 80%;height: 3.5em;">\n					Back to Shop\n				</button>\n			</div>\n		</div>\n	</div>\n\n	<div *ngIf="ordersList.length > 0">\n\n		<ion-item>\n			<ion-icon name="basket" item-start style="font-size:4rem;" color="danger"></ion-icon>\n			Total Order\n			<ion-badge item-end>{{ ordersList.length }}</ion-badge>\n		</ion-item>\n\n		<ion-list>\n			<!-- <ion-card *ngFor="let order of ordersList">\n				<ion-card-header>\n			    	<ion-grid>\n			    		<ion-row>\n			    			<ion-col col-7>Order #{{ order.order_id }}</ion-col>\n			    			<ion-col col-5>{{ order.date_payment | date}}</ion-col>\n			    		</ion-row>\n			    	</ion-grid>\n			  	</ion-card-header>\n			  	<hr>\n				<ion-item>\n				  	<ion-thumbnail item-start>\n				      	<img src="assets/imgs/slide2.png">\n				    </ion-thumbnail>\n				    <h2>Status: {{ order.order_status }}</h2>\n				    <p>{{ order.payment_curr }} {{ order.grand_total }}</p>\n				    <button ion-button clear item-end (click)="orderDetails(order.order_id)">\n				    	View More\n				    </button>\n				</ion-item>\n			</ion-card> -->\n\n\n			<ion-card *ngFor="let order of ordersList">\n				<ion-grid>\n					<ion-row>\n						<ion-col col-9>\n							<!-- <ion-label color="dark" style="background: gray; color: #FFF; width: 4vh; height: 4vh; border-radius: 50%; padding: inherit; text-align: center;">1</ion-label> -->\n							<!-- <h1>1</h1> -->\n							<button ion-button color="dark" style="margin-left: auto;">{{ order.generate_code }}</button>\n							<ion-label>{{ order.date_time }}</ion-label>\n							<ion-label>{{ order.payment_method }}</ion-label>\n							<ion-label>{{ order.delivery_option }}</ion-label>\n							<button ion-button outline>{{ order.order_status }}</button>\n						</ion-col>\n						<ion-col col-3 style="text-align: -webkit-right;">\n							<button style="width: -webkit-fill-available;" ion-button outline color="secondary" (click)="orderDetails(order.order_id)">Details</button><!-- margin-top: 9vh; -->\n							<h2><span [innerHtml]=\'currencySymbol\'></span>{{ order.grand_total }}</h2>\n						</ion-col>\n					</ion-row>\n				</ion-grid>\n			</ion-card>\n\n			<!-- <ion-card>\n				<ion-card-header>\n			    	<ion-grid>\n			    		<ion-row>\n			    			<ion-col col-7>Order #00002</ion-col>\n			    			<ion-col col-5>2017-11-02</ion-col>\n			    		</ion-row>\n			    	</ion-grid>\n			  	</ion-card-header>\n			  	<hr>\n				<ion-item>\n				  	<ion-thumbnail item-start>\n				      	<img src="assets/imgs/slide2.png">\n				    </ion-thumbnail>\n				    <h2>Product 2</h2>\n				    <p>$198.8</p>\n				    <button ion-button clear item-end>\n				    	View More\n				    </button>\n				</ion-item>\n			</ion-card>\n\n			<ion-card>\n				<ion-card-header>\n			    	<ion-grid>\n			    		<ion-row>\n			    			<ion-col col-7>Order #00003</ion-col>\n			    			<ion-col col-5>2017-11-02</ion-col>\n			    		</ion-row>\n			    	</ion-grid>\n			  	</ion-card-header>\n			  	<hr>\n				<ion-item>\n				  	<ion-thumbnail item-start>\n				      	<img src="assets/imgs/slide2.png">\n				    </ion-thumbnail>\n				    <h2>Product 3</h2>\n				    <p>$198.8</p>\n				    <button ion-button clear item-end>\n				    	View More\n				    </button>\n				</ion-item>\n			</ion-card> -->\n		</ion-list>\n	</div>\n\n</ion-content>'/*ion-inline-end:"E:\Projects\IonicBackendBrains\FamilovApp\src\pages\my-orders\my-orders.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */]])
    ], MyOrdersPage);
    return MyOrdersPage;
}());

//# sourceMappingURL=my-orders.js.map

/***/ }),

/***/ 217:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__my_cart_my_cart__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import { HomePage } from '../home/home';

var OrderDetailsPage = (function () {
    function OrderDetailsPage(navCtrl, alertCtrl, restProvider, navParams) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.restProvider = restProvider;
        this.navParams = navParams;
        this.cartItemsLen = 0;
        this.apiSuccessMsg = 'Success';
        this.apiErrorMsg = 'Error';
        this.order_id = this.navParams.get('order_id');
        this.customer_id = this.navParams.get('customer_id');
        this.orderItemsCnt = 0;
        this.currencySymbol = '&euro;';
        this.currencyName = 'EUR';
        this.cartItems = localStorage.getItem('cartItems');
        if (this.cartItems != undefined || this.cartItems != null) {
            this.cartItemsLen = JSON.parse(this.cartItems).length;
        }
        this.currencyData = localStorage.getItem('currencyData');
        if (this.currencyData == undefined || this.currencyData == null) {
            this.currencyData = [];
            this.currencySymbol = '&euro;';
            this.currencyName = 'EUR';
        }
        else {
            this.currencyData = JSON.parse(this.currencyData);
            this.currencySymbol = this.currencyData.vSymbol;
            this.currencyName = this.currencyData.vName;
        }
        this.orderDetails();
    }
    OrderDetailsPage.prototype.openCartPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__my_cart_my_cart__["a" /* MyCartPage */]);
    };
    OrderDetailsPage.prototype.orderDetails = function () {
        var _this = this;
        // console.log('test');
        // return false;
        this.restProvider.getOrderDetails(this.customer_id, this.order_id)
            .then(function (result) {
            if (result.status == true) {
                _this.orderDetails = result.data.order_info;
                _this.orderItems = result.data.order_items;
                _this.orderItemsCnt = result.data.order_items.length;
                // console.log(this.orderDetails);
                // console.log(this.orderItems);
            }
            else {
                _this.restProvider.showErrorMsg(_this.apiErrorMsg, 'Something went wrong, Please try again.');
            }
        }, function (err) {
            // console.log(err);
            _this.restProvider.showErrorMsg(_this.statusError, 'Opps..!! <br> Something went wrong.');
        });
    };
    OrderDetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-order-details',template:/*ion-inline-start:"E:\Projects\IonicBackendBrains\FamilovApp\src\pages\order-details\order-details.html"*/'<ion-header>\n	<ion-navbar>\n		<button ion-button menuToggle class="buttonWhiteFont">\n			<ion-icon name="menu"></ion-icon>\n		</button>\n		<ion-title>\n			<ion-grid>\n	    		<ion-row>\n	    			<ion-col col-10 class="headerTitle">Order Details</ion-col>\n	    			<ion-col col-2 class="badgeCol">\n	    				<!-- <ion-icon class="right" name="cart" item-end (click)="openCartPage()"></ion-icon> -->\n	    				<span class="badgeIcon"> {{ cartItemsLen }}</span>\n						<ion-icon name="cart" item-end (click)="openCartPage()"></ion-icon>\n	    			</ion-col>\n	    		</ion-row>\n	    	</ion-grid>\n		</ion-title>\n	</ion-navbar>\n</ion-header>\n<ion-content padding id="page5">\n	\n	<div class="spacer" style="height:30px;" id="login-spacer2"></div>\n\n	<h2 align="right" style="margin: 1vh;" class="textBlue">Order Details: {{ orderDetails.generate_code }}</h2>\n	<ion-grid>\n		<ion-row>\n			<ion-col col-6>\n				<p><b>Order Code: </b></p>\n				<p><b>Receiver name: </b></p>\n				<p><b>Receiver Contact: </b></p>\n				<p><b>Greeting Text: </b></p>\n				<p style="min-height: 5vh;"><b>Order date: </b></p>\n				<p style="min-height: 4vh;"><b>Payment method: </b></p>\n				<p><b>Order status: </b></p>\n			</ion-col>\n			<ion-col col-6>\n				<p class="textRight">{{ orderDetails.generate_code }}</p>\n				<p class="textRight">{{ orderDetails.receiver_name }}</p>\n				<p class="textRight">{{ orderDetails.receiver_phone }}</p>\n				<p class="textRight">{{ orderDetails.greeting_msg }}</p>\n				<p class="textRight" style="min-height: 5vh;">{{ orderDetails.date_time }}</p>\n				<p style="min-height: 4vh;" class="textRight">{{ orderDetails.payment_method }}</p>\n				<p class="textRight">{{ orderDetails.order_status }}</p>\n			</ion-col>\n		</ion-row>\n	</ion-grid>\n\n	<!-- <div style="margin: 0vh 1vh;">\n		<ion-grid class="grayBackground">\n			<ion-row>\n				<ion-col col-6 style="border: 5px solid #FFF;">\n					<h4 align="center"> Order Number </h4>\n					<h4 align="center"> {{ orderDetails.order_id }} </h4>\n				</ion-col>\n				<ion-col col-6 style="border: 5px solid #FFF;">\n					<h4 align="center"> Order Total </h4>\n					<h4 align="center"> {{ orderDetails.grand_total }} </h4>\n				</ion-col>\n			</ion-row>\n		</ion-grid>\n	</div> -->\n\n	<div class="spacer" style="height:25px;" id="login-spacer4"></div>\n\n	<!-- <div style="margin: 1vh;">\n		<ion-grid>\n			<ion-row>\n				<ion-col col-6 style="border: 5px solid #FFF;">\n					<h1 style="float: left;"> Items </h1>\n				</ion-col>\n				<ion-col col-6 style="border: 5px solid #FFF;">\n					<h1 style="float: right;"> {{ orderItemsCnt }} </h1>\n				</ion-col>\n			</ion-row>\n		</ion-grid>\n	</div> -->\n\n	<h2 align="center">Items: </h2>\n	<ion-grid>\n		<ion-row>\n			<ion-col style="border: 1px solid #EEE;" col-3><b>Picture</b></ion-col>\n			<ion-col style="border: 1px solid #EEE;" col-4><b>Name</b></ion-col>\n			<ion-col style="border: 1px solid #EEE;" col-3><b>Amount X Unit Price</b></ion-col>\n			<ion-col style="border: 1px solid #EEE;" col-2><b>Total Price</b></ion-col>\n		</ion-row>\n\n		<ion-row *ngFor="let item of orderItems">\n			<ion-col style="border: 1px solid #EEE;" col-3>\n				<img src="{{ item.product_image }}" style="max-height: 150px;width: auto;">\n			</ion-col>\n			<ion-col style="border: 1px solid #EEE;" col-4>\n				{{ item.product_name }}\n			</ion-col>\n			<ion-col style="border: 1px solid #EEE;" col-3 class="textRight">\n				{{item.product_prize}} X {{ item.quantity }}\n			</ion-col>\n			<ion-col style="border: 1px solid #EEE;" col-2>\n				{{ item.total_prize }}\n			</ion-col>\n		</ion-row>\n\n	</ion-grid>\n\n	<!-- <ion-card class="ion-card-scroll grayBackground" *ngFor="let item of orderItems" style="margin-bottom: 1vh;">\n		<ion-grid style="margin: 1vh;">\n			<ion-row>\n				<ion-col col-9>\n					<h2 align="left">{{ item.product_name }}</h2>\n					<hr>\n					<img src="{{ item.product_image }}" style="max-height: 150px;width: auto;">\n				</ion-col>\n				<ion-col col-3>\n					<h2 align="right">{{item.product_prize}} * {{ item.quantity }}</h2>\n					<hr>\n					<h2 align="right">{{ item.total_prize }}</h2>\n				</ion-col>\n			</ion-row>\n		</ion-grid>\n	</ion-card> -->\n\n	<ion-grid style="margin-bottom: 5vh;">\n		<ion-row>\n			<ion-col col-6>\n				<p><b>Sub Total: </b></p>\n				<p><b>Service Charge: </b></p>\n				<p><b>{{ orderDetails.payment_method }}: </b></p>\n				<p><b>Promo code: </b></p>\n				<h1>Total Price: </h1>\n			</ion-col>\n			<ion-col col-6>\n				<p class="textRight"><span [innerHtml]="currencySymbol"></span> {{ orderDetails.sub_total }}</p>\n				<p class="textRight"><span [innerHtml]="currencySymbol"></span> {{ orderDetails.service_tax }}</p>\n				<p class="textRight"><span [innerHtml]="currencySymbol"></span> {{ orderDetails.delivery_price }}</p>\n				<p class="textRight"><span [innerHtml]="currencySymbol"></span> {{ orderDetails.promo_discount_amount }}</p>\n				<h1 class="textRight" [innerHtml]="orderDetails.amount"></h1>\n			</ion-col>\n		</ion-row>\n	</ion-grid>\n\n	<!-- <ion-card class="ion-card-scroll" style="margin-bottom: 3vh;">\n		<h1 align="center" style="margin-top: 1vh;">Order details</h1><hr>\n		<ion-grid>\n			<ion-row>\n				<ion-col col-4>\n					<p><b>Order Date:: </b></p>\n					<p><b>Greeting Text:</b></p>\n				</ion-col>\n				<ion-col col-8>\n					<p align="right">{{orderDetails.date_time}}</p>\n					<p align="right">{{ orderDetails.greeting_msg }}</p>\n				</ion-col>\n			</ion-row>\n		</ion-grid>\n	</ion-card>\n\n	<ion-card class="ion-card-scroll" style="margin-bottom: 3vh;">\n		<h1 align="center" style="margin-top: 1vh;">Delivery Status</h1><hr>\n\n		<ion-grid>\n			<ion-row>\n				<ion-col col-4>\n					<p><b>Delivery Type:</b></p>\n					<p><b>Delivery Price:</b></p>\n					<p><b>Status:</b></p>\n				</ion-col>\n				<ion-col col-8>\n					<p>{{ orderDetails.delivery_option }}</p>\n					<p>{{ orderDetails.delivery_price }}</p>\n					<p>{{ orderDetails.order_status }}</p>\n				</ion-col>\n			</ion-row>\n		</ion-grid>\n	</ion-card>\n\n	<ion-card class="ion-card-scroll" style="margin-bottom: 3vh;">\n		<h1 align="center" style="margin-top: 1vh;">Payment details</h1><hr>\n		<ion-grid>\n			<ion-row>\n				<ion-col col-6>\n					<p><b>Payment Method:</b></p>\n					<p><b>Payment Currency:</b></p>\n				</ion-col>\n				<ion-col col-6>\n					<p>{{ orderDetails.payment_method }}</p>\n					<p>{{ orderDetails.payment_curr }}</p>\n				</ion-col>\n			</ion-row>\n		</ion-grid>\n	</ion-card>\n\n	<ion-card class="ion-card-scroll" style="margin-bottom: 3vh;">\n		<h1 align="center" style="margin-top: 1vh;">Receiver details</h1><hr>\n		<ion-grid>\n			<ion-row>\n				<ion-col col-4>\n					<p><b>Name:</b></p>\n					<p><b>Contact No.:</b></p>\n				</ion-col>\n				<ion-col col-8>\n					<p>{{ orderDetails.receiver_name }}</p>\n					<p>{{ orderDetails.receiver_phone }}</p>\n				</ion-col>\n			</ion-row>\n		</ion-grid>\n	</ion-card> -->\n\n</ion-content>'/*ion-inline-end:"E:\Projects\IonicBackendBrains\FamilovApp\src\pages\order-details\order-details.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], OrderDetailsPage);
    return OrderDetailsPage;
}());

//# sourceMappingURL=order-details.js.map

/***/ }),

/***/ 218:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InviteFriendPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var InviteFriendPage = (function () {
    function InviteFriendPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.frequentQuestions = [
            {
                title: "What is Familov.com?",
                description: "Everything is in the name. With familov.com, order from abroad and we deliver your family to the country in less than 36 hours."
            },
            {
                title: "How can I create an account?",
                description: "Sur la page de démarrage de Familov, cliquez sur Inscrivez-vous (ou allez directement à la page Créer un compte). Remplissez les informations et votre mot de passe et confirmez-les une deuxième fois. Votre adresse de courriel sera directement liée au compte."
            },
            {
                title: "I forgot my password?",
                description: "Vous avez déjà créé un compte mais vous ne vous souvenez plus de votre mot de passe? Cliquez sur «Login» en haut de la page. Puis cliquez sur 'Mot de passe oublié?'. Remplissez votre adresse e-mail et un nouveau mot de passe vous sera envoyé par e-mail."
            },
            {
                title: "How long does a delivery take?",
                description: "En moyenne, nous avons besoin de moins de 36 heures à partir du moment où nous avons recu le payement jusqu'au moment de la livraison à votre proche. Le délai de livraison peut varier d'une boutique à l'autre et dépend du nombre de commandes que la boutique doit préparer et la disponibilité du bénéficiaire."
            },
            {
                title: "How much is shipping?",
                description: "Nous facturons un forfait de 2,95 € (2,50€ pendant le phase béta) pour le service et 0,00€ pour les livraison en magasin.En fonction de lieu de residence le coût des livraison à domicile varie."
            },
            {
                title: "What happens after placing my order?",
                description: "L'ordre arrive dans notre système et après que nous recevons le paiement, il est envoyé directement à la boutique (Shopper) que vous avez commandé. Après seulement quelques secondes, les magasins reçoivent l'ordre par le biais de notre propre système Shop-Connect.<br> <br>1- Le magasin recoit tous les détails de votre commande <br> 2- Votre proche recoit le code de retrait par SMS<br> 3-Le livreur prépare le paquet et recontacte le bénéficiaire dans les 36 heures pour la livraison <br>4-Vous recevez une confirmation par SMS ou e-mail que votre commande est bien livrée. <br>NB: Nous travaillons avec partenaires n et des livreurs proffessionnels pour gerer vos commanades. Ne vous inquiétez pas, ils ont été formés individuellement pes sont formés pour donner la meilleure qualité , soigner vos articles et les livrer."
            },
            {
                title: "Where can I see my receipts?",
                description: "Vous pouvez afficher vos reçus de deux façons: Email: Lorsque votre commande est passée, vous recevrez un courriel avec un reçu. <br> <br> Sur le site Web cliquez sur votre nom dans le coin supérieur droit et un menu apparaîtra. Sélectionnez Mes commandes. Cliquez sur un numéro de commande pour afficher un reçu détaillé de cet ordre."
            },
            {
                title: "How is my order confirmed?",
                description: "Une fois votre commande passée, vous recevrez un e-mail de confirmation envoyé à l'adresse e-mail que vous avez saisie lors de votre commande. Conservez ce courriel et votre numéro de commande pour toute question ou remarque concernant votre commande. Lorsque vous recevez l'e-mail de confirmation, Familov fait le reste. Il suffit de s'asseoir, de se détendre et de profiter!"
            },
            {
                title: "I have not received a confirmation email",
                description: "Si un numéro de commande s'affiche à l'écran, cela signifie que votre commande a été envoyée. L'e-mail de confirmation peut être dans votre «spam» plutôt que dans votre boîte de réception. Si vous ne recevez pas de numéro de commande, veuillez contacter notre service à la clientèle pour connaître l'état de votre commande. Vous devez fournir l'adresse e-mail que vous avez saisie lors de la commande et le nom de la boutique sélectionnée afin que nous puissions trouver votre commande."
            }
        ];
        this.paymentQuestions = [
            {
                title: "What types of payment can I use?",
                description: "Carte de crédit (VISA / American Express / Master Card /)<br>Pay Pal <br> Virement"
            },
            {
                title: "How secure are online payments?",
                description: "Lors du traitement des paiements en ligne sur Familov.com, nous utilisons des pages SSL sécurisées. Cela signifie que ces pages assurent la protection de vos données personnelles et de paiement. De cette façon, vous pouvez être sûr que cette information est seulement visible pour vous et pour nous, pour le traitement de votre commande."
            },
            {
                title: "Why do I have to pay transaction fees?",
                description: "Familov.com est seulement un intermédiaire, entre vous en tant que consommateur et la Boutique. Vu le faible revenu que nous gagnons sur chaque commande, nous devons facturer pour l'utilisation d'une méthode de paiement en ligne. Les coûts couvrent l'entretien technique des paiements en ligne, les coûts de transaction que les banques facturent et nos frais administratifs pour le traitement des paiements. Tous les sites facturent ces coûts, mais ils les ajoutent souvent directement aux frais d'envoi et / ou d'administration.. Nous vous remercions pour votre compréhension."
            },
            {
                title: "My online payment failed",
                description: "Si vous pensez que le paiement en ligne a échoué, vérifiez toujours si vous avez reçu un e-mail de confirmation. Cet e-mail vous permettra de voir si votre commande a été soumise et si la commande est déjà payée ou non. <br><br> L'e-mail de confirmation est envoyé directement une fois le paiement terminé. Cela peut prendre un peu plus longtemps si le paiement est traité par la banque, la société de carte de crédit ou toute autre société de paiement. Cela peut prendre jusqu'à 15 minutes. Si après 15 minutes vous n'avez toujours pas de courriel de confirmation dans votre boîte de réception, contactez notre service à la clientèle. Ils vous diront si l'ordre et le paiement ont réussi."
            },
            {
                title: "I still have a lot of questions to ask you?",
                description: "Contactez nous! Nous serons là à :<br><br> Email:hello@familov.com <br><br> Whatsapp : (+49 1525 9948834)<br><br> Chat : www.familov.com"
            }
        ];
        this.shownGroup = null;
        // If we navigated to this page, we will have an item available as a nav param
        this.selectedItem = navParams.get('item');
        // Let's populate this page with some filler content for funzies
        this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
            'american-football', 'boat', 'bluetooth', 'build'];
        this.items = [];
        for (var i = 1; i < 11; i++) {
            this.items.push({
                title: 'Item ' + i,
                note: 'This is item #' + i,
                icon: this.icons[Math.floor(Math.random() * this.icons.length)]
            });
        }
    }
    InviteFriendPage.prototype.toggleGroup = function (group) {
        if (this.isGroupShown(group)) {
            this.shownGroup = null;
        }
        else {
            this.shownGroup = group;
        }
    };
    ;
    InviteFriendPage.prototype.isGroupShown = function (group) {
        return this.shownGroup === group;
    };
    ;
    InviteFriendPage.prototype.itemTapped = function (event, item) {
        // That's right, we're pushing to ourselves!
        this.navCtrl.push(FaqPage, {
            item: item
        });
    };
    InviteFriendPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-invite-friend',template:/*ion-inline-start:"E:\Projects\IonicBackendBrains\FamilovApp\src\pages\invite-friend\invite-friend.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Invite Your Friends</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n\n<h3 align="center" style="margin: 5vh auto;">Invite your friend to earn money.</h3>\n\n  <!-- <button ion-button color="dark">Dark</button> -->\n\n  <!-- <ion-item> -->\n      <ion-input style="display: none;" value="http://103.21.58.248/familov/sign-up/referral58ef1d0f3f0d56.33885431" disabled></ion-input>\n      <p align="center" style="background: lightgray;margin: 1vh;padding: 1vh;word-break: break-all;">http://103.21.58.248/familov/sign-up/referral58ef1d0f3f0d56.33885431</p>\n  <!-- </ion-item> -->\n\n  <div align="center">\n      <button ion-button color="secondary">Copy the link</button>\n  </div>\n\n  <h3 align="center" style="margin: 5vh auto;">Or click to invite friends on Facebook</h3>\n\n  <div align="center">\n      <button ion-button color="secondary">Facebook</button>\n  </div>\n\n  <ion-grid style="margin: 2vh auto;">\n      <div style="padding: 2vh;background: #EEE;" align="center">\n        <ion-icon style="font-size: 3em;" name="cash"></ion-icon>\n        <p>Your Win</p>\n        <button ion-button round color="secondary">0.00</button>\n      </div>\n  </ion-grid>\n\n  <ion-grid style="margin: 2vh auto;">\n        <div style="padding: 2vh;background: #EEE;" align="center">\n          <ion-icon style="font-size: 3em;" name="checkmark-circle-outline"></ion-icon>\n          <p>You have used always</p>\n          <button ion-button round color="secondary">0.00</button>\n        </div>\n  </ion-grid>\n\n  <ion-grid style="margin: 2vh auto;">\n        <div style="padding: 2vh;background: #EEE;" align="center">\n          <ion-icon style="font-size: 3em;" name="logo-usd"></ion-icon>\n          <p>Available for checkout</p>\n          <button ion-button round color="secondary">0.00</button>\n        </div>\n  </ion-grid>\n\n\n</ion-content>\n'/*ion-inline-end:"E:\Projects\IonicBackendBrains\FamilovApp\src\pages\invite-friend\invite-friend.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], InviteFriendPage);
    return InviteFriendPage;
}());

//# sourceMappingURL=invite-friend.js.map

/***/ }),

/***/ 219:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutUsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AboutUsPage = (function () {
    function AboutUsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AboutUsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-about-us',template:/*ion-inline-start:"E:\Projects\IonicBackendBrains\FamilovApp\src\pages\about-us\about-us.html"*/'<ion-header>\n    <ion-navbar>\n        <button ion-button menuToggle>\n            <ion-icon name="menu"></ion-icon>\n        </button>\n        <ion-title>About US</ion-title>\n    </ion-navbar>\n</ion-header>\n<ion-content padding>\n\n    <h3 align="center" style="margin: 5vh auto;">Familov is a web platform that allows people living abroad to purchase products and services from local stores and have them delivered to their families back home.</h3>\n\n    <div align="center">\n        <img src="assets/imgs/teamwork.png" style="max-height: 150px;">\n        <p>"We are an entrepreneurial social and collaborative venture, with a real mission ...</p>\n    </div>\n    \n    <div align="center" style="margin-top: 5vh;padding: 1vh;">\n        <h3>Why us ?</h3>\n        <p>\n            Far from the major international institutions, the African diaspora remains the continent\'s largest donor. In 2015, the equivalent of $ 35 billion was transferred by expatriate African communities to their relatives who remained in the countries of origin. An invaluable gain, but part of the value remains confiscated upstream because of high commissions, which can easily exceed the 10% threshold. Sometimes 15%. A shortfall for the families who receive the funds.\n        </p>\n    </div>\n\n    <ion-card style="background: #36cd75;padding: 1vh;color: #FFF;">\n        <div align="center" style="margin-top: 1vh;">\n            <h1>Our Mission</h1>\n            <p>Maximize the financial, social and human impact of money transfer</p>\n        </div>\n    </ion-card>\n\n    <ion-card style="background: #36cd75;padding: 1vh;color: #FFF;">\n        <div align="center" style="margin-top: 1vh;">\n            <h1>Price</h1>\n            <p>Social Innovation Award at the start of Google Week-End Saar 2015</p>\n        </div>\n    </ion-card>\n    \n\n    <h1 align="center" style="margin-top: 2vh;">Our value</h1>\n    \n    <ion-card style="padding: 2vh;margin-bottom: 2vh;">\n        <div align="center" style="margin-top: 1vh;">\n            <ion-icon name="thumbs-up"></ion-icon>\n            <h3>Embrace transparency</h3>\n            <p>We value and respect our customers and partners. If it was not for them, we would not be us.</p>\n        </div>\n    </ion-card>\n\n    <ion-card style="padding: 2vh;margin-bottom: 2vh;">\n        <div align="center" style="margin-top: 1vh;">\n            <ion-icon name="football"></ion-icon>\n            <h3>Positive impact</h3>\n            <p>We care about each other and believe in the relationships in which people value and support each other.</p>\n        </div>\n    </ion-card>\n\n    <ion-card style="padding: 2vh;margin-bottom: 2vh;">\n        <div align="center" style="margin-top: 1vh;">\n            <ion-icon name="heart"></ion-icon>\n            <h3>Passion</h3>\n            <p>We like to help our clients achieve their goals, take care of their loved ones or simply increase their savings.</p>\n        </div>\n    </ion-card>\n\n</ion-content>'/*ion-inline-end:"E:\Projects\IonicBackendBrains\FamilovApp\src\pages\about-us\about-us.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], AboutUsPage);
    return AboutUsPage;
}());

//# sourceMappingURL=about-us.js.map

/***/ }),

/***/ 220:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(242);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 242:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(284);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_products_products__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_my_cart_my_cart__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_my_orders_my_orders__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_my_profile_my_profile__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_login_login__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_signup_signup__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_forgot_password_forgot_password__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_change_password_change_password__ = __webpack_require__(301);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_payment_payment__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_thankyou_thankyou__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_product_details_product_details__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_checkout_checkout__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_confirm_order_confirm_order__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_order_details_order_details__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_faq_faq__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_invite_friend_invite_friend__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_terms_terms__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_about_us_about_us__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ionic_native_status_bar__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__ionic_native_splash_screen__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__angular_common_http__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__providers_rest_rest__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__ionic_native_paypal__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__ionic_native_stripe__ = __webpack_require__(214);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




// import { HomePage } from '../pages/home/home';
// import { ListPage } from '../pages/list/list';

























var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* FamiLov */],
                __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_products_products__["a" /* ProductsPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_my_cart_my_cart__["a" /* MyCartPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_checkout_checkout__["a" /* CheckoutPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_confirm_order_confirm_order__["a" /* ConfirmOrderPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_my_orders_my_orders__["a" /* MyOrdersPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_my_profile_my_profile__["a" /* MyProfilePage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_signup_signup__["a" /* SignupPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_forgot_password_forgot_password__["a" /* ForgotPasswordPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_change_password_change_password__["a" /* ChangePasswordPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_order_details_order_details__["a" /* OrderDetailsPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_payment_payment__["a" /* PaymentPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_thankyou_thankyou__["a" /* ThankYouPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_product_details_product_details__["a" /* ProductDetailsPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_faq_faq__["a" /* FaqPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_invite_friend_invite_friend__["a" /* InviteFriendPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_terms_terms__["a" /* TermsPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_about_us_about_us__["a" /* AboutUsPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_25__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* FamiLov */], {}, {
                    links: []
                }),
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* FamiLov */],
                __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_products_products__["a" /* ProductsPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_my_cart_my_cart__["a" /* MyCartPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_checkout_checkout__["a" /* CheckoutPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_confirm_order_confirm_order__["a" /* ConfirmOrderPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_my_orders_my_orders__["a" /* MyOrdersPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_my_profile_my_profile__["a" /* MyProfilePage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_signup_signup__["a" /* SignupPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_forgot_password_forgot_password__["a" /* ForgotPasswordPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_change_password_change_password__["a" /* ChangePasswordPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_order_details_order_details__["a" /* OrderDetailsPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_payment_payment__["a" /* PaymentPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_thankyou_thankyou__["a" /* ThankYouPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_product_details_product_details__["a" /* ProductDetailsPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_faq_faq__["a" /* FaqPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_invite_friend_invite_friend__["a" /* InviteFriendPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_terms_terms__["a" /* TermsPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_about_us_about_us__["a" /* AboutUsPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_23__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_24__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_26__providers_rest_rest__["a" /* RestProvider */],
                __WEBPACK_IMPORTED_MODULE_27__ionic_native_paypal__["a" /* PayPal */],
                __WEBPACK_IMPORTED_MODULE_28__ionic_native_stripe__["a" /* Stripe */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 284:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FamiLov; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_rest_rest__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_home_home__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_my_cart_my_cart__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_my_orders_my_orders__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_my_profile_my_profile__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_login_login__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_signup_signup__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_faq_faq__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_invite_friend_invite_friend__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_terms_terms__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_about_us_about_us__ = __webpack_require__(219);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
















var FamiLov = (function () {
    function FamiLov(platform, statusBar, splashScreen, restProvider, toastCtrl, alertCtrl) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.restProvider = restProvider;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */];
        this.cartItemsLen = 0;
        this.initializeApp();
        /*this.restProvider.LoadingSpinner();
    setTimeout( () => {
        alert('1234');
        this.restProvider.hideLoadingSpinner();
    },300);*/
        /*this.platform.registerBackButtonAction(
          () => {
            if(this.nav.canGoBack()) {
              this.nav.pop();
            }
            else {
              if(this.alert){
                this.alert.dismiss();
                this.alert =null;
              }
              else{
                this.showAlert();
              }
            }
        });*/
        // used for an example of ngFor and navigation
        this.apiToken = localStorage.getItem('apiToken');
        if (this.apiToken == undefined || this.apiToken == null) {
            this.getToken();
        }
        this.currencyData = localStorage.getItem('currencyData');
        if (this.currencyData == 'undefined' || this.currencyData == null) {
            this.getCurrency();
        }
        this.cartItems = localStorage.getItem('cartItems');
        if (this.cartItems != undefined || this.cartItems != null) {
            this.cartItemsLen = JSON.parse(this.cartItems).length;
        }
        this.LoginUser = localStorage.getItem('LoginUser');
        if (this.LoginUser == undefined || this.LoginUser == null) {
            this.IsUserLogin = false;
            this.pages = [
                { title: 'Home', component: __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */], icon: 'home' },
                { title: 'Shopping Cart', component: __WEBPACK_IMPORTED_MODULE_6__pages_my_cart_my_cart__["a" /* MyCartPage */], icon: 'cart' },
                { title: 'Need help?', component: __WEBPACK_IMPORTED_MODULE_11__pages_faq_faq__["a" /* FaqPage */], icon: 'help-circle' },
                { title: 'Terms of services', component: __WEBPACK_IMPORTED_MODULE_13__pages_terms_terms__["a" /* TermsPage */], icon: 'warning' },
                { title: 'About', component: __WEBPACK_IMPORTED_MODULE_14__pages_about_us_about_us__["a" /* AboutUsPage */], icon: 'information-circle' }
            ];
        }
        else {
            this.IsUserLogin = true;
            this.LoginUser = JSON.parse(this.LoginUser);
            this.pages = [
                { title: 'Home', component: __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */], icon: 'home' },
                { title: 'Shopping Cart', component: __WEBPACK_IMPORTED_MODULE_6__pages_my_cart_my_cart__["a" /* MyCartPage */], icon: 'cart' },
                { title: 'My Profile', component: __WEBPACK_IMPORTED_MODULE_8__pages_my_profile_my_profile__["a" /* MyProfilePage */], icon: 'person' },
                { title: 'Orders History', component: __WEBPACK_IMPORTED_MODULE_7__pages_my_orders_my_orders__["a" /* MyOrdersPage */], icon: 'time' },
                { title: 'Earn money', component: __WEBPACK_IMPORTED_MODULE_12__pages_invite_friend_invite_friend__["a" /* InviteFriendPage */], icon: 'trophy' },
                { title: 'Need help?', component: __WEBPACK_IMPORTED_MODULE_11__pages_faq_faq__["a" /* FaqPage */], icon: 'help-circle' },
                { title: 'Terms of services', component: __WEBPACK_IMPORTED_MODULE_13__pages_terms_terms__["a" /* TermsPage */], icon: 'warning' },
                { title: 'About', component: __WEBPACK_IMPORTED_MODULE_14__pages_about_us_about_us__["a" /* AboutUsPage */], icon: 'information-circle' }
            ];
        }
    }
    FamiLov.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    FamiLov.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        if (page.component == __WEBPACK_IMPORTED_MODULE_8__pages_my_profile_my_profile__["a" /* MyProfilePage */]) {
            this.nav.push(page.component);
        }
        else {
            this.nav.setRoot(page.component);
        }
    };
    FamiLov.prototype.getToken = function () {
        this.restProvider.getToken().then(function (data) {
            // console.log(data);
            // alert();
            var token = data.key;
            // console.log(token);
            localStorage.setItem('apiToken', token);
        });
    };
    FamiLov.prototype.getCurrency = function () {
        var _this = this;
        this.restProvider.getCurrency().then(function (data) {
            console.log(data);
            if (data.status == true || data.status == 'true') {
                _this.currencyData = data.data.currency_data;
                localStorage.setItem('currencyData', JSON.stringify(_this.currencyData));
            }
        });
    };
    FamiLov.prototype.openLogin = function () {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_9__pages_login_login__["a" /* LoginPage */]);
    };
    FamiLov.prototype.openSignup = function () {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_10__pages_signup_signup__["a" /* SignupPage */]);
    };
    FamiLov.prototype.logoutUser = function () {
        localStorage.removeItem('LoginUser');
        // localStorage.removeItem('cartItems');
        location.reload();
    };
    FamiLov.prototype.showAlert = function () {
        var _this = this;
        this.alert = this.alertCtrl.create({
            title: 'Exit?',
            message: 'Do you want to exit the app?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                        _this.alert = null;
                    }
                },
                {
                    text: 'Exit',
                    handler: function () {
                        _this.platform.exitApp();
                    }
                }
            ]
        });
        alert.present();
    };
    FamiLov.prototype.showToast = function () {
        var toast = this.toastCtrl.create({
            message: 'Press Again to exit',
            duration: 2000,
            position: 'bottom'
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Nav */])
    ], FamiLov.prototype, "nav", void 0);
    FamiLov = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"E:\Projects\IonicBackendBrains\FamilovApp\src\app\app.html"*/'\n<ion-menu [content]="content">\n	<!-- <ion-header>\n		<ion-toolbar>\n			<ion-title>Menu</ion-title>\n		</ion-toolbar>\n	</ion-header> -->\n\n	<ion-content>\n\n		<div class="grayBackground" *ngIf="IsUserLogin">\n			<div class="spacer" style="width:268px;height:20px;" id="famiLov-spacer4"></div>\n			<div>\n				<img src="assets/imgs/avatar.ico" style="display:block;width:130px;height:130px;margin-left:auto;margin-right:auto;border-radius: 50%;border: 1px solid #FFF;" />\n			</div>\n			<h3 align="center">{{ LoginUser.username }} {{ LoginUser.last_name }}</h3>\n			<div class="spacer" style="width:268px;height:10px;" id="famiLov-spacer5"></div>\n		</div>\n\n		<div class="grayBackground" align="center" *ngIf="!IsUserLogin">\n			<div class="spacer" style="width:268px;height:20px;" id="famiLov-spacer4"></div>\n			<img src="assets/imgs/attention (1).png" style="display:block;width:55px;height:55px;margin-left:auto;margin-right:auto;" />\n			<h3 align="center">Please login or signup</h3>\n			<ion-grid>\n				<ion-row>\n					<ion-col col-6 style="padding: 3px;">\n						<button class="signupBtn" ion-button menuToggle (click)="openSignup()">\n						Sign up\n						</button>\n					</ion-col>\n					<ion-col col-6 style="padding: 3px;">\n						<button style="width: 100%;height: 2.3em;text-transform: none;font-size: 20px;font-weight: bold; " ion-button color="secondary" menuToggle (click)="openLogin()">\n						Login\n						</button>\n					</ion-col>\n				</ion-row>\n			</ion-grid>\n		</div>\n\n		<ion-list>\n			<ion-item color="none" menuClose *ngFor="let p of pages" (click)="openPage(p)" id="famiLov-menu-{{p.title}}">\n				{{p.title}}\n				<ion-icon name="{{p.icon}}" color="secondary" item-start></ion-icon>\n			</ion-item>\n		</ion-list>\n		<br>\n		<div class="fixed-outside" style="position: fixed;bottom: 0;left: 0;width: 100%;background: #FFF;" align="center">\n			<button *ngIf="IsUserLogin" class="button-md-height" color="secondary" ion-button block style="margin-bottom: 0px;" (click)="logoutUser()">Logout</button>\n			<ion-label *ngIf="!IsUserLogin" class="button-md-height" style="margin-bottom: 1vh;">2017@FamiLov</ion-label>\n		</div>\n\n	</ion-content>\n\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"E:\Projects\IonicBackendBrains\FamilovApp\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_4__providers_rest_rest__["a" /* RestProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], FamiLov);
    return FamiLov;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 300:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__my_profile_my_profile__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__my_cart_my_cart__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EditProfilePage = (function () {
    function EditProfilePage(navCtrl) {
        this.navCtrl = navCtrl;
        this.cartItemsLen = 0;
        this.cartItems = localStorage.getItem('cartItems');
        if (this.cartItems != undefined || this.cartItems != null) {
            this.cartItemsLen = JSON.parse(this.cartItems).length;
        }
    }
    EditProfilePage.prototype.goToProfile = function (params) {
        if (!params)
            params = {};
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__my_profile_my_profile__["a" /* MyProfilePage */]);
    };
    EditProfilePage.prototype.openCartPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__my_cart_my_cart__["a" /* MyCartPage */]);
    };
    EditProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-edit-profile',template:/*ion-inline-start:"E:\Projects\IonicBackendBrains\FamilovApp\src\pages\edit-profile\edit-profile.html"*/'<ion-header>\n	<ion-navbar>\n		<button ion-button menuToggle class="buttonWhiteFont">\n			<ion-icon name="menu"></ion-icon>\n		</button>\n		<ion-title>\n			<ion-grid>\n    		<ion-row>\n    			<ion-col col-10 class="headerTitle">Edit-Profile</ion-col>\n    			<ion-col col-2 class="badgeCol">\n					<!-- <ion-icon class="right" name="cart" item-end (click)="openCartPage()"></ion-icon> -->\n					<span class="badgeIcon"> {{ cartItemsLen }}</span>\n					<ion-icon name="cart" item-end (click)="openCartPage()"></ion-icon>\n    			</ion-col>\n    		</ion-row>\n    	</ion-grid>\n		</ion-title>\n	</ion-navbar>\n</ion-header>\n\n<ion-content padding id="EditProfile">\n	\n	<!-- <div class="spacer" style="height:15px;" id="login-spacer1"></div> -->\n	<!-- <h2 align="center">Create your account. <b>It\'s free.!</b></h2> -->\n	<div class="spacer" style="height:15px;" id="login-spacer2"></div>\n\n	<form id="editProfile-form">\n		<ion-list id="editProfile-list">\n			\n			<ion-item id="signup-input5">\n				<ion-label floating>\n					Full Name\n				</ion-label>\n				<ion-input type="text" placeholder=""></ion-input>\n			</ion-item>\n			\n			<ion-item id="signup-input6">\n				<ion-label floating>\n					E-mail\n				</ion-label>\n				<ion-input type="email" placeholder=""></ion-input>\n			</ion-item>\n			\n			<ion-item id="signup-input6">\n				<ion-label floating>\n					Your mobile number\n				</ion-label>\n				<ion-input type="number" placeholder=""></ion-input>\n			</ion-item>\n\n		</ion-list>\n		<button id="signup-button5" ion-button color="secondary" block (click)="goToProfile()">\n			Update\n		</button>\n\n	</form>\n</ion-content>'/*ion-inline-end:"E:\Projects\IonicBackendBrains\FamilovApp\src\pages\edit-profile\edit-profile.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]])
    ], EditProfilePage);
    return EditProfilePage;
}());

//# sourceMappingURL=edit-profile.js.map

/***/ }),

/***/ 301:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChangePasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__my_profile_my_profile__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__my_cart_my_cart__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ChangePasswordPage = (function () {
    function ChangePasswordPage(navCtrl, restProvider) {
        this.navCtrl = navCtrl;
        this.restProvider = restProvider;
        this.changePass = {};
        this.cartItemsLen = 0;
        this.LoginUser = JSON.parse(localStorage.getItem('LoginUser'));
        // console.log(this.LoginUser.customer_id);
        this.apiSuccessMsg = 'Success';
        this.apiErrorMsg = 'Error';
        this.cartItems = localStorage.getItem('cartItems');
        if (this.cartItems != undefined || this.cartItems != null) {
            this.cartItemsLen = JSON.parse(this.cartItems).length;
        }
    }
    ChangePasswordPage.prototype.goToProfile = function (params) {
        if (!params)
            params = {};
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__my_profile_my_profile__["a" /* MyProfilePage */]);
    };
    ChangePasswordPage.prototype.openCartPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__my_cart_my_cart__["a" /* MyCartPage */]);
    };
    ChangePasswordPage.prototype.submitRequest = function () {
        var _this = this;
        this.changePass.customer_id = this.LoginUser.customer_id;
        this.restProvider.changePassword(this.changePass)
            .then(function (result) {
            // console.log(result);
            if (result.status == true) {
                _this.restProvider.showSuccessMsg(_this.apiSuccessMsg, result.message);
                _this.changePass.current_password = '';
                _this.changePass.new_password = '';
                _this.changePass.repeat_new_password = '';
            }
            else {
                _this.restProvider.showErrorMsg(_this.apiErrorMsg, 'Something went wrong, Please try again.');
            }
        }, function (err) {
            // console.log(err);
            _this.restProvider.showErrorMsg(_this.statusError, 'Opps..!! <br> Something went wrong.');
        });
    };
    ChangePasswordPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-change-password',template:/*ion-inline-start:"E:\Projects\IonicBackendBrains\FamilovApp\src\pages\change-password\change-password.html"*/'<ion-header>\n	<ion-navbar>\n		<button ion-button menuToggle class="buttonWhiteFont">\n			<ion-icon name="menu"></ion-icon>\n		</button>\n		<ion-title>\n			<!-- Change Password -->\n			<ion-grid>\n	    		<ion-row>\n	    			<ion-col col-10 class="headerTitle">Change Password</ion-col>\n	    			<ion-col col-2 class="badgeCol">\n						<!-- <ion-icon class="right" name="cart" item-end (click)="openCartPage()"></ion-icon> -->\n						<span class="badgeIcon"> {{ cartItemsLen }}</span>\n						<ion-icon name="cart" item-end (click)="openCartPage()"></ion-icon>\n	    			</ion-col>\n	    		</ion-row>\n	    	</ion-grid>\n		</ion-title>\n	</ion-navbar>\n</ion-header>\n<ion-content padding id="page5">\n	\n	<div class="spacer" style="height:20px;" id="login-spacer1"></div>\n	<!-- <h2 align="center">Enter password and confirm password to change your account password.</h2> -->\n	<h2 align="center">Change my password.</h2>\n	<br>\n	<div align="center"><ion-icon name="lock" color="secondary" style="font-size: 4em"></ion-icon></div>\n	<div class="spacer" style="height:20px;" id="login-spacer2"></div>\n\n	<form id="login-form1" (ngSubmit)="submitRequest()" style="margin: 2vh 5vh;">\n		<ion-list id="login-list1">\n			<ion-item id="login-input1" style="border: 1px solid #EEE;margin: 1vh auto;">\n				<ion-label><!-- floating -->\n					<ion-icon name="lock"></ion-icon>\n				</ion-label>\n				<ion-input type="password" placeholder="Old Password" [(ngModel)]="changePass.current_password" name="current_password"></ion-input>\n			</ion-item>\n			<ion-item id="login-input1" style="border: 1px solid #EEE;margin: 1vh auto;">\n				<ion-label><!-- floating -->\n					<ion-icon name="lock"></ion-icon>\n				</ion-label>\n				<ion-input type="password" placeholder="New Password" [(ngModel)]="changePass.new_password" name="new_password"></ion-input>\n			</ion-item>\n			<ion-item id="login-input1" style="border: 1px solid #EEE;margin: 1vh auto;">\n				<ion-label><!-- floating -->\n					<ion-icon name="lock"></ion-icon>\n				</ion-label>\n				<ion-input type="password" placeholder="Confirm Password" [(ngModel)]="changePass.repeat_new_password" name="repeat_new_password"></ion-input>\n			</ion-item>\n		</ion-list>\n\n		<div class="fixed-outside" style="position: fixed;bottom: 0;left: 0;width: 100%;" align="center">\n			<button type="submit" color="secondary" ion-button block style="text-transform: none;margin-bottom: 0;padding: 1em;font-size: 20px;" icon-right>Change <ion-icon name="checkmark"></ion-icon></button>\n		</div>\n\n	</form>\n\n\n</ion-content>'/*ion-inline-end:"E:\Projects\IonicBackendBrains\FamilovApp\src\pages\change-password\change-password.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */]])
    ], ChangePasswordPage);
    return ChangePasswordPage;
}());

//# sourceMappingURL=change-password.js.map

/***/ }),

/***/ 34:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__products_products__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__my_cart_my_cart__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__faq_faq__ = __webpack_require__(111);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import { ProductDetailsPage } from '../product-details/product-details';



var HomePage = (function () {
    function HomePage(navCtrl, restProvider, alertCtrl) {
        this.navCtrl = navCtrl;
        this.restProvider = restProvider;
        this.alertCtrl = alertCtrl;
        this.CountryList = {};
        this.cartItemsLen = 0;
        /*this.restProvider.LoadingSpinner();
        
        this.CountryList = localStorage.getItem('CountryList');
        if (this.CountryList == null || this.CountryList == undefined) {
            this.getHomeData();
        }

        if (this.CountryList == null || this.CountryList == undefined) {
            this.CountryList = [{'country_id':'-1','country_name':'Select country'}];
        }
        if (this.CityList == null || this.CityList == undefined) {
            this.CityList = [{'city_id':'-1','city_name':'Select city'}];
        }
        if (this.ShopList == null || this.ShopList == undefined) {
            this.ShopList = [{'shop_id':'-1','shop_name':'Select shop'}];
        }
        // console.log(this.CityList);

        if(typeof this.CountryList == 'string') {
            this.CountryList = JSON.parse(this.CountryList);
        }

        this.cartItems = localStorage.getItem('cartItems');
        if (this.cartItems != undefined || this.cartItems != null) {
          this.cartItemsLen = JSON.parse(this.cartItems).length;
        }

        this.restProvider.hideLoadingSpinner();

        // console.log(this.CountryList);
        // setTimeout( () => {
        // 	this.restProvider.LoadingSpinner();
        // },500);*/
    }
    HomePage.prototype.ionViewCanEnter = function () {
        this.restProvider.LoadingSpinner();
        this.CountryList = localStorage.getItem('CountryList');
        if (this.CountryList == null || this.CountryList == undefined) {
            this.getHomeData();
        }
        if (this.CountryList == null || this.CountryList == undefined) {
            this.CountryList = [{ 'country_id': '-1', 'country_name': 'Select country' }];
        }
        if (this.CityList == null || this.CityList == undefined) {
            this.CityList = [{ 'city_id': '-1', 'city_name': 'Select city' }];
        }
        if (this.ShopList == null || this.ShopList == undefined) {
            this.ShopList = [{ 'shop_id': '-1', 'shop_name': 'Select shop' }];
        }
        // console.log(this.CityList);
        if (typeof this.CountryList == 'string') {
            this.CountryList = JSON.parse(this.CountryList);
        }
        this.cartItems = localStorage.getItem('cartItems');
        if (this.cartItems != undefined || this.cartItems != null) {
            this.cartItemsLen = JSON.parse(this.cartItems).length;
        }
        this.restProvider.hideLoadingSpinner();
        console.log('enter');
    };
    HomePage.prototype.goToHelp = function (params) {
        if (!params)
            params = {};
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__faq_faq__["a" /* FaqPage */]);
    };
    HomePage.prototype.searchProducts = function (params) {
        var _this = this;
        var searchData = {};
        searchData.country_id = this.receiver_country;
        searchData.city_id = this.receiver_city;
        searchData.shop_id = this.receiver_shop;
        var msg = '';
        if (searchData.country_id == null || searchData.country_id == undefined) {
            msg += 'Select country <br>';
        }
        if (searchData.city_id == null || searchData.city_id == undefined) {
            msg += 'Select city <br>';
        }
        if (searchData.shop_id == null || searchData.shop_id == undefined) {
            msg += 'Select shop <br>';
        }
        if (msg != '') {
            this.restProvider.showWarningMsg('Warning', msg);
            return false;
        }
        var isConfirm = false;
        var selectedShop = localStorage.getItem('selectedShopLoc');
        var cartItems = localStorage.getItem('cartItems');
        if (cartItems != undefined && cartItems != null) {
            cartItems = JSON.parse(cartItems);
        }
        else {
            cartItems = [];
        }
        if (cartItems != null && cartItems != undefined && cartItems.length > 0) {
            if (selectedShop != null && selectedShop != undefined && selectedShop != '') {
                selectedShop = JSON.parse(selectedShop);
                console.log(selectedShop);
                console.log(searchData);
                if (searchData.country_id == selectedShop.country_id && searchData.city_id == selectedShop.city_id && searchData.shop_id == selectedShop.shop_id) {
                    isConfirm = true;
                    localStorage.setItem('selectedShopLoc', JSON.stringify(searchData));
                    var params = {};
                    params.searchData = searchData;
                    this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__products_products__["a" /* ProductsPage */], params);
                }
                else {
                    var title = 'Are You Sure Want To Change Shop?';
                    var message = 'If you change stores, you will lose your cart.';
                    var alert_1 = this.alertCtrl.create({
                        title: title,
                        message: message,
                        buttons: [
                            {
                                text: 'No',
                                role: 'cancel',
                                handler: function () {
                                    console.log('Cancel clicked');
                                    _this.openCartPage();
                                }
                            },
                            {
                                text: 'Yes',
                                handler: function () {
                                    localStorage.removeItem('cartItems');
                                    isConfirm = true;
                                    localStorage.setItem('selectedShopLoc', JSON.stringify(searchData));
                                    var params = {};
                                    params.searchData = searchData;
                                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__products_products__["a" /* ProductsPage */], params);
                                }
                            }
                        ]
                    });
                    alert_1.present();
                }
            }
            else {
                isConfirm = true;
                localStorage.removeItem('cartItems');
                localStorage.setItem('selectedShopLoc', JSON.stringify(searchData));
                var params = {};
                params.searchData = searchData;
                this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__products_products__["a" /* ProductsPage */], params);
            }
        }
        else {
            isConfirm = true;
            localStorage.setItem('selectedShopLoc', JSON.stringify(searchData));
            var params = {};
            params.searchData = searchData;
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__products_products__["a" /* ProductsPage */], params);
        }
        /*if (isConfirm == true) {
            var params = {};
            params.searchData = searchData;

            this.navCtrl.setRoot(ProductsPage, params);
        }*/
        /*this.restProvider.getProducts(searchData)
        .then(
            (result) => {
                // console.log(result);
                if (result.status == true) {
                    if(result.data.total_products > 0) {
                        var params = {};
                        params.category_list = result.data.category_list;
                        params.product_list = result.data.product_list;
                        params.shop_info = result.data.shop_info;
                        params.searchData = searchData;
                        this.navCtrl.setRoot(ProductsPage, params);
                    }
                    else {
                        // this.restProvider.showErrorMsg(this.statusError,'Opps..!! <br> No products found in selected area.!!!');
                    }
                }
                else {
                    if (typeof result.message == "string") {
                        this.restProvider.showErrorMsg(this.statusError,result.message);
                    }
                    else {
                        this.restProvider.showErrorMsg(this.statusError,'Opps..!! <br> Something went wrong.');
                    }
                }
            },
            (err) => {
                // console.log(err);
                this.restProvider.showErrorMsg(this.statusError,'Opps..!! <br> Something went wrong.');
            }
        );*/
        return false;
    };
    HomePage.prototype.openCartPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__my_cart_my_cart__["a" /* MyCartPage */]);
    };
    HomePage.prototype.getHomeData = function () {
        var _this = this;
        this.restProvider.getHomeData()
            .then(function (result) {
            if (result.status == true) {
                _this.CountryList = result.data.country_list;
                localStorage.setItem('CountryList', JSON.stringify(result.data.country_list));
            }
            else {
                if (typeof result.message == "string") {
                    _this.restProvider.showErrorMsg(_this.statusError, result.message);
                }
                else {
                    _this.restProvider.showErrorMsg(_this.statusError, 'Opps..!! <br> No internet connection, <br> Please check you internet connection.');
                }
            }
        }, function (err) {
            console.log(err);
        });
    };
    HomePage.prototype.getCityList = function ($event) {
        var _this = this;
        // alert($event);
        // console.log($event);
        this.restProvider.getCities($event)
            .then(function (result) {
            if (result.status == true) {
                _this.CityList = result.data.city_list;
            }
            else {
                if (typeof result.message == "string") {
                    _this.restProvider.showErrorMsg(_this.statusError, result.message);
                }
                else {
                    // this.restProvider.showErrorMsg(this.statusError,'Opps..!! <br> Something went wrong.');
                    _this.restProvider.showErrorMsg(_this.statusError, 'Opps..!! <br> No internet connection, <br> Please check you internet connection.');
                }
            }
        }, function (err) {
            // console.log(err);
            // this.restProvider.showErrorMsg(this.statusError,'Opps..!! <br> Something went wrong.');
            _this.restProvider.showErrorMsg(_this.statusError, 'Opps..!! <br> No internet connection, <br> Please check you internet connection.');
        });
    };
    HomePage.prototype.getShopList = function ($event) {
        var _this = this;
        // alert($event);
        // console.log($event);
        this.restProvider.getShops($event)
            .then(function (result) {
            if (result.status == true) {
                if (result.data.shop_list.length > 0) {
                    _this.ShopList = result.data.shop_list;
                }
                else {
                    // this.restProvider.showWarningMsg('Warning','No shop fount for selected city');
                }
            }
            else {
                if (typeof result.message == "string") {
                    _this.restProvider.showErrorMsg(_this.statusError, result.message);
                }
                else {
                    // this.restProvider.showErrorMsg(this.statusError,'Opps..!! <br> Something went wrong.');
                    _this.restProvider.showErrorMsg(_this.statusError, 'Opps..!! <br> No internet connection, <br> Please check you internet connection.');
                }
            }
        }, function (err) {
            // console.log(err);
            // this.restProvider.showErrorMsg(this.statusError,'Opps..!! <br> Something went wrong.');
            _this.restProvider.showErrorMsg(_this.statusError, 'Opps..!! <br> No internet connection, <br> Please check you internet connection.');
        });
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"E:\Projects\IonicBackendBrains\FamilovApp\src\pages\home\home.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle class="buttonWhiteFont">\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>\n    	<ion-grid>\n    		<ion-row>\n    			<ion-col col-10 class="headerTitle">\n    				<div align="center">FamiLov</div>\n    			</ion-col>\n    			<ion-col col-2 class="badgeCol">\n					<!-- <ion-icon class="right" name="cart" item-end (click)="openCartPage()"></ion-icon> -->\n					<span class="badgeIcon"> {{ cartItemsLen }}</span>\n					<ion-icon name="cart" item-end (click)="openCartPage()"></ion-icon>\n    			</ion-col>\n    		</ion-row>\n    	</ion-grid>\n	</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n  <!-- <h3 align="center">Welcome</h3> -->\n\n  <div style="min-height: 10vh;padding: 3vh;">\n    <!-- <ion-slides autoplay=\'2000\' loop=\'true\'>\n    <ion-slide>\n      <img src="assets/imgs/slide3.png">\n    </ion-slide>\n    <ion-slide>\n      <img src="assets/imgs/slide2.png">\n    </ion-slide>\n    <ion-slide>\n      <img src="assets/imgs/slide1.png">\n    </ion-slide>\n  </ion-slides> -->\n  <div align="center">\n	  <img src="assets/imgs/angel.png" style="height: 10vh; width: auto;">\n	  <h1 style="font-weight: bold;" align="center">How do you want to support your family today?</h1>\n	</div>\n</div>\n\n<!-- <div class="spacer" style="height:20px;" id="login-spacer2"></div> -->\n<!-- <hr> -->\n  <!-- <h3 align="center">Our Products</h3> -->\n  <!-- <h3 align="center">Search Products</h3> -->\n  <h4 align="center" style="color: lightgray;">\n  	Buy from abroad foods and basics needs <br>\n  	we deliver your loved ones within 36 h.\n  </h4>\n<!-- <hr> -->\n\n<!-- <ion-grid>\n  <ion-row>\n    <ion-col col-6 align="center" class="products">\n        <img src="assets/imgs/slide2.png">\n        <br>\n        <span class="productTitle">Product 1</span>\n        <button color="danger" class="moreDetails" ion-button (click)="viewProductDetails()">More Info</button>\n    </ion-col>\n    <ion-col col-6 align="center" class="products">\n      <img src="assets/imgs/slide2.png">\n      <br>\n      <span class="productTitle">Product 2</span>\n      <button color="danger" class="moreDetails" ion-button (click)="viewProductDetails()">More Info</button>\n    </ion-col>\n    <ion-col col-6 align="center" class="products">\n      <img src="assets/imgs/slide3.png">\n      <br>\n      <span class="productTitle">Product 3</span>\n      <button color="danger" class="moreDetails" ion-button (click)="viewProductDetails()">More Info</button>\n    </ion-col>\n    <ion-col col-6 align="center" class="products">\n      <img src="assets/imgs/slide3.png">\n      <br>\n      <span class="productTitle">Product 3</span>\n      <button color="danger" class="moreDetails" ion-button (click)="viewProductDetails()">More Info</button>\n    </ion-col>\n  </ion-row>\n  </ion-grid> -->\n\n<!-- <ion-card> -->\n<div align="center">\n<ion-item style="border: 1px solid #eee; margin: 1vh 0vh;max-width: 75%;">\n  <ion-label> <ion-icon color="light" name="pin"></ion-icon> &nbsp;Receiver Country</ion-label><!-- planet -->\n  <ion-select [(ngModel)]="receiver_country" (ionChange)="getCityList($event)"><!-- placeholder="Receiver Country" -->\n  		<ion-option *ngFor="let country of CountryList" value="{{ country.country_id }}"> {{ country.country_name }} </ion-option>\n  </ion-select>\n</ion-item>\n\n<ion-item style="border: 1px solid #eee; margin: 1vh 0vh;max-width: 75%;">\n  <ion-label> <ion-icon color="light" name="pin"></ion-icon> &nbsp;Receiver City</ion-label><!-- navigate -->\n  <ion-select [(ngModel)]="receiver_city" (ionChange)="getShopList($event)"><!-- placeholder="Receiver City" -->\n    <ion-option *ngFor="let city of CityList" value="{{ city.city_id }}"> {{ city.city_name }} </ion-option>\n  </ion-select>\n</ion-item>\n\n\n<ion-item style="border: 1px solid #eee; margin: 1vh 0vh;max-width: 75%;">\n  <ion-label> <ion-icon color="light" name="briefcase"></ion-icon> &nbsp;Receiver Shop</ion-label><!-- locate -->\n  <ion-select [(ngModel)]="receiver_shop"><!-- placeholder="Receiver Shop" -->\n    <ion-option *ngFor="let shop of ShopList" value="{{ shop.shop_id }}"> {{ shop.shop_name }} </ion-option>\n  </ion-select>\n</ion-item>\n</div>\n<div align="center">\n	<br>\n	<button color="secondary" ion-button (click)="searchProducts()" style="width: 47vh;">Enter</button>\n	<br>\n	<button color="secondary" ion-button (click)="goToHelp()" style="width: 20vh;text-transform: none;" clear>Need Help?</button>\n</div>\n\n<!-- </ion-card> -->\n\n<!-- <hr>\n	<h3 align="center" style="color: #302E64;font-weight: bold;text-transform: uppercase;">How it works</h3>\n	\n	<hr>\n	\n	<ion-card class="grayBackground">\n		<div align="center" style="padding: 10px;">\n			<h2 class="howWorksNumber" color="secondary" ion-button>1</h2>\n			<br>\n			<ion-icon name=\'cart\' class="bigIcon" color="secondary"></ion-icon>\n			<br><br>\n			<h2><b>Make the choice online</b></h2>\n			<h3 style="margin: 2vh;padding: 3vh;">Choose the products you want to offer and tell us the name and phone number of the recipient.</h3>\n		</div>		\n	</ion-card>\n\n	<ion-card class="grayBackground">\n		<div align="center" style="padding: 10px;">\n			<h2 class="howWorksNumber" color="secondary" ion-button>2</h2>\n			<br>\n			<ion-icon name=\'cloud-done\' class="bigIcon" color="secondary"></ion-icon>\n			<br><br>\n			<h2><b>Pay securely</b></h2>\n			<h3 style="margin: 2vh;padding: 3vh;">Use the secure method of payment that suits you - credit card (VISA, MASTER) or your PayPal account.</h3>\n		</div>		\n	</ion-card>\n\n	<ion-card class="grayBackground">\n		<div align="center" style="padding: 10px;">\n			<h2 class="howWorksNumber" color="secondary" ion-button>3</h2>\n			<br>\n			<ion-icon name=\'chatbubbles\' class="bigIcon" color="secondary"></ion-icon>\n			<br><br>\n			<h2><b>SMS withdrawal code</b></h2>\n			<h3 style="margin: 2vh;padding: 3vh;">We send the details of your package to the store and the unique SMS withdrawal code to the recipient.</h3>\n		</div>		\n	</ion-card>\n\n	<ion-card class="grayBackground">\n		<div align="center" style="padding: 10px;">\n			<ion-icon name="checkmark-circle-outline" class="bigIcon" color="secondary"></ion-icon>\n			<br>\n			<ion-icon name=\'cube\' class="bigIcon" color="secondary"></ion-icon>\n			<br><br>\n			<h2><b>Delivery and confirmation</b></h2>\n			<h3 style="margin: 2vh;padding: 3vh;">The package is prepared and the beneficiary is contacted within 36 H * for delivery and you receive confirmation that everything went well.</h3>\n		</div>		\n	</ion-card>\n<hr>\n\n<div class="grayBackground" align="center" style="padding: 1vh;">\n	<h3 align="center" style="margin-top: 1vh;">Unbeatable hardships</h3>\n	<ion-grid>\n		<ion-row class="rowPadding">\n			<ion-col col-6>\n				<ion-icon name="card" class="bigIcon" color="primary"></ion-icon><br><br>\n				<b>Send more, buy less</b><br>\n				Save up to 95% vs. traditional money transfer provider and local price Guarantee!\n			</ion-col>\n			<ion-col col-6>\n				<ion-icon name="lock" class="bigIcon" color="primary"></ion-icon><br><br>\n				<b>Send more, buy less</b><br>\n				Save up to 95% vs. traditional money transfer provider and local price Guarantee!\n			</ion-col>\n		</ion-row>\n\n		<ion-row class="rowPadding">\n			<ion-col col-6>\n				<ion-icon name="lock" class="bigIcon" color="primary"></ion-icon><br><br>\n				<b>Send more, buy less</b><br>\n				Save up to 95% vs. traditional money transfer provider and local price Guarantee!\n			</ion-col>\n			<ion-col col-6>\n				<ion-icon name="laptop" class="bigIcon" color="primary"></ion-icon><br><br>\n				<b>Send more, buy less</b><br>\n				Save up to 95% vs. traditional money transfer provider and local price Guarantee!\n			</ion-col>\n		</ion-row>\n\n		<ion-row class="rowPadding">\n			<ion-col col-6>\n				<ion-icon name="people" class="bigIcon" color="primary"></ion-icon><br><br>\n				<b>Send more, buy less</b><br>\n				Save up to 95% vs. traditional money transfer provider and local price Guarantee!\n			</ion-col>\n			<ion-col col-6>\n				<ion-icon name="help-circle" class="bigIcon" color="primary"></ion-icon><br><br>\n				<b>Send more, buy less</b><br>\n				Save up to 95% vs. traditional money transfer provider and local price Guarantee!\n			</ion-col>\n		</ion-row>\n\n	</ion-grid>\n</div> -->\n<br>\n<br>\n<br>\n  <!-- <p>\n    If you get lost, the <a href="http://ionicframework.com/docs/v2">docs</a> will show you the way.\n  </p> -->\n\n  <!-- <button ion-button secondary menuToggle>Toggle Menu</button> -->\n</ion-content>\n'/*ion-inline-end:"E:\Projects\IonicBackendBrains\FamilovApp\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 44:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__signup_signup__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__forgot_password_forgot_password__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__my_cart_my_cart__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var LoginPage = (function () {
    function LoginPage(navCtrl, restProvider) {
        this.navCtrl = navCtrl;
        this.restProvider = restProvider;
        this.loginData = {};
        this.statusSuccess = 'Success';
        this.statusError = 'Error';
    }
    LoginPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    LoginPage.prototype.goToHome = function (params) {
        if (!params)
            params = {};
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */]);
    };
    LoginPage.prototype.goToSignup = function (params) {
        if (!params)
            params = {};
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__signup_signup__["a" /* SignupPage */]);
    };
    LoginPage.prototype.goToForgotPass = function (params) {
        if (!params)
            params = {};
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__forgot_password_forgot_password__["a" /* ForgotPasswordPage */]);
    };
    LoginPage.prototype.openCartPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__my_cart_my_cart__["a" /* MyCartPage */]);
    };
    LoginPage.prototype.doLogin = function () {
        var _this = this;
        if (this.loginData.email == "" || this.loginData.email == undefined) {
            alert("Email is required");
            return false;
        }
        if (this.loginData.password == "" || this.loginData.password == undefined) {
            alert("Password is required");
            return false;
        }
        this.restProvider.LoadingSpinner();
        this.restProvider.loginUser(this.loginData)
            .then(function (result) {
            _this.restProvider.hideLoadingSpinner();
            // alert('result');
            if (result.status == true) {
                _this.restProvider.showSuccessMsg(_this.statusSuccess, result.message);
                localStorage.setItem('LoginUser', JSON.stringify(result.data));
                setTimeout(function () {
                    // this.goToHome();
                    location.reload();
                }, 500);
            }
            else {
                // this.restProvider.showErrorMsg(this.statusError,result.message);
                // this.restProvider.showErrorMsg(this.statusError,'Something went wrong, Please try again.');
                if (typeof result.message == "object") {
                    var message = '';
                    for (var i in result.message) {
                        message += result.message[i] + '<br>';
                    }
                    _this.restProvider.showErrorMsg(_this.statusError, message);
                }
                else if (typeof result.message == "string") {
                    _this.restProvider.showErrorMsg(_this.statusError, result.message);
                }
                else {
                    // this.restProvider.showErrorMsg(this.statusError,result.message);
                    _this.restProvider.showErrorMsg(_this.statusError, 'Invalid username or password..!!');
                }
            }
        }, function (err) {
            _this.restProvider.hideLoadingSpinner();
            console.log(err);
        });
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"E:\Projects\IonicBackendBrains\FamilovApp\src\pages\login\login.html"*/'<!-- <ion-header style="background: #FFF;">\n	<ion-navbar>\n		<button ion-button menuToggle class="buttonWhiteFont">\n			<ion-icon name="menu"></ion-icon>\n		</button>\n		<ion-title>\n			<ion-grid>\n	    		<ion-row>\n	    			<ion-col col-8 class="headerTitle">Login</ion-col>\n	    			<ion-col col-4><ion-icon class="right" name="cart" item-end (click)="openCartPage()"></ion-icon></ion-col>\n	    		</ion-row>\n	    	</ion-grid>\n		</ion-title>\n	</ion-navbar>\n</ion-header> -->\n\n<ion-content padding id="page5">\n\n	<ion-grid>\n		<ion-row>\n			<ion-col col-2>\n				<ion-icon color="secondary" name="arrow-back" (click)="goBack()" style="margin-top: 2vh;"></ion-icon>\n			</ion-col>\n			<ion-col col-8>\n				<img src="assets/imgs/familov-logo.png" style="max-height: 10vh;">\n			</ion-col>\n		</ion-row>\n	</ion-grid>\n\n	<div class="spacer" style="height:20px;" id="login-spacer1"></div>\n	<!-- <h2 align="center">Hi. Great to meet you.!</h2> -->\n	<div align="center"><img src="assets/imgs/laugh.png" style="max-height: 10vh;" /></div>\n	<!-- <br> -->\n	<h2 align="center" class="textBlue">Good to see you again!</h2>\n	<br>\n	<!-- <div class="spacer" style="height:20px;" id="login-spacer2"></div> -->\n\n	<form id="login-form1" (ngSubmit)="doLogin()">\n		<ion-list id="login-list1">\n			<ion-item class="item80" id="login-input1" style="border: 1px solid #eee;margin: 1vh auto;">\n				<ion-label>\n					<ion-icon name="person" color="light"></ion-icon>\n				</ion-label>\n				<ion-input type="email" placeholder="Email" [(ngModel)]="loginData.email" name="email"></ion-input>\n			</ion-item>\n			<ion-item class="item80" id="login-input2" style="border: 1px solid #eee;margin: 1vh auto;">\n				<ion-label>\n					<ion-icon name="lock" color="light"></ion-icon>\n				</ion-label>\n				<ion-input type="password" placeholder="Password" [(ngModel)]="loginData.password" name="password"></ion-input>\n			</ion-item>\n		</ion-list>\n\n		<!-- <div class="spacer" style="height:50px;" id="login-spacer3"></div> -->\n		<div align="center" style="margin-top: 1vh;height: 5vh;">\n			<div class="item80">\n				<div style="float: left;">\n					<ion-checkbox color="secondary" checked="false" style="position: absolute;"></ion-checkbox>\n					<span style="margin-left: 4vh;font-size: 15px;">Remember Me</span>\n				</div>\n				<div style="float: right;color:#32db64;font-size: 15px;" id="forgotPassword" (click)="goToForgotPass()"><span>Forgot your password ?</span></div>\n			</div>\n		</div>\n		<!-- <ion-grid>\n			<ion-row>\n				<ion-col col-6>\n					<div>\n						<ion-label>Remember Me</ion-label>\n						<ion-checkbox color="secondary" checked="false" style="margin-right: 1vh;"></ion-checkbox>\n					</div>\n					<!- - <ion-label>\n						Remember Me\n					</ion-label> - - >\n				</ion-col>\n				<ion-col col-6>\n					<button id="forgotPassword" class="right" ion-button clear color="secondary" (click)="goToForgotPass()" type="button">\n						<span style="border-bottom: 1px solid;">Forgot your password ?</span>\n					</button>\n				</ion-col>\n			</ion-row>\n		</ion-grid> -->\n		\n		<!-- <div class="spacer" style="height:15px;" id="login-spacer4"></div> -->\n\n		<!-- <button id="login-button1" ion-button color="secondary" block type="submit">\n			Log in\n		</button> -->\n\n		<!-- <button ion-button block color="secondary" type="submit" icon-right>\n			Log in\n			<ion-icon name="arrow-rounf-forward"></ion-icon>\n		</button> -->\n		<!-- <button ion-button icon-right color="secondary" type="submit">\n			Log In\n			<ion-icon name="facebook"></ion-icon>\n		</button> -->\n		<div align="center">\n			<button class="item70" ion-button icon-right color="secondary" style="height: 4rem;" type="submit">\n				Log In\n				<ion-icon name="arrow-round-forward"></ion-icon>\n			</button>\n\n			<h4 align="center" style="margin: 1vh;font-size: 1.2em;">OR</h4>\n\n			<button class="item70" ion-button icon-right color="secondary" style="background: #3b5998 !important;height: 4rem;">\n				With Facebook\n				<ion-icon name="logo-facebook"></ion-icon>\n			</button>\n		</div>\n\n		<!-- <button id="login-button1" ion-button color="default" icon-right block style="background: #3b5998 !important">\n			With Facebook\n			<ion-icon name="facebook"></ion-icon>\n		</button> -->\n		<br>\n\n		<p align="center">\n			<b>Not Registered?</b>\n			<a color="secondary" ion-button clear (click)="goToSignup()" style="margin-top: -3px;text-transform: none;">\n				Sign up Here\n			</a>\n		</p>		\n\n	</form>\n</ion-content>'/*ion-inline-end:"E:\Projects\IonicBackendBrains\FamilovApp\src\pages\login\login.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 55:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login_login__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__my_cart_my_cart__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__terms_terms__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var SignupPage = (function () {
    function SignupPage(navCtrl, restProvider) {
        this.navCtrl = navCtrl;
        this.restProvider = restProvider;
        this.signupData = {};
        this.cartItemsLen = 0;
        this.restProvider.LoadingSpinner();
        this.statusSuccess = 'Success';
        this.statusError = 'Error';
        this.cartItems = localStorage.getItem('cartItems');
        if (this.cartItems != undefined || this.cartItems != null) {
            this.cartItemsLen = JSON.parse(this.cartItems).length;
        }
        this.restProvider.hideLoadingSpinner();
    }
    SignupPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    SignupPage.prototype.goToLogin = function (params) {
        if (!params)
            params = {};
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__login_login__["a" /* LoginPage */]);
    };
    SignupPage.prototype.openCartPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__my_cart_my_cart__["a" /* MyCartPage */]);
    };
    SignupPage.prototype.doSignup = function () {
        var _this = this;
        // Post api call
        if (this.signupData.first_name == "" || this.signupData.first_name == undefined) {
            alert("Name is required");
            return false;
        }
        if (this.signupData.last_name == "" || this.signupData.last_name == undefined) {
            alert("Full name is required");
            return false;
        }
        if (this.signupData.email == "" || this.signupData.email == undefined) {
            alert("Email is required");
            return false;
        }
        if (this.signupData.phone_number == "" || this.signupData.phone_number == undefined) {
            alert("Phone number is required");
            return false;
        }
        if (this.signupData.password == "" || this.signupData.password == undefined) {
            alert("Password is required");
            return false;
        }
        if (this.signupData.repeat_password == "" || this.signupData.repeat_password == undefined) {
            alert("Confirm password is required");
            return false;
        }
        this.restProvider.LoadingSpinner();
        this.signupData.phone_code = '+91';
        this.restProvider.signupUser(this.signupData)
            .then(function (result) {
            _this.restProvider.hideLoadingSpinner();
            // result = JSON.parse(result);
            if (result.status == true) {
                _this.restProvider.showSuccessMsg(_this.statusSuccess, result.message);
                setTimeout(function () {
                    _this.goToLogin(null);
                }, 200);
            }
            else {
                if (typeof result.message == "object") {
                    var message = '';
                    for (var i in result.message) {
                        message += result.message[i] + '<br>';
                    }
                    _this.restProvider.showErrorMsg(_this.statusError, message);
                }
                else if (typeof result.message == "string") {
                    _this.restProvider.showErrorMsg(_this.statusError, result.message);
                }
                else {
                    // this.restProvider.showErrorMsg(this.statusError,result.message);
                    _this.restProvider.showErrorMsg(_this.statusError, 'Something went wrong, Please try again.');
                }
            }
        }, function (err) {
            _this.restProvider.hideLoadingSpinner();
            console.log(err);
        });
    };
    SignupPage.prototype.goToTerms = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__terms_terms__["a" /* TermsPage */]);
    };
    SignupPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-signup',template:/*ion-inline-start:"E:\Projects\IonicBackendBrains\FamilovApp\src\pages\signup\signup.html"*/'<!-- <ion-header>\n	<ion-navbar>\n		<button ion-button menuToggle class="buttonWhiteFont">\n			<ion-icon name="menu"></ion-icon>\n		</button>\n		<ion-title>\n			<ion-grid>\n    		<ion-row>\n    			<ion-col col-8 class="headerTitle">Signup</ion-col>\n    			<ion-col col-4><ion-icon class="right" name="cart" item-end (click)="openCartPage()"></ion-icon></ion-col>\n    		</ion-row>\n    	</ion-grid>\n		</ion-title>\n	</ion-navbar>\n</ion-header> -->\n\n<ion-content padding id="page7">\n	\n	<ion-grid>\n		<ion-row>\n			<ion-col col-2>\n				<ion-icon color="secondary" name="arrow-back" (click)="goBack()" style="margin-top: 2vh;"></ion-icon>\n			</ion-col>\n			<ion-col col-8>\n				<img src="assets/imgs/familov-logo.png" style="max-height: 10vh;">\n			</ion-col>\n		</ion-row>\n	</ion-grid>\n\n	<div class="spacer" style="height:15px;" id="login-spacer1"></div>\n	<div align="center"><img src="assets/imgs/laugh.png" style="max-height: 15vh;" /></div>\n	<h2 align="center" style="color: lightgray;">Register  <b style="color: #000">It\'s free !</b></h2>\n	<div align="center">\n		<button id="login-button1" icon-right ion-button color="default" style="background: #3b5998 !important;width: 40vh;">\n			With Facebook\n			<ion-icon name="logo-facebook"></ion-icon>\n		</button>\n	</div>\n	<h4 align="center" style="color: lightgray;">Or create your account.</h4>\n	<div class="spacer" style="height:15px;" id="login-spacer2"></div>\n\n	<form id="signup-form3" (ngSubmit)="doSignup()" method="post">\n		<ion-list id="signup-list3">\n			\n			<ion-item id="signup-input5" class="item80" style="border: 1px solid #eee;margin: 1vh auto;">\n				<ion-label>\n					<!-- Nick Name -->\n					<ion-icon name="person" color="light"></ion-icon>\n				</ion-label>\n				<ion-input type="text" placeholder="Nick Name" [(ngModel)]="signupData.first_name" name="first_name"></ion-input>\n			</ion-item>\n\n			<ion-item id="signup-input5" class="item80" style="border: 1px solid #eee;margin: 1vh auto;">\n				<ion-label>\n					<!-- Name -->\n					<ion-icon name="contact" color="light"></ion-icon>\n				</ion-label>\n				<ion-input type="text" placeholder="Name" [(ngModel)]="signupData.last_name" name="last_name"></ion-input>\n			</ion-item>\n			\n			<ion-item id="signup-input6" class="item80" style="border: 1px solid #eee;margin: 1vh auto;">\n				<ion-label>\n					<!-- E-mail -->\n					<ion-icon name="mail" color="light"></ion-icon>\n				</ion-label>\n				<ion-input type="email" placeholder="E-mail" [(ngModel)]="signupData.email" name="email"></ion-input>\n			</ion-item>\n			\n			<ion-item id="signup-input6" class="item80" style="border: 1px solid #eee;margin: 1vh auto;">\n				<ion-label>\n					<!-- Phone -->\n					<ion-icon name="call" color="light"></ion-icon>\n				</ion-label>\n				<ion-input type="number" placeholder="Phone" [(ngModel)]="signupData.phone_number" name="phone_number"></ion-input>\n			</ion-item>\n\n			<ion-item id="signup-input7" class="item80" style="border: 1px solid #eee;margin: 1vh auto;">\n				<ion-label>\n					<!-- Password -->\n					<ion-icon name="lock" color="light"></ion-icon>\n				</ion-label>\n				<ion-input type="password" placeholder="Password" [(ngModel)]="signupData.password" name="password"></ion-input>\n			</ion-item>\n\n			<ion-item id="signup-input7" class="item80" style="border: 1px solid #eee;margin: 1vh auto;">\n				<ion-label>\n					<!-- Confirm Password -->\n					<ion-icon name="lock" color="light"></ion-icon>\n				</ion-label>\n				<ion-input type="password" placeholder="Confirm Password" [(ngModel)]="signupData.repeat_password" name="repeat_password"></ion-input>\n			</ion-item>\n\n		</ion-list>\n\n		<p align="center">\n			<!-- By clicking Signup you agree to the <a>Terms</a> and <a>Services</a>. -->\n			By creation an account you accept our <a style="color: #32db64;" (click)="goToTerms()">Terms of service</a>\n			<br>\n			<!-- Do you already have a Familov account? <br>\n			<button id="login-button2" ion-button clear color="positive" (click)="goToLogin()">\n				Connect\n			</button> -->\n		</p>\n\n		<div align="center">\n			<button id="signup-button5" ion-button color="secondary" style="width: 40vh;font-weight: bold;" type="submit">\n				Create account\n			</button>\n		</div>\n\n		<!-- <br> -->\n\n		<p align="center">\n			<b>Already have an account?</b>\n			<a color="secondary" ion-button (click)="goToLogin()" clear style="margin-top: -1px;text-transform: none;">\n				Sign In\n			</a>\n		</p>\n\n	</form>\n</ion-content>'/*ion-inline-end:"E:\Projects\IonicBackendBrains\FamilovApp\src\pages\signup\signup.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */]])
    ], SignupPage);
    return SignupPage;
}());

//# sourceMappingURL=signup.js.map

/***/ }),

/***/ 56:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__edit_profile_edit_profile__ = __webpack_require__(300);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__my_cart_my_cart__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_rest_rest__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyProfilePage = (function () {
    function MyProfilePage(navCtrl, restProvider) {
        this.navCtrl = navCtrl;
        this.restProvider = restProvider;
        this.changePass = {};
        this.cartItemsLen = 0;
        /*this.restProvider.LoadingSpinner();
        this.viewContent = true;
        
        this.CountryList = localStorage.getItem('CountryList');
        if (this.CountryList == null || this.CountryList == undefined) {
            this.getHomeData();
        }
        else {
            this.CountryList = JSON.parse(this.CountryList);
        }

        // this.tabOne = MyProfilePage;
        // this.tabTwo = ChangePasswordPage;

        this.LoginUser = localStorage.getItem('LoginUser');
        this.LoginUser = JSON.parse(this.LoginUser);

        this.apiSuccessMsg = 'Success';
        this.apiErrorMsg = 'Error';

        this.profileData = {};
        this.profileData.first_name = this.LoginUser.username;
        this.profileData.last_name = this.LoginUser.lastname;
        this.profileData.email = this.LoginUser.email_address;
        this.profileData.sender_number = this.LoginUser.sender_phone;
        this.profileData.phone_code = '+91'; // this.LoginUser.phone_code;
        this.profileData.country_id = this.LoginUser.country_id;
        this.profileData.city_id = this.LoginUser.city_id;
        this.profileData.home_address = this.LoginUser.home_address;
        this.profileData.postal_code = this.LoginUser.postal_code;
        this.profileData.customer_id = this.LoginUser.customer_id;

        if (this.profileData.city_id != null || this.profileData.city_id != undefined) {
            this.getCityList(this.profileData.country_id);
        }

        // if (this.CountryList == null || this.CountryList == undefined) {
        // this.CountryList = [{'country_id':'-1','country_name':'Select country'}];
        // }
        // if (this.CityList == null || this.CityList == undefined) {
        // 	this.CityList = [{'city_id':'-1','city_name':'Select city'}];
        // }

        this.cartItems = localStorage.getItem('cartItems');
        if (this.cartItems != undefined || this.cartItems != null) {
            this.cartItemsLen = JSON.parse(this.cartItems).length;
        }

        this.restProvider.hideLoadingSpinner();*/
    }
    MyProfilePage.prototype.ionViewCanEnter = function () {
        this.restProvider.LoadingSpinner();
        this.viewContent = true;
        this.CountryList = localStorage.getItem('CountryList');
        if (this.CountryList == null || this.CountryList == undefined) {
            this.getHomeData();
        }
        else {
            this.CountryList = JSON.parse(this.CountryList);
        }
        // this.tabOne = MyProfilePage;
        // this.tabTwo = ChangePasswordPage;
        this.LoginUser = localStorage.getItem('LoginUser');
        this.LoginUser = JSON.parse(this.LoginUser);
        this.apiSuccessMsg = 'Success';
        this.apiErrorMsg = 'Error';
        this.profileData = {};
        this.profileData.first_name = this.LoginUser.username;
        this.profileData.last_name = this.LoginUser.lastname;
        this.profileData.email = this.LoginUser.email_address;
        this.profileData.sender_number = this.LoginUser.sender_phone;
        this.profileData.phone_code = '+91'; // this.LoginUser.phone_code;
        this.profileData.country_id = this.LoginUser.country_id;
        this.profileData.city_id = this.LoginUser.city_id;
        this.profileData.home_address = this.LoginUser.home_address;
        this.profileData.postal_code = this.LoginUser.postal_code;
        this.profileData.customer_id = this.LoginUser.customer_id;
        if (this.profileData.city_id != null || this.profileData.city_id != undefined) {
            this.getCityList(this.profileData.country_id);
        }
        // if (this.CountryList == null || this.CountryList == undefined) {
        // this.CountryList = [{'country_id':'-1','country_name':'Select country'}];
        // }
        // if (this.CityList == null || this.CityList == undefined) {
        // 	this.CityList = [{'city_id':'-1','city_name':'Select city'}];
        // }
        this.cartItems = localStorage.getItem('cartItems');
        if (this.cartItems != undefined || this.cartItems != null) {
            this.cartItemsLen = JSON.parse(this.cartItems).length;
        }
        this.restProvider.hideLoadingSpinner();
    };
    MyProfilePage.prototype.goToEditProfile = function (params) {
        if (!params)
            params = {};
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__edit_profile_edit_profile__["a" /* EditProfilePage */]);
    };
    MyProfilePage.prototype.openCartPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__my_cart_my_cart__["a" /* MyCartPage */]);
    };
    MyProfilePage.prototype.showHideView = function (view) {
        if (view == 'profile') {
            this.viewContent = true;
        }
        else {
            this.viewContent = false;
        }
    };
    MyProfilePage.prototype.getHomeData = function () {
        var _this = this;
        this.restProvider.getHomeData()
            .then(function (result) {
            if (result.status == true) {
                _this.CountryList = result.data.country_list;
                localStorage.setItem('CountryList', JSON.stringify(result.data.country_list));
            }
            else {
                if (typeof result.message == "string") {
                    _this.restProvider.showErrorMsg(_this.statusError, result.message);
                }
                else {
                    _this.restProvider.showErrorMsg(_this.statusError, 'Opps..!! <br> Something went wrong.');
                }
            }
        }, function (err) {
            console.log(err);
        });
    };
    MyProfilePage.prototype.getCityList = function ($event) {
        var _this = this;
        // alert($event);
        // console.log($event);
        this.restProvider.getCities($event)
            .then(function (result) {
            if (result.status == true) {
                _this.CityList = result.data.city_list;
            }
            else {
                if (typeof result.message == "string") {
                    _this.restProvider.showErrorMsg(_this.statusError, result.message);
                }
                else {
                    _this.restProvider.showErrorMsg(_this.statusError, 'Opps..!! <br> Something went wrong.');
                }
            }
        }, function (err) {
            // console.log(err);
            _this.restProvider.showErrorMsg(_this.statusError, 'Opps..!! <br> Something went wrong.');
        });
    };
    MyProfilePage.prototype.submitRequest = function () {
        var _this = this;
        var msg = '';
        if (this.changePass.current_password == null || this.changePass.current_password == undefined) {
            msg += 'Enter current password <br>';
        }
        if (this.changePass.new_password == null || this.changePass.new_password == undefined) {
            msg += 'Enter new password <br>';
        }
        if (this.changePass.repeat_new_password == null || this.changePass.repeat_new_password == undefined) {
            msg += 'Enter new confirm password <br>';
        }
        if (msg != '') {
            this.restProvider.showWarningMsg('Warning', msg);
            return false;
        }
        this.restProvider.LoadingSpinner();
        this.changePass.customer_id = this.LoginUser.customer_id;
        this.restProvider.changePassword(this.changePass)
            .then(function (result) {
            _this.restProvider.hideLoadingSpinner();
            // console.log(result);
            if (result.status == true) {
                _this.restProvider.showSuccessMsg(_this.apiSuccessMsg, result.message);
                _this.changePass.current_password = '';
                _this.changePass.new_password = '';
                _this.changePass.repeat_new_password = '';
            }
            else {
                if (typeof result.message == "string") {
                    _this.restProvider.showErrorMsg(_this.statusError, result.message);
                }
                else {
                    var message = '';
                    for (var i in result.message) {
                        message += result.message[i] + '<br>';
                    }
                    _this.restProvider.showErrorMsg(_this.statusError, message);
                    // this.restProvider.showErrorMsg(this.statusError,'Opps..!! <br> Something went wrong.');
                }
                // this.restProvider.showErrorMsg(this.apiErrorMsg, 'Something went wrong, Please try again.');
            }
        }, function (err) {
            _this.restProvider.hideLoadingSpinner();
            // console.log(err);
            _this.restProvider.showErrorMsg(_this.statusError, 'Opps..!! <br> Something went wrong.');
        });
    };
    MyProfilePage.prototype.updateProfile = function () {
        var _this = this;
        // console.log(this.profileData);
        // return false;
        this.restProvider.LoadingSpinner();
        this.restProvider.updateProfile(this.profileData)
            .then(function (result) {
            _this.restProvider.hideLoadingSpinner();
            console.log(result);
            if (result.status == true) {
                _this.getUserData();
                _this.restProvider.showSuccessMsg(_this.apiSuccessMsg, result.message);
            }
            else {
                _this.restProvider.showErrorMsg(_this.apiErrorMsg, 'Something went wrong, Please try again.');
            }
        }, function (err) {
            _this.restProvider.hideLoadingSpinner();
            // console.log(err);
            _this.restProvider.showErrorMsg(_this.statusError, 'Opps..!! <br> Something went wrong.');
        });
    };
    MyProfilePage.prototype.getUserData = function () {
        var _this = this;
        this.restProvider.getUserData(this.profileData.customer_id)
            .then(function (result) {
            if (result.status == true) {
                _this.profileData = result.data;
                localStorage.setItem('LoginUser', JSON.stringify(_this.profileData));
            }
            else {
                if (typeof result.message == "string") {
                    _this.restProvider.showErrorMsg(_this.statusError, result.message);
                }
                else {
                    _this.restProvider.showErrorMsg(_this.statusError, 'Opps..!! <br> Something went wrong.');
                }
            }
        }, function (err) {
            // console.log(err);
            _this.restProvider.showErrorMsg(_this.statusError, 'Opps..!! <br> Something went wrong.');
        });
    };
    MyProfilePage.prototype.selectedImg = function (key) {
        // var file = document.getElementById('profile_image').files;
        console.log(key);
        // console.log(file);
    };
    MyProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-my-profile',template:/*ion-inline-start:"E:\Projects\IonicBackendBrains\FamilovApp\src\pages\my-profile\my-profile.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle class="buttonWhiteFont">\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>\n      <ion-grid>\n    		<ion-row>\n    			<ion-col col-10 class="headerTitle"><div align="center">My Profile</div></ion-col>\n    			<ion-col col-2 class="badgeCol">\n    				<!-- <ion-icon class="right" name="cart" item-end (click)="openCartPage()"></ion-icon> -->\n    				<span class="badgeIcon"> {{ cartItemsLen }}</span>\n					<ion-icon name="cart" item-end (click)="openCartPage()"></ion-icon>\n    			</ion-col>\n    		</ion-row>\n    	</ion-grid>\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content padding id="page9">\n	\n\n	<div id="tabs">\n		<ion-grid>\n			<ion-row>\n				<ion-col col-6>\n					<button ion-button color="{{ viewContent ? \'primary\' : \'light\' }}" style="width: 100%;" (click)="showHideView(\'profile\')">\n						Details\n					</button>\n				</ion-col>\n				<ion-col col-6>\n					<button ion-button color="{{ !viewContent ? \'primary\' : \'light\' }}" style="width: 100%;" (click)="showHideView(\'password\')">\n						Change Password\n					</button>					\n				</ion-col>\n			</ion-row>			\n		</ion-grid>		\n	</div>\n\n	<div *ngIf="viewContent" style="padding: 1vh;">\n		<br>\n		<h4 style="margin-left: 2vh;">1. Personal Informations</h4>\n		\n		<form id="editProfile-form">\n			<ion-list id="editProfile-list">\n				\n				<ion-item class="item80" id="signup-input5" style="border: 1px solid #eee;margin: 1vh auto;">\n					<ion-label><ion-icon color="light" name="image"></ion-icon></ion-label>\n					<ion-input style="color: #32db64;" type="file" id="profile_image" name="profile_image" placeholder="Upload my profile image"></ion-input><!-- [(ngModel)]="profileData.first_name" -->\n				</ion-item>\n\n				<ion-item class="item80" id="signup-input5" style="border: 1px solid #eee;margin: 1vh auto;">\n					<ion-label><ion-icon color="light" name="person"></ion-icon></ion-label>\n					<ion-input style="color: #32db64;" type="text" name="first_name" [(ngModel)]="profileData.first_name" placeholder="Nick Name"></ion-input>\n				</ion-item>\n\n				<ion-item class="item80" id="signup-input5" style="border: 1px solid #eee;margin: 1vh auto;">\n					<ion-label><ion-icon color="light" name="contact"></ion-icon></ion-label>\n					<ion-input style="color: #32db64;" type="text" name="last_name" [(ngModel)]="profileData.last_name" placeholder="Full name"></ion-input>\n				</ion-item>\n				\n				<ion-item class="item80" id="signup-input6" style="border: 1px solid #eee;margin: 1vh auto;">\n					<ion-label><ion-icon color="light" name="mail"></ion-icon></ion-label>\n					<ion-input style="color: #32db64;" type="email" name="email" [(ngModel)]="profileData.email" placeholder="Email address"></ion-input>\n				</ion-item>\n				\n				<ion-item class="item80" id="signup-input6" style="border: 1px solid #eee;margin: 1vh auto;">\n					<ion-label><ion-icon color="light" name="call"></ion-icon></ion-label>\n					<ion-input style="color: #32db64;" type="number" name="sender_number" [(ngModel)]="profileData.sender_number" placeholder="Contact number"></ion-input>\n				</ion-item>\n\n				<ion-item class="item80" id="signup-input5" style="border: 1px solid #eee;margin: 1vh auto;">\n					<ion-label><ion-icon color="light" name="camera"></ion-icon></ion-label>\n					<ion-input style="color: #32db64;" type="file" name="passport" [(ngModel)]="profileData.passport" placeholder="Upload my passport copy"></ion-input>\n				</ion-item>\n\n			</ion-list>\n\n		</form>\n\n		<h4 style="margin-left: 2vh;">2. Address</h4>\n\n		<form id="editProfile-form" (ngSubmit)="updateProfile()">\n			<ion-list id="editProfile-list">\n\n				<ion-grid style="padding: 1vh 5vh;">\n					<ion-row>\n						<ion-col col-6>\n							<ion-item style="border: 1px solid #eee; margin: 1vh 0vh;">\n							  <!-- <ion-label>Country</ion-label> -->\n							  <ion-select style="width: 100%;max-width: 90%;" placeholder="Country" [(ngModel)]="profileData.country_id" name="country_id" (ionChange)="getCityList($event)">\n							    <ion-option *ngFor="let country of CountryList" value="{{ country.country_id }}"> {{ country.country_name }} </ion-option>\n							  </ion-select>\n							</ion-item>\n						</ion-col>\n						<ion-col col-6>\n							<ion-item style="border: 1px solid #eee; margin: 1vh 0vh;">\n							  <!-- <ion-label>City</ion-label> -->\n							  <ion-select style="width: 100%;max-width: 90%;" placeholder="City" [(ngModel)]="profileData.city_id" name="city_id">\n							    <ion-option *ngFor="let city of CityList" value="{{ city.city_id }}"> {{ city.city_name }} </ion-option>\n							  </ion-select>\n							</ion-item>\n						</ion-col>\n					</ion-row>\n\n					<ion-row>\n						<ion-col col-6>\n							<ion-item style="border: 1px solid #eee; margin: 1vh 0vh;">\n							  <ion-label>Postal Code</ion-label>\n							  <ion-input type="number" name="postal_code" [(ngModel)]="profileData.postal_code" placeholder="Postal Code"></ion-input>\n							  <!-- <ion-select [(ngModel)]="profileData.postal_code" name="postal_code">\n							    <ion-option value="f">Code 1</ion-option>\n							    <ion-option value="m">Code 2</ion-option>\n							  </ion-select> -->\n							</ion-item>\n						</ion-col>\n						<ion-col col-6>\n							<ion-item style="border: 1px solid #eee; margin: 1vh 0vh;">\n							  <ion-input type="text" name="home_address" [(ngModel)]="profileData.home_address" placeholder="Street / hs"></ion-input>\n							</ion-item>\n						</ion-col>\n					</ion-row>\n\n				</ion-grid>\n\n			</ion-list>\n\n			<div class="fixed-outside" style="position: fixed;bottom: 0;left: 0;width: 100%;display: inline-flex;z-index: 9999;" align="center">\n				<button id="login-button1" ion-button color="secondary" block style="margin-bottom: 0;" type="submit">\n					Edit <ion-icon name="checkmark" style="margin-left: 2vh;"></ion-icon><!-- (click)="goToEditProfile()" -->\n				</button>\n			</div>\n		</form>\n\n		<p align="center">\n			<ion-checkbox></ion-checkbox>\n			Let me know about news and offers\n		</p><br><br>\n	</div>\n\n	<div *ngIf="!viewContent">\n		<div class="spacer" style="height:20px;" id="login-spacer1"></div>\n		<!-- <h2 align="center">Enter password and confirm password to change your account password.</h2> -->\n		<h2 align="center">Change my password.</h2>\n		<br>\n		<div align="center"><ion-icon name="lock" color="secondary" style="font-size: 4em"></ion-icon></div>\n		<div class="spacer" style="height:20px;" id="login-spacer2"></div>\n\n		<form id="login-form1" (ngSubmit)="submitRequest()" style="margin: 2vh 5vh;">\n			<ion-list id="login-list1">\n				<ion-item class="item80" id="login-input1" style="border: 1px solid #EEE;margin: 1vh auto;">\n					<ion-label><!-- floating -->\n						<ion-icon name="lock"></ion-icon>\n					</ion-label>\n					<ion-input type="password" placeholder="Old Password" [(ngModel)]="changePass.current_password" name="current_password"></ion-input>\n				</ion-item>\n				<ion-item class="item80" id="login-input1" style="border: 1px solid #EEE;margin: 1vh auto;">\n					<ion-label><!-- floating -->\n						<ion-icon name="lock"></ion-icon>\n					</ion-label>\n					<ion-input type="password" placeholder="New Password" [(ngModel)]="changePass.new_password" name="new_password"></ion-input>\n				</ion-item>\n				<ion-item class="item80" id="login-input1" style="border: 1px solid #EEE;margin: 1vh auto;">\n					<ion-label><!-- floating -->\n						<ion-icon name="lock"></ion-icon>\n					</ion-label>\n					<ion-input type="password" placeholder="Confirm Password" [(ngModel)]="changePass.repeat_new_password" name="repeat_new_password"></ion-input>\n				</ion-item>\n			</ion-list>\n\n			<div class="fixed-outside" style="position: fixed;bottom: 0;left: 0;width: 100%;" align="center">\n				<button type="submit" color="secondary" icon-right ion-button block style="text-transform: none;margin-bottom: 0;height: 3em;">Change <ion-icon name="checkmark"></ion-icon></button>\n			</div>\n\n		</form>\n	</div>\n	<!-- <ion-card>\n	  <img src="assets/imgs/avatar.ico"/>\n	  <ion-card-content>\n	    <ion-card-title>\n	      Full name\n	      </ion-card-title>\n\n	      <ion-grid>\n	      	<ion-row>\n	      		<ion-col col-4>Email</ion-col>\n	      		<ion-col col-8>user@gmail.com</ion-col>\n	      	</ion-row>\n	      	<ion-row>\n	      		<ion-col col-4>Mobile</ion-col>\n	      		<ion-col col-8>+91 9999988888</ion-col>\n	      	</ion-row>\n	      </ion-grid>\n	  </ion-card-content>\n	</ion-card> -->\n\n	<!-- <button id="login-button1" ion-button color="secondary" block (click)="goToEditProfile()">\n		Edit Profile\n	</button> -->\n\n</ion-content>'/*ion-inline-end:"E:\Projects\IonicBackendBrains\FamilovApp\src\pages\my-profile\my-profile.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_rest_rest__["a" /* RestProvider */]])
    ], MyProfilePage);
    return MyProfilePage;
}());

//# sourceMappingURL=my-profile.js.map

/***/ })

},[220]);
//# sourceMappingURL=main.js.map